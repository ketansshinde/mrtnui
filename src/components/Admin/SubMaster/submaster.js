import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { getLogin } from "../../../reducer/loginReducer";
import queryString from "query-string";
import * as moment from "moment-timezone";

import {
    createMuiTheme,
    MuiThemeProvider,
    withStyles,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Paper from '@material-ui/core/Paper';
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { SubMasterService } from "../../../services/AdminService/SubMasterService";

export class SubMaster extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            masterName: "",
            masterId: "",
            subMasterId: 0,
            subMasterName: "",
            updateState: false,
            showForm: false,
        };
        this.initialState = this.state;
    }

    componentDidMount() {
    }

    //Load data
    _loaddatatable() {
        const { fetchSubMaster } = this.props;
        fetchSubMaster();
    }

    //Open account form

    _openForm() {
        this.setState({ showForm: true });
    }

    // hide form on cancel button
    _hideForm = (e) => {
        this.setState(this.initialState);
    };

    _handleSubmit = (values, { resetForm }, actions) => {

        var MasterData = this.props.subMasterData.filter(x => x.masterId == Number(values.masterId) && x.name == values.subMasterName);
        if (MasterData.length !== 0) {
            warning("Sub - Master value already exists..!", warningNotification);
            return;
        }
        if (this.state.updateState) {
            console.log("updated");
            var passValue = {
                subMasterId: Number(values.subMasterId),
                masterId: Number(values.masterId),
                name: values.subMasterName,
                isActive: true
            };
            this.props.EditSubMaster(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Master updated successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
        else {
            console.log("Added");
            var passValue = {
                masterId: Number(values.masterId),
                name: values.subMasterName
            };
            this.props.AddSubMaster(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Sub - Master added successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
    }

    _deleteSubMaster(value) {

        console.log("deleted");
        var passValue = {
            subMasterId: value,
        };
        SubMasterService.DeleteSubMaster(passValue,
            (res) => {

                if (res.data.success) {
                    success("Sub - master deleted successfully..!", successNotification);
                    this.setState(this.initialState);
                    this._loaddatatable();
                }
                else {
                    warning("something wents wrong..!", warningNotification);
                }
            },
            (error) => {
                error("Invalid user.", errorNotification);
                console.log(error);
            }
        );
    }

    _subMasterSchema = Yup.object().shape({
        masterId: Yup.string().required("Select master"),
        subMasterName: Yup.string().required("Sub - Master name is required"),
    });

    render() {


        const columns = [

            {
                name: "name",
                label: "Sub-Master Name",
                field: "name",
            },
            {
                name: "masterName",
                label: "Master Name",
                field: "masterName",
            },
            {
                name: "isActive",
                label: "Active",
                field: "isActive",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        var data = value === true ? "Active" : "InActive";
                        return (
                            <div>{data}</div>
                        );
                    }
                }
            },
            {
                name: "createdDate",
                label: "Created Date",
                field: "createdDate",
            },
            {
                name: "modifiedDate",
                label: "Modified Date",
                field: "modifiedDate",
            },
            {
                name: "subMasterId",
                label: "Actions",
                field: "subMasterId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "200px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <a className="mar-left">
                                    <Tooltip title="Edit Master">
                                        <IconButton color="inherit" size="small">
                                            <EditIcon
                                                onClick={() => {

                                                    var subMasterData = this.props.subMasterData.filter(x => x.subMasterId == value);
                                                    this.setState({
                                                        subMasterName: subMasterData[0].name,
                                                        subMasterId: subMasterData[0].subMasterId,
                                                        masterId: subMasterData[0].masterId,
                                                        updateState: true,
                                                        showForm: true
                                                    });

                                                }}>
                                            </EditIcon >
                                        </IconButton>
                                    </Tooltip>
                                </a>
                                <a className="mar-left">
                                    <Tooltip title="Delete Master">
                                        <IconButton color="inherit" size="small">
                                            <DeleteIcon onClick={() => {
                                                this._deleteSubMaster(value);
                                            }}
                                            ></DeleteIcon>
                                        </IconButton>
                                    </Tooltip>
                                </a>
                            </div>
                        );
                    }
                }
            }
        ];
        return (

            <React.Fragment>

                <Formik
                    enableReinitialize
                    initialValues={{
                        data: this.state.data,
                        masterName: this.state.masterName,
                        masterId: this.state.masterId,
                        subMasterId: this.state.subMasterId,
                        subMasterName: this.state.subMasterName,
                        updateState: this.state.updateState,
                        showForm: this.state.showForm,
                    }}
                    validationSchema={this._subMasterSchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">Sub - Master</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">Configuration</li>
                                        </ol>
                                        {this.state.showForm ? (
                                            <Paper>
                                                <div className="mrtn-formlayout-box">
                                                    <div className="mrtn-formlayout-box-inner">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label htmlFor="" className="required">Master Name</label>

                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        as="select"
                                                                        name="masterId"

                                                                        className="form-control"
                                                                        onChange={(event) => {

                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");
                                                                            setFieldValue(
                                                                                (values.masterId = event.target.value),
                                                                            );
                                                                        }}
                                                                    >
                                                                        <option value="">Select Master</option>
                                                                        {(this.props.masterData || []).map(
                                                                            (_masterData) => (
                                                                                <option
                                                                                    data-key={_masterData.masterId}
                                                                                    key={_masterData.masterId}
                                                                                    value={JSON.stringify(
                                                                                        _masterData.masterId
                                                                                    )}
                                                                                >
                                                                                    {_masterData.name}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="masterId"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label htmlFor="" className="required">Sub - Master Name</label>
                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        type="text"
                                                                        id="subMasterName"
                                                                        name="subMasterName"
                                                                        maxLength="50"
                                                                        placeholder="Sub - Master Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue((values.subMasterName = event.target.value));
                                                                        }}
                                                                        className={`form-control ${touched.subMasterName && errors.subMasterName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="subMasterName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>

                                                            </div>
                                                        </div>


                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div style={{ margin: "10px 0", float: "right" }}>
                                                                    <CloseButton
                                                                        size="small"
                                                                        type="button"
                                                                        variant="contained"
                                                                        onClick={this._hideForm}
                                                                    >
                                                                        {this.state.updateState ? "Cancel" : "Cancel"}
                                                                    </CloseButton>
                                                                    {" "}
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                    >
                                                                        {this.state.updateState ? "Update Sub-Master" : "Save Sub-Master"}
                                                                    </SubmitButton>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </Paper>
                                        ) : null}
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="create-user-title">
                                                    <b>Sub - Master List</b>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                {!this.state.showForm ? (
                                                    <div style={{ float: "right" }}>
                                                        <SubmitButton
                                                            size="small"
                                                            type="button"
                                                            variant="contained"
                                                            className="mb-3"
                                                            onClick={() => { this._openForm() }}
                                                        >
                                                            New Sub - Master
                                                    </SubmitButton>

                                                    </div>
                                                ) : null}

                                            </div>
                                        </div>
                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.subMasterData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>

                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(SubMaster);
