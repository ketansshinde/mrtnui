import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Paper from '@material-ui/core/Paper';
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { SubCasteService } from "../../../services/AdminService/SubCasteService";
import { fetchSubCaste } from "../../../action/AdminAction/SubCasteAction";

export class SubCaste extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            countryName: "",
            countryId: "",
            stateName: "",
            updateState: false,
            showForm: false,
            stateId: 0
        };

        this.initialState = this.state;
    }

    componentDidMount() {
    }

    //Load data
    _loaddatatable() {
        const { fetchSubCaste } = this.props;
        fetchSubCaste();
    }

    //Open account form

    _openForm() {
        this.setState({ showForm: true });
    }

    // hide form on cancel button
    _hideForm = (e) => {
        this.setState(this.initialState);
    };

    _handleSubmit = (values, { resetForm }, actions) => {

        var CountryData = this.props.fetchSubCaste.filter(x => x.SubCasteid == Number(values.countryId) && x.stateName == values.stateName);
        if (CountryData.length !== 0) {
            warning("Sub-Caste already exists..!", warningNotification);
            return;
        }
        if (this.state.updateState) {
            console.log("updated");
            var passValue = {
                stateId: Number(values.stateId),
                countryId: Number(values.countryId),
                stateName: values.stateName,
                isActive: true
            }
            this.props.EditState(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Sub-Caste updated successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
        else {
            console.log("Added");
            var passValue = {
                countryId: Number(values.countryId),
                stateName: values.stateName,
            };
            this.props.AddState(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Sub-Caste added successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
    }

    _deleteState(value) {
        console.log("deleted");
        var passValue = {
            stateId: value,
        };
        StateService.DeleteState(passValue,
            (res) => {

                if (res.data.success) {
                    success("Sub-Caste deleted successfully..!", successNotification);
                    this.setState(this.initialState);
                    this._loaddatatable();
                }
                else {
                    warning("something wents wrong..!", warningNotification);
                }
            },
            (error) => {
                error("Invalid user.", errorNotification);
                console.log(error);
            }
        );
    }

    _stateSchema = Yup.object().shape({
        countryId: Yup.string().required("Select Caste"),
        stateName: Yup.string().required("Sub Caste name is required"),
    });

    render() {
        const columns = [
            {
                name: "stateName",
                label: "Sub-Caste",
                field: "stateName",
            },
            {
                name: "countryName",
                label: "Caste Name",
                field: "countryName",
            },
            {
                name: "isActive",
                label: "Active",
                field: "isActive",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        var data = value === true ? "Active" : "InActive";
                        return (
                            <div>{data}</div>
                        );
                    }
                }
            },
            {
                name: "createdDate",
                label: "Created Date",
                field: "createdDate",

            },
            {
                name: "modifiedDate",
                label: "Modified Date",
                field: "modifiedDate",
            },
            {
                name: "stateId",
                label: "Actions",
                field: "stateId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "200px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <a className="mar-left">
                                    <Tooltip title="Edit Contact">
                                        <IconButton color="inherit" size="small">
                                            <EditIcon
                                                onClick={() => {

                                                    var stateData = this.props.stateData.filter(x => x.stateId == value);
                                                    this.setState({
                                                        countryId: stateData[0].countryId,
                                                        stateId: value,
                                                        stateName: stateData[0].stateName,
                                                        updateState: true,
                                                        showForm: true
                                                    });

                                                }}>
                                            </EditIcon >
                                        </IconButton>
                                    </Tooltip>
                                </a>
                                <a className="mar-left">
                                    <Tooltip title="Delete Contact">
                                        <IconButton color="inherit" size="small">
                                            <DeleteIcon onClick={() => {
                                                this._deleteState(value);
                                            }}
                                            ></DeleteIcon>
                                        </IconButton>
                                    </Tooltip>
                                </a>
                            </div>
                        );
                    }
                }
            }
        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        data: this.state.data,
                        stateName: this.state.stateName,
                        stateId: this.state.stateId,
                        countryId: this.state.countryId,
                        updateState: this.state.updateState,
                        showForm: this.state.showForm,
                    }}
                    validationSchema={this._stateSchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">State</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">Configuration</li>
                                        </ol>
                                        {this.state.showForm ? (
                                            <Paper>
                                                <div className="mrtn-formlayout-box">
                                                    <div className="mrtn-formlayout-box-inner">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label htmlFor="" className="required">Country Name</label>

                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        as="select"
                                                                        name="countryId"

                                                                        className="form-control"
                                                                        onChange={(event) => {

                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");
                                                                            setFieldValue(
                                                                                (values.countryId = event.target.value),
                                                                            );
                                                                        }}
                                                                    >
                                                                        <option value="">Select Country</option>
                                                                        {(this.props.CountryData || []).map(
                                                                            (_CountryData) => (
                                                                                <option
                                                                                    data-key={_CountryData.countryId}
                                                                                    key={_CountryData.countryId}
                                                                                    value={JSON.stringify(
                                                                                        _CountryData.countryId
                                                                                    )}
                                                                                >
                                                                                    {_CountryData.countryName}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="countryId"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label for="" className="required">State Name</label>
                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        type="text"
                                                                        id="stateName"
                                                                        name="stateName"
                                                                        maxLength="50"
                                                                        placeholder="State Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.stateName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.stateName && errors.stateName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="stateName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6"></div>
                                                        </div>


                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div style={{ margin: "10px 0", float: "right" }}>
                                                                    <CloseButton
                                                                        size="small"
                                                                        type="button"
                                                                        variant="contained"
                                                                        onClick={this._hideForm}
                                                                    >
                                                                        {this.state.updateState ? "Cancel" : "Cancel"}
                                                                    </CloseButton>
                                                                    {" "}
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                    >
                                                                        {this.state.updateState ? "Update State" : "Save State"}
                                                                    </SubmitButton>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </Paper>
                                        ) : null}
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="create-user-title">
                                                    <b>State List</b>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                {!this.state.showForm ? (
                                                    <div style={{ float: "right" }}>
                                                        <SubmitButton
                                                            size="small"
                                                            type="button"
                                                            variant="contained"
                                                            className="mb-3"
                                                            onClick={() => { this._openForm() }}
                                                        >
                                                            New State
                                                    </SubmitButton>

                                                    </div>
                                                ) : null}

                                            </div>
                                        </div>
                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.fetchSubCaste}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchSubCaste: fetchSubCaste
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(SubCaste);
