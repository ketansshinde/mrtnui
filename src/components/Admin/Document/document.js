import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Modal from 'react-bootstrap/Modal';
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { Global_var } from "../../../global/global_var";


export class Document extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Openpopup: false,
            documentId: 0,
            documentName: "",
            documentType: "",
            documentUserid: 0,
            firstName: "",
            lastName: ""
        };
    }

    componentDidMount() {
    }

    _handleClickOpenpopup = () => {
        this.setState({ Openpopup: true });
    };

    _handleClosepopup = () => {
        this.setState({ Openpopup: false });
    };


    render() {
        const columns = [
            {
                name: "firstName",
                label: "First Name",
                field: "firstName",
            },
            {
                name: "lastName",
                label: "Last Name",
                field: "lastName",
            },
            {
                name: "documentType",
                label: "Document Type",
                field: "documentType",

            },
            {
                name: "documentName",
                label: "Document Name",
                field: "documentName",
            },
            {
                name: "createdDate",
                label: "Created Date",
                field: "createdDate",
            },
            {
                name: "documentId",
                label: "Actions",
                field: "documentId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "200px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        return (
                            <div className="action-otherbox">
                                <a className="margin-left">
                                    <Tooltip title="Show Profile Photo">
                                        <IconButton color="inherit" size="small">
                                            <VisibilityIcon
                                                onClick={() => {
                                                    
                                                    var profileData = tableMeta.tableData[tableMeta.rowIndex];;
                                                    this.setState({
                                                        documentId: profileData.documentId,
                                                        documentName: profileData.documentName,
                                                        documentType: profileData.documentType,
                                                        documentUserid: profileData.documentUserid,
                                                        firstName: profileData.firstName,
                                                        lastName: profileData.lastName,
                                                    });
                                                    this._handleClickOpenpopup();
                                                }}>
                                            </VisibilityIcon >
                                        </IconButton>
                                    </Tooltip>
                                </a>
                                <a className="margin-left">
                                    <Tooltip title="Delete Profile Photo">
                                        <IconButton color="inherit" size="small">
                                            <DeleteIcon onClick={() => {
                                                //this._deleteMaster(value);
                                            }}
                                            ></DeleteIcon>
                                        </IconButton>
                                    </Tooltip>
                                </a>
                            </div>
                        );
                    }
                }
            }
        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        Openpopup: this.state.Openpopup,
                        documentId: this.state.documentId,
                        documentName: this.state.documentName,
                        documentType: this.state.documentType,
                        documentUserid: this.state.documentUserid,
                        firstName: this.state.firstName,
                        lastName: this.state.lastName
                    }}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">User Profile Photo</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">User</li>
                                            <li className="breadcrumb-item active">User Profile</li>
                                        </ol>

                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.DocumentData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>

                                            <Modal
                                                show={values.Openpopup}
                                                onHide={this._handleClosepopup}
                                                backdrop="static"
                                                keyboard={false}
                                                centered
                                            >
                                                <Modal.Header closeButton>
                                                    <Modal.Title>Profile Photo ({values.firstName + " " + values.lastName})</Modal.Title>
                                                </Modal.Header>
                                                <Modal.Body>
                                                    <div className="text_align_center">
                                                        <img src={Global_var.URL_USER_PROFILE_PHOTO + values.documentName} alt="Profile Photo" width="400" height="400"></img>
                                                    </div>
                                                </Modal.Body>
                                                <Modal.Footer>
                                                    <CloseButton
                                                        variant="contained"
                                                        onClick={(event) => {

                                                            this._handleClosepopup(values);
                                                        }}
                                                    >
                                                        Reject Photo
                                                    </CloseButton>
                                                    <SubmitButton
                                                        type="submit"
                                                        variant="contained"
                                                        onClick={(event) => {

                                                            //this._sendingMessage(values);
                                                        }}
                                                    >
                                                        Accept Photo
                                                    </SubmitButton>
                                                </Modal.Footer>
                                            </Modal>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Document);
