import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";

export class ErrorLog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
    }

    render() {
        const columns = [
            {
                name: "logId",
                label: "Log NO.",
                field: "logId",
            },
            {
                name: "moduleName",
                label: "Method Name",
                field: "moduleName",
            },
            {
                name: "errorMessage",
                label: "Message",
                field: "errorMessage",
            },
            {
                name: "createdDate",
                label: "Created Date",
                field: "createdDate",

            }
        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                    }}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">Method Logs List</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">Configuration</li>
                                            <li className="breadcrumb-item active">Log</li>
                                        </ol>

                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.ErrorLogData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(ErrorLog);
