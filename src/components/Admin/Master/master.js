import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Paper from '@material-ui/core/Paper';
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { MasterService } from "../../../services/AdminService/MasterService";

export class Master extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            masterName: "",
            masterId: 0,
            updateState: false,
            showForm: false,
        };

        this.initialState = this.state;
    }

    componentDidMount() {
    }

    //Load data
    _loaddatatable() {
        const { fetchMaster } = this.props;
        fetchMaster();
    }

    //Open account form

    _openForm() {
        this.setState({ showForm: true });
    }

    // hide form on cancel button
    _hideForm = (e) => {
        this.setState(this.initialState);
    };

    _handleSubmit = (values, { resetForm }, actions) => {

        var MasterData = this.props.masterData.filter(x => x.name == values.masterName);
        if (MasterData.length !== 0) {
            warning("Master value already exists..!", warningNotification);
            return;
        }
        if (this.state.updateState) {
            console.log("updated");
            var passValue = {
                masterId: values.masterId,
                name: values.masterName,
                isActive: true
            };
            this.props.EditMaster(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Master updated successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
        else {
            console.log("Added");
            var passValue = {
                name: values.masterName,
            };
            this.props.AddMaster(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Master added successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
    }

    _deleteMaster(value) {
        console.log("deleted");
        var passValue = {
            masterId: value,
        };
        MasterService.DeleteMaster(passValue,
            (res) => {

                if (res.data.success) {
                    success("Master deleted successfully..!", successNotification);
                    this.setState(this.initialState);
                    this._loaddatatable();
                }
                else {
                    warning("something wents wrong..!", warningNotification);
                }
            },
            (error) => {
                error("Invalid user.", errorNotification);
                console.log(error);
            }
        );
    }

    _countrySchema = Yup.object().shape({
        masterName: Yup.string()
            .required('Country name is required!'),
    });

    render() {
        const columns = [
            {
                name: "name",
                label: "Master Name",
                field: "name",
            },
            {
                name: "isActive",
                label: "Active",
                field: "isActive",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        var data = value === true ? "Active" : "InActive";
                        return (
                            <div>{data}</div>
                        );
                    }
                }
            },
            {
                name: "createDate",
                label: "Created Date",
                field: "createDate",

            },
            {
                name: "modifiedDate",
                label: "Modified Date",
                field: "modifiedDate",
            },
            {
                name: "masterId",
                label: "Actions",
                field: "masterId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "200px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <a className="mar-left">
                                    <Tooltip title="Edit Master">
                                        <IconButton color="inherit" size="small">
                                            <EditIcon
                                                onClick={() => {

                                                    var MasterData = this.props.masterData.filter(x => x.masterId == value);
                                                    this.setState({
                                                        masterName: MasterData[0].name,
                                                        masterId: value,
                                                        updateState: true,
                                                        showForm: true
                                                    });

                                                }}>
                                            </EditIcon >
                                        </IconButton>
                                    </Tooltip>
                                </a>
                                <a className="mar-left">
                                    <Tooltip title="Delete Master">
                                        <IconButton color="inherit" size="small">
                                            <DeleteIcon onClick={() => {
                                                this._deleteMaster(value);
                                            }}
                                            ></DeleteIcon>
                                        </IconButton>
                                    </Tooltip>
                                </a>
                            </div>
                        );
                    }
                }
            }
        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        data: this.state.data,
                        masterName: this.state.masterName,
                        masterId: this.state.masterId,
                        updateState: this.state.updateState,
                        showForm: this.state.showForm,
                    }}
                    validationSchema={this._countrySchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">Master</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">Configuration</li>
                                        </ol>
                                        {this.state.showForm ? (
                                            <Paper>
                                                <div className="mrtn-formlayout-box">
                                                    <div className="mrtn-formlayout-box-inner">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label for="" className="required">Master Name</label>
                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        type="text"
                                                                        id="masterName"
                                                                        name="masterName"
                                                                        maxLength="50"
                                                                        placeholder="Master Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.masterName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.masterName && errors.masterName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="masterName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6"></div>
                                                        </div>


                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div style={{ margin: "10px 0", float: "right" }}>
                                                                    <CloseButton
                                                                        size="small"
                                                                        type="button"
                                                                        variant="contained"
                                                                        onClick={this._hideForm}
                                                                    >
                                                                        {this.state.updateState ? "Cancel" : "Cancel"}
                                                                    </CloseButton>
                                                                    {" "}
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                    >
                                                                        {this.state.updateState ? "Update Master" : "Save Master"}
                                                                    </SubmitButton>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </Paper>
                                        ) : null}
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="create-user-title">
                                                    <b>Master List</b>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                {!this.state.showForm ? (
                                                    <div style={{ float: "right" }}>
                                                        <SubmitButton
                                                            size="small"
                                                            type="button"
                                                            variant="contained"
                                                            className="mb-3"
                                                            onClick={() => { this._openForm() }}
                                                        >
                                                            New Master
                                                    </SubmitButton>

                                                    </div>
                                                ) : null}

                                            </div>
                                        </div>
                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.masterData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Master);
