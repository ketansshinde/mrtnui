import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import {
    SubmitButton,
} from "../../../assets/MaterialControl";
import {
    CryptoCode,
} from "../../../common/cryptoCode";
import background from "./../../../assets/images/DSC_5535.jpg";

export class AdminLoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            ipaddress: "168.192.86.12",
            roleid: "1",
            DateTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        };
    }

    componentDidMount() {

    }

    _userAccountSchema = Yup.object().shape({
        username: Yup.string()
            .required('Email-ID is required!')
            .email('Invalid email address format!'),
        password: Yup.string().required("Password is required!")
    });


    _handleSubmit = (values, { resetForm }, actions) => {
        var passValue = {
            username: values.username,
            password: values.password,
            ipaddress: values.ipaddress,
            roleid: 1
        };
        loginService.Authenticate(passValue,
            (res) => {

                if (res.status === "success") {
                    localStorage.setItem("userData", CryptoCode.encryption(JSON.stringify(res)));
                    localStorage.setItem("token", CryptoCode.encryption(res.token));
                    this.props.history.push("/admindashboard");
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    render() {
        return (

            <React.Fragment>
                <div className="bg-primary" style={{ backgroundImage: `url(${background})`, backgroundSize: "cover", backgroundRepeat: "no-repeat" }}>
                    <div id="layoutAuthentication">
                        <div id="layoutAuthentication_content">
                            <Formik
                                enableReinitialize
                                initialValues={{
                                    username: this.state.username,
                                    password: this.state.password,
                                    ipaddress: this.state.ipaddress,
                                    roleid: this.state.roleid,
                                }}
                                validationSchema={this._userAccountSchema}
                                onSubmit={this._handleSubmit}
                            >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                                <Form>
                                    {/* <section className="pricing py-3">
                                        <div className="main-layout"> */}
                                            <div className="container" >
                                                <div className="row justify-content-center">
                                                    <div className="col-lg-4">
                                                        <div className="card1 shadow-lg border-0 rounded-lg mt-5">
                                                            <div className="card-header"><h3 className="text-center my-4">Admin Panel</h3></div>
                                                            <div className="card-body">
                                                                <div className="form-group">
                                                                    <label className="medium" htmlFor="inputEmailAddress">Email-ID</label>
                                                                    <Field
                                                                        type="email"
                                                                        id="username"
                                                                        name="username"
                                                                        maxLength="50"
                                                                        placeholder="Email"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.username = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.username && errors.username
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="username"
                                                                        className="text-danger"
                                                                    />

                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="medium" htmlFor="inputPassword">Password</label>
                                                                    <Field
                                                                        type="password"
                                                                        id="password"
                                                                        name="password"
                                                                        maxLength="50"
                                                                        placeholder="Password"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.password = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.password && errors.password
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="password"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="form-group">
                                                                    <div className="custom-control custom-checkbox">
                                                                        <input className="custom-control-input" id="rememberPasswordCheck" type="checkbox" />
                                                                        <label className="custom-control-label" htmlFor="rememberPasswordCheck">Remember password</label>
                                                                    </div>
                                                                </div>
                                                                <div className="form-group d-flex align-items-center justify-content-between">
                                                                    <a className="small" href="password.html"></a>
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                    >
                                                                        Login
                                                                    </SubmitButton>
                                                                    {/* <a className="btn btn-primary" href="index.html">Login</a> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {/* </div>
                                    </section> */}
                                </Form>
                            )}
                            </Formik>
                        </div>
                        
                    </div>
                </div>

            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(AdminLoginPage);
