import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../../common/cryptoCode";
import { connect } from 'react-redux';
import { bindActionCreators, compose } from "redux";
import male from "../../../../assets/images/man.png";
import female from "../../../../assets/images/woman.png";
import { IconButton } from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import PersonIcon from '@material-ui/icons/Person';
import MessageIcon from '@material-ui/icons/Message';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import EmailIcon from '@material-ui/icons/Email';
import NotificationsIcon from '@material-ui/icons/Notifications';

class AdminHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        this.setState({ name: username });
    }

    _dashboard = () => {
        this.props.history.push("/admindashboard");
    };
    _admincity = () => {
        this.props.history.push("/admincity");
    };
    _admincountry = () => {
        this.props.history.push("/admincountry");
    };
    _adminstate = () => {
        this.props.history.push("/adminstate");
    };

    _adminmaster = () => {
        this.props.history.push("/adminmaster");
    };
    _adminsubmaster = () => {
        this.props.history.push("/adminsubmaster");
    };

    _adminerrorlog = () => {
        this.props.history.push("/adminerrorlog");
    };
    _adminuserdetail = () => {
        this.props.history.push("/adminuserdetail");
    };

    _adminCaste = () => {
        this.props.history.push("/admincaste");
    };

    _admindocument = () => {
        this.props.history.push("/admindocument");
    };

    _userpayment = () => {
        this.props.history.push("/userorder");
    };


    render() {
        return (
            <React.Fragment>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark-header">
                    <a className="navbar-brand userheader-logo" href="#">Perfect<span>Match</span></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarsExample04">
                        <ul className="navbar-nav mr-auto li-padding">
                            <li className="nav-item active">
                                <a className="nav-link margin-right" onClick={this._dashboard}>Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle margin-right" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Master
                                </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                    <a className="dropdown-item" href="#" onClick={this._adminmaster}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <EmailIcon></EmailIcon>
                                            </IconButton> */}
                                        </span>
                                        Master Table</a>
                                    <a className="dropdown-item" onClick={this._adminsubmaster}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <MessageIcon></MessageIcon>
                                            </IconButton> */}
                                        </span>
                                    Sub Master Table</a>
                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item" href="#" onClick={this._adminCaste}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <EmailIcon></EmailIcon>
                                            </IconButton> */}
                                        </span>
                                        Caste</a>
                                    <a className="dropdown-item" onClick={this._adminstate}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <MessageIcon></MessageIcon>
                                            </IconButton> */}
                                        </span>
                                    Sub-Caste</a>

                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item" href="#" onClick={this._admincountry}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <EmailIcon></EmailIcon>
                                            </IconButton> */}
                                        </span>
                                        Country</a>
                                    <a className="dropdown-item" onClick={this._adminstate}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <MessageIcon></MessageIcon>
                                            </IconButton> */}
                                        </span>
                                    State</a>
                                    <a className="dropdown-item" onClick={this._admincity}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                            <VisibilityIcon></VisibilityIcon>
                                        </IconButton> */}
                                        </span>
                                    City</a>
                                </div>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle margin-right" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    User
                                </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                    <a className="dropdown-item" href="#" onClick={this._adminuserdetail}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <EmailIcon></EmailIcon>
                                            </IconButton> */}
                                        </span>
                                        User Profile Detail</a>
                                    <a className="dropdown-item" onClick={this._admindocument}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <MessageIcon></MessageIcon>
                                            </IconButton> */}
                                        </span>
                                        User Profile Photo</a>
                                    <a className="dropdown-item" onClick={this._adminsubmaster}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <MessageIcon></MessageIcon>
                                            </IconButton> */}
                                        </span>
                                        User Gallery Photo</a>
                                    <a className="dropdown-item" onClick={this._adminsubmaster}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <MessageIcon></MessageIcon>
                                            </IconButton> */}
                                        </span>
                                        User Gov. Proof ID </a>
                                </div>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle margin-right" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Order Payment
                                </a>
                                <div className="dropdown-menu" aria-labelledby="navbarDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                    <a className="dropdown-item" href="#" onClick={this._userpayment}>
                                        <span className="padding-right">
                                            {/* <IconButton color="inherit" size="small">
                                                <EmailIcon></EmailIcon>
                                            </IconButton> */}
                                        </span>
                                       Payment History</a>
                                </div>
                            </li>
                            <li className="nav-item active">
                                <a className="nav-link" onClick={this._adminerrorlog}>Log <span className="sr-only">(current)</span></a>
                            </li>
                        </ul>
                        <div className="form-inline my-2 my-md-0">
                            <ul className="navbar-nav ml-auto ml-md-0">
                                <li className="nav-item dropdown">
                                    <a className="logout-header" style={{ color: "#fff", textDecoration: "none" }} id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {/* <i className="far fa-user-circle" aria-hidden="true"></i> */}
                                        <span className="header-profile-name"> {this.state.name}</span>
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                        <a className="dropdown-item" onClick={this._editprofile}>
                                            <IconButton color="inherit" size="small">
                                                <PersonIcon></PersonIcon>
                                            </IconButton>   Profile : {this.state.name}</a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item" onClick={this._changeprofilephoto}>
                                            <span className="padding-right">
                                                <IconButton color="inherit" size="small">
                                                    <SettingsIcon></SettingsIcon>
                                                </IconButton>
                                            </span>
                                            Settings</a>
                                        <a className="dropdown-item" href="#">
                                            <span className="padding-right">
                                                <IconButton color="inherit" size="small">
                                                    <ExitToAppIcon></ExitToAppIcon>
                                                </IconButton>
                                            </span>Activity Log</a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item" href="/">
                                            <span className="padding-right">
                                                <IconButton color="inherit" size="small">
                                                    <ExitToAppIcon></ExitToAppIcon>
                                                </IconButton>
                                            </span>
                                        Logout</a>
                                    </div>
                                </li>
                            </ul>
                            {/* <input className="form-control" type="text" placeholder="Search" /> */}
                        </div>
                    </div>
                </nav>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
        },
        dispatch
    );
export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(AdminHeader);