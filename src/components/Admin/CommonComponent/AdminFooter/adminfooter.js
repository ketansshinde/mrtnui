import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class AdminFooter extends Component {
    render() {
        return (
            <React.Fragment>
                 <div>
                   <div className="footer">	&#169; 20201. All Rights Reserved</div>

                </div>
            </React.Fragment>
        );
    }
}
export default AdminFooter;
