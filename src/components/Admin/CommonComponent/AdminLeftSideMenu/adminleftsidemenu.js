import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import { CryptoCode } from "../../../../common/cryptoCode";

class AdminLeftSideMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        this.setState({ name: username });
    }

    _dashboard = () => {
        this.props.history.push("/dashboard");
    };
    _admincity = () => {
        this.props.history.push("/admincity");
    };
    _admincountry = () => {
        this.props.history.push("/admincountry");
    };
    _adminstate = () => {
        this.props.history.push("/adminstate");
    };

    _adminmaster = () => {
        this.props.history.push("/adminmaster");
    };
    _adminsubmaster = () => {
        this.props.history.push("/adminsubmaster");
    };

    _adminerrorlog = () => {
        this.props.history.push("/adminerrorlog");
    };
    _adminuserdetail = () => {
        this.props.history.push("/adminuserdetail");
    };

    render() {
        return (
            <React.Fragment>
                <div id="layoutSidenav_nav">
                    <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                        <div className="sb-sidenav-menu">
                            <div className="nav">
                                {/* <div className="sb-sidenav-menu-heading">Core</div> */}
                                <a className="nav-link" href="#" onClick={this._dashboard}>
                                    <div className="sb-nav-link-icon"><i className="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                                {/* <div className="sb-sidenav-menu-heading">Interface</div> */}
                                <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                                User
                                <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </a>
                                <div className="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <nav className="sb-sidenav-menu-nested nav">
                                        <a className="nav-link" href="#" onClick={this._adminuserdetail}>User Detail</a>
                                    </nav>
                                </div>
                                <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                    <div className="sb-nav-link-icon"><i className="fas fa-book-open"></i></div>
                                Configuration
                                <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </a>
                                <div className="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                    <nav className="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">

                                        <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                            Country
                                        <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                        </a>
                                        <div className="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                            <nav className="sb-sidenav-menu-nested nav">
                                                <a className="nav-link" href="#" onClick={this._admincountry}>Courty</a>
                                                <a className="nav-link" href="#" onClick={this._adminstate}>State</a>
                                                <a className="nav-link" href="#" onClick={this._admincity}>City</a>
                                            </nav>
                                        </div>

                                        <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                            Master
                                        <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                        </a>
                                        <div className="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                            <nav className="sb-sidenav-menu-nested nav">
                                                <a className="nav-link" href="#" onClick={this._adminmaster}>Master</a>
                                                <a className="nav-link" href="#" onClick={this._adminsubmaster}>Sub Master</a>
                                            </nav>
                                        </div>
                                    </nav>
                                </div>

                                <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Logs" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div className="sb-nav-link-icon"><i className="fas fa-columns"></i></div>
                                Logs
                                <div className="sb-sidenav-collapse-arrow"><i className="fas fa-angle-down"></i></div>
                                </a>
                                <div className="collapse" id="Logs" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <nav className="sb-sidenav-menu-nested nav">
                                        <a className="nav-link" href="#" onClick={this._adminerrorlog}>Error Log</a>
                                    </nav>
                                </div>


                                {/* <div className="sb-sidenav-menu-heading">Addons</div> */}
                                <a className="nav-link" href="charts.html">
                                    <div className="sb-nav-link-icon"><i className="fas fa-chart-area"></i></div>
                                Charts
                            </a>
                                <a className="nav-link" href="tables.html">
                                    <div className="sb-nav-link-icon"><i className="fas fa-table"></i></div>
                                Tables
                            </a>
                            </div>
                        </div>
                        <div className="sb-sidenav-footer">
                            <div className="small">Logged in as:</div>
                            {this.state.name}
                        </div>
                    </nav>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(AdminLeftSideMenu);