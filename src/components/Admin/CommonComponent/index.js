// import Loader from "./loader/Loader";
import AdminFooter from "./AdminFooter/adminfooter";
import AdminHeader from "./AdminHeader/adminheader";
import AdminLeftSideMenu from "./AdminLeftSideMenu/adminleftsidemenu";

export {
    //   Loader,
    AdminFooter,
    AdminHeader,
    AdminLeftSideMenu,
};
