import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton,
    ProSubButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { CryptoCode } from "../../../common/cryptoCode";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CurrencyValue } from "../../User/CommonComponent";
import MUIDataTable from "mui-datatables";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { optionsOtherGrid, themeTransaction } from "../../../common/columnFeature";

class UserOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TransactionData: [],
        };
    }

    componentDidMount() {
        this._LoadDataTable();
    }

    _LoadDataTable() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        };
        
        this.props.GetTrasactionDetail(passValue,
            (res) => {
                if (res.success) {
                    this.setState({ TransactionData: res.responseList });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );

    }

    render() {
        const columns = [
            {
                name: "firstName",
                label: "First Name",
                field: "firstName",
            },
            {
                name: "lastName",
                label: "Last Name",
                field: "lastName",
            },
            {
                name: "profileUniqueId",
                label: "Profile ID",
                field: "profileUniqueId",
                options: {
                    setCellProps: () => ({ style: { width: "150px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div>
                                {value}
                            </div>
                        );
                    }
                }

            },
            // {
            //     name: "mobileNo",
            //     label: "Mobile",
            //     field: "mobileNo",
            // },
            {
                name: "orderCode",
                label: "Order ID",
                field: "orderCode",
            },
            {
                name: "subcriptionName",
                label: "Plan",
                field: "subcriptionName",

            },
            {
                name: "subcriptionPrice",
                label: "Price",
                field: "subcriptionPrice",
                options: {
                    setCellProps: () => ({ style: { width: "100px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var amount = value.toString();
                        return (
                            <div>
                                <b> {"₹  " + amount}</b>
                            </div>
                        );
                    }
                }

            },
            {
                name: "subcriptionMonth",
                label: "Month",
                field: "subcriptionMonth",

            },
            {
                name: "discount",
                label: "Discount(%)",
                field: "discount",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var discount = value.toString();
                        return (
                            <div>
                                <b> {discount + " %"}</b>
                            </div>
                        );
                    }
                }

            },
            // {
            //     name: "firstName",
            //     label: "Name",
            //     field: "firstName",
            // },
            // {
            //     name: "lastName",
            //     label: "Last Name",
            //     field: "lastName",
            // },

            {
                name: "orderStatus",
                label: "Status",
                field: "orderStatus",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var Status = value.toString();
                        if (Status === "Pending") {
                            return (
                                <div className="text-warning">
                                    <b> {Status}</b>
                                </div>
                            );
                        }
                        if (Status === "Failed") {
                            return (
                                <div className="text-danger">
                                    <b> {Status}</b>
                                </div>
                            );
                        }
                        if (Status === "Success") {
                            return (
                                <div className="text-success">
                                    <b> {Status}</b>
                                </div>
                            );
                        }

                    }
                }
            },
            {
                name: "orderAmount",
                label: "Paid Amount",
                field: "orderAmount",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var amount = value.toString();
                        return (
                            <div>
                                <b> {"₹ " + amount}</b>
                            </div>
                        );
                    }
                }
            },
            {
                name: "casteId",
                label: "Actions",
                field: "casteId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "170px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <ProSubButton
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                >
                                    Check Payment
                                 </ProSubButton>{" "}
                            </div>
                        );
                    }
                }
            }
        ];
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        TransactionData: this.state.TransactionData
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <h4 className="mt-4">User Order Payment History</h4>
                                            <div className="layout-margin">
                                                <MuiThemeProvider
                                                    theme={themeTransaction}
                                                >
                                                    <MUIDataTable
                                                        data={values.TransactionData || []}
                                                        columns={columns}
                                                        options={optionsOtherGrid}
                                                    />
                                                </MuiThemeProvider>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}
export default UserOrder;
