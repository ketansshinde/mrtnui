import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Paper from '@material-ui/core/Paper';
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";

export class UserDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        };

        this.initialState = this.state;
    }

    componentDidMount() {

    }


    render() {
        const columns = [
            {
                name: "firstName",
                label: "First Name",
                field: "firstName",
            },
            {
                name: "lastName",
                label: "Last Name",
                field: "lastName",
            },
            {
                name: "mobileNo",
                label: "Mob No",
                field: "mobileNo",
            },
            {
                name: "emailId",
                label: "Email Id",
                field: "emailId",
            },
            {
                name: "profileUniqueId",
                label: "Profile Unique",
                field: "profileUniqueId",
            },
            {
                name: "ipaddress",
                label: "IP Address",
                field: "ipaddress",
            },
            {
                name: "isActive",
                label: "Active",
                field: "isActive",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        var data = value === true ? "Active" : "InActive";
                        return (
                            <div>{data}</div>
                        );
                    }
                }
            },
            {
                name: "modifiedDate",
                label: "Created Date",
                field: "modifiedDate",

            },
            {
                name: "lastLogin",
                label: "Last Login",
                field: "lastLogin",
            }

        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        data: this.state.data,
                    }}
                    validationSchema={this._stateSchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">User Detail</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">User</li>
                                        </ol>

                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="create-user-title">
                                                    <b>User List</b>
                                                </div>
                                            </div>
                                            <div className="col-md-6">

                                            </div>
                                        </div>
                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.UserDetailData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);
