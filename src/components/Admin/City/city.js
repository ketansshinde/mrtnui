import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Paper from '@material-ui/core/Paper';
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { CityService } from "../../../services/AdminService/CityService";

export class City extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            cityName: "",
            stateId: "",
            stateName: "",
            updateState: false,
            showForm: false,
            cityId: 0
        };

        this.initialState = this.state;
    }

    componentDidMount() {
    }

    //Load data
    _loaddatatable() {
        const { fetchCity } = this.props;
        fetchCity();
    }

    //Open account form

    _openForm() {
        this.setState({ showForm: true });
    }

    // hide form on cancel button
    _hideForm = (e) => {
        this.setState(this.initialState);
    };

    _handleSubmit = (values, { resetForm }, actions) => {

        var CityData = this.props.CityData.filter(x => x.stateId == Number(values.stateId) && x.cityName == values.cityName);
        if (CityData.length !== 0) {
            warning("City already exists..!", warningNotification);
            return;
        }
        if (this.state.updateState) {
            console.log("updated");
            var passValue = {
                cityId: Number(values.cityId),
                stateId: Number(values.stateId),
                cityName: values.cityName,
                isActive: true
            }
            this.props.EditCity(passValue,
                (res) => {

                    if (res.data.success) {
                        success("City updated successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
        else {
            console.log("Added");
            var passValue = {
                stateId: Number(values.stateId),
                cityName: values.cityName,
            };
            this.props.AddCity(passValue,
                (res) => {

                    if (res.data.success) {
                        success("City added successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
    }

    _deleteCity(value) {
        console.log("deleted");
        var passValue = {
            cityId: value,
        };
        CityService.DeleteCity(passValue,
            (res) => {

                if (res.data.success) {
                    success("City deleted successfully..!", successNotification);
                    this.setState(this.initialState);
                    this._loaddatatable();
                }
                else {
                    warning("something wents wrong..!", warningNotification);
                }
            },
            (error) => {
                error("Invalid user.", errorNotification);
                console.log(error);
            }
        );
    }

    _stateSchema = Yup.object().shape({
        stateId: Yup.string().required("Select state"),
        cityName: Yup.string().required("City name is required"),
    });

    render() {
        const columns = [

            {
                name: "cityName",
                label: "City Name",
                field: "cityName",
            },
            {
                name: "stateName",
                label: "State Name",
                field: "stateName",
            },
            {
                name: "isActive",
                label: "Active",
                field: "isActive",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        var data = value === true ? "Active" : "InActive";
                        return (
                            <div>{data}</div>
                        );
                    }
                }
            },
            {
                name: "createDate",
                label: "Created Date",
                field: "createDate",

            },
            {
                name: "modifiedDate",
                label: "Modified Date",
                field: "modifiedDate",
            },
            {
                name: "cityId",
                label: "Actions",
                field: "cityId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "200px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <a className="mar-left">
                                    <Tooltip title="Edit Contact">
                                        <IconButton color="inherit" size="small">
                                            <EditIcon
                                                onClick={() => {

                                                    var cityData = this.props.CityData.filter(x => x.cityId == value);
                                                    this.setState({
                                                        stateId: cityData[0].stateId,
                                                        cityId: value,
                                                        cityName: cityData[0].cityName,
                                                        updateState: true,
                                                        showForm: true
                                                    });

                                                }}>
                                            </EditIcon >
                                        </IconButton>
                                    </Tooltip>
                                </a>
                                <a className="mar-left">
                                    <Tooltip title="Delete Contact">
                                        <IconButton color="inherit" size="small">
                                            <DeleteIcon onClick={() => {
                                                this._deleteCity(value);
                                            }}
                                            ></DeleteIcon>
                                        </IconButton>
                                    </Tooltip>
                                </a>
                            </div>
                        );
                    }
                }
            }
        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        data: this.state.data,
                        cityName: this.state.cityName,
                        stateId: this.state.stateId,
                        cityId: this.state.cityId,
                        updateState: this.state.updateState,
                        showForm: this.state.showForm,
                    }}
                    validationSchema={this._stateSchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">City</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">Configuration</li>
                                        </ol>
                                        {this.state.showForm ? (
                                            <Paper>
                                                <div className="mrtn-formlayout-box">
                                                    <div className="mrtn-formlayout-box-inner">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label htmlFor="" className="required">Country Name</label>

                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        as="select"
                                                                        name="stateId"

                                                                        className="form-control"
                                                                        onChange={(event) => {

                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");
                                                                            setFieldValue(
                                                                                (values.stateId = event.target.value),
                                                                            );
                                                                        }}
                                                                    >
                                                                        <option value="">Select State</option>
                                                                        {(this.props.stateData || []).map(
                                                                            (_stateData) => (
                                                                                <option
                                                                                    data-key={_stateData.stateId}
                                                                                    key={_stateData.stateId}
                                                                                    value={JSON.stringify(
                                                                                        _stateData.stateId
                                                                                    )}
                                                                                >
                                                                                    {_stateData.stateName}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="stateId"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label for="" className="required">State Name</label>
                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        type="text"
                                                                        id="cityName"
                                                                        name="cityName"
                                                                        maxLength="50"
                                                                        placeholder="City Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.cityName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.cityName && errors.cityName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="cityName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6"></div>
                                                        </div>


                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div style={{ margin: "10px 0", float: "right" }}>
                                                                    <CloseButton
                                                                        size="small"
                                                                        type="button"
                                                                        variant="contained"
                                                                        onClick={this._hideForm}
                                                                    >
                                                                        {this.state.updateState ? "Cancel" : "Cancel"}
                                                                    </CloseButton>
                                                                    {" "}
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                    >
                                                                        {this.state.updateState ? "Update State" : "Save State"}
                                                                    </SubmitButton>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </Paper>
                                        ) : null}
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="create-user-title">
                                                    <b>City List</b>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                {!this.state.showForm ? (
                                                    <div style={{ float: "right" }}>
                                                        <SubmitButton
                                                            size="small"
                                                            type="button"
                                                            variant="contained"
                                                            className="mb-3"
                                                            onClick={() => { this._openForm() }}
                                                        >
                                                            New State
                                                    </SubmitButton>

                                                    </div>
                                                ) : null}

                                            </div>
                                        </div>
                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.CityData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(City);
