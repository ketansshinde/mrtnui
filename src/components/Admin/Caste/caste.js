import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import {
    MuiThemeProvider,
} from "@material-ui/core/styles";
import { themeOtherGrid, optionsOtherGrid } from "../../../common/columnFeature";
import MUIDataTable from "mui-datatables";
import Paper from '@material-ui/core/Paper';
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { CasteService } from "../../../services/AdminService/CasteService";

export class Caste extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            casteName: "",
            casteId: 0,
            updateState: false,
            showForm: false,
        };

        this.initialState = this.state;
    }

    componentDidMount() {
    }

    //Load data
    _loaddatatable() {
        const { fetchCaste } = this.props;
        fetchCaste();
    }

    //Open account form

    _openForm() {
        this.setState({ showForm: true });
    }

    // hide form on cancel button
    _hideForm = (e) => {
        this.setState(this.initialState);
    };

    _handleSubmit = (values, { resetForm }, actions) => {

        var CasteData = this.props.CasteData.filter(x => x.casteName == values.casteName);
        if (CasteData.length !== 0) {
            warning("Caste already exists..!", warningNotification);
            return;
        }
        if (this.state.updateState) {
            console.log("updated");
            var passValue = {
                casteId: values.casteId,
                casteName: values.casteName,
                isActive: true
            };
            this.props.EditCaste(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Caste updated successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
        else {
            console.log("Added");
            var passValue = {
                casteName: values.casteName,
            };
            this.props.AddCoutry(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Caste added successfully..!", successNotification);
                        this.setState(this.initialState);
                        this._loaddatatable();
                    }
                    else {
                        warning("something wents wrong..!", warningNotification);
                    }
                },
                (error) => {
                    error("Invalid user.", errorNotification);
                    console.log(error);
                }
            );
        }
    }

    _deleteCaste(value) {
        console.log("deleted");
        var passValue = {
            casteId: value,
        };
        CasteService.DeleteCaste(passValue,
            (res) => {

                if (res.data.success) {
                    success("Caste deleted successfully..!", successNotification);
                    this.setState(this.initialState);
                    this._loaddatatable();
                }
                else {
                    warning("something wents wrong..!", warningNotification);
                }
            },
            (error) => {
                error("Invalid user.", errorNotification);
                console.log(error);
            }
        );
    }

    _casteSchema = Yup.object().shape({
        casteName: Yup.string()
            .required('Master name is required!'),
    });

    render() {
        const columns = [
            {
                name: "casteName",
                label: "Caste Name",
                field: "casteName",
            },
            {
                name: "isActive",
                label: "Active",
                field: "isActive",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        var data = value === true ? "Active" : "InActive";
                        return (
                            <div>{data}</div>
                        );
                    }
                }
            },
            {
                name: "createdDate",
                label: "Created Date",
                field: "createdDate",

            },
            {
                name: "modifiedDate",
                label: "Modified Date",
                field: "modifiedDate",
            },
            {
                name: "casteId",
                label: "Actions",
                field: "casteId",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "200px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <a className="mar-left">
                                    <Tooltip title="Edit Contact">
                                        <IconButton color="inherit" size="small">
                                            <EditIcon
                                                onClick={() => {

                                                    var casteData = this.props.CasteData.filter(x => x.casteId == value);
                                                    this.setState({
                                                        casteName: casteData[0].casteName,
                                                        casteId: value,
                                                        updateState: true,
                                                        showForm: true
                                                    });

                                                }}>
                                            </EditIcon >
                                        </IconButton>
                                    </Tooltip>
                                </a>
                                <a className="mar-left">
                                    <Tooltip title="Delete Contact">
                                        <IconButton color="inherit" size="small">
                                            <DeleteIcon onClick={() => {
                                                this._deleteCaste(value);
                                            }}
                                            ></DeleteIcon>
                                        </IconButton>
                                    </Tooltip>
                                </a>
                            </div>
                        );
                    }
                }
            }
        ];
        return (

            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        data: this.state.data,
                        casteName: this.state.casteName,
                        casteId: this.state.casteId,
                        updateState: this.state.updateState,
                        showForm: this.state.showForm,
                    }}
                    validationSchema={this._casteSchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container-fluid">
                                        <h3 className="mt-4">Caste</h3>
                                        <ol className="breadcrumb mb-4">
                                            <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                            <li className="breadcrumb-item active">Configuration</li>
                                        </ol>
                                        {this.state.showForm ? (
                                            <Paper>
                                                <div className="mrtn-formlayout-box">
                                                    <div className="mrtn-formlayout-box-inner">
                                                        <div className="row">
                                                            <div className="col-md-6">
                                                                <div className="form-group lable-style">
                                                                    <label for="" className="required">Caste Name</label>
                                                                    {/* <input type="text" className="form-control" placeholder="Company Name" /> */}
                                                                    <Field
                                                                        type="text"
                                                                        id="casteName"
                                                                        name="casteName"
                                                                        maxLength="50"
                                                                        placeholder="Caste Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.casteName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.casteName && errors.casteName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="casteName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6"></div>
                                                        </div>


                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div style={{ margin: "10px 0", float: "right" }}>
                                                                    <CloseButton
                                                                        size="small"
                                                                        type="button"
                                                                        variant="contained"
                                                                        onClick={this._hideForm}
                                                                    >
                                                                        {this.state.updateState ? "Cancel" : "Cancel"}
                                                                    </CloseButton>
                                                                    {" "}
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                        color="primary"
                                                                    >
                                                                        {this.state.updateState ? "Update Caste" : "Save Caste"}
                                                                    </SubmitButton>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </Paper>
                                        ) : null}
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="create-user-title">
                                                    <b>Caste List</b>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                {!this.state.showForm ? (
                                                    <div style={{ float: "right" }}>
                                                        <SubmitButton
                                                            size="small"
                                                            type="button"
                                                            variant="contained"
                                                            className="mb-3"
                                                            onClick={() => { this._openForm() }}
                                                        >
                                                            New Caste
                                                    </SubmitButton>

                                                    </div>
                                                ) : null}

                                            </div>
                                        </div>
                                        <div className="card mb-4">
                                            <MuiThemeProvider theme={themeOtherGrid}>
                                                <MUIDataTable
                                                    data={this.props.CasteData}
                                                    columns={columns}
                                                    options={optionsOtherGrid}
                                                />
                                            </MuiThemeProvider>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </Form>

                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Caste);
