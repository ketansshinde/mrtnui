import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";


export class AdminDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {


    }

    render() {
        return (

            <React.Fragment>

                <section className="pricing py-3">
                    <div className="main-layout">
                        <div className="container-fluid">
                            <h1 className="mt-4">Static Navigation</h1>
                            <ol className="breadcrumb mb-4">
                                <li className="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                <li className="breadcrumb-item active">Static Navigation</li>
                            </ol>
                            <div className="card mb-4">
                                <div className="card-body">
                                    <p className="mb-0">
                                        This page is an example of using static navigation. By removing the
                                    <code>.sb-nav-fixed</code>
                                    class from the
                                    <code>body</code>
                                    , the top navigation and side navigation will become static on scroll. Scroll down this page to see an example.
                                </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(AdminDashboard);
