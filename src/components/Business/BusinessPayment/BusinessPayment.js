import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./BusinessPayment.css";
import {
    SubmitButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { CryptoCode } from "../../../common/cryptoCode";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CurrencyValue } from "../../User/CommonComponent";

class BusinessPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            buid: "",
            businessCode: "",
            businessName: ""
        };
        this.initialState = this.state;
    }


    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        debugger;
        this.setState({
            buid: UserData.buid,
            name: UserData.fullname,
            businessCode: UserData.businessCode,
            businessName: UserData.businessName
        });

        this.props.FetchSubscription(
            (res) => {
                
                if (res.success) {
                    this.setState({ SubcriptionData: res.responseList });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _upgradeSubscription(subscriptionPlan, amount, subcriptionName) {
        var encryptionURL = btoa(subscriptionPlan + "&" + amount + "&" + subcriptionName);
        this.props.history.push("/busiessupgrade?order=" + encryptionURL);
    }


    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        SubcriptionData: this.state.SubcriptionData
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="pricing py-3">

                                <div className="pricing-header ">
                                    <div className="offer-content"> Get a special discount of 30% on Silver, Gold and Platinum Pack.</div>
                                    <div className="pricing-title"> Select Membership Package</div>
                                </div>


                                <div className="main-layout pricing-main-layout">
                                    <div className="container">

                                        <div className="row">
                                            {(values.SubcriptionData || []).map(
                                                (subscriptionPlan) => (
                                                    <div className="col-lg-4">
                                                        <div className="cardPricing mb-5 mb-lg-0">
                                                            <div className="card-body">
                                                                <h5 className="card-title text-uppercase text-center">
                                                                    {subscriptionPlan.subcriptionName}
                                                                </h5>
                                                                <h6 className="card-price text-center">
                                                                    ₹ <CurrencyValue state={((subscriptionPlan.subcriptionPrice) - (subscriptionPlan.subcriptionPrice / 100) * subscriptionPlan.discount).toFixed(0) || 0}></CurrencyValue>
                                                                    <span className="period">/ {subscriptionPlan.subcriptionMonth} month</span>
                                                                </h6>
                                                                <hr />
                                                                <ul className="fa-ul">
                                                                    <li><span className="fa-li"><i className="fas fa-check"></i></span>Single Business</li>
                                                                    {/* <li><span className="fa-li"><i className="fas fa-check"></i></span>Send Unlimited message</li> */}
                                                                    <li><span className="fa-li"><i className="fas fa-check"></i></span>Business Advertisement </li>
                                                                    <li><span className="fa-li"><i className="fas fa-check"></i></span>Business Title on Home page</li>
                                                                </ul>
                                                                <hr />
                                                                <ul className="fa-ul">
                                                                    <li className="pricing-blue"><span className="fa-li"><i className="fas fa-check"></i></span>
                                                                        Actual Price ₹ <CurrencyValue state={subscriptionPlan.subcriptionPrice || 0}></CurrencyValue>
                                                                    </li>
                                                                    <li className="pricing-orangered"><span className="fa-li"><i className="fas fa-check"></i></span>
                                                                        {subscriptionPlan.discount}% OFF ₹ <CurrencyValue state={(subscriptionPlan.subcriptionPrice / 100) * subscriptionPlan.discount || 0}></CurrencyValue>
                                                                    </li>
                                                                    <li><span className="fa-li"><i className="fas fa-check"></i></span>------------------</li>
                                                                    <li className="pricing-green"><span className="fa-li"><i className="fas fa-check"></i></span>
                                                                        Final Total: ₹ <CurrencyValue state={((subscriptionPlan.subcriptionPrice) - (subscriptionPlan.subcriptionPrice / 100) * subscriptionPlan.discount).toFixed(0) || 0}></CurrencyValue>
                                                                    </li>
                                                                </ul>
                                                                <hr />
                                                                <div className="text-center">
                                                                    <SubmitButton
                                                                        variant="contained"
                                                                        color="primary"
                                                                        onClick={(event) => {
                                                                            var amount = ((subscriptionPlan.subcriptionPrice) - (subscriptionPlan.subcriptionPrice / 100) * subscriptionPlan.discount).toFixed(0) || 0;
                                                                            this._upgradeSubscription(subscriptionPlan.subcriptionId, amount, subscriptionPlan.subcriptionName);
                                                                        }}
                                                                    >
                                                                        Purchase Plan
                                                                    </SubmitButton>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                )
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default BusinessPayment;