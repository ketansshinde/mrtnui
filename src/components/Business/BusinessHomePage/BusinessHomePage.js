import React, { Component } from "react";
import { Formik, Form } from "formik";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import background from "./../../../assets/images/Bandbanner.jpg";
import "./BusinessHomePage.css";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter';
import { IconButton } from "@material-ui/core";
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';

export class BusinessHomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.initialState = this.state;
    }


    componentDidMount() {
        // var lang = localStorage.getItem("lang");
        // i18n.changeLanguage(lang);

    }

    _handleSubmit = (values, { resetForm }, actions) => {


    }

    _businessregistration = () => {
        this.props.history.push("/businessregistration");
    };

    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <div>
                        <section className="py-3" style={{ backgroundImage: `url(${background})`, backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundColor: "#000", height: "90vh" }}>
                            <Formik
                                enableReinitialize
                                initialValues={{

                                }}
                                validationSchema={this._userAccountSchema}
                                onSubmit={this._handleSubmit}
                            >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                                <Form>
                                    <div className="container">
                                        <div className="main-layout home-main-layout">
                                            <div className="row">
                                                <div className="col-lg-12">

                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-12 banner-content">
                                                    <div className="home-heading">
                                                        Welcome to the business board
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 banner-content">
                                                    <div className="sub-home-heading ">
                                                        Let's register business now
                                                    </div>
                                                </div>

                                                <div className="col-lg-12">
                                                    <div className="text_align_center">
                                                        <div className="title-margin" style={{ marginTop: "50px" }}>
                                                            <span onClick={this._businessregistration} class="registration-btn" role="button" aria-pressed="true">
                                                                Business Registration
                                                            </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row">

                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                            </Formik>
                        </section>
                        <section className="py-3" style={{ borderTop: "3px solid #fb6a9b", padding: "15px" }}>
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12 text_align_center">
                                        <div className="business-section-header">
                                            Business expantions
                                        </div>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <div className="col-lg-4">
                                        <div className="business-box text_align_center">
                                            <div className="business-icon">
                                                <i class="fa fa-map-marker business-icon" aria-hidden="true"></i>
                                            </div>
                                            <div className="business-count">4+</div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="business-box text_align_center">
                                            <div className="business-icon">
                                                <i class="fa fa-briefcase business-icon" aria-hidden="true"></i>
                                            </div>
                                            <div className="business-count">20+</div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="business-box text_align_center">
                                            <div className="business-icon">
                                                <i class="fa fa-users business-icon" aria-hidden="true"></i>
                                            </div>
                                            <div className="business-count">250+</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </I18nextProvider>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(BusinessHomePage);
