import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton,
    CloseButton,
    ProSubButton
} from "../../../assets/MaterialControl";
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { CryptoCode } from "../../../common/cryptoCode";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import queryString from "query-string";
import { CurrencyValue } from "../../User/CommonComponent";

class BusinessUpgradeSubscription extends Component {
    constructor(props) {
        
        super(props);
        this.state = {
            OrderDetail: [],
            SubcriptionId: 0,
            SubscriptionAmount: 0,
            SubcriptionName: "",
            mobileNo: "",
            email: "",
        };
    }

    componentDidMount() {
        
        if (this.props.location.search === "") {
            this.props.history.push("/businesspayment");
            return;
        }
        const values = queryString.parse(this.props.location.search);
        console.log(values.order); // "top"
        var param = atob(values.order).split("&");
        var SubcriptionId = param[0];
        var SubscriptionAmount = param[1];
        var SubcriptionName = param[2];

        
        this.setState({
            SubcriptionId: SubcriptionId,
            SubscriptionAmount: SubscriptionAmount,
            SubcriptionName: SubcriptionName
        });


    }

    _pricing = () => {
        this.props.history.push("/businesspayment");
    }

    _userAccountSchema = Yup.object().shape({
        email: Yup.string().required("Email is required!")
            .email('Invalid email address format!'),
        mobileNo: Yup.string()
            .required('Mobile no is required !')
            .matches(/^[0-9]+$/, "Must be only digits"),
    });


    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
            orderCode: "",
            orderStatus: "",
            orderAmount:1,// Number(values.SubscriptionAmount),
            subscriptionId: Number(values.SubcriptionId),
            mobileNo: values.mobileNo,
            emailId: values.email,
            callBackURL: ""//"http://localhost:3000/orderdetail"
        };
        this.props.GetOrderDetail(passValue,
            (res) => {
                if (res.success) {
                    
                    //OrderDetail: res.responseList
                    var callBackURL = res.responseList[0].callBackURL;
                    //const linkSource = `http://localhost:8083/PaymentRequest.aspx?PaymentProcess=` + callBackURL;
                    const linkSource = `http://localhost:8082/home/Checkout?PaymentProcess=` + callBackURL;
                    const downloadLink = document.createElement("a");
                    downloadLink.href = linkSource;
                    downloadLink.target = "_blank";
                    downloadLink.click();
                    //this.setState({ OrderDetail: res.responseList });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        SubcriptionData: this.state.SubcriptionData,
                        SubcriptionId: this.state.SubcriptionId,
                        SubscriptionAmount: this.state.SubscriptionAmount,
                        SubcriptionName: this.state.SubcriptionName,
                        mobileNo: this.state.mobileNo,
                        email: this.state.email,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="pricing py-3">
                                <div className="main-layout">
                                    <div className="container">
                                        <div className="row justify-content-center">
                                            <div className="col-lg-5">
                                                <div className="card mb-5 mb-lg-0">
                                                    <div className="card-body">
                                                        <h5 className="card-title text-uppercase text-center">
                                                            Subcription {" " + values.SubcriptionName}
                                                        </h5>
                                                        <div className="text-center">

                                                            <label className="medium" htmlFor="Last">Total Amount</label>
                                                            {":  ₹ " + values.SubscriptionAmount}
                                                        </div>
                                                        <hr />
                                                        <div className="col-lg-12 mb-2">
                                                            <label className="medium" htmlFor="First">Email Address</label>
                                                            <Field
                                                                type="email"
                                                                id="email"
                                                                name="email"
                                                                maxLength="50"
                                                                placeholder="Enter Email-ID"
                                                                onChange={(event) => {
                                                                    setFieldValue(
                                                                        (values.email = event.target.value)
                                                                    );
                                                                }}
                                                                className={`form-control ${touched.email && errors.email
                                                                    ? "is-invalid"
                                                                    : ""
                                                                    }`}
                                                            />
                                                            <ErrorMessage
                                                                component="div"
                                                                name="email"
                                                                className="text-danger"
                                                            />
                                                        </div>
                                                        <div className="col-lg-12 mb-2">
                                                            <label className="medium" htmlFor="Middle">Mobile Number</label>
                                                            <Field
                                                                type="text"
                                                                id="mobileNo"
                                                                name="mobileNo"
                                                                maxLength="10"
                                                                placeholder="Mobile No."
                                                                onChange={(event) => {

                                                                    setFieldValue(
                                                                        (values.mobileNo = event.target.value)
                                                                    );
                                                                }}
                                                                className={`form-control ${touched.mobileNo && errors.mobileNo
                                                                    ? "is-invalid"
                                                                    : ""
                                                                    }`}
                                                            />
                                                            <ErrorMessage
                                                                component="div"
                                                                name="mobileNo"
                                                                className="text-danger"
                                                            />
                                                        </div>

                                                        <hr />
                                                        <div className="text-center">
                                                            <SubmitButton
                                                                variant="contained"
                                                                color="primary"
                                                                type="submit"
                                                            >
                                                                Pay Now
                                                                    </SubmitButton>{" "}
                                                            <CloseButton
                                                                variant="contained"
                                                                color="primary"
                                                                onClick={this._pricing}
                                                            >
                                                                Cancel
                                                            </CloseButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}
export default BusinessUpgradeSubscription;
