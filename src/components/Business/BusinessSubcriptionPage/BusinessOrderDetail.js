import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton,
    ProSubButton,
    CloseButton,
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { CryptoCode } from "../../../common/cryptoCode";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CurrencyValue } from "../../User/CommonComponent";
import MUIDataTable from "mui-datatables";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { optionsOtherGrid, themeTransaction } from "../../../common/columnFeature";

class BusinessOrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            TransactionData: [],
        };
    }

    componentDidMount() {
        this._LoadDataTable();
    }

    _LoadDataTable() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        };
        
        this.props.GetTrasactionDetailByUserID(passValue,
            (res) => {
                if (res.success) {
                    this.setState({ TransactionData: res.responseList });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );

    }

    render() {
        const columns = [
            // {
            //     name: "emailId",
            //     label: "Email",
            //     field: "emailId",
            // },
            // {
            //     name: "mobileNo",
            //     label: "Mobile",
            //     field: "mobileNo",
            // },
            {
                name: "orderCode",
                label: "Payment Order ID",
                field: "orderCode",

            },
            {
                name: "subcriptionName",
                label: "Plan",
                field: "subcriptionName",

            },
            {
                name: "subcriptionPrice",
                label: "Original Price",
                field: "subcriptionPrice",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var amount = value.toString();
                        return (
                            <div>
                                <b> {"₹  " + amount}</b>
                            </div>
                        );
                    }
                }

            },
            {
                name: "subcriptionMonth",
                label: "Month",
                field: "subcriptionMonth",

            },
            {
                name: "discount",
                label: "Discount (%)",
                field: "discount",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var discount = value.toString();
                        return (
                            <div>
                                <b> {discount + " %"}</b>
                            </div>
                        );
                    }
                }

            },
            // {
            //     name: "firstName",
            //     label: "Name",
            //     field: "firstName",
            // },
            // {
            //     name: "lastName",
            //     label: "Last Name",
            //     field: "lastName",
            // },

            {
                name: "orderStatus",
                label: "Order Status",
                field: "orderStatus",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var Status = value.toString();
                        if (Status === "Pending") {
                            return (
                                <div className="text-warning">
                                    <b> {Status}</b>
                                </div>
                            );
                        }
                        if (Status === "Failed") {
                            return (
                                <div className="text-danger">
                                    <b> {Status}</b>
                                </div>
                            );
                        }
                        if (Status === "Success") {
                            return (
                                <div className="text-success">
                                    <b> {Status}</b>
                                </div>
                            );
                        }

                    }
                }
            },
            {
                name: "orderAmount",
                label: "Paid Amount",
                field: "orderAmount",
                options: {
                    customBodyRender: (value, tableMeta, updateValue) => {
                        
                        var amount = value.toString();
                        return (
                            <div>
                                <b> {"₹  " + amount}</b>
                            </div>
                        );
                    }
                }
            },
            {
                name: "casteId",
                label: "Actions",
                field: "casteId",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <div className="action-otherbox">
                                <ProSubButton
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                >
                                    Check Payment
                                 </ProSubButton>{" "}
                            </div>
                        );
                    }
                }
            }
        ];
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        TransactionData: this.state.TransactionData
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="page-title">Payment History</div>
                                            <div className="layout-margin">
                                                <MuiThemeProvider
                                                    theme={themeTransaction}
                                                >
                                                    <MUIDataTable
                                                        data={values.TransactionData || []}
                                                        columns={columns}
                                                        options={optionsOtherGrid}
                                                    />
                                                </MuiThemeProvider>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}
export default BusinessOrderDetail;
