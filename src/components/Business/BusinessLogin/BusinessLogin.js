import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { businessLoginService } from "../../../services/businessLoginService";
import {
  error,
  success,
  warning,
  warningNotification,
  errorNotification,
  successNotification,
} from "../../notification/notifications";
import background from "./../../../assets/images/Bandbanner.jpg";
import {
  SubmitButton, CloseButton, RegisterButton
} from "../../../assets/MaterialControl";
import {
  CryptoCode,
} from "../../../common/cryptoCode";


export class BusinessLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      ipaddress: "168.192.86.12",
      roleid: "1",
      DateTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      showHide: false
    };

    this.initialState = this.state;
  }


  componentDidMount() {



  }

  _userAccountSchema = Yup.object().shape({
    username: Yup.string()
      .required('Email-ID is required!')
      .email('Invalid email address format!'),
    password: Yup.string().required("Password is required!")
  });


  _handleSubmit = (values, { resetForm }, actions) => {

    var passValue = {
      username: values.username,
      password: values.password,
      ipaddress: values.ipaddress,
      roleId: 3
    };
    businessLoginService.Authenticate(passValue,
      (res) => {
        
        if (res.status === "success") {
          localStorage.setItem("userData", CryptoCode.encryption(JSON.stringify(res)));
          localStorage.setItem("token", CryptoCode.encryption(res.token));
          this.props.history.push("/businesspage");
          //this._getloginUserDetail();

        }
        else {
          error("Invalid user.", errorNotification);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  _getloginUserDetail() {
    const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    var passValue = {
      userid: UserData.id,
      profileUniqueId: UserData.profileUniqueId
    };

    businessLoginService.GetBusinessUserLoginDetail(passValue,
      (res) => {

        if (res.data.success) {
          localStorage.setItem("userloginDetail", CryptoCode.encryption(JSON.stringify(res.data.responseList[0])));
          this.props.history.push("/userhomepage");
        }
        else {
          error("Invalid user.", errorNotification);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  handleHide() {
    this.setState(this.initialState);
  }

  _forgetpassword = () => {
    this.props.history.push("/businessforgotpassword");
  }

  _businessregistration = () => {
    this.props.history.push("/businessregistration");
  };

  render() {
    return (

      <React.Fragment>
        <div>
          <section className="py-3" style={{ backgroundImage: `url(${background})`, backgroundSize: "cover", backgroundRepeat: "no-repeat", height: "596px" }}>

            <Formik
              enableReinitialize
              initialValues={{
                username: this.state.username,
                password: this.state.password,
                ipaddress: this.state.ipaddress,
                roleid: this.state.roleid,
              }}
              validationSchema={this._userAccountSchema}
              onSubmit={this._handleSubmit}
            >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
              <Form>
                <div className="container" >
                  <div className="main-layout home-main-layout">
                    <div className="row login-box">

                      <div className="col-lg-4">
                        <div className="card1 shadow-lg border-0 rounded-lg">
                          <div className="card-body">
                            <div className="login-title-margin">
                              {/* <h3 className="text-center login-title">Welcome ! Shivaji</h3> */}
                              <h3 className="text-center register-subtitle">Login to Business Card</h3>
                            </div>
                            <div className="form-group">
                              <label className="medium" htmlFor="inputEmailAddress">Email-ID</label>
                              <Field
                                type="email"
                                id="username"
                                name="username"
                                maxLength="50"
                                placeholder="Enter Email-ID"
                                onChange={(event) => {
                                  setFieldValue(
                                    (values.username = event.target.value)
                                  );
                                }}
                                className={`form-control ${touched.username && errors.username
                                  ? "is-invalid"
                                  : ""
                                  }`}
                              />
                              <ErrorMessage
                                component="div"
                                name="username"
                                className="text-danger"
                              />

                            </div>
                            <div className="form-group">
                              <label className="medium" htmlFor="inputEmailAddress">Password</label>
                              <Field
                                type="password"
                                id="password"
                                name="password"
                                maxLength="50"
                                placeholder="Enter Password"
                                onChange={(event) => {
                                  setFieldValue(
                                    (values.password = event.target.value)
                                  );
                                }}
                                className={`form-control ${touched.password && errors.password
                                  ? "is-invalid"
                                  : ""
                                  }`}
                              />
                              <ErrorMessage
                                component="div"
                                name="password"
                                className="text-danger"
                              />
                            </div>
                            <div className="form-group">
                              <div className="custom-control custom-checkbox">
                                <input className="custom-control-input" id="rememberPasswordCheck" type="checkbox" />
                                <label className="custom-control-label" htmlFor="rememberPasswordCheck">Remember password</label>
                                <span className="text_align_right forgot-hyperlink " style={{
                                  float: "right",
                                  // marginTop: "5px",
                                  cursor: "pointer"
                                }} onClick={this._forgetpassword}>
                                  Forgot password?
                              </span>
                              </div>

                            </div>

                            <div className="form-group">
                              <span className="text_align_left">
                                <SubmitButton
                                  type="submit"
                                  variant="contained"
                                  className="login-button"
                                >
                                  Login
                                </SubmitButton>
                              </span>

                            </div>

                            <hr></hr>

                            <h3 className="text-center register-subtitle register-btn-title">Don’t have an account?</h3>

                            <RegisterButton
                              type="submit"
                              variant="contained"
                              className="register-button"
                              onClick={this._businessregistration}
                            >
                              Start a Free Trial
                                </RegisterButton>


                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Form>
            )}
            </Formik>
          </section>
        </div>
      </React.Fragment >
    );
  }
}

const mapStateToProps = (state) => ({
  //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {

    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(BusinessLogin);
