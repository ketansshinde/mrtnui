import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import background from "./../../../assets/images/Bandbanner.jpg";


const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );



class BusinessForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ""
        };
    }
    componentDidMount() {


    }

    _handleSubmit = (values, { resetForm }, actions) => {


        var passValue = {
            email: values.profileExpId,
        }
        
        // this.props.UpdateUserExpectation(passValue,
        //     (res) => {
        //         if (res.data.success) {
        //             success("Expectation updated.", successNotification);
        //         }
        //         else {
        //             error("Something wents worng.", errorNotification);
        //         }
        //     },
        //     (error) => {
        //         console.log(error);
        //     }
        // );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        email: this.state.email
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <section className="py-3" style={{ backgroundImage: `url(${background})`}}>
                            <div className="main-layout">
                                <div className="row">
                                    <div className="col-lg-6 offset-md-3">
                                        <div className="card layout-margin">
                                            <div className="card-body">
                                                <h5 className="card-title card-title-set">Forget Password</h5>

                                                <div className="row">
                                                    <div className="col-lg-12 mb-2">

                                                        <label className="medium" htmlFor="Email">Email-ID</label>
                                                        <Field
                                                            type="email"
                                                            id="email"
                                                            name="email"
                                                            maxLength="50"
                                                            minLength="5"
                                                            placeholder="Enter Email-ID"
                                                            onChange={(event) => {
                                                                setFieldValue(
                                                                    (values.email = event.target.value)
                                                                );
                                                            }}
                                                            className="form-control"
                                                        />
                                                    </div>
                                                    <div className="col-lg-12 mb-2 text_align_right">
                                                        <SubmitButton
                                                            type="submit"
                                                            variant="contained"
                                                        >
                                                            Reset
                                                    </SubmitButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BusinessForgotPassword);