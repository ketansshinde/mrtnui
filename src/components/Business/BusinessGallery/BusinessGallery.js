import React, { Component } from 'react';
import { CryptoCode } from "../../../common/cryptoCode";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators, compose } from "redux";

class BusinessGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            buid: "",
            businessCode: "",
            businessName: ""
        };
        this.initialState = this.state;
    }


    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        debugger;
        this.setState({
            buid: UserData.buid,
            name: UserData.fullname,
            businessCode: UserData.businessCode,
            businessName: UserData.businessName
        });
    }
    render() {
        return (
            <React.Fragment>

                <Formik
                    enableReinitialize
                    initialValues={{
                        name: this.state.name,
                        buid: this.state.buid,
                        businessCode: this.state.businessCode,
                        businessName: this.state.businessName
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="" >
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <span>
                                                    BusinessGallery
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default BusinessGallery;