
import BusinessUserFooter from "./BusinessUserFooter/businessuserfooter";
import BusinessUserHeader from "./BusinessUserHeader/businessuserheader";
import BusinessHomeHeader from "./BusinessHomeHeader/businesshomeheader";
import BusinessHomeFooter from "./BusinessHomeFooter/businesshomefooter";
//import Loader from "./Loader/Loader";

export {
    //   Loader,
    BusinessUserFooter,
    BusinessUserHeader,
    BusinessHomeHeader,
    BusinessHomeFooter,
    //Loader
};
