import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import CallIcon from '@material-ui/icons/Call';
import EmailRoundedIcon from '@material-ui/icons/EmailRounded';
import { IconButton } from "@material-ui/core";
import "./businesshomefooter.css";
import i18n from "../../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from 'react-i18next';


const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );

class BusinessHomeFooter extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {

    }
    // Legal Policy

    _aboutus = () => {
        this.props.history.push("aboutus");
    }

    _cookiespolicy = () => {
        this.props.history.push("cookiespolicy");
    }

    _fraudalert = () => {
        this.props.history.push("fraudalert");
    }

    _privacypolicy = () => {
        this.props.history.push("privacypolicy");
    }

    _termsoduse = () => {
        this.props.history.push("termsoduse");
    }

    //Help 
    _feedback = () => {
        this.props.history.push("feedback");
    }
    _contactus = () => {
        this.props.history.push("contactus");
    }




    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <div>
                        <div className="footer">
                            <div className="footer-website-detail">
                                <div className="row">
                                    {/* <div className="col-lg-3 text-lg-left">
                                    <div className="footer-heading-title text_align_center">Explore</div>
                                </div> */}
                                    <div className="col-lg-4 text-lg-left">
                                        <div className="footer-heading-title text_align_left">
                                            <span>
                                                <Translation>
                                                    {(t, { i18n }) => (
                                                        <span>{t("Legal_Policy")}</span>
                                                    )}
                                                </Translation>
                                            </span>
                                        </div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._aboutus}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("About_Us")}</span>
                                                )}
                                            </Translation></span> </div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._fraudalert}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Fraud_Alert")}</span>
                                                )}
                                            </Translation></span></div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._termsoduse}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Terms_of_use")}</span>
                                                )}
                                            </Translation></span></div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._privacypolicy}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Privacy_policy")}</span>
                                                )}
                                            </Translation></span></div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._cookiespolicy}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Cookie_policy")}</span>
                                                )}
                                            </Translation></span></div>

                                    </div>
                                    <div className="col-lg-4 text-lg-left">
                                        <div className="footer-heading-title text_align_left">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Services_Product")}</span>
                                                )}
                                            </Translation></div>
                                        <div className="footer-heading-subtitle text_align_left">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Business_Card")}</span>
                                                )}
                                            </Translation></div>
                                        <div className="footer-heading-subtitle text_align_left">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Wedding_Website")}</span>
                                                )}
                                            </Translation></div>
                                        <div className="footer-heading-subtitle text_align_left">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Membership_Options")}</span>
                                                )}
                                            </Translation></div>

                                    </div>
                                    <div className="col-lg-4 text-lg-left">
                                        <div className="footer-heading-title text_align_left">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Help")}</span>
                                                )}
                                            </Translation></div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._contactus}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Contact_Us")}</span>
                                                )}
                                            </Translation></span></div>
                                        <div className="footer-heading-subtitle text_align_left"><span onClick={this._feedback}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Feedback")}</span>
                                                )}
                                            </Translation></span></div>
                                        <div className="footer-heading-subtitle text_align_left">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("FAQ")}</span>
                                                )}
                                            </Translation></div>
                                        <div className="footer-heading-subtitle text_align_left"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="dropdown-divider"></div>
                            <div className="row">
                                <div className="col-lg-6 text-lg-left footer-copy-right">
                                    &#169; 2021. All Rights Reserved
                            </div>
                                <div className="col-lg-6 text-lg-right">
                                    <IconButton color="inherit">
                                        <HomeRoundedIcon></HomeRoundedIcon>
                                    </IconButton>
                                    <IconButton color="inherit">
                                        <FacebookIcon></FacebookIcon>
                                    </IconButton>
                                    <IconButton color="inherit">
                                        <WhatsAppIcon></WhatsAppIcon>
                                    </IconButton>
                                    <IconButton color="inherit">
                                        <CallIcon></CallIcon>
                                    </IconButton>
                                    <IconButton color="inherit">
                                        <EmailRoundedIcon></EmailRoundedIcon>
                                    </IconButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </I18nextProvider>
            </React.Fragment >
        );
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(BusinessHomeFooter);