import React, { Component } from "react";
import { withRouter, useHistory } from "react-router-dom";
import {
    SubmitButton,
    CloseButton,
} from "../../../../assets/MaterialControl";
import { connect } from 'react-redux';
import { bindActionCreators, compose } from "redux";
import logo from "./../../../../assets/images/logo.png";
import i18n from "../../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from "react-i18next";

class BusinessHomeHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedLanguage: "English",
        };

        this.initialState = this.state;
    }


    componentDidMount() {
        // i18n.changeLanguage("mr");
    }

    _login = () => {
        this.props.history.push("/businesslogin");
    };

    _homePage = () => {
        this.props.history.push("/");
    };

    _changelang(lang) {

        if (lang === "en") {
            i18n.changeLanguage("en");
            localStorage.setItem("lang", "en");
            //window.location.reload();
        }
        else if (lang === "mr") {
            i18n.changeLanguage("mr");
            localStorage.setItem("lang", "mr");
            //window.location.reload();
        }
        else {
            i18n.changeLanguage("en");
            localStorage.setItem("lang", "en");
            // window.location.reload();
        }
    }

    _businesshome = () => {
        this.props.history.push("/businesshome");
    }
    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark-header home-navbar">
                        <a className="navbar-brand userheader-logo" href="#" onClick={this._businesshome}> <img src={logo} width="35"></img> Business Card</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse text-align-right" id="navbarsExample04">
                            <ul className="navbar-nav mr-auto li-padding">
                                <li className="nav-item active">
                                    <span className="nav-link margin-right" onClick={this._businesshome}>Home <span className="sr-only">(current)</span></span>
                                </li>
                                <li className="nav-item active">
                                    <span className="nav-link margin-right">Service <span className="sr-only">(current)</span></span>
                                </li>
                                <li className="nav-item active">
                                    <span className="nav-link margin-right">About Us <span className="sr-only">(current)</span></span>
                                </li>
                                {/* <li className="nav-item dropdown">
                                    <select class="form-control"
                                        value={this.state.selectedLanguage}
                                        onChange={(event) => {
                                            this.setState({ selectedLanguage: event.target.value });
                                            this._changelang(event.target.value);
                                        }}>
                                        <option>Select Language</option>
                                        <option value="en">
                                            English
                                        </option>
                                        <option value="mr">
                                            मराठी
                                        </option>
                                    </select>
                                </li> */}
                            </ul>
                            <div className="form-inline my-2 my-md-0 head-login-box">
                                <ul className="navbar-nav ml-auto ml-md-0">
                                    <li className="nav-item dropdown">
                                        <span className="already-member" onClick={this._login}>
                                            <span style={{ marginRight: "10px" }}> Already Business? </span>
                                            <SubmitButton
                                                variant="contained"
                                                color="primary"
                                            >
                                                Login to Card
                                            </SubmitButton>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </I18nextProvider>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
        },
        dispatch
    );

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(BusinessHomeHeader);