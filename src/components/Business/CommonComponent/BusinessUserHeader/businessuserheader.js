import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../../common/cryptoCode";
import male from "../../../../assets/images/man.png";
import female from "../../../../assets/images/woman.png";
import { IconButton } from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from "redux";
import PersonIcon from '@material-ui/icons/Person';
import MessageIcon from '@material-ui/icons/Message';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import EmailIcon from '@material-ui/icons/Email';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { Global_var } from "../../../../global/global_var";
import { Formik, Form, Field, ErrorMessage } from "formik";
import HelpIcon from '@material-ui/icons/Help';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FilterListIcon from '@material-ui/icons/FilterList';
import BlockIcon from '@material-ui/icons/Block';
import SettingsBackupRestoreIcon from '@material-ui/icons/SettingsBackupRestore';
import "./businessuserheader.css";
import ReportIcon from '@material-ui/icons/Report';
import FeedbackIcon from '@material-ui/icons/Feedback';

class BusinessUserHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            buid: "",
            businessCode: "",
        };
    }

    componentDidMount() {
        
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.fullname;
        this.setState({ buid: UserData.buid, name: username, businessCode: UserData.businessCode });
        // const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        // this.setState({ profilePhoto: userloginDetail.documentName });

        // if (localStorage.getItem("ProfilePhoto") !== null) {
        //     const profilePhotoData = JSON.parse(CryptoCode.decryption(localStorage.getItem("ProfilePhoto")));
        //     if (profilePhotoData.length !== 0)
        //         this.setState({ profilePhoto: profilePhotoData[0].documentType + "," + profilePhotoData[0].documentBase });
        // }
    }

    // _userhomepage = () => {
    //     this.props.history.push("/userhomepage");
    // }

    // _usersearch = () => {
    //     this.props.history.push("/usersearch");
    // }

    

    // _editprofile = () => {
    //     this.props.history.push("/editprofile");
    // }

    // _changeprofilephoto = () => {
    //     this.props.history.push("/changeprofilephoto");
    // }

    // _sendmessage = () => {
    //     this.props.history.push("/sendmessage");
    // }

    // _orderDetail = () => {
    //     this.props.history.push("/orderdetail");
    // }

    // _profileViewer = () => {
    //     this.props.history.push("/profilevisitor");
    // }

    _profileShortlist = () => {
        this.props.history.push("/profileshortlist");
    }

    _businesspage = () => {
        this.props.history.push("/businesspage");
    }
    _businesspayment = () => {
        this.props.history.push("/businesspayment");
    }

    // _profileShortlistbyYou = () => {
    //     this.props.history.push("/profileshortlistbyyou");
    // }
    // _profilerequest = () => {
    //     this.props.history.push("/profilerequest");
    // }
    // _profileDecline = () => {
    //     this.props.history.push("/profiledecline");
    // }
    // _profileDeclineByYou = () => {
    //     this.props.history.push("/profiledeclinebyyou");
    // }




    render() {
        return (
            <React.Fragment>

                <Formik
                    enableReinitialize
                    initialValues={{
                        name: this.state.name,
                        buid: this.state.buid,
                        businessCode: this.state.businessCode,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <nav className="navbar navbar-expand-md navbar-dark bg-dark-header">
                            <a className="navbar-brand userheader-logo" onClick={this._businesspage}>Business Card</a>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <div className="collapse navbar-collapse" id="navbarsExample04">
                                <ul className="navbar-nav mr-auto li-padding">
                                    <li className="nav-item active">
                                        <a className="nav-link margin-right" onClick={this._businesspage}>My Home<span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link margin-right" onClick={this._usersearch}>Gallery<span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link margin-right" onClick={this._usersearch}>Amenities<span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle margin-right" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Report
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                            <a className="dropdown-item" onClick={this._sendmessage}>
                                                <span className="padding-right">
                                                    <IconButton color="inherit" size="small">
                                                        <ReportIcon></ReportIcon>
                                                    </IconButton>
                                                </span>
                                                Enquiry</a>
                                            <a className="dropdown-item" onClick={this._sendmessage}>
                                                <span className="padding-right">
                                                    <IconButton color="inherit" size="small">
                                                        <FeedbackIcon></FeedbackIcon>
                                                    </IconButton>
                                                </span>
                                                Feedback</a>

                                        </div>
                                    </li>
                                    <li className="nav-item active" onClick={this._businesspayment}>
                                        <a className="nav-link" onClick={this._pricing}>Purchase Plan <span className="sr-only">(current)</span></a>
                                    </li>
                                </ul>
                                <div className="form-inline my-2 my-md-0">
                                    <span onClick={this._pricing}>
                                        <span className="padding-right">
                                            <IconButton>
                                                <NotificationsIcon></NotificationsIcon>
                                            </IconButton>
                                        </span>
                                    </span>
                                    <ul className="navbar-nav ml-auto ml-md-0">
                                        <li className="nav-item dropdown">
                                            <a className="logout-header" style={{ color: "#fff", textDecoration: "none" }} id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span className="header-profile-name"> {this.state.name}</span>
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                                <a className="dropdown-item" onClick={this._editprofile}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <PersonIcon></PersonIcon>
                                                        </IconButton>
                                                    </span>My Profile ({values.businessCode})</a>
                                                <div className="dropdown-divider"></div>
                                                <a className="dropdown-item" onClick={this._changeprofilephoto}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <SettingsIcon></SettingsIcon>
                                                        </IconButton>
                                                    </span>
                                                    Settings &amp; Privacy</a>
                                                <a className="dropdown-item" onClick={this._orderDetail}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <FavoriteBorderIcon></FavoriteBorderIcon>
                                                        </IconButton>
                                                    </span>
                                                    Active Plan</a>
                                                <a className="dropdown-item" onClick={this._changeprofilephoto}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <HelpIcon></HelpIcon>
                                                        </IconButton>
                                                    </span>
                                                    Help &amp; Support</a>
                                                <div className="dropdown-divider"></div>
                                                <a className="dropdown-item" href="/businesshome">
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <ExitToAppIcon></ExitToAppIcon>
                                                        </IconButton>
                                                    </span>
                                                    Logout</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
        },
        dispatch
    );
export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(BusinessUserHeader);