import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import CallIcon from '@material-ui/icons/Call';
import EmailRoundedIcon from '@material-ui/icons/EmailRounded';
import { IconButton } from "@material-ui/core";
import { CryptoCode } from "../../../../common/cryptoCode";
import * as moment from "moment-timezone";

class BusinessUserFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            LastLogin: "",
            IPAddress: "",
        };
    }
    componentDidMount() {
        
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        // const username = UserData.firstName + " " + UserData.lastName;

        // var lastLogin = moment(UserData.lastLogin).format('LL');
        // this.setState({ LastLogin: lastLogin, IPAddress: UserData.ipaddress });
    }
    render() {
        return (
            <React.Fragment>
                <div>
                    <div className="footer">
                        <div className="row">
                            <div className="col-lg-3 text-lg-left">
                                &#169; 2021. All Rights Reserved
                            </div>
                            <div className="col-lg-6 text-lg-center" style={{color:"#b6b6b6"}}>
                                <b>Last Login:</b> {this.state.LastLogin} | <b>IP Address:</b> {this.state.IPAddress}
                            </div>
                            <div className="col-lg-3 text-lg-right">
                                <IconButton color="inherit">
                                    <HomeRoundedIcon></HomeRoundedIcon>
                                </IconButton>
                                <IconButton color="inherit">
                                    <FacebookIcon></FacebookIcon>
                                </IconButton>
                                <IconButton color="inherit">
                                    <CallIcon></CallIcon>
                                </IconButton>
                                <IconButton color="inherit">
                                    <WhatsAppIcon></WhatsAppIcon>
                                </IconButton>
                                <IconButton color="inherit">
                                    <EmailRoundedIcon></EmailRoundedIcon>
                                </IconButton>
                            </div>
                        </div>
                    </div>

                </div>
            </React.Fragment>
        );
    }
}
export default BusinessUserFooter;
