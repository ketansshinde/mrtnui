import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import {
    CryptoCode,
} from "../../../common/cryptoCode";
import queryString from "query-string";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { CasteValue } from "../../../common/CasteValue";
import * as moment from "moment-timezone";
import "./BusinessRegistration.css";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from 'react-i18next';
import background from "./../../../assets/images/Bandbanner.jpg";


export class BusinessRegistration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            businessType: "",
            businessName: "",
            fullname: "",
            emailId: "",
            password: "",
            mobileNumber: "",
            ipaddress: "168.192.16.21",
            businessList: [],
            confirmPassword: ""
        };
    }

    componentDidMount() {
        var BusinessData = SubMasterValue.SubMaster(MasterValue.Business);
        this.setState({
            businessList: BusinessData,
        });
    }

    _userAccountSchema = Yup.object().shape({
        businessType: Yup.string()
            .required("Select business type."),
        fullname: Yup.string()
            .required("Owner name is required.")
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed."),
        businessName: Yup.string()
            .required("Business name is required.")
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed."),
        emailId: Yup.string()
            .required("Email-ID name is required.")
            .email("Invalid email address."),
        mobileNumber: Yup.string()
            .required("Mobile number is required.")
            .matches(/^[0-9]+$/, "Must be only digits."),
        password: Yup.string()
            .required("Password is required.")
            .min(8, 'Password must be 8 characters at minimum.')
            .max(16, 'Password must be 16 characters at maximum.')
            .matches(/[a-z]/, 'At least one lowercase char.')
            .matches(/[A-Z]/, 'At least one uppercase char.')
            .matches(/(?=.*\d)/, 'Must contain a number.')
            .matches(/[!@#$%^&*(),.?":{}|<>]/, 'Must contain special character.')
            .matches(/^\S*$/, 'Password should not allow space.'),
        confirmPassword: Yup.string()
            .required("Confirm password is required.")
            .min(8, 'Password must be 8 characters at minimum.')
            .oneOf([Yup.ref('password'), null], 'Passwords must match.'),
    });


    _handleSubmit = (values, { resetForm }, actions) => {
        debugger;
        var passValue = {
            businessType: Number(values.businessType),
            businessName: values.businessName,
            fullname: values.fullname,
            emailId: values.emailId,
            password: values.password,
            mobileNumber: values.mobileNumber,
            ipaddress: values.ipaddress,
        }
        this.props.SaveBusinessRegistration(passValue,
            (res) => {
                debugger;
                if (res.data.success) {
                    success("Business has been register successfully. Please check your confirmation mail on you Mail-ID.", successNotification);
                    this.props.history.push("/businesslogin");
                }
                else {
                    error(res.data.errorlog, errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    render() {
        return (

            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <div>
                        {/* register-bg  */}
                        <section className="py-3 reg-background" style={{ backgroundImage: `url(${background})` }}>

                            <Formik
                                enableReinitialize
                                initialValues={{
                                    businessType: this.state.businessType,
                                    businessName: this.state.businessName,
                                    fullname: this.state.fullname,
                                    emailId: this.state.emailId,
                                    password: this.state.password,
                                    mobileNumber: this.state.mobileNumber,
                                    ipaddress: this.state.ipaddress,
                                    businessList: this.state.businessList,
                                    confirmPassword: this.state.confirmPassword
                                }}
                                validationSchema={this._userAccountSchema}
                                onSubmit={this._handleSubmit}
                            >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                                <Form>
                                    <div className="main-layout">
                                        <div className="container">
                                            <div className="row justify-content-center">
                                                <div className="col-lg-8">
                                                    <div className="card">
                                                        <div className="card-body">
                                                            <h5 className="card-title card-title-set text_align_center">
                                                                <span className="business-header">Business Registration</span>
                                                            </h5>
                                                            <div className="dropdown-divider mb-2"></div>
                                                            <div className="row">
                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="BusinessType">
                                                                        Business type
                                                                    </label>
                                                                    <Field
                                                                        as="select"
                                                                        name="businessType"

                                                                        className={`form-control ${touched.businessType && errors.businessType
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                        onChange={(event) => {

                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");
                                                                            setFieldValue(
                                                                                (values.businessType = event.target.value),
                                                                            );
                                                                        }}
                                                                    >
                                                                        <option value="">Select Business Type</option>
                                                                        {(values.businessList || []).map(
                                                                            (businessList) => (
                                                                                <option
                                                                                    data-key={businessList.subMasterId}
                                                                                    key={businessList.subMasterId}
                                                                                    value={JSON.stringify(
                                                                                        businessList.subMasterId
                                                                                    )}
                                                                                >
                                                                                    {businessList.name}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="businessType"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="businessName">
                                                                        Business Name
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="businessName"
                                                                        name="businessName"
                                                                        maxLength="50"
                                                                        placeholder="Business Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.businessName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.businessName && errors.businessName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="businessName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="fullname">
                                                                        Owner Name
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="fullname"
                                                                        name="fullname"
                                                                        maxLength="50"
                                                                        placeholder="Owner Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.fullname = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.fullname && errors.fullname
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="fullname"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="emailId">
                                                                        Email-ID
                                                                    </label>
                                                                    <Field
                                                                        type="email"
                                                                        id="emailId"
                                                                        name="emailId"
                                                                        maxLength="50"
                                                                        placeholder="Enter Email-ID"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.emailId = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.emailId && errors.emailId
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="emailId"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="mobileNumber">
                                                                        Mobile Number
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="mobileNumber"
                                                                        name="mobileNumber"
                                                                        maxLength="10"
                                                                        placeholder="Mobile Number"
                                                                        onInput={(e) => {
                                                                            e.target.value = Math.max(0, parseInt(e.target.value)).toString().slice(0, 10)
                                                                        }}
                                                                        onChange={(event) => {

                                                                            setFieldValue(
                                                                                (values.mobileNumber = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.mobileNumber && errors.mobileNumber
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="mobileNumber"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-6 mb-2"></div>

                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="password">
                                                                        Password
                                                                    </label>

                                                                    <Field
                                                                        type="password"
                                                                        id="password"
                                                                        name="password"
                                                                        maxLength="50"
                                                                        placeholder="Password e.g - Abc@12345"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.password = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.password && errors.password
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="password"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-6 mb-2">
                                                                    <label className="medium" htmlFor="confirmPassword">
                                                                        Confirm Password
                                                                    </label>
                                                                    <Field
                                                                        type="password"
                                                                        id="confirmPassword"
                                                                        name="confirmPassword"
                                                                        maxLength="50"
                                                                        placeholder="Re-Enter Password"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.confirmPassword = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.confirmPassword && errors.confirmPassword
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="confirmPassword"
                                                                        className="text-danger"
                                                                    />
                                                                </div>

                                                            </div>
                                                            <div className="row">
                                                                <div className="col-lg-12 mb-2 text_align_right">
                                                                    <div className="dropdown-divider mb-2"></div>
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                    >
                                                                        Register
                                                                    </SubmitButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                            </Formik>
                        </section>
                    </div>
                </I18nextProvider>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(BusinessRegistration);
