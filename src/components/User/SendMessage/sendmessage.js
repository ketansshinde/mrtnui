import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import {
    CryptoCode,
} from "../../../common/cryptoCode";
import queryString from "query-string";
import TextField from '@material-ui/core/TextField';
import "./sendmessage.css";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import * as moment from "moment-timezone";
import SendIcon from '@material-ui/icons/Send';
import {
    IconButton
} from "@material-ui/core";
import { SendMessageDetail } from "../../../action/UserAction/SendMessageAction";
import { Global_var } from "../../../global/global_var";


export class SendMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            userid: 0,
            profileUsers: [],
            messageData: [],
            messageStore: [],
            message: "",
            MessageUserid: 0
        };
    }

    componentDidMount() {
        this._loadChatMessage();
    }

    _loadChatMessage() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({
            userid: UserData.id,
            name: username,
            profileUniqueId: UserData.profileUniqueId,
            gender: UserData.gender
        });

        var passValue = {
            userid: UserData.id,
            casteId: 1,
            gender: UserData.gender,
            profileUniqueId: UserData.profileUniqueId
        };

        this.props.FetchSendMessage(passValue,
            (res) => {
                debugger;
                if (res.success) {
                    var messageData = res.responseList;
                    var messsageDataReturn = Array.from(new Set((messageData).map(s => s.userid)))
                        .map(userid => {
                            return {
                                userid: userid,
                                firstName: messageData.find(s => s.userid == userid).firstName,
                                lastName: messageData.find(s => s.userid == userid).lastName,
                                gender: messageData.find(s => s.userid == userid).gender,
                                createdDate: moment(messageData.find(s => s.userid == userid).createdDate).format('DD-MMM-YYYY HH:MM A'),
                                profileUniqueId: messageData.find(s => s.userid == userid).profileUniqueId
                            };
                        });

                    this.setState({
                        messageData: res.responseList,
                        MessageUserid: messsageDataReturn.filter(k => k.userid !== UserData.id)[0].userid,
                        profileUsers: messsageDataReturn.filter(k => k.userid !== UserData.id),
                        messageStore: (res.responseList).filter(k => k.myUserid == Number(messsageDataReturn.filter(k => k.userid !== UserData.id)[0].userid) || k.requestedUserid == Number(messsageDataReturn.filter(k => k.userid !== UserData.id)[0].userid))
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }



    _handleSubmit = (values, { resetForm }, actions) => {
        var passValue = {

        }
        this.props.AddRegistration(passValue,
            (res) => {

                if (res.data.success) {
                    success("User register successfully.", successNotification);
                    this.props.history.push("/login");
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _sendingMessage(values) {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        if (values.message === "") {
            warning("Please enter the message.", warningNotification);
            return;
        }

        var passValue = {
            userid: values.MessageUserid,
            requestedUserid: UserData.id,
            message: values.message
        };
        this.props.SendMessageDetail(passValue,
            (res) => {

                if (res.data.success) {
                    success("Message Send.", successNotification);
                    this._handleClosepopup();
                    this._loadChatMessage();
                    this.setState({ message: "", MessageUserid: passValue.userid });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (

            <React.Fragment>
                <div>
                    <section className="py-3">

                        <Formik
                            enableReinitialize
                            initialValues={{
                                messageData: this.state.messageData,
                                messageStore: this.state.messageStore,
                                profileUsers: this.state.profileUsers,
                                userid: this.state.userid,
                                MessageUserid: this.state.MessageUserid,
                                message: this.state.message,
                            }}
                            validationSchema={this._userAccountSchema}
                            onSubmit={this._handleSubmit}
                        >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                            <Form>
                                <div className="main-layout">
                                    {/* <div className="page-title">
                                        <div className="row gutters">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h5 className="title">Sent Messages</h5>
                                            </div>
                                        </div>
                                    </div> */}
                                    <div className="content-wrapper">
                                        <div className="row gutters">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div className="page-title">Sent Message conversion</div>
                                                <div className="card m-0">
                                                    <div className="row no-gutters">
                                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-3">
                                                            <div className="users-container">
                                                                <div className="chat-search-box">
                                                                    <div className="input-group">
                                                                        <Field
                                                                            type="text"
                                                                            id="otherEdu"
                                                                            name="otherEdu"
                                                                            maxLength="50"
                                                                            placeholder="Search"
                                                                            className="form-control"
                                                                            onChange={(event) => {
                                                                                setFieldValue(
                                                                                    (values.otherEdu = event.target.value)
                                                                                );
                                                                            }}
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <ul className="users">
                                                                    {(values.profileUsers || []).map(
                                                                        (_profileUsers) => (
                                                                            <li className="person" data-chat="person1" id={_profileUsers.userid}
                                                                                onClick={(event) => {

                                                                                    var messageChat = values.messageData.filter(k => k.myUserid == Number(event.currentTarget.id) || k.requestedUserid == Number(event.currentTarget.id));
                                                                                    setFieldValue(
                                                                                        (values.messageStore = messageChat),
                                                                                        (values.useridto = _profileUsers.userid)
                                                                                    );
                                                                                    var objDiv = document.getElementById("parentDiv");
                                                                                    objDiv.scrollTop = objDiv.scrollHeight;
                                                                                }}>
                                                                                <div className="user" >
                                                                                    <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + _profileUsers.userid + "_ProfilePhoto.jpg" : (this.state.gender == 22 ? male : female)} ></img>
                                                                                    {/* <img src={this.state.gender == 22 ? female : male} alt={_profileUsers.firstName + " " + _profileUsers.lastName} /> */}
                                                                                    <span className="status busy"></span>
                                                                                </div>
                                                                                <p className="name-time">
                                                                                    <span className="name">{_profileUsers.firstName + " " + _profileUsers.lastName}</span>
                                                                                    <span className="time">{_profileUsers.createdDate}</span>
                                                                                </p>
                                                                            </li>
                                                                        )
                                                                    )}
                                                                    {/* <li className="person" data-chat="person1">
                                                                        <div className="user">
                                                                            <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" />
                                                                            <span className="status busy"></span>
                                                                        </div>
                                                                        <p className="name-time">
                                                                            <span className="name">Steve Bangalter</span>
                                                                            <span className="time">15/02/2019</span>
                                                                        </p>
                                                                    </li>
                                                                    <li className="person" data-chat="person1">
                                                                        <div className="user">
                                                                            <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" />
                                                                            <span className="status offline"></span>
                                                                        </div>
                                                                        <p className="name-time">
                                                                            <span className="name">Steve Bangalter</span>
                                                                            <span className="time">15/02/2019</span>
                                                                        </p>
                                                                    </li>
                                                                    <li className="person active-user" data-chat="person2">
                                                                        <div className="user">
                                                                            <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" />
                                                                            <span className="status away"></span>
                                                                        </div>
                                                                        <p className="name-time">
                                                                            <span className="name">Peter Gregor</span>
                                                                            <span className="time">12/02/2019</span>
                                                                        </p>
                                                                    </li>
                                                                    <li className="person" data-chat="person3">
                                                                        <div className="user">
                                                                            <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" />
                                                                            <span className="status busy"></span>
                                                                        </div>
                                                                        <p className="name-time">
                                                                            <span className="name">Jessica Larson</span>
                                                                            <span className="time">11/02/2019</span>
                                                                        </p>
                                                                    </li>
                                                                    <li className="person" data-chat="person4">
                                                                        <div className="user">
                                                                            <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" />
                                                                            <span className="status offline"></span>
                                                                        </div>
                                                                        <p className="name-time">
                                                                            <span className="name">Lisa Guerrero</span>
                                                                            <span className="time">08/02/2019</span>
                                                                        </p>
                                                                    </li>
                                                                    <li className="person" data-chat="person5">
                                                                        <div className="user">
                                                                            <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" />
                                                                            <span className="status away"></span>
                                                                        </div>
                                                                        <p className="name-time">
                                                                            <span className="name">Michael Jordan</span>
                                                                            <span className="time">05/02/2019</span>
                                                                        </p>
                                                                    </li> */}
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-9">
                                                            <div>
                                                                <div className="selected-user">
                                                                    <span>To:
                                                                    <span className="name">
                                                                            {((values.messageStore).filter(k => k.userid !== values.userid)[0] || []).firstName + " " + ((values.messageStore).filter(k => k.userid !== values.userid)[0] || []).lastName + " (" + ((values.messageStore).filter(k => k.userid !== values.userid)[0] || []).profileUniqueId + ")"}
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                <div id="parentDiv" className="chat-container">
                                                                    {(values.messageStore || []).map(
                                                                        (messageStore) => (
                                                                            <ul className="chat-box chatContainerScroll">
                                                                                {messageStore.userid !== values.userid ? (
                                                                                    <li className="chat-left">
                                                                                        <div className="chat-avatar">
                                                                                            <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + messageStore.userid + "_ProfilePhoto.jpg" : (this.state.gender == 22 ? male : female)} ></img>
                                                                                            {/* <img src={this.state.gender == 22 ? female : male} alt="Retail Admin" /> */}
                                                                                            <div className="chat-name">{messageStore.firstName}</div>
                                                                                        </div>
                                                                                        <div className="chat-text">{messageStore.message}</div>
                                                                                        <div className="chat-hour">{moment(messageStore.createdDate, ["HH.mm"]).format("hh:mm a")}</div>
                                                                                    </li>
                                                                                ) : (
                                                                                    <li className="chat-right">
                                                                                        <div className="chat-hour">{moment(messageStore.createdDate, ["HH.mm"]).format("hh:mm a")} <span className="fa fa-check-circle"></span></div>
                                                                                        <div className="chat-text">{messageStore.message}</div>
                                                                                        <div className="chat-avatar">
                                                                                            <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.userid + "_ProfilePhoto.jpg" : (this.state.gender == 22 ? male : female)} ></img>
                                                                                            {/* <img src={this.state.gender == 22 ? male : female} alt="Retail Admin" /> */}
                                                                                            <div className="chat-name">{messageStore.firstName}</div>
                                                                                        </div>
                                                                                    </li>
                                                                                )}
                                                                            </ul>
                                                                        )
                                                                    )}

                                                                </div>

                                                                <div className="form-group mb-0 message-text-bg">
                                                                    <div className="row">
                                                                        <div className="col-lg-10 mb-2">
                                                                            <Field
                                                                                type="text"
                                                                                id="message"
                                                                                name="message"
                                                                                maxLength="200"
                                                                                placeholder="Type your message here..."
                                                                                component="textarea"
                                                                                rows="2"
                                                                                with="200px"
                                                                                onChange={(event) => {
                                                                                    setFieldValue(
                                                                                        (values.message = event.target.value)
                                                                                    );
                                                                                }}
                                                                                className="form-control"
                                                                            />
                                                                        </div>
                                                                        <div className="col-lg-2 mb-2">
                                                                            <span>
                                                                                <IconButton color="inherit" onClick={(event) => {
                                                                                    this._sendingMessage(values);
                                                                                }}>
                                                                                    <SendIcon></SendIcon>
                                                                                </IconButton>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        )}
                        </Formik>
                    </section>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            SendMessageDetail: SendMessageDetail
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(SendMessage);
