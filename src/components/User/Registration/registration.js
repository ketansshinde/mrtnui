import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import {
    CryptoCode,
} from "../../../common/cryptoCode";
import queryString from "query-string";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { CasteValue } from "../../../common/CasteValue";
import TextField from '@material-ui/core/TextField';
import * as moment from "moment-timezone";
import "./registration.css";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from 'react-i18next';
import { Loader } from "../CommonComponent";
import background from "./../../../assets/images/IMG_71781.jpg";


export class RegistrationPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ipaddress: "168.192.86.12",
            roleid: "0",
            profileCreatedBy: "",
            firstName: "",
            lastName: "",
            motherName: "",
            gender: "",
            mobileNo: "",
            emailId: "",
            password: "",
            religion: "",
            caste: "",
            dob: "",
            religiondata: [],
            GenderData: [],
            CasteData: [],
            submitting: false,
            loaderMgs: ""
        };
    }

    componentDidMount() {

        const values = queryString.parse(this.props.location.search);

        var param = atob(values.id).split("&");
        var profile = param[0];
        //var fname = param[1];
        var email = param[1];

        var ReligionData = SubMasterValue.SubMaster(MasterValue.Religion);
        var GenderData = SubMasterValue.SubMaster(MasterValue.Gender);

        var CasteData = CasteValue.Caste();


        this.setState({
            profileCreatedBy: profile,
           // firstName: fname,
            emailId: email,
            religiondata: ReligionData,
            GenderData: GenderData,
            CasteData: CasteData
        });
    }

    _userAccountSchema = Yup.object().shape({
        firstName: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_First_Name")}</span>
                    )}
                </Translation>
            )
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed."),
        lastName: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Last_Name")}</span>
                    )}
                </Translation>
            )
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed."),
        middleName: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Middle_Name")}</span>
                    )}
                </Translation>
            )
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed."),
        gender: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Gender")}</span>
                    )}
                </Translation>
            ),
        mobileNo: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Mobile_Number")}</span>
                    )}
                </Translation>
            )
            .matches(/^[0-9]+$/, "Must be only digits."),
        password: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Password")}</span>
                    )}
                </Translation>
            )
            .min(8, 'Password must be 8 characters at minimum.')
            .max(16, 'Password must be 16 characters at maximum.')
            .matches(/[a-z]/, 'At least one lowercase char.')
            .matches(/[A-Z]/, 'At least one uppercase char.')
            .matches(/(?=.*\d)/, 'Must contain a number.')
            .matches(/[!@#$%^&*(),.?":{}|<>]/, 'Must contain special character.')
            .matches(/^\S*$/, 'Password should not allow space.'),
        confirmPassword: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Confirm_Password")}</span>
                    )}
                </Translation>
            )
            .min(8, 'Password must be 8 characters at minimum.')
            .oneOf([Yup.ref('password'), null], 'Passwords must match.'),
        religion: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Religion")}</span>
                    )}
                </Translation>
            ),
        caste: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Caste")}</span>
                    )}
                </Translation>
            ),
        dob: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Registration_Validation_Date_Of_Birth")}</span>
                    )}
                </Translation>
            ),

    });


    _handleSubmit = (values, { resetForm }, actions) => {
        this.setState({ submitting: true });
        var years = moment().diff(values.dob, 'years', false);
        if (Number(values.gender) === 22 && years < 21) {
            warning("Age must be 21", warningNotification);
            return;
        }
        else if (Number(values.gender) === 23 && years < 18) {
            warning("Age must be 18", warningNotification);
            return;
        }

        var passValue = {
            firstName: values.firstName,
            middleName: values.middleName,
            lastName: values.lastName,
            gender: Number(values.gender),
            mobileNo: values.mobileNo,
            emailId: values.emailId,
            password: values.password,
            profileCreatedBy: Number(values.profileCreatedBy),
            ipaddress: "196.168.25.21",
            roleId: 1,
            dob: values.dob,
            religion: Number(values.religion),
            caste: Number(values.caste),
        }
        this.props.AddRegistration(passValue,
            (res) => {

                if (res.data.success) {
                    success("Kindly check your email to confirm account.", successNotification);
                    this.props.history.push("/login");
                }
                else {
                    error(res.data.errorlog, errorNotification);
                }
                this.setState({ submitting: false });
            },
            (error) => {
                console.log(error);
                this.setState({ submitting: false });
            }
        );
    }


    render() {
        return (

            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <div>
                        {/* register-bg  */}
                        <section className="py-3 reg-background" style={{ backgroundImage: `url(${background})` }}>

                            <Formik
                                enableReinitialize
                                initialValues={{
                                    ipaddress: "168.192.86.12",
                                    roleid: "0",
                                    profileCreatedBy: this.state.profileCreatedBy,
                                    firstName: this.state.firstName,
                                    middleName: this.state.middleName,
                                    lastName: this.state.lastName,
                                    gender: this.state.gender,
                                    mobileNo: this.state.mobileNo,
                                    emailId: this.state.emailId,
                                    password: this.state.password,
                                    religion: this.state.religion,
                                    caste: this.state.caste,
                                    dob: this.state.dob,
                                    religiondata: this.state.religiondata,
                                    confirmPassword: this.state.confirmPassword,
                                    CasteData: this.state.CasteData,
                                    GenderData: this.state.GenderData,
                                    submitting: this.state.submitting,
                                    loaderMgs: this.state.loaderMgs
                                }}
                                validationSchema={this._userAccountSchema}
                                onSubmit={this._handleSubmit}
                            >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                                <Form>
                                    <div className="main-layout">
                                        {values.submitting ? <Loader state={values.loaderMgs}></Loader> : null}
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <div className="card">
                                                        <div className="card-body">
                                                            <h5 className="card-title card-title-set text_align_left">
                                                                <Translation>
                                                                    {(t, { i18n }) => (
                                                                        <span>{t("Registration_page")}</span>
                                                                    )}
                                                                </Translation> - ( {values.emailId} )</h5>
                                                            <div className="row">
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="firstName">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_First_Name")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="firstName"
                                                                        name="firstName"
                                                                        maxLength="50"
                                                                        placeholder="First Name"
                                                                        //disabled
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.firstName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.firstName && errors.firstName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="firstName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="middleName">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Middle_Name")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="middleName"
                                                                        name="middleName"
                                                                        maxLength="50"
                                                                        placeholder="Middle Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.middleName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.middleName && errors.middleName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="middleName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="lastName">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Last_Name")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="lastName"
                                                                        name="lastName"
                                                                        maxLength="50"
                                                                        placeholder="Last Name"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.lastName = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.lastName && errors.lastName
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="lastName"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                            </div>

                                                            <div className="row">

                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="mobileNo">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Mobile_Number")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        type="text"
                                                                        id="mobileNo"
                                                                        name="mobileNo"
                                                                        maxLength="10"
                                                                        placeholder="Mobile Number"
                                                                        onChange={(event) => {

                                                                            setFieldValue(
                                                                                (values.mobileNo = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.mobileNo && errors.mobileNo
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="mobileNo"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="gender">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Gender")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        as="select"
                                                                        name="gender"
                                                                        onChange={(event) => {
                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");

                                                                            setFieldValue(
                                                                                (values.gender = event.target.value),
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.gender && errors.gender
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    >
                                                                        <option value="">Select Groom / Bride</option>
                                                                        {(values.GenderData || []).map(
                                                                            (_subMasterData) => (
                                                                                <option
                                                                                    data-key={_subMasterData.subMasterId}
                                                                                    key={_subMasterData.subMasterId}
                                                                                    value={JSON.stringify(
                                                                                        _subMasterData.subMasterId
                                                                                    )}
                                                                                >
                                                                                    {_subMasterData.name}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="gender"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="dob">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Date_Of_Birth")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        type="date"
                                                                        id="dob"
                                                                        name="dob"
                                                                        //component={TextField}
                                                                        defaultValue="2000-05-24"
                                                                        // timezone={Intl.DateTimeFormat().resolvedOptions().timeZone}
                                                                        inputlabelpops={{
                                                                            shrink: true,
                                                                        }}
                                                                        placeholder="MM/DD/YYYY"

                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.dob = event.target.value)
                                                                            );
                                                                        }}
                                                                        className="form-control"
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="dob"
                                                                        className="text-danger"
                                                                    />


                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="religion">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Religion")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        as="select"
                                                                        name="religion"

                                                                        className={`form-control ${touched.religion && errors.religion
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                        onChange={(event) => {

                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");
                                                                            setFieldValue(
                                                                                (values.religion = event.target.value),
                                                                            );
                                                                        }}
                                                                    >
                                                                        <option value="">Select Religion</option>
                                                                        {(values.religiondata || []).map(
                                                                            (_subMasterData) => (
                                                                                <option
                                                                                    data-key={_subMasterData.subMasterId}
                                                                                    key={_subMasterData.subMasterId}
                                                                                    value={JSON.stringify(
                                                                                        _subMasterData.subMasterId
                                                                                    )}
                                                                                >
                                                                                    {_subMasterData.name}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="religion"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="caste">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Caste")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        as="select"
                                                                        name="caste"
                                                                        className={`form-control ${touched.caste && errors.caste
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                        onChange={(event) => {

                                                                            const relationship = event.target.options[
                                                                                event.target.options.selectedIndex
                                                                            ].getAttribute("data-key");
                                                                            setFieldValue(
                                                                                (values.caste = event.target.value),
                                                                            );
                                                                        }}
                                                                    >
                                                                        <option value="">Select Caste</option>
                                                                        {(values.CasteData || []).map(
                                                                            (_subMasterData) => (
                                                                                <option
                                                                                    data-key={_subMasterData.casteId}
                                                                                    key={_subMasterData.casteId}
                                                                                    value={JSON.stringify(
                                                                                        _subMasterData.casteId
                                                                                    )}
                                                                                >
                                                                                    {_subMasterData.casteName}
                                                                                </option>
                                                                            )
                                                                        )}
                                                                    </Field>
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="caste"
                                                                        className="text-danger"
                                                                    />
                                                                </div>

                                                            </div>

                                                            <div className="row">
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="password">

                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Password")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>

                                                                    <Field
                                                                        type="password"
                                                                        id="password"
                                                                        name="password"
                                                                        maxLength="50"
                                                                        placeholder="Password e.g - Abc@12345"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.password = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.password && errors.password
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="password"
                                                                        className="text-danger"
                                                                    />
                                                                </div>
                                                                <div className="col-lg-4 mb-2">
                                                                    <label className="medium" htmlFor="confirmPassword">
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Confirm_Password")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </label>
                                                                    <Field
                                                                        type="password"
                                                                        id="confirmPassword"
                                                                        name="confirmPassword"
                                                                        maxLength="50"
                                                                        placeholder="Re-Enter Password"
                                                                        onChange={(event) => {
                                                                            setFieldValue(
                                                                                (values.confirmPassword = event.target.value)
                                                                            );
                                                                        }}
                                                                        className={`form-control ${touched.confirmPassword && errors.confirmPassword
                                                                            ? "is-invalid"
                                                                            : ""
                                                                            }`}
                                                                    />
                                                                    <ErrorMessage
                                                                        component="div"
                                                                        name="confirmPassword"
                                                                        className="text-danger"
                                                                    />
                                                                </div>

                                                            </div>
                                                            <div className="row">
                                                                <div className="col-lg-12 mb-2 text_align_right">
                                                                    <SubmitButton
                                                                        type="submit"
                                                                        variant="contained"
                                                                    >
                                                                        <Translation>
                                                                            {(t, { i18n }) => (
                                                                                <span>{t("Registration_Register")}</span>
                                                                            )}
                                                                        </Translation>
                                                                    </SubmitButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                            </Formik>
                        </section>
                    </div>
                </I18nextProvider>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationPage);
