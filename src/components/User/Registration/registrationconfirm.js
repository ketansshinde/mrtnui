import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import queryString from "query-string";


const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );



class RegistrationConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            guidDetail: "",
            formView: false,
        };
    }
    componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        var param = values.id;
        this.setState({ guidDetail: param });
        this._AccountConfirmation(param);
    }

    _AccountConfirmation(guidDetail) {
        if (guidDetail === undefined || guidDetail === "") {
            warning("Invalid Page", warningNotification);
            this.props.history.push("/");
            return;
        }
        debugger;
        this.setState({ formView: true });
        var passValue = {
            userGuid: guidDetail,
        }
        this.props.UserRegistrationConfirm(passValue,
            (res) => {
                if (res.data.success) {
                    success("Account has been activited", successNotification);
                }
                else {
                    error(res.data.errorlog, errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    _login = () => {
        this.props.history.push("/login");
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        email: this.state.email,
                        formView: this.state.formView
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <section className="py-3">
                            <div className="main-layout">
                                <div className="row">
                                    <div className="col-lg-6 offset-md-3">
                                        {values.formView ?
                                            <div className="card layout-margin">
                                                <div className="card-body">
                                                    {/* <h5 className="card-title card-title-set">Forget Password</h5> */}

                                                    <div className="row" >

                                                        <div className="col-lg-12 mb-2 text_align_center">
                                                            <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                                                <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                                                <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                                            </svg>
                                                        </div>
                                                        <div className="col-lg-12 mb-2 text_align_center">

                                                            <b>Your account has been activated</b>

                                                        </div>
                                                        <div className="col-lg-12 mb-2 text_align_center">

                                                            <SubmitButton
                                                                type="submit"
                                                                variant="contained"
                                                                onClick={this._login}
                                                            >
                                                                Login
                                                    </SubmitButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            : "Page not found"}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationConfirm);