import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { getLogin } from "../../../reducer/loginReducer";
import queryString from "query-string";
import * as moment from "moment-timezone";
import background from "./../../../assets/images/IMG_71781.jpg";
import { Button, Modal } from 'react-bootstrap'
import {
    SubmitButton, CloseButton, LoginButton, InviteButton
} from "../../../assets/MaterialControl";
import { IconButton, Tooltip } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import {
    CryptoCode,
} from "../../../common/cryptoCode";

import { MasterValue } from "../../../global/global_var";
import "./home.css";
import explogo from "./../../../assets/images/DSC_5535.jpg";
import SharedDetail from "./sharedetail";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from 'react-i18next';
import { withTranslation } from 'react-i18next';
import HomeSlider from "../CommonComponent/HomeSlider/HomeSlider";



export class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: "",
            email: "",
            name: "",
        };
        this.initialState = this.state;
    }


    componentDidMount() {
        // var lang = localStorage.getItem("lang");
        // i18n.changeLanguage(lang);

    }

    _userAccountSchema = Yup.object().shape({
        profile: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Form_Home_Page_created_for")}</span>
                    )}
                </Translation>
            ),
        // name: Yup.string()
        //     .required(<Translation>
        //         {(t, { i18n }) => (
        //             <span>{t("Form_Home_Page_fname")}</span>
        //         )}
        //     </Translation>),
        email: Yup.string()
            .required(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Form_Home_Page_emailId")}</span>
                    )}
                </Translation>
            ).email(
                <Translation>
                    {(t, { i18n }) => (
                        <span>{t("Form_Home_Page_emailId_error")}</span>
                    )}
                </Translation>
            ),
    });


    _handleSubmit = (values, { resetForm }, actions) => {

        var profile = values.profile;
        //var name = values.name;
        var email = values.email;
        //var encryptionURL = btoa(profile + "&" + name + "&" + email);
        var encryptionURL = btoa(profile + "&" + email);
        this.props.history.push("/registration?id=" + encryptionURL);
    }

    render() {
        return (

            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <div>
                        <section className="py-3">
                            <div className="main-layout home-main-layout">
                                <div className="row">
                                    <div className="col-lg-12">
                                        <HomeSlider></HomeSlider>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section className="py-3 registration-section-border" id="registrationPage">
                            <Formik
                                enableReinitialize
                                initialValues={{
                                    //name: this.state.name,
                                    email: this.state.email,
                                    profile: this.state.profile,
                                }}
                                validationSchema={this._userAccountSchema}
                                onSubmit={this._handleSubmit}
                            >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                                <Form>
                                    <div className="container">
                                        <div className="row home-section">
                                            <div className="col-lg-12 text_align_center home-section-header">
                                               Free Registration
                                            </div>
                                            {/* style={{ backgroundImage: `url(${background})`, backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundColor: "#000" }} */}
                                            <div className="col-lg-8 banner-content" >
                                                <div className="home-heading headerSection">
                                                    <Translation>
                                                        {(t, { i18n }) => (
                                                            <span>{t("Someone_is_looking_for_you")}</span>
                                                        )}
                                                    </Translation><br />
                                                    <Translation>
                                                        {(t, { i18n }) => (
                                                            <span>{t("Be_found_and_choose")}</span>
                                                        )}
                                                    </Translation>
                                                </div>

                                            </div>
                                            <div className="col-lg-4">
                                                <div className="card1 shadow-lg border-0 rounded-lg mt-4">
                                                    <div className=" title-margin">
                                                        <h3 className="text-center register-title">
                                                            <Translation>
                                                                {(t, { i18n }) => (
                                                                    <span>{t("We_find_perfect_matches_for_you")}</span>
                                                                )}
                                                            </Translation>
                                                        </h3>
                                                        <h3 className="text-center register-subtitle">
                                                            <Translation>
                                                                {(t, { i18n }) => (
                                                                    <span>{t("Let_create_your_free_account")}</span>
                                                                )}
                                                            </Translation>

                                                        </h3>
                                                    </div>

                                                    <div className="card-body">
                                                        <div className="form-group">
                                                            <label className="medium" htmlFor="inputEmailAddress">
                                                                <Translation>
                                                                    {(t, { i18n }) => (
                                                                        <span>{t("Create_for")}</span>
                                                                    )}
                                                                </Translation>
                                                            </label>
                                                            <Field
                                                                as="select"
                                                                name="profile"

                                                                className="form-control"
                                                                onChange={(event) => {

                                                                    const relationship = event.target.options[
                                                                        event.target.options.selectedIndex
                                                                    ].getAttribute("data-key");
                                                                    setFieldValue(
                                                                        (values.profile = event.target.value),
                                                                    );
                                                                }}
                                                            >
                                                                <option value="">Select Profile</option>
                                                                {((this.props.subMasterData).filter(x => x.masterId === MasterValue.Profile_created_by) || []).map(
                                                                    (_subMasterData) => (
                                                                        <option
                                                                            data-key={_subMasterData.subMasterId}
                                                                            key={_subMasterData.sun}
                                                                            value={JSON.stringify(
                                                                                _subMasterData.subMasterId
                                                                            )}
                                                                        >
                                                                            {_subMasterData.name}
                                                                        </option>
                                                                    )
                                                                )}
                                                            </Field>
                                                            <ErrorMessage
                                                                component="div"
                                                                name="profile"
                                                                className="text-danger"
                                                            />

                                                        </div>
                                                        {/* <div className="form-group">
                                                                <label className="medium" htmlFor="inputEmailAddress">
                                                                    <Translation>
                                                                        {(t, { i18n }) => (
                                                                            <span>{t("Name")}</span>
                                                                        )}
                                                                    </Translation>
                                                                </label>
                                                                <Field
                                                                    type="name"
                                                                    id="name"
                                                                    name="name"
                                                                    maxLength="50"
                                                                    placeholder="e.g. First Name"
                                                                    onChange={(event) => {
                                                                        setFieldValue(
                                                                            (values.name = event.target.value)
                                                                        );
                                                                    }}
                                                                    className={`form-control ${touched.name && errors.name
                                                                        ? "is-invalid"
                                                                        : ""
                                                                        }`}
                                                                />
                                                                <ErrorMessage
                                                                    component="div"
                                                                    name="name"
                                                                    className="text-danger"
                                                                />

                                                            </div> */}
                                                        <div className="form-group">
                                                            <label className="medium" htmlFor="inputEmailAddress">
                                                                <Translation>
                                                                    {(t, { i18n }) => (
                                                                        <span>{t("Email_ID")}</span>
                                                                    )}
                                                                </Translation>
                                                            </label>
                                                            <Field
                                                                type="email"
                                                                id="email"
                                                                name="email"
                                                                maxLength="50"
                                                                placeholder="Enter Email-ID"
                                                                onChange={(event) => {
                                                                    setFieldValue(
                                                                        (values.email = event.target.value)
                                                                    );
                                                                }}
                                                                className={`form-control ${touched.email && errors.email
                                                                    ? "is-invalid"
                                                                    : ""
                                                                    }`}
                                                            />
                                                            <ErrorMessage
                                                                component="div"
                                                                name="email"
                                                                className="text-danger"
                                                            />
                                                        </div>


                                                        <LoginButton
                                                            type="submit"
                                                            variant="contained"
                                                        >
                                                            <Translation>
                                                                {(t, { i18n }) => (
                                                                    <span>{t("Next")}</span>
                                                                )}
                                                            </Translation>
                                                        </LoginButton>
                                                        <div style={{ fontSize: "12px", textAlign: "center", marginTop: "30px" }}>
                                                            <Translation>
                                                                {(t, { i18n }) => (
                                                                    <span>{t("By_clicking_register_free_I_agree_to_the_TC_and_Privacy_Policy")}</span>
                                                                )}
                                                            </Translation>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Form>
                            )}
                            </Formik>
                        </section>
                        {/* <section className="py-3">
                            <div className="">
                                <div className="home-main-layout">
                                    <div className="row">
                                        <div className="col-lg-12 banner-content">
                                            <HomeSlider></HomeSlider>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section> */}
                        {/* Why Perfect match? */}
                        <section className="py-3">
                            <div className="container">
                                <div className="row home-section">
                                    <div className="col-lg-12 text_align_center home-section-header">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Why_Perfect_Match")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 text_align_center home-why-desc">
                                        <div className="home-why-desc-line">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("online_marriage_bureau")}</span>
                                                )}
                                            </Translation>
                                        </div>
                                        <div className="home-why-desc-line">

                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("online_marriage_bureau1")}</span>
                                                )}
                                            </Translation>
                                        </div>
                                        <div className="home-why-desc-line">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("online_marriage_bureau2")}</span>
                                                )}
                                            </Translation>
                                        </div>
                                    </div>
                                    {/* <div className="col-lg-12">
                                Why Perfect match?
                            </div> */}
                                </div>
                            </div>
                        </section>
                        {/* <section className="py-3 experience-border">
                            <div className="container">
                                <div className="row home-section">
                                    <div className="col-lg-12 text_align_center home-section-header">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Experience_truly_Perfect_Matchmaking")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-6 text_align_center ">
                                        <img src={explogo} className="experience-img"></img>
                                    </div>
                                    <div className="col-lg-6 text_align_center">
                                        <SharedDetail {...this.props}></SharedDetail>
                                    </div>
                                </div>
                            </div>
                        </section> */}
                    </div>
                </I18nextProvider>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
