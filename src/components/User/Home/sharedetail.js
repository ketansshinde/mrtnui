import React, { Component } from 'react';
import {
    SubmitButton, CloseButton, LoginButton, InviteButton
} from "../../../assets/MaterialControl";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from 'react-i18next';

class SharedDetail extends Component {
    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <div className=" experience-form">
                        <div className="experience-shared-detail text_align_center">
                            <Translation>
                                {(t, { i18n }) => (
                                    <span>{t("Share_your_details")}</span>
                                )}
                            </Translation>
                        </div>
                        <div className="experience-formline-spacing">
                            <input
                                type="name"
                                id="name"
                                name="name"
                                maxLength="50"
                                placeholder="Full Name"
                                onChange={(event) => {

                                }}
                                className="form-control experience-txt-control"
                            />
                        </div>
                        <div className="experience-formline-spacing">
                            <input
                                type="text"
                                id="name"
                                name="EmailID"
                                maxLength="50"
                                placeholder="Email ID Address"
                                onChange={(event) => {

                                }}
                                className="form-control experience-txt-control"
                            />
                        </div>
                        <div className="experience-formline-spacing">
                            <div className="row">
                                <div className="col-lg-3">
                                    <input
                                        type="text"
                                        id="name"
                                        name="countryCode"
                                        maxLength="50"
                                        placeholder=""
                                        value="+91"
                                        onChange={(event) => {

                                        }}
                                        className="form-control experience-txt-control"
                                    />
                                </div>
                                <div className="col-lg-9">
                                    <input
                                        type="text"
                                        id="name"
                                        name="mobileno"
                                        maxLength="50"
                                        placeholder="Mobile No."
                                        onChange={(event) => {

                                        }}
                                        className="form-control experience-txt-control"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="experience-formline-spacing text_align_right">
                            <InviteButton
                                type="submit"
                                variant="contained"
                            >
                                <Translation>
                                    {(t, { i18n }) => (
                                        <span>{t("Submit_Details")}</span>
                                    )}
                                </Translation>
                            </InviteButton>
                        </div>
                    </div>
                </I18nextProvider>
            </React.Fragment>
        );
    }
}

export default SharedDetail;