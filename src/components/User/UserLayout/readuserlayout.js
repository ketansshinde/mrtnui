import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import UserLeftSideNotification from "../UserLeftSideNotification/userleftsidenotificatio";
import ReadUserProfile from "./readuserprofile";
import ReadUserAddress from "./readuseraddress";
import ReadUserPersonal from "./readuserpersonal";
import ReadUserReligion from "./readuserreligion";
import ReadUserOccupation from "./readuseroccupation";
import ReadUserLifeStyle from "./readuserlifestyle";
import ReadUserFamilyBG from "./readuserfamilybg";
import ReadUserExpectation from "./readuserexpectation";
import { Formik, Form, Field, ErrorMessage } from "formik";
import FeedbackIcon from '@material-ui/icons/Feedback';
import { IconButton } from "@material-ui/core";
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import { Global_var } from "../../../global/global_var";
import "./userlayout.css";

class ReadUserLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            profileBase64string: "",
            profilePhoto: ""
        };
    }

    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });

        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        this.setState({ profilePhoto: userloginDetail.documentName });
    }
    getFiles(files) {

        this.setState({ files: files, profileBase64string: files.base64 })
    }

    _changePhoto = () => {
        this.props.history.push("/changeprofilephoto");
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <Formik
                        enableReinitialize
                        initialValues={{
                            profileBase64string: this.state.profileBase64string,
                            profilePhoto: this.state.profilePhoto
                        }}
                        validationSchema={this._userAccountSchema}
                        onSubmit={this._handleSubmit}
                    >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <div className="userlayout-leftside">
                                                <div className="card-body">
                                                    <h5 className="card-title card-title-set text_align_center">{this.state.name}</h5>
                                                    <h6 className="card-subtitle mb-2 text_align_center padding-bottum" >{this.state.profileUniqueId}</h6>
                                                    <div className="text_align_center" onClick={this._changePhoto}>
                                                        <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.profilePhoto : (this.state.gender == 22 ? male : female)} className="userlayout-profile-image" alt="logo"></img>
                                                    </div>

                                                    <div className="text_align_center layout-margin">
                                                        <SubmitButton
                                                            type="submit"
                                                            variant="contained"
                                                            onClick={this._changePhoto}
                                                        >
                                                            Change Profile Photo
                                                        </SubmitButton>

                                                    </div>
                                                    <hr />

                                                    <div className="layout-margin hyperlink" onClick={this._changeprofilephoto}>
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-info-circle" aria-hidden="true"></i>
                                                            </IconButton></span>
                                                        <span className="text_align_left">About Me</span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-map-marker" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left"> <a href="#address">Address Details</a></span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-user-circle" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left " >Horoscope</span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-globe" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left " >Religious Background</span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left " >Education &amp; Occupation</span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-life-ring" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left " >Life Style</span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-heartbeat" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left ">Family Details</span>
                                                    </div>
                                                    <div className="layout-margin hyperlink">
                                                        <span className="padding-right">
                                                            <IconButton color="inherit" size="small">
                                                                <i className="fa fa-outdent" aria-hidden="true"></i>
                                                            </IconButton>
                                                        </span>
                                                        <span className="text_align_left " >Expectation</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-9">
                                            <ReadUserProfile {...this.props}></ReadUserProfile>
                                            <ReadUserAddress {...this.props}></ReadUserAddress>
                                            <ReadUserPersonal {...this.props}></ReadUserPersonal>
                                            <ReadUserReligion {...this.props}></ReadUserReligion>
                                            <ReadUserOccupation {...this.props}></ReadUserOccupation>
                                            <ReadUserLifeStyle {...this.props}></ReadUserLifeStyle>
                                            <ReadUserFamilyBG {...this.props}></ReadUserFamilyBG>
                                            <ReadUserExpectation {...this.props}></ReadUserExpectation>

                                        </div>
                                    </div>
                                </div>
                            </section>

                        </Form>
                    )}
                    </Formik>
                </div>
            </React.Fragment>
        );
    }
}
export default ReadUserLayout;
