import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { UpdateUserReligion } from "../../../action/UserAction/UserLayoutAction";
import { CasteValue } from "../../../common/CasteValue";
import { SubCasteValue } from "../../../common/SubCasteValue";
import { IconButton } from "@material-ui/core";


const mapStateToProps = (state) => ({
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserReligion: UpdateUserReligion
        },
        dispatch
    );


class ReadUserReligion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            RelationData: [],
            CasteData: [],
            SubCasteData: [],
            MotherTongueData: [],
            ManglikData: [],
            PatrickaData: [],
            InterCasteData: [],
            FamilyValuesData: [],
            FamilyValueDetailData: [],

            patrikaId: 0,
            userid: 0,
            religion: 0,
            caste: 0,
            subCaste: 0,
            motherTongue: 0,
            manglik: 0,
            patrikRequired: 0,
            interCaste: 0,
            familyValues: 0,
            familyValuesDetail: 0,

        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var RelationData = SubMasterValue.SubMaster(MasterValue.Religion);
        //var CasteData = SubMasterValue.SubMaster(MasterValue.caste);
        //var SubCasteData = SubMasterValue.SubMaster(MasterValue.sub_caste);
        var MotherTongueData = SubMasterValue.SubMaster(MasterValue.Mother_Tongue);
        var ManglikData = SubMasterValue.SubMaster(MasterValue.Manglik);
        var PatrickaData = SubMasterValue.SubMaster(MasterValue.Patricka);
        var InterCasteData = SubMasterValue.SubMaster(MasterValue.Inter_caste);
        var FamilyValuesData = SubMasterValue.SubMaster(MasterValue.Family_values);
        var FamilyValueDetailData = SubMasterValue.SubMaster(MasterValue.Family_Value_Detail);
        var CasteData = CasteValue.Caste();

        var SubCasteData = SubCasteValue.SubCaste();


        this.setState({
            RelationData: RelationData,
            CasteData: CasteData,
            SubCasteData: SubCasteData,
            MotherTongueData: MotherTongueData,
            ManglikData: ManglikData,
            PatrickaData: PatrickaData,
            InterCasteData: InterCasteData,
            FamilyValuesData: FamilyValuesData,
            FamilyValueDetailData: FamilyValueDetailData
        });

        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserReligionById(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        patrikaId: UserData.patrikaId,
                        userid: UserData.userid,
                        religion: UserData.religion,
                        caste: UserData.caste,
                        subCaste: UserData.subCaste,
                        motherTongue: UserData.motherTongue,
                        manglik: UserData.manglik,
                        patrikRequired: UserData.patrikRequired,
                        interCaste: UserData.interCaste,
                        familyValues: UserData.familyValues,
                        familyValuesDetail: UserData.familyValuesDetail,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            patrikaId: Number(values.patrikaId) || 0,
            userid: UserData.id,
            religion: Number(values.religion),
            caste: Number(values.caste),
            subCaste: Number(values.subCaste),
            motherTongue: Number(values.motherTongue),
            manglik: Number(values.manglik),
            patrikRequired: Number(values.patrikRequired),
            interCaste: Number(values.interCaste),
            familyValues: Number(values.familyValues),
            familyValuesDetail: Number(values.familyValuesDetail),
        }

        this.props.UpdateUserReligion(passValue,
            (res) => {
                if (res.data.success) {
                    success("Religion updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        RelationData: this.state.RelationData,
                        CasteData: this.state.CasteData,
                        SubCasteData: this.state.SubCasteData,
                        SubCasteDataFilter: this.state.SubCasteData,
                        MotherTongueData: this.state.MotherTongueData,
                        ManglikData: this.state.ManglikData,
                        PatrickaData: this.state.PatrickaData,
                        InterCasteData: this.state.InterCasteData,
                        FamilyValuesData: this.state.FamilyValuesData,
                        FamilyValueDetailData: this.state.FamilyValueDetailData,

                        patrikaId: this.state.patrikaId,
                        userid: this.state.userid,
                        religion: this.state.religion,
                        caste: this.state.caste,
                        subCaste: this.state.subCaste,
                        motherTongue: this.state.motherTongue,
                        manglik: this.state.manglik,
                        patrikRequired: this.state.patrikRequired,
                        interCaste: this.state.interCaste,
                        familyValues: this.state.familyValues,
                        familyValuesDetail: this.state.familyValuesDetail,

                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="user-religion layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    My Religion</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="religion">Religion</label>
                                        <Field
                                            as="select"
                                            name="religion"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.religion = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Religion</option>
                                            {(values.RelationData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="caste">Caste</label>
                                        <Field
                                            as="select"
                                            name="caste"

                                            className="form-control"
                                            onChange={(event) => {
                                                
                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                const casteid = event.target.value;
                                                const SubCaste = values.SubCasteData;

                                                setFieldValue(
                                                    (values.caste = casteid),
                                                    (values.SubCasteDataFilter = SubCaste.filter(x => x.casteId === Number(casteid)))
                                                );

                                            }}
                                        >
                                            <option value="">Select Caste</option>
                                            {(values.CasteData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.casteId}
                                                        key={_subMasterData.casteId}
                                                        value={JSON.stringify(
                                                            _subMasterData.casteId
                                                        )}
                                                    >
                                                        {_subMasterData.casteName}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="subCaste">Sub-Caste</label>
                                        <Field
                                            as="select"
                                            name="subCaste"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.subCaste = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Sub-Caste</option>
                                            {(values.SubCasteDataFilter || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subCasteId}
                                                        key={_subMasterData.subCasteId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subCasteId
                                                        )}
                                                    >
                                                        {_subMasterData.subCasteName}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="motherTongue">Mother Tongue</label>
                                        <Field
                                            as="select"
                                            name="motherTongue"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.motherTongue = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Mother Tongue</option>
                                            {(values.MotherTongueData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="manglik">Manglik</label>
                                        <Field
                                            as="select"
                                            name="manglik"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.manglik = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Manglik</option>
                                            {(values.ManglikData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Patrika Required</label>
                                        <Field
                                            as="select"
                                            name="patrikRequired"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.patrikRequired = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Patrika Required</option>
                                            {(values.PatrickaData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="interCaste">Inter Caste</label><br />
                                        <Field
                                            as="select"
                                            name="interCaste"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.interCaste = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Inter Caste</option>
                                            {(values.InterCasteData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="familyValues">Family Value</label><br />
                                        <Field
                                            as="select"
                                            name="familyValues"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.familyValues = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Family Value</option>
                                            {(values.FamilyValuesData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="familyValuesDetail">Family Value Detail</label><br />
                                        <Field
                                            as="select"
                                            name="familyValuesDetail"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.familyValuesDetail = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Family Detail</option>
                                            {(values.FamilyValueDetailData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Religion
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadUserReligion);