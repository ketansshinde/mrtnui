import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";

import { fetchCountry } from "../../../action/AdminAction/CountryAction";
import { GetCountry } from "../../../reducer/AdminReducer/CountryReducer";

import { fetchState } from "../../../action/AdminAction/StateAction";
import { GetState } from "../../../reducer/AdminReducer/StateReducer";

import { fetchCity } from "../../../action/AdminAction/CityAction";
import { GetCity } from "../../../reducer/AdminReducer/CityReducer";

import { UpdateUserAddress } from "../../../action/UserAction/UserLayoutAction";

import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CryptoCode } from "../../../common/cryptoCode";
import { IconButton } from "@material-ui/core";
import LocationOnIcon from '@material-ui/icons/LocationOn';


const mapStateToProps = (state) => ({
    CountryData: GetCountry(state.CountryReducer),
    stateData: GetState(state.StateReducer),
    CityData: GetCity(state.CityReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchCountry: fetchCountry,
            fetchState: fetchState,
            fetchCity: fetchCity,
            UpdateUserAddress: UpdateUserAddress
        },
        dispatch
    );



class ReadUserAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userRegistrationId: 0,
            userid: 0,
            addressDetail: "",
            country: 0,
            state: 0,
            area: "",
            city: 0,
            pincode: 0,
            registrationDate: "",
            expirationDate: "",
            activationDate: "",
            createdDate: "",
            modifiedDate: "",
            user: ""
        };
    }
    componentDidMount() {
        debugger;
        const { fetchCountry } = this.props;
        fetchCountry();

        const { fetchState } = this.props;
        fetchState();

        const { fetchCity } = this.props;
        fetchCity();

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));


        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserAddressDetail(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        userRegistrationId: UserData.userRegistrationId,
                        userid: UserData.userid,
                        addressDetail: UserData.addressDetail,
                        country: UserData.country,
                        state: UserData.state,
                        area: UserData.area,
                        city: UserData.city,
                        pincode: UserData.pincode,
                        // registrationDate: UserData.userRegistrationId,
                        // expirationDate: UserData.userRegistrationId,
                        // activationDate: UserData.userRegistrationId,
                        // createdDate: UserData.userRegistrationId,
                        // modifiedDate: UserData.userRegistrationId,
                        // user: UserData.userRegistrationId,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            userRegistrationId: values.userRegistrationId,
            userid: UserData.id,
            addressDetail: values.addressDetail,
            country: Number(values.country),
            state: Number(values.state),
            area: values.area,
            city: Number(values.city),
            pincode: Number(values.pincode),
        }

        this.props.UpdateUserAddress(passValue,
            (res) => {
                if (res.data.success) {
                    success("Address updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        userRegistrationId: this.state.userRegistrationId,
                        userid: this.state.userid,
                        addressDetail: this.state.addressDetail,
                        country: this.state.country,
                        state: this.state.state,
                        area: this.state.area,
                        city: this.state.city,
                        pincode: this.state.pincode,
                        registrationDate: this.state.registrationDate,
                        expirationDate: this.state.expirationDate,
                        activationDate: this.state.activationDate,
                        createdDate: this.state.createdDate,
                        modifiedDate: this.state.modifiedDate,
                        user: this.state.user,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div id="address" className="user-address layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <LocationOnIcon></LocationOnIcon>
                                        </IconButton>
                                    </span>
                                    Address Details</h5>
                                <div className="row">

                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="country">Country</label>
                                        <Field
                                            as="select"
                                            name="country"

                                            className={`form-control ${touched.country && errors.country
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.country = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Country</option>
                                            {(this.props.CountryData || []).map(
                                                (_CountryData) => (
                                                    <option
                                                        data-key={_CountryData.countryId}
                                                        key={_CountryData.countryId}
                                                        value={JSON.stringify(
                                                            _CountryData.countryId
                                                        )}
                                                    >
                                                        {_CountryData.countryName}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                        <ErrorMessage
                                            component="div"
                                            name="country"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="state">State</label>
                                        <Field
                                            as="select"
                                            name="state"

                                            className={`form-control ${touched.state && errors.state
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.state = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select State</option>
                                            {(this.props.stateData.filter(x => x.countryId == values.country) || []).map(
                                                (_stateData) => (
                                                    <option
                                                        data-key={_stateData.stateId}
                                                        key={_stateData.stateId}
                                                        value={JSON.stringify(
                                                            _stateData.stateId
                                                        )}
                                                    >
                                                        {_stateData.stateName}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                        <ErrorMessage
                                            component="div"
                                            name="state"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="city">City</label>
                                        <Field
                                            as="select"
                                            name="city"

                                            className={`form-control ${touched.city && errors.city
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.city = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select City</option>
                                            {(this.props.CityData.filter(x => x.stateId == values.state) || []).map(
                                                (_CityData) => (
                                                    <option
                                                        data-key={_CityData.cityId}
                                                        key={_CityData.cityId}
                                                        value={JSON.stringify(
                                                            _CityData.cityId
                                                        )}
                                                    >
                                                        {_CityData.cityName}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                        <ErrorMessage
                                            component="div"
                                            name="city"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="AddressDetail">Address</label>
                                        <Field
                                            type="text"
                                            id="addressDetail"
                                            name="addressDetail"
                                            maxLength="200"
                                            placeholder="Address"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.addressDetail = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.addressDetail && errors.addressDetail
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="addressDetail"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="pincode">Pincode</label>

                                        <Field
                                            type="text"
                                            id="pincode"
                                            name="pincode"
                                            maxLength="6"
                                            minLength="6"
                                            placeholder="Pincode"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.pincode = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.pincode && errors.pincode
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="pincode"
                                            className="text-danger"
                                        />
                                    </div>
                                    {/* <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Registration Date</label>
                                        <input type="text" className="form-control" placeholder="Email Id" />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Expiration Date</label>
                                        <input type="text" className="form-control" placeholder="Email Id" />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Activation Date</label>
                                        <input type="text" className="form-control" placeholder="Email Id" />
                                    </div> */}
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Address
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReadUserAddress);
