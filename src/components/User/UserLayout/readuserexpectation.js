import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CryptoCode } from "../../../common/cryptoCode";
import { UpdateUserExpectation } from "../../../action/UserAction/UserLayoutAction";
import { IconButton } from "@material-ui/core";


const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserExpectation: UpdateUserExpectation
        },
        dispatch
    );



class ReadUserExpectation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profileExpId: 0,
            userid: 0,
            yourSelf: "",
            expectation: "",
            createdDate: "",
            modifiedDate: "",
            user: ""
        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));


        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserExpectationDetail(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        profileExpId: UserData.profileExpId,
                        userid: UserData.userid,
                        yourSelf: UserData.yourSelf,
                        expectation: UserData.expectation,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            profileExpId: values.profileExpId,
            userid: UserData.id,
            yourSelf: values.yourSelf,
            expectation: values.expectation,
        }

        this.props.UpdateUserExpectation(passValue,
            (res) => {
                if (res.data.success) {
                    success("Expectation updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        profileExpId: this.state.profileExpId,
                        userid: this.state.userid,
                        yourSelf: this.state.yourSelf,
                        expectation: this.state.expectation,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div id="userexpectation" className="user-expect layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-outdent" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                                        Personal Expectation (What I am looking for)</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Your Self</label>
                                        <Field
                                            type="text"
                                            id="yourSelf"
                                            name="yourSelf"
                                            maxLength="200"
                                            placeholder="Your Self"
                                            component="textarea"
                                            rows="4"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.yourSelf = event.target.value)
                                                );
                                            }}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="col-lg-8 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Expectation</label>
                                        <Field
                                            type="text"
                                            id="expectation"
                                            name="expectation"
                                            maxLength="200"
                                            placeholder="Expectation"
                                            component="textarea"
                                            rows="4"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.expectation = event.target.value)
                                                );
                                            }}
                                            className="form-control"
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Expectation
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadUserExpectation);