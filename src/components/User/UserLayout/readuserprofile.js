import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as Yup from "yup";
import { UpdateUserProfile } from "../../../action/UserAction/UserLayoutAction";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({
    // UserProfileData: GetUserDetailByID(state.RegistrationReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserProfile: UpdateUserProfile
        },
        dispatch
    );


class ReadUserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            GenderData: [],
            ProfileCreatedData: [],
            profileUniqueId: "",
            userid: 0,
            firstName: "",
            middleName: "",
            lastName: "",
            motherName: "",
            gender: 22,
            mobileNo: "",
            emailId: "",
            password: "",
            profileCreatedBy: "",
            isActive: true,
            lastLogin: "",
            ipaddress: "",
        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var GenderData = SubMasterValue.SubMaster(MasterValue.Gender);
        var ProfileCreatedData = SubMasterValue.SubMaster(MasterValue.Profile_created_by);
        this.setState({
            GenderData: GenderData,
            ProfileCreatedData: ProfileCreatedData,
            profileUniqueId: UserData.profileUniqueId,
        });
        

        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserDetailByID(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        profileUniqueId: UserData.profileUniqueId,
                        userid: UserData.userid,
                        firstName: UserData.firstName,
                        middleName: UserData.middleName,
                        lastName: UserData.lastName,
                        motherName: UserData.motherName,
                        gender: UserData.gender,
                        mobileNo: UserData.mobileNo,
                        emailId: UserData.emailId,
                        profileCreatedBy: UserData.profileCreatedBy,
                        isActive: UserData.isActive,
                        lastLogin: UserData.lastLogin,
                        ipaddress: UserData.ipaddress,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _userProfileSchema = Yup.object().shape({
        emailId: Yup.string()
            .required('Email-ID is required!')
            .email('Invalid email address format!'),
        firstName: Yup.string()
            .required('First name is required!')
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed !"),
        lastName: Yup.string()
            .required('Last name is required!')
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed !"),
        middleName: Yup.string()
            .required('Middle name is required!')
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed !"),
        motherName: Yup.string()
            .required('Mother name is required!')
            .matches(/^[aA-zZ\s ]+$/, "Only alphabets are allowed !"),
        gender: Yup.string()
            .required('Gender is required!'),
        mobileNo: Yup.string()
            .required('Mobile no is required !')
            .matches(/^[0-9]+$/, "Must be only digits"),
        profileCreatedBy: Yup.string()
            .required('This field is required!'),
    });

    _handleSubmit = (values, { resetForm }, actions) => {
        var passValue = {
            profileUniqueId: values.profileUniqueId,
            userid: values.userid,
            firstName: values.firstName,
            middleName: values.middleName,
            lastName: values.lastName,
            motherName: values.motherName,
            gender: values.gender,
            mobileNo: values.mobileNo,
            emailId: values.emailId,
            profileCreatedBy: values.profileCreatedBy,
            ipaddress: values.ipaddress,
        }

        this.props.UpdateUserProfile(passValue,
            (res) => {
                if (res.data.success) {
                    const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
                    var UserPersonalDetail = {
                        firstName: values.firstName,
                        gender: values.gender,
                        id: UserData.id,
                        lastLogin: UserData.lastLogin,
                        lastName: values.lastName,
                        profileUniqueId: UserData.profileUniqueId,
                        roleId: UserData.roleId,
                        status: UserData.status,
                        token: UserData.token,
                        username: UserData.username
                    }
                    localStorage.setItem("userData", CryptoCode.encryption(JSON.stringify(UserPersonalDetail)));
                    success("Profile updated.", successNotification);
                    window.location.reload();
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        GenderData: this.state.GenderData,
                        ProfileCreatedData: this.state.ProfileCreatedData,
                        profileUniqueId: this.state.profileUniqueId,
                        userid: this.state.userid,
                        firstName: this.state.firstName,
                        middleName: this.state.middleName,
                        lastName: this.state.lastName,
                        motherName: this.state.motherName,
                        gender: this.state.gender,
                        mobileNo: this.state.mobileNo,
                        emailId: this.state.emailId,
                        profileCreatedBy: this.state.profileCreatedBy,
                        isActive: this.state.isActive,
                        lastLogin: this.state.lastLogin,
                        ipaddress: this.state.ipaddress,
                    }}
                    validationSchema={this._userProfileSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="userprofile">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-info-circle" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                     Your Profile ({values.profileUniqueId})</h5>
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="First">First Name</label>
                                        <Field
                                            type="text"
                                            id="firstName"
                                            name="firstName"
                                            maxLength="50"
                                            placeholder="First Name"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.firstName = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.firstName && errors.firstName
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="firstName"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Middle">Middle Name</label>
                                        <Field
                                            type="text"
                                            id="middleName"
                                            name="middleName"
                                            maxLength="50"
                                            placeholder="Middle name"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.middleName = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.middleName && errors.middleName
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="middleName"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Last">Last Name</label>

                                        <Field
                                            type="text"
                                            id="lastName"
                                            name="lastName"
                                            maxLength="50"
                                            placeholder="Last name"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.lastName = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.lastName && errors.lastName
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="lastName"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Mother">Mother Name</label>
                                        <Field
                                            type="text"
                                            id="motherName"
                                            name="motherName"
                                            maxLength="50"
                                            placeholder="Mother name"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.motherName = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.motherName && errors.motherName
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="motherName"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Mobile">Mobile No.</label>

                                        <Field
                                            type="text"
                                            id="mobileNo"
                                            name="mobileNo"
                                            maxLength="10"
                                            placeholder="Mobile No."
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.mobileNo = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.mobileNo && errors.mobileNo
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="mobileNo"
                                            className="text-danger"
                                        />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Email ID</label><br />
                                        <label className="medium" htmlFor="Mother">{values.emailId}</label>
                                        {/* <Field
                                            type="email"
                                            id="emailId"
                                            name="emailId"
                                            maxLength="10"
                                            placeholder="Email Id"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.emailId = event.target.value)
                                                );
                                            }}
                                            className={`form-control ${touched.emailId && errors.emailId
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        />
                                        <ErrorMessage
                                            component="div"
                                            name="emailId"
                                            className="text-danger"
                                        /> */}
                                    </div>

                                </div>  <hr />
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Profile">Profile Created By</label>
                                        <Field
                                            as="select"
                                            name="profileCreatedBy"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.profileCreatedBy = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Profile</option>
                                            {(values.ProfileCreatedData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                        <ErrorMessage
                                            component="div"
                                            name="profileCreatedBy"
                                            className="text-danger"
                                        />
                                    </div>

                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Gender">Gender</label>
                                        <Field
                                            as="select"
                                            name="gender"
                                            onChange={(event) => {
                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");

                                                setFieldValue(
                                                    (values.gender = event.target.value),
                                                );
                                            }}
                                            className={`form-control ${touched.gender && errors.gender
                                                ? "is-invalid"
                                                : ""
                                                }`}
                                        >
                                            <option value="">Select Gender</option>
                                            {(values.GenderData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                        <ErrorMessage
                                            component="div"
                                            name="gender"
                                            className="text-danger"
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Profile
                                        </SubmitButton>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadUserProfile);