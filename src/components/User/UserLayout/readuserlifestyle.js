import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { UpdateUserLifeStyle } from "../../../action/UserAction/UserLayoutAction";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserLifeStyle: UpdateUserLifeStyle
        },
        dispatch
    );


class ReadUserLifeStyle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DietData: [],
            SmokeData: [],
            DrinkData: [],
            lifeStyleId: 0,
            userid: 0,
            diet: 0,
            smoke: 0,
            drink: 0,
            createdDate: "",
            modifiedDate: "",
            user: ""
        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var DietData = SubMasterValue.SubMaster(MasterValue.Diet);
        var SmokeData = SubMasterValue.SubMaster(MasterValue.Smoke);
        var DrinkData = SubMasterValue.SubMaster(MasterValue.Drink);

        this.setState({
            DietData: DietData,
            SmokeData: SmokeData,
            DrinkData: DrinkData,
        });


        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserLifeStyle(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        userid: UserData.userid,
                        diet: UserData.diet,
                        smoke: UserData.smoke,
                        drink: UserData.drink,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            userid: UserData.id,
            diet: Number(values.diet),
            smoke: Number(values.smoke),
            drink: Number(values.drink),
        }

        this.props.UpdateUserLifeStyle(passValue,
            (res) => {
                if (res.data.success) {
                    success("Life Style updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        DietData: this.state.DietData,
                        SmokeData: this.state.SmokeData,
                        DrinkData: this.state.DrinkData,
                        userid: this.state.userid,
                        diet: this.state.diet,
                        smoke: this.state.smoke,
                        drink: this.state.drink,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="user-life-style layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-life-ring" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Life Style</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="diet">Diet</label>
                                        <Field
                                            as="select"
                                            name="diet"
                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.diet = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Diet</option>
                                            {(values.DietData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="smoke">Smoke</label>
                                        <Field
                                            as="select"
                                            name="smoke"
                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.smoke = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Smoke</option>
                                            {(values.SmokeData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="drink">Drink</label>
                                        <Field
                                            as="select"
                                            name="drink"
                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.drink = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Drink</option>
                                            {(values.DrinkData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Life Style
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadUserLifeStyle);