import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";

import { Formik, Form, Field, ErrorMessage } from "formik";

import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import { CurrencyWordFormat } from "../CommonComponent";
import { UpdateUserOccupation } from "../../../action/UserAction/UserLayoutAction";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserOccupation: UpdateUserOccupation
        },
        dispatch
    );


class ReadUserOccupation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            EducationData: [],
            BranchData: [],
            occupationId: 0,
            userid: 0,
            educationId: 0,
            branchId: 0,
            otherEdu: "",
            occupation: "",
            companyName: "",
            designation: "",
            monthlyIncome: "",
            companyAddress: "",
            contactNo: "",
        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var EducationData = SubMasterValue.SubMaster(MasterValue.Education);
        var BranchData = SubMasterValue.SubMaster(MasterValue.Branch);


        this.setState({
            EducationData: EducationData,
            BranchData: BranchData,
        });

        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserOccupation(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        occupationId: UserData.occupationId,
                        userid: UserData.userid,
                        educationId: UserData.educationId,
                        branchId: UserData.branchId,
                        otherEdu: UserData.otherEdu,
                        occupation: UserData.occupation,
                        companyName: UserData.companyName,
                        designation: UserData.designation,
                        monthlyIncome: UserData.monthlyIncome,
                        companyAddress: UserData.companyAddress,
                        contactNo: UserData.contactNo,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            occupationId: values.occupationId,
            userid: UserData.id,
            educationId: Number(values.educationId),
            branchId: Number(values.branchId),
            otherEdu: values.otherEdu,
            occupation: values.occupation,
            companyName: values.companyName,
            designation: values.designation,
            monthlyIncome: Number(values.monthlyIncome),
            companyAddress: values.companyAddress,
            contactNo: values.contactNo,
        }

        this.props.UpdateUserOccupation(passValue,
            (res) => {
                if (res.data.success) {
                    success("Occupation updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        EducationData: this.state.EducationData,
                        BranchData: this.state.BranchData,
                        occupationId: this.state.occupationId,
                        userid: this.state.userid,
                        educationId: this.state.educationId,
                        branchId: this.state.branchId,
                        otherEdu: this.state.otherEdu,
                        occupation: this.state.occupation,
                        companyName: this.state.companyName,
                        designation: this.state.designation,
                        monthlyIncome: this.state.monthlyIncome,
                        companyAddress: this.state.companyAddress,
                        contactNo: this.state.contactNo,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="useroccupation layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Education &amp; Occupation</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="educationId">Education</label>
                                        <Field
                                            as="select"
                                            name="educationId"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.educationId = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Education</option>
                                            {(values.EducationData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="branchId">Branch</label>
                                        <Field
                                            as="select"
                                            name="branchId"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.branchId = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Branch</option>
                                            {(values.BranchData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="OtherEducation">Other Education</label>
                                        <Field
                                            type="text"
                                            id="otherEdu"
                                            name="otherEdu"
                                            maxLength="50"
                                            placeholder="Other Education"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.otherEdu = event.target.value)
                                                );
                                            }}

                                        />

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="occupation">Occupation</label>
                                        <Field
                                            type="text"
                                            id="occupation"
                                            name="occupation"
                                            maxLength="45"
                                            placeholder="Occupation"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.occupation = event.target.value)
                                                );
                                            }}
                                        />

                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Company Name</label>
                                        <Field
                                            type="text"
                                            id="companyName"
                                            name="companyName"
                                            maxLength="45"
                                            placeholder="Company Name"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.companyName = event.target.value)
                                                );
                                            }}

                                        />

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="designation">Designation</label>
                                        <Field
                                            type="text"
                                            id="designation"
                                            name="designation"
                                            maxLength="45"
                                            placeholder="Designation"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.designation = event.target.value)
                                                );
                                            }}

                                        />

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="monthlyIncome">Monthly</label>
                                        <Field
                                            type="text"
                                            id="monthlyIncome"
                                            name="monthlyIncome"
                                            maxLength="10"
                                            minLength="4"
                                            placeholder="Monthly Income"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.monthlyIncome = event.target.value)
                                                );
                                            }}

                                        />
                                        <span className="currency-word">
                                            <CurrencyWordFormat
                                                state={values.monthlyIncome}
                                            ></CurrencyWordFormat>
                                        </span>

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Company Address</label>
                                        <Field
                                            type="text"
                                            id="companyAddress"
                                            name="companyAddress"
                                            maxLength="100"
                                            placeholder="Company Address"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.companyAddress = event.target.value)
                                                );
                                            }}

                                        />

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="contactNo">Contact Number</label>
                                        <Field
                                            type="text"
                                            id="contactNo"
                                            name="contactNo"
                                            maxLength="10"
                                            placeholder="Contact Number"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.contactNo = event.target.value)
                                                );
                                            }}

                                        />

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Occupation
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadUserOccupation);