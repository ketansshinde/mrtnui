import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { UpdateUserFamilyBackgeound } from "../../../action/UserAction/UserLayoutAction";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserFamilyBackgeound: UpdateUserFamilyBackgeound
        },
        dispatch
    );


class ReadUserFamilyBG extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Familyoccupation: [],
            userFamilyBackgroundId: 0,
            userid: 0,
            fatherOccupation: 0,
            motherOccupation: 0,
            noOfBrother: 0,
            noOfBrotherMarried: 0,
            noOfSister: 0,
            noOfSisterMarried: 0,
        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var Familyoccupation = SubMasterValue.SubMaster(MasterValue.Family_occupation);

        this.setState({
            Familyoccupation: Familyoccupation,
        });


        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserFamilyBackGround(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        userFamilyBackgroundId: UserData.userFamilyBackgroundId,
                        userid: UserData.userid,
                        fatherOccupation: UserData.fatherOccupation,
                        motherOccupation: UserData.motherOccupation,
                        noOfBrother: UserData.noOfBrother,
                        noOfBrotherMarried: UserData.noOfBrotherMarried,
                        noOfSister: UserData.noOfSister,
                        noOfSisterMarried: UserData.noOfSisterMarried,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            userFamilyBackgroundId: values.userFamilyBackgroundId,
            userid: UserData.id,
            fatherOccupation: Number(values.fatherOccupation),
            motherOccupation: Number(values.motherOccupation),
            noOfBrother: Number(values.noOfBrother) || 0,
            noOfBrotherMarried: Number(values.noOfBrotherMarried) || 0,
            noOfSister: Number(values.noOfSister) || 0,
            noOfSisterMarried: Number(values.noOfSisterMarried) || 0,
        }

        this.props.UpdateUserFamilyBackgeound(passValue,
            (res) => {
                if (res.data.success) {
                    success("Family updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        Familyoccupation: this.state.Familyoccupation,
                        patrikaId: this.state.patrikaId,
                        userFamilyBackgroundId: this.state.userFamilyBackgroundId,
                        userid: this.state.userid,
                        fatherOccupation: this.state.fatherOccupation,
                        motherOccupation: this.state.motherOccupation,
                        noOfBrother: this.state.noOfBrother,
                        noOfBrotherMarried: this.state.noOfBrotherMarried,
                        noOfSister: this.state.noOfSister,
                        noOfSisterMarried: this.state.noOfSisterMarried,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="user-background-family layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-heartbeat" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Family Background</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Father">Father</label>
                                        <Field
                                            as="select"
                                            name="fatherOccupation"
                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.fatherOccupation = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Occupation</option>
                                            {(values.Familyoccupation || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Mother">Mother</label>
                                        <Field
                                            as="select"
                                            name="motherOccupation"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.motherOccupation = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Occupation</option>
                                            {(values.Familyoccupation || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfBrother">No. of Bother</label>
                                        <Field
                                            type="number"
                                            id="noOfBrother"
                                            name="noOfBrother"
                                            max="5"
                                            min="0"
                                            placeholder="No. of Bother"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.noOfBrother = event.target.value)
                                                );
                                            }}
                                        />
                                    </div>
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfBrotherMarried">Marital Status of Brothers</label>
                                        <Field
                                            type="number"
                                            id="noOfBrotherMarried"
                                            name="noOfBrotherMarried"
                                            max="5"
                                            min="0"
                                            placeholder="Marital Status of Brothers"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.noOfBrotherMarried = event.target.value)
                                                );
                                            }}
                                        />
                                    </div>
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfSister">No. of Sister</label>
                                        <Field
                                            type="number"
                                            id="noOfSister"
                                            name="noOfSister"
                                            min="0"
                                            max="5"
                                            placeholder="No. of Sister"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.noOfSister = event.target.value)
                                                );
                                            }}
                                        />
                                    </div>
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfSisterMarried">Marital Status of Sisters</label>
                                        <Field
                                            type="number"
                                            id="noOfSisterMarried"
                                            name="noOfSisterMarried"
                                            max="5"
                                            min="0"
                                            placeholder="Marital Status of Sisters"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.noOfSisterMarried = event.target.value)
                                                );
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Family
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadUserFamilyBG);