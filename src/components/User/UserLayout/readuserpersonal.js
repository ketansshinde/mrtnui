import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import TextField from '@material-ui/core/TextField';
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import { UpdateUserPersonal } from "../../../action/UserAction/UserLayoutAction";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            UpdateUserPersonal: UpdateUserPersonal
        },
        dispatch
    );


class ReadUserPersonal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            GanData: [],
            NadiData: [],
            NakashatraData: [],
            RasData: [],
            CharanData: [],
            MaritalStatusData: [],
            BloodGroupData: [],
            BodyTypeData: [],
            ComplexionData: [],
            SpectableData: [],
            AnyDisablityData: [],

            profileId: 0,
            userid: 0,
            dob: "",
            birthTime: "",
            age: "",
            birthPlace: "",
            placeOrigin: "",
            gan: 0,
            nadi: 0,
            nakshtra: 0,
            ras: 0,
            charan: 0,
            bloodGroup: 0,
            maritalStatus: 0,
            hightFeet: 0,
            hightInch: 0,
            bodyType: 0,
            complexion: 0,
            spectacle: 0,
            anyDisability: 0,
        };
    }
    componentDidMount() {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var GanData = SubMasterValue.SubMaster(MasterValue.Gan);
        var NadiData = SubMasterValue.SubMaster(MasterValue.Nadi);
        var NakashatraData = SubMasterValue.SubMaster(MasterValue.Nakshatra);
        var RasData = SubMasterValue.SubMaster(MasterValue.Ras);
        var CharanData = SubMasterValue.SubMaster(MasterValue.Charan);
        var MaritalStatusData = SubMasterValue.SubMaster(MasterValue.Marital_Status);
        var BloodGroupData = SubMasterValue.SubMaster(MasterValue.Blood_Group);
        var BodyTypeData = SubMasterValue.SubMaster(MasterValue.Body_Type);
        var ComplexionData = SubMasterValue.SubMaster(MasterValue.Complexion);
        var SpectableData = SubMasterValue.SubMaster(MasterValue.Spectacle);
        var AnyDisablityData = SubMasterValue.SubMaster(MasterValue.Any_Disability);

        this.setState({
            GanData: GanData,
            NadiData: NadiData,
            NakashatraData: NakashatraData,
            RasData: RasData,
            CharanData: CharanData,
            MaritalStatusData: MaritalStatusData,
            BloodGroupData: BloodGroupData,
            BodyTypeData: BodyTypeData,
            ComplexionData: ComplexionData,
            SpectableData: SpectableData,
            AnyDisablityData: AnyDisablityData,
        });

        var passValue = {
            userid: UserData.id,
            profileuniqueid: UserData.profileUniqueId
        };
        debugger;
        UserLayoutService.GetUserPersonalById(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    var date = moment(UserData.dob).format('YYYY-MM-DD');
                    //var DateDetail = new Date(date);

                    this.setState({
                        profileId: UserData.profileId,
                        userid: UserData.userid,
                        dob: date,
                        birthTime: UserData.birthTime,
                        age: UserData.age,
                        birthPlace: UserData.birthPlace,
                        placeOrigin: UserData.placeOrigin,
                        gan: UserData.gan,
                        nadi: UserData.nadi,
                        nakshtra: UserData.nakshtra,
                        ras: UserData.ras,
                        charan: UserData.charan,
                        bloodGroup: UserData.bloodGroup,
                        maritalStatus: UserData.maritalStatus,
                        hightFeet: UserData.hightFeet,
                        hightInch: UserData.hightInch,
                        bodyType: UserData.bodyType,
                        complexion: UserData.complexion,
                        spectacle: UserData.spectacle,
                        anyDisability: UserData.anyDisability,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    
        var years = moment().diff(values.dob, 'years', false);
        if (Number(UserData.gender) === 22 && years < 21) {
            warning("Age must be 21", warningNotification);
            return;
        }
        else if (Number(UserData.gender) === 23 && years < 18) {
            warning("Age must be 18", warningNotification);
            return;
        }

        var passValue = {
            profileId: values.profileId,
            userid: UserData.id,
            dob: values.dob,
            birthTime: values.birthTime,
            age: values.age,
            birthPlace: values.birthPlace,
            placeOrigin: values.placeOrigin,
            gan: Number(values.gan),
            nadi: Number(values.nadi),
            nakshtra: Number(values.nakshtra),
            ras: Number(values.ras),
            charan: Number(values.charan),
            bloodGroup: Number(values.bloodGroup),
            maritalStatus: Number(values.maritalStatus),
            hightFeet: Number(values.hightFeet),
            hightInch: Number(values.hightInch),
            bodyType: Number(values.bodyType),
            complexion: Number(values.complexion),
            spectacle: Number(values.spectacle),
            anyDisability: Number(values.anyDisability),
        }

        this.props.UpdateUserPersonal(passValue,
            (res) => {
                if (res.data.success) {
                    success("Profile updated.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        GanData: this.state.GanData,
                        NadiData: this.state.NadiData,
                        NakashatraData: this.state.NakashatraData,
                        RasData: this.state.RasData,
                        CharanData: this.state.CharanData,
                        MaritalStatusData: this.state.MaritalStatusData,
                        BloodGroupData: this.state.BloodGroupData,
                        BodyTypeData: this.state.BodyTypeData,
                        ComplexionData: this.state.ComplexionData,
                        SpectableData: this.state.SpectableData,
                        AnyDisablityData: this.state.AnyDisablityData,

                        profileId: this.state.profileId,
                        userid: this.state.userid,
                        dob: this.state.dob,
                        birthTime: this.state.birthTime,
                        age: this.state.age,
                        birthPlace: this.state.birthPlace,
                        placeOrigin: this.state.placeOrigin,
                        gan: this.state.gan,
                        nadi: this.state.nadi,
                        nakshtra: this.state.nakshtra,
                        ras: this.state.ras,
                        charan: this.state.charan,
                        bloodGroup: this.state.bloodGroup,
                        maritalStatus: this.state.maritalStatus,
                        hightFeet: this.state.hightFeet,
                        hightInch: this.state.hightInch,
                        bodyType: this.state.bodyType,
                        complexion: this.state.complexion,
                        spectacle: this.state.spectacle,
                        anyDisability: this.state.anyDisability,
                    }}
                    // validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="userpersonal layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right"> <IconButton color="inherit" size="small">
                                        <AccountCircleIcon></AccountCircleIcon>
                                    </IconButton>
                                    </span>
                                    Horoscope</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Date Of Birth</label>

                                        <Field
                                            type="date"
                                            id="dob"
                                            name="dob"
                                            //component={TextField}
                                            //defaultValue="2000-05-24"
                                            // timezone={Intl.DateTimeFormat().resolvedOptions().timeZone}
                                            inputlabelpops={{
                                                shrink: true,
                                            }}
                                            placeholder="Date Of Birth"

                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.dob = event.target.value)
                                                );
                                            }}
                                            className="form-control"
                                        />
                                        <span className="currency-word">
                                            Date : MM/DD/YYYY
                                        </span>

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Birth Time</label>
                                        <input type="time"
                                            className="form-control"
                                            placeholder="Birth Time"
                                            //Component={TextField}
                                            //defaultValue="07:30"
                                            inputlabelprops={{
                                                shrink: true,
                                            }}
                                            // InputProps={{
                                            //     step: 300, // 5 min
                                            // }}
                                            name="birthTime"
                                            value={values.birthTime}
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.birthTime = event.target.value)
                                                );
                                            }}
                                        />

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="birthPlace">Birth Place</label>
                                        <Field
                                            type="text"
                                            id="birthPlace"
                                            name="birthPlace"
                                            max="45"
                                            placeholder="Birth Place"
                                            className="form-control"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.birthPlace = event.target.value)
                                                );
                                            }}
                                        />
                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Gan</label>
                                        <Field
                                            as="select"
                                            name="gan"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.gan = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Gan</option>
                                            {(values.GanData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Nadi</label>
                                        <Field
                                            as="select"
                                            name="nadi"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.nadi = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Nadi</option>
                                            {(values.NadiData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Nakashatra</label>
                                        <Field
                                            as="select"
                                            name="nakshtra"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.nakshtra = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Nakashatra</option>
                                            {(values.NakashatraData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Ras</label>
                                        <Field
                                            as="select"
                                            name="ras"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.ras = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Ras</option>
                                            {(values.RasData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Charan</label>
                                        <Field
                                            as="select"
                                            name="charan"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.charan = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Charan</option>
                                            {(values.CharanData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Marital Status</label>
                                        <Field
                                            as="select"
                                            name="maritalStatus"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.maritalStatus = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Marital Status</option>
                                            {(values.MaritalStatusData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Hight-Feet</label>
                                        {/* <input type="Number" max="1" min="8" className="form-control" placeholder="Hight-Feet" /> */}
                                        <Field
                                            type="number"
                                            id="hightFeet"
                                            name="hightFeet"
                                            max="8"
                                            min="3"
                                            placeholder="Hight-Feet"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.hightFeet = event.target.value)
                                                );
                                            }}
                                            className="form-control"
                                        />

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Hight-Inch</label>

                                        <Field
                                            type="number"
                                            id="hightInch"
                                            name="hightInch"
                                            max="11" min="0"
                                            placeholder="Hight-Inch"
                                            onChange={(event) => {
                                                setFieldValue(
                                                    (values.hightInch = event.target.value)
                                                );
                                            }}
                                            className="form-control"
                                        />


                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Blood Group</label>
                                        <Field
                                            as="select"
                                            name="bloodGroup"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.bloodGroup = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Blood Group</option>
                                            {(values.BloodGroupData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>

                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="bodyType">Body Type</label>
                                        <Field
                                            as="select"
                                            name="bodyType"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.bodyType = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Body Type</option>
                                            {(values.BodyTypeData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Complexion</label>
                                        <Field
                                            as="select"
                                            name="complexion"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.complexion = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Complexion</option>
                                            {(values.ComplexionData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="spectacle">Spectable</label>
                                        <Field
                                            as="select"
                                            name="spectacle"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.spectacle = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Spectable</option>
                                            {(values.SpectableData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="anyDisability">Any Disablity</label>
                                        <Field
                                            as="select"
                                            name="anyDisability"

                                            className="form-control"
                                            onChange={(event) => {

                                                const relationship = event.target.options[
                                                    event.target.options.selectedIndex
                                                ].getAttribute("data-key");
                                                setFieldValue(
                                                    (values.anyDisability = event.target.value),
                                                );
                                            }}
                                        >
                                            <option value="">Select Any Disablity</option>
                                            {(values.AnyDisablityData || []).map(
                                                (_subMasterData) => (
                                                    <option
                                                        data-key={_subMasterData.subMasterId}
                                                        key={_subMasterData.subMasterId}
                                                        value={JSON.stringify(
                                                            _subMasterData.subMasterId
                                                        )}
                                                    >
                                                        {_subMasterData.name}
                                                    </option>
                                                )
                                            )}
                                        </Field>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-12 mb-2 text_align_right">
                                        <SubmitButton
                                            type="submit"
                                            variant="contained"
                                        >
                                            Save Information
                                                    </SubmitButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReadUserPersonal);
