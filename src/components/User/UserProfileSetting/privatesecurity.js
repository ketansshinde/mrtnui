import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";

import { Formik, Form, Field, ErrorMessage } from "formik";
import SettingLinks from "./settinglinks";
import Checkbox from '@material-ui/core/Checkbox';
import SecurityIcon from '@material-ui/icons/Security';
import { IconButton } from "@material-ui/core";

class PrivateSecurity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <Formik
                        enableReinitialize
                        initialValues={{


                        }}
                        validationSchema={this._userAccountSchema}
                        onSubmit={this._handleSubmit}
                    >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <SettingLinks {...this.props}></SettingLinks>
                                        </div>
                                        <div className="col-lg-9">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h5 className="card-title card-title-set text_align_left">
                                                        <IconButton color="inherit" size="small">
                                                            <SecurityIcon></SecurityIcon>
                                                        </IconButton>
                                                        {" "}Private Security</h5>
                                                    <div className="row padding-top">
                                                        <div className="col-lg-12 mb-2 text_align_left">
                                                            <div><b>Photo Privacy</b></div>
                                                            {/* onChange={handleChange}  checked={gilad}*/}
                                                            <div><Checkbox name="gilad" />Visible to all (Matches).</div>
                                                            <div><Checkbox name="gilad" />Visibility only to paid members.</div>
                                                            <div><Checkbox name="gilad" />Visible only to those you have accepted or expressed interest in.</div>
                                                            <div><Checkbox name="gilad" />Please note this will reduce the number of interests that you will receive.</div>
                                                            <div className="padding-top" >
                                                                <SubmitButton
                                                                    type="submit"
                                                                    variant="contained"
                                                                >
                                                                    Save Changes
                                                                </SubmitButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="dropdown-divider"></div>
                                                    <div className="row padding-top" style={{ lineHeight: "2.1" }}>
                                                        <div className="col-lg-12 mb-2 text_align_left">
                                                            <div><b>Temporary InActive Account</b></div>
                                                            {/* onChange={handleChange}  checked={gilad}*/}
                                                            <div className="padding-top"><b>&gt; </b> Your profile Temporary inactive when you click on button.</div>
                                                            <div><b>&gt; </b> No one can see your profile.</div>
                                                            <div><b>&gt; </b> If you are not going use your profile account in 3 month. Then community will remove you account.</div>
                                                            <div className="padding-top" >
                                                                <SubmitButton
                                                                    type="submit"
                                                                    variant="contained"
                                                                >
                                                                    InActive
                                                                </SubmitButton>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="dropdown-divider"></div>
                                                    <div className="row padding-top" style={{ lineHeight: "2.1" }}>
                                                        <div className="col-lg-12 mb-2 text_align_left">
                                                            <div><b>Delete My Profile Account</b></div>
                                                            <div className="padding-top"><b>&gt; </b> If you are not going use your profile account in 3 month. Then community will remove you account.</div>
                                                            <div><b>&gt; </b> If you click delete account , then all data will remove in community.</div>
                                                            <div><b>&gt; </b> Like eg. Profile Photo, Gallery Photo, Goverment ID proof  and etc.</div>
                                                            <div className="padding-top" >
                                                                <SubmitButton
                                                                    type="submit"
                                                                    variant="contained"
                                                                >
                                                                    Delete Account
                                                             </SubmitButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </Form>
                    )}
                    </Formik>
                </div>
            </React.Fragment >
        );
    }
}
export default PrivateSecurity;
