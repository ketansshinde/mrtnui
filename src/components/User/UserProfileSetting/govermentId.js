import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";

import { Formik, Form, Field, ErrorMessage } from "formik";
import SettingLinks from "./settinglinks";
import adharcard from "../../../assets/images/adharcard.png";
import drivinglicense from "../../../assets/images/drivinglicense.png";
import pancard from "../../../assets/images/pancard.PNG";
import { IconButton } from "@material-ui/core";
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import PanCard from "../GovermentID/PanCard/pancard";
import AdharCard from "../GovermentID/AdharCard/adharcard";
import DrivingLicense from "../GovermentID/DrivingLicense/drivinglicense";

class GovermentId extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <Formik
                        enableReinitialize
                        initialValues={{


                        }}
                        validationSchema={this._userAccountSchema}
                        onSubmit={this._handleSubmit}
                    >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <SettingLinks {...this.props}></SettingLinks>
                                        </div>
                                        <div className="col-lg-9">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h5 className="card-title card-title-set text_align_left">
                                                        <IconButton color="inherit" size="small">
                                                            <VerifiedUserIcon></VerifiedUserIcon>
                                                        </IconButton>
                                                        {" "} Government ID Proof Verification</h5>
                                                    <PanCard {...this.props}></PanCard>
                                                    <div className="dropdown-divider"></div>
                                                    <AdharCard {...this.props}></AdharCard>
                                                    <div className="dropdown-divider"></div>
                                                    <DrivingLicense {...this.props}></DrivingLicense>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </Form>
                    )}
                    </Formik>
                </div>
            </React.Fragment >
        );
    }
}
export default GovermentId;
