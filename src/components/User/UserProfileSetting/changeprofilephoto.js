import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Formik, Form, Field, ErrorMessage } from "formik";
import './ChangeProfileSetting.css';
import Axios from "axios";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CryptoCode } from "../../../common/cryptoCode";
import { SaveProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { UserSettingService } from "../../../services/UserService/UserSettingService";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import { Global_var } from "../../../global/global_var";
import { IconButton } from "@material-ui/core";
import { ValidPhoto } from "../CommonComponent";
import FaceIcon from '@material-ui/icons/Face';

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            SaveProfilePhoto: SaveProfilePhoto
        },
        dispatch
    );

class ChangeProfilePhoto extends Component {
    constructor(props) {

        super(props);
        this.state = {
            src: null,
            crop: {
                unit: '%',
                width: 50,
                aspect: 1 / 1,
            },
            croppedIma: "",
            croppedImageUrl: "",
            imageDetail: "",
            profilePhoto: "",
            Userid: ""
        };
    }

    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        this.setState({ Userid: UserData.id, name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        this.setState({ profilePhoto: userloginDetail.documentName });
        //const profilePhotoData = JSON.parse(CryptoCode.decryption(localStorage.getItem("ProfilePhoto")));
        // 
        // if (profilePhotoData.length !== 0) {
        //     this.setState({ profilePhoto: profilePhotoData[0].documentType + "," + profilePhotoData[0].documentBase });
        // }
    }

    onSelectFile = e => {

        this.setState({ imageDetail: e.target.files[0] });
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);

    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({ croppedImageUrl });
            this.setState({ croppedIma: croppedImageUrl });
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.setState({ blobUrl: this.fileUrl })
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }


    _handleSubmit = (values, { resetForm }, actions) => {

        Axios({
            method: "get",
            url: values.croppedIma, // blob url eg. blob:http://127.0.0.1:8000/e89c5d87-a634-4540-974c-30dc476825cc
            responseType: "blob",
        }).then(function (response) {

            var reader = new FileReader();
            reader.readAsDataURL(response.data);
            reader.onloadend = function () {
                var base64data = reader.result;

                const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
                var imageDetail = values.imageDetail;
                var passValue = {
                    documentId: 0,
                    userid: UserData.id,
                    documentName: "temp",
                    documentType: imageDetail.type,
                    documentBase: base64data,
                };
                UserSettingService.SaveProfilePhotoService(passValue,
                    (res) => {
                        if (res.data.success) {
                            success("Profile Photo updated successfully", successNotification);
                            window.location.reload();
                        }
                        else {
                            error("Something wents worng.", errorNotification);
                        }
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            }
        });
    }

    render() {
        const { crop } = this.state;

        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        src: this.state.src,
                        crop: this.state.crop,
                        croppedIma: this.state.croppedIma,
                        crop: this.state.crop,
                        croppedImageUrl: this.state.croppedImageUrl,
                        imageDetail: this.state.imageDetail,
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid
                    }}
                    validationSchema={this._countrySchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>

                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title card-title-set text_align_left">
                                        <IconButton color="inherit" size="small">
                                            <FaceIcon></FaceIcon>
                                        </IconButton>
                                        {" "}
                                        Profile Photo</h5>

                                    <ValidPhoto {...this.props}></ValidPhoto>

                                    <div className="dropdown-divider"></div>
                                    <div className="row padding-top">
                                        <div className="col-lg-12 mb-2 text_align_left">
                                            <div><b>Set Profile Photo</b></div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12 mb-2 ">
                                            <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.profilePhoto : (this.state.gender == 22 ? male : female)} className="profile-image" alt="logo" width="250" height="250"></img>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-lg-12 mb-2">
                                            <input type="file" accept="image/*" onChange={this.onSelectFile} />
                                            {"    "}
                                            <SubmitButton
                                                type="submit"
                                                variant="contained"
                                            >
                                                Update Profile Photo
                                          </SubmitButton>

                                        </div>

                                        <div className="col-lg-8 mb-2">
                                            {values.src && (
                                                <ReactCrop
                                                    src={values.src}
                                                    crop={crop}
                                                    ruleOfThirds
                                                    onImageLoaded={this.onImageLoaded}
                                                    onComplete={this.onCropComplete}
                                                    onChange={this.onCropChange}
                                                />
                                            )}
                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            {values.croppedImageUrl && (
                                                <img alt="Crop" width="300" height="300" src={values.croppedImageUrl} />
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Form>
                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangeProfilePhoto);
