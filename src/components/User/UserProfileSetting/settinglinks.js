import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import MessageIcon from '@material-ui/icons/Message';
import { IconButton } from "@material-ui/core";
import EmailIcon from '@material-ui/icons/Email';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import SettingsIcon from '@material-ui/icons/Settings';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PersonIcon from '@material-ui/icons/Person';
import PaymentIcon from '@material-ui/icons/Payment';
import FaceIcon from '@material-ui/icons/Face';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import SecurityIcon from '@material-ui/icons/Security';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';

class SettingLinks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
    }

    _changePassword = () => {
        this.props.history.push("/changePassword");
    }

    _changeProfilePhoto = () => {
        this.props.history.push("/changeprofilephoto");
    }

    _privateSecurity = () => {
        this.props.history.push("/privatesecurity");
    }

    _govermentidproof = () => {
        this.props.history.push("/govermentidproof");
    }

    _usergallery = () => {
        this.props.history.push("/usergallery");
    }
    render() {
        return (
            <React.Fragment>
                <div>
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title card-title-set text_align_center">{this.state.name}</h5>
                            <div className="layout-margin hyperlink">
                                <span className="padding-right">
                                    <IconButton color="inherit" size="small">
                                        <FaceIcon></FaceIcon>
                                    </IconButton>
                                </span>
                                <span className="text_align_left" onClick={this._changeProfilePhoto}>View Profile Photo</span>
                            </div>
                            <div className="layout-margin hyperlink">
                                <span className="padding-right">
                                    <IconButton color="inherit" size="small">
                                        <AssignmentIndIcon></AssignmentIndIcon>
                                    </IconButton>
                                </span>
                                <span className="text_align_left" onClick={this._usergallery}>View Photo Gallery</span>
                            </div>
                            <hr />
                            <div className="layout-margin hyperlink" >
                                <span className="padding-right">
                                    <IconButton color="inherit" size="small">
                                        <VpnKeyIcon></VpnKeyIcon>
                                    </IconButton>
                                </span>
                                <span className="text_align_left" onClick={this._changePassword}>Change Password</span>
                            </div>
                            <div className="layout-margin hyperlink">
                                <span className="padding-right">
                                    <IconButton color="inherit" size="small">
                                        <SecurityIcon></SecurityIcon>
                                    </IconButton>
                                </span>
                                <span className="text_align_left " onClick={this._privateSecurity}>Private Security Settings</span>
                            </div>
                            <div className="layout-margin hyperlink">
                                <span className="padding-right">
                                    <IconButton color="inherit" size="small">
                                        <VerifiedUserIcon></VerifiedUserIcon>
                                    </IconButton>
                                </span>
                                <span className="text_align_left" onClick={this._govermentidproof}>Geverment Proof ID</span>
                            </div>
                        </div>
                    </div>
                </div >
            </React.Fragment >
        );
    }
}
export default SettingLinks;
