import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import * as Yup from "yup";
import { Formik, Form, Field, ErrorMessage } from "formik";
import SettingLinks from "./settinglinks";
import { IconButton } from "@material-ui/core";
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ChangePasswordUser } from "../../../action/UserAction/UserSettingAction";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            ChangePasswordUser: ChangePasswordUser
        },
        dispatch
    );

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
    }

    _userAccountSchema = Yup.object().shape({
        existingPassword: Yup.string()
            .required('Existing password is required')
            .min(8, 'Password must be 8 characters at minimum')
            .max(16, 'Password must be 16 characters at maximum')
            .matches(/[a-z]/, 'At least one lowercase char')
            .matches(/[A-Z]/, 'At least one uppercase char')
            .matches(/(?=.*\d)/, 'Must contain a number.')
            .matches(/[!@#$%^&*(),.?":{}|<>]/, 'Must contain special character')
            .matches(/^\S*$/, 'Password should not allow space'),
        password: Yup.string()
            .required('New password is required')
            .min(8, 'Password must be 8 characters at minimum')
            .max(16, 'Password must be 16 characters at maximum')
            .matches(/[a-z]/, 'At least one lowercase char')
            .matches(/[A-Z]/, 'At least one uppercase char')
            .matches(/(?=.*\d)/, 'Must contain a number.')
            .matches(/[!@#$%^&*(),.?":{}|<>]/, 'Must contain special character')
            .matches(/^\S*$/, 'Password should not allow space'),
        confirmPassword: Yup.string()
            .required('Confirm password is required !')
            .min(8, 'Password must be 8 characters at minimum')
            .oneOf([Yup.ref('password'), null], 'Passwords must match'),
    });

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {

            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
            password: values.password,
            existingPassword: values.existingPassword,

        }
        
        this.props.ChangePasswordUser(passValue,
            (res) => {
                
                if (res.data.success) {
                    success("Password updated successfully.", successNotification);
                    this.props.history.push("/login");
                }
                else {
                    error(res.data.errorlog, errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <Formik
                        enableReinitialize
                        initialValues={{
                            existingPassword: "",
                            password: "",
                            confirmPassword: "",
                        }}
                        validationSchema={this._userAccountSchema}
                        onSubmit={this._handleSubmit}
                    >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <SettingLinks {...this.props}></SettingLinks>
                                        </div>
                                        <div className="col-lg-9">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h5 className="card-title card-title-set text_align_left">
                                                        <IconButton color="inherit" size="small">
                                                            <VpnKeyIcon></VpnKeyIcon>
                                                        </IconButton>
                                                        {" "}Change Password</h5>
                                                    <div className="row">
                                                        <div className="col-lg-6 mb-2">
                                                            <label className="medium" htmlFor="First">Existing Password</label>
                                                            <Field
                                                                type="password"
                                                                id="existingPassword"
                                                                name="existingPassword"
                                                                maxLength="50"
                                                                placeholder="Password e.g - Abc@12345"
                                                                onChange={(event) => {
                                                                    setFieldValue(
                                                                        (values.existingPassword = event.target.value)
                                                                    );
                                                                }}
                                                                className={`form-control ${touched.existingPassword && errors.existingPassword
                                                                    ? "is-invalid"
                                                                    : ""
                                                                    }`}
                                                            />
                                                            <ErrorMessage
                                                                component="div"
                                                                name="existingPassword"
                                                                className="text-danger"
                                                            />
                                                        </div>
                                                        <div className="col-lg-6 mb-2"></div>
                                                        <div className="col-lg-6 mb-2">
                                                            <label className="medium" htmlFor="password">New Password</label>

                                                            <Field
                                                                type="password"
                                                                id="password"
                                                                name="password"
                                                                maxLength="50"
                                                                placeholder="Password e.g - Abc@12345"
                                                                onChange={(event) => {
                                                                    setFieldValue(
                                                                        (values.password = event.target.value)
                                                                    );
                                                                }}
                                                                className={`form-control ${touched.password && errors.password
                                                                    ? "is-invalid"
                                                                    : ""
                                                                    }`}
                                                            />
                                                            <ErrorMessage
                                                                component="div"
                                                                name="password"
                                                                className="text-danger"
                                                            />
                                                        </div>
                                                        <div className="col-lg-6 mb-2"></div>
                                                        <div className="col-lg-6 mb-2">
                                                            <label className="medium" htmlFor="confirmPassword">Confirm Password</label>
                                                            <Field
                                                                type="password"
                                                                id="confirmPassword"
                                                                name="confirmPassword"
                                                                maxLength="50"
                                                                placeholder="Re-Enter Password"
                                                                onChange={(event) => {
                                                                    setFieldValue(
                                                                        (values.confirmPassword = event.target.value)
                                                                    );
                                                                }}
                                                                className={`form-control ${touched.confirmPassword && errors.confirmPassword
                                                                    ? "is-invalid"
                                                                    : ""
                                                                    }`}
                                                            />
                                                            <ErrorMessage
                                                                component="div"
                                                                name="confirmPassword"
                                                                className="text-danger"
                                                            />
                                                        </div>
                                                        <div className="col-lg-6 mb-2"></div>
                                                        <div className="col-lg-12 mb-2">

                                                            <SubmitButton
                                                                type="submit"
                                                                variant="contained"
                                                            >
                                                                Update Password
                                                            </SubmitButton>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>



                        </Form>
                    )}
                    </Formik>
                </div>
            </React.Fragment >
        );
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);