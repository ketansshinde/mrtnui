import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";

import { Formik, Form, Field, ErrorMessage } from "formik";
import SettingLinks from "./settinglinks";
import ChangeProfilePhoto from "./changeprofilephoto";

class UserProfileSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            profileBase64string: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
    }
    getFiles(files) {
        
        this.setState({ files: files, profileBase64string: files.base64 })
    }
    render() {
        return (
            <React.Fragment>
                <div>
                    <Formik
                        enableReinitialize
                        initialValues={{
                            profileBase64string: this.state.profileBase64string
                        }}
                        validationSchema={this._userAccountSchema}
                        onSubmit={this._handleSubmit}
                    >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <SettingLinks {...this.props}></SettingLinks>
                                        </div>
                                        <div className="col-lg-9">
                                            <ChangeProfilePhoto {...this.props}></ChangeProfilePhoto>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </Form>
                    )}
                    </Formik>
                </div>
            </React.Fragment>
        );
    }
}
export default UserProfileSetting;
