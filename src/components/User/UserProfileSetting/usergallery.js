import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Formik, Form, Field, ErrorMessage } from "formik";
import './ChangeProfileSetting.css';
import Axios from "axios";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CryptoCode } from "../../../common/cryptoCode";
import { SaveProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { UserSettingService } from "../../../services/UserService/UserSettingService";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import { Global_var } from "../../../global/global_var";
import { IconButton } from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';
import SettingLinks from "./settinglinks";
import { ValidPhoto } from "../CommonComponent";
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import { FetchGalleryPhoto, DeleteGalleryPhoto } from "../../../action/UserAction/UserSettingAction";
import { GetGalleryPhotoData } from "../../../reducer/UserReducer/UserSettingReducer";

const mapStateToProps = (state) => ({
    GalleryPhotoData: GetGalleryPhotoData(state.UserSettingReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            FetchGalleryPhoto: FetchGalleryPhoto,
            DeleteGalleryPhoto: DeleteGalleryPhoto
        },
        dispatch
    );

class UserGallery extends Component {
    constructor(props) {

        super(props);
        this.state = {
            src: null,
            crop: {
                unit: '%',
                width: 50,
                aspect: 1 / 1,
            },
            croppedIma: "",
            croppedImageUrl: "",
            imageDetail: "",
            profilePhoto: "",
            Userid: "",
            galleryPhoto: []
        };
    }

    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        this.setState({ Userid: UserData.id, name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        this.setState({ profilePhoto: userloginDetail.documentName });
        this._loadDataTable();
    }

    _loadDataTable() {
        
        const { FetchGalleryPhoto } = this.props;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        }
        this.props.FetchGalleryPhoto(passValue,
            (res) => {
                
                if (res.success) {
                    this.setState({ galleryPhoto: res.responseList });
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }





    onSelectFile = e => {

        this.setState({ imageDetail: e.target.files[0] });
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);

    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({ croppedImageUrl });
            this.setState({ croppedIma: croppedImageUrl });
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.setState({ blobUrl: this.fileUrl })
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }


    _handleSubmit = (values, { resetForm }, actions) => {
        if ((values.galleryPhoto).length >= 4) {
            warning("You can upload only 4 photo in gallery", warningNotification);
            return;
        }
        var dd = this.props;
        Axios({
            method: "get",
            url: values.croppedIma, // blob url eg. blob:http://127.0.0.1:8000/e89c5d87-a634-4540-974c-30dc476825cc
            responseType: "blob",
        }).then(function (response) {

            var reader = new FileReader();
            reader.readAsDataURL(response.data);
            reader.onloadend = function () {
                var base64data = reader.result;

                const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
                var imageDetail = values.imageDetail;
                var passValue = {
                    documentId: 0,
                    userid: UserData.id,
                    documentName: "Gallery",
                    documentType: imageDetail.type,
                    documentBase: base64data,
                };
                UserSettingService.SaveProfilePhotoGalleryService(passValue,
                    (res) => {
                        if (res.data.success) {
                            success("Gallery photo uploaded successfully", successNotification);
                            this._loadDataTable();  
                        }
                        else {
                            error("Something wents worng.", errorNotification);
                        }
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            }
        });
        window.location.reload();
    }

    // ---------------------- Romove Gallery Photo------------------------------------------

    _removeGalleryPhoto(galleryPhotoId) {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            Userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
            DocumentGalId: galleryPhotoId
        }
        this.props.DeleteGalleryPhoto(passValue,
            (res) => {
                if (res.data.success) {
                    success("Gallery photo deleted successfully", successNotification);
                    this._loadDataTable();
                    //window.location.reload();
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        const { crop } = this.state;

        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        src: this.state.src,
                        crop: this.state.crop,
                        croppedIma: this.state.croppedIma,
                        crop: this.state.crop,
                        croppedImageUrl: this.state.croppedImageUrl,
                        imageDetail: this.state.imageDetail,
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid,
                        galleryPhoto: this.state.galleryPhoto
                    }}
                    validationSchema={this._countrySchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <SettingLinks {...this.props}></SettingLinks>
                                        </div>
                                        <div className="col-lg-9">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h5 className="card-title card-title-set text_align_left">
                                                        <IconButton color="inherit" size="small">
                                                            <AssignmentIndIcon></AssignmentIndIcon>
                                                        </IconButton>
                                                        {" "}
                                                                User Photo Gallery
                                                                </h5>

                                                    <ValidPhoto {...this.props}></ValidPhoto>

                                                    <div className="dropdown-divider"></div>
                                                    <div className="row padding-top">
                                                        <div className="col-lg-12 mb-2 text_align_left">
                                                            <div><b>
                                                                Gallery Photo</b></div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        {(values.galleryPhoto || []).map(
                                                            (item) => {
                                                                
                                                                return (
                                                                    <div className="col-lg-3 text_align_center">
                                                                        <img src={values.documentName !== null ? Global_var.URL_USER_GALLERY + item.documentName : (this.state.gender == 22 ? male : female)} className="gallery-image" alt="logo" width="230" height="230"></img>
                                                                        <div>
                                                                            <span style={{ cursor: "pointer" }} onClick={(event) => {
                                                                                
                                                                                var documentGalId = item.documentGalId;
                                                                                this._removeGalleryPhoto(documentGalId);
                                                                            }}>
                                                                                <IconButton color="inherit" size="small">
                                                                                    <CancelIcon></CancelIcon>
                                                                                </IconButton>
                                                                            Remove Photo
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                );
                                                            }
                                                        )}
                                                    </div>
                                                    <div className="dropdown-divider"></div>
                                                    <div className="row padding-top">
                                                        <div className="col-lg-12 mb-2 text_align_left">
                                                            <div><b>You can upload four photo's for gallery</b></div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-12 mb-2">
                                                            <input type="file" accept="image/*" onChange={this.onSelectFile} />
                                                            {"    "}
                                                            <SubmitButton
                                                                type="submit"
                                                                variant="contained"
                                                            >
                                                                Upload Photo
                                                            </SubmitButton>

                                                        </div>

                                                        <div className="col-lg-8 mb-2">
                                                            {values.src && (
                                                                <ReactCrop
                                                                    src={values.src}
                                                                    crop={crop}
                                                                    ruleOfThirds
                                                                    onImageLoaded={this.onImageLoaded}
                                                                    onComplete={this.onCropComplete}
                                                                    onChange={this.onCropChange}
                                                                />
                                                            )}
                                                        </div>
                                                        <div className="col-lg-4 mb-2">
                                                            {values.croppedImageUrl && (
                                                                <img alt="Crop" width="300" height="300" src={values.croppedImageUrl} />
                                                            )}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </Form>
                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserGallery);
