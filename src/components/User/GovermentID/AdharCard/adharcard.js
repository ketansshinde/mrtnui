import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../../common/cryptoCode";
import {
    SubmitButton, CloseButton
} from "../../../../assets/MaterialControl";
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import adharcard from "../../../../assets/images/adharcard.png";
import { IconButton } from "@material-ui/core";
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../../notification/notifications";
import { UserSettingService } from "../../../../services/UserService/UserSettingService";
import { Global_var, RegexCode } from "../../../../global/global_var";
import { CallCommanAPI } from "../../CommonComponent/CallCommanAPI/CallCommanAPI";


class AdharCard extends Component {
    constructor(props) {
        
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",

            src: null,
            crop: {
                unit: '%',
                width: 50,
                aspect: 16 / 9,
            },
            croppedIma: "",
            croppedImageUrl: "",
            imageDetail: "",
            Userid: "",

            adharcard: "",
            adharcardSRC: "",
            adharNumber: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
        this._loadDatatable();
    }

    _loadDatatable() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        }
        CallCommanAPI.GetGovIDService(passValue,
            (res) => {
                var data = (res.responseList || []).filter(x => x.document === "ADHAR");
                var PathURL = Global_var.URL_USER_GOV_ID + data[0].documentName;
                var adharNumber = data[0].documentName.split("_")[0];
                this.setState({
                    adharcardSRC: PathURL,
                    adharNumber: adharNumber
                });
            }
        );
    }

    onSelectFile = e => {

        this.setState({ imageDetail: e.target.files[0] });
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);

    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({ croppedImageUrl });
            this.setState({ croppedIma: croppedImageUrl });
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.setState({ blobUrl: this.fileUrl })
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }

    _handleSubmit = (values, { resetForm }, actions) => {
        
        var ADHARNumber = localStorage.getItem("ADHAR");
        var regex = RegexCode.ADHAR;
        if (ADHARNumber === "") {
            warning("Please enter ADHAR Number", warningNotification);
            return;
        }
        if (regex.test(ADHARNumber)) {
            warning("Invalid ADHAR Number", warningNotification);
            return;
        }
        if (values.croppedIma === "") {
            warning("Please choose image file", warningNotification);
            return;
        }

        Axios({
            method: "get",
            url: values.croppedIma, // blob url eg. blob:http://127.0.0.1:8000/e89c5d87-a634-4540-974c-30dc476825cc
            responseType: "blob",
        }).then(function (response) {

            var reader = new FileReader();
            reader.readAsDataURL(response.data);
            reader.onloadend = function () {
                var base64data = reader.result;

                const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
                var imageDetail = values.imageDetail;
                var passValue = {
                    documentId: 0,
                    userid: UserData.id,
                    document: localStorage.getItem("ADHAR"),
                    documentName: "ADHAR",
                    documentType: imageDetail.type,
                    documentBase: base64data,
                };
                localStorage.removeItem("ADHAR");
                UserSettingService.SaveProfileGovIdService(passValue,
                    (res) => {
                        if (res.data.success) {
                            success("ADHAR card updated successfully", successNotification);
                            window.location.reload();
                        }
                        else {
                            error("Something wents worng.", errorNotification);
                        }
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            }
        });
        this._loadDatatable();
    }


    render() {
        const { crop } = this.state;
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        src: this.state.src,
                        crop: this.state.crop,
                        croppedIma: this.state.croppedIma,
                        crop: this.state.crop,
                        croppedImageUrl: this.state.croppedImageUrl,
                        imageDetail: this.state.imageDetail,
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid,
                        adharcard: this.state.adharcard,
                        adharcardSRC: this.state.adharcardSRC,
                        adharNumber: this.state.adharNumber,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>

                        <div className="row">
                            <div className="col-lg-6 mb-2">
                                <label className="medium" htmlFor="First">ADHAR Card Number</label> (size. 150x300)
                                <Field
                                    type="text"
                                    id="adharcard"
                                    name="adharcard"
                                    maxLength="12"
                                    placeholder="ADHAR Card Number"
                                    onChange={(event) => {
                                        
                                        this.setState({ adharcard: event.target.value });
                                        localStorage.setItem("ADHAR", event.target.value);
                                    }}
                                    className={`form-control ${touched.adharcard && errors.adharcard
                                        ? "is-invalid"
                                        : ""
                                        }`}
                                />
                                <ErrorMessage
                                    component="div"
                                    name="adharcard"
                                    className="text-danger"
                                />
                                <input type="file" accept="image/*" onChange={this.onSelectFile} />
                                {" "}  <SubmitButton
                                    type="submit"
                                    variant="contained"
                                >
                                    Save ADHAR-Card
                                                            </SubmitButton>

                            </div>

                            <div className="col-lg-6 mb-2 text_align_center">
                                <label className="medium" htmlFor="First">{"ADHAR NUMBER - "} </label>{values.adharNumber}<br />
                                <img src={values.adharcardSRC === "" ? adharcard : values.adharcardSRC} alt="Pan Card" className="img-govermentId"></img>
                            </div>
                            <div className="col-lg-6 mb-2">
                                {values.src && (
                                    <ReactCrop
                                        src={values.src}
                                        crop={crop}
                                        ruleOfThirds
                                        onImageLoaded={this.onImageLoaded}
                                        onComplete={this.onCropComplete}
                                        onChange={this.onCropChange}
                                    />
                                )}
                            </div>
                            <div className="col-lg-6 mb-2 text_align_center">
                                {values.croppedImageUrl && (
                                    <img alt="Crop" width="300" height="150" className="cropImg" src={values.croppedImageUrl} />
                                )}
                            </div>
                        </div>

                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}
export default AdharCard;
