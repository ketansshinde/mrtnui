import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../../common/cryptoCode";
import {
    SubmitButton, CloseButton
} from "../../../../assets/MaterialControl";
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import Axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import pancard from "../../../../assets/images/pancard.PNG";
import { IconButton } from "@material-ui/core";
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../../notification/notifications";
import { UserSettingService } from "../../../../services/UserService/UserSettingService";
import { Global_var, RegexCode } from "../../../../global/global_var";
import { CallCommanAPI } from "../../CommonComponent/CallCommanAPI/CallCommanAPI";

class PanCard extends Component {
    constructor(props) {

        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",

            src: null,
            crop: {
                unit: '%',
                width: 50,
                aspect: 16 / 9,
            },
            croppedIma: "",
            croppedImageUrl: "",
            imageDetail: "",
            Userid: "",

            pancard: "",
            panSRC: "",
            panNumber: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({ name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
        this._loadDatatable();
    }

    _loadDatatable() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        }
        CallCommanAPI.GetGovIDService(passValue,
            (res) => {
                var data = (this.props.GovData.responseList || []).filter(x => x.document === "PAN");
                var PathURL = Global_var.URL_USER_GOV_ID + data[0].documentName;
                var panNumber = data[0].documentName.split("_")[0];
                this.setState({
                    panSRC: PathURL,
                    panNumber: panNumber
                });
            }
        );
    }

    onSelectFile = e => {

        this.setState({ imageDetail: e.target.files[0] });
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({ src: reader.result })
            );
            reader.readAsDataURL(e.target.files[0]);
        }
    };

    // If you setState the crop in here you should return false.
    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);

    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        // this.setState({ crop: percentCrop });
        this.setState({ crop });
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({ croppedImageUrl });
            this.setState({ croppedIma: croppedImageUrl });
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    //reject(new Error('Canvas is empty'));
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.setState({ blobUrl: this.fileUrl })
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });

    }

    _handleSubmit = (values, { resetForm }, actions) => {

        var PANNumber = localStorage.getItem("PANCARD");
        var regex = RegexCode.PAN;
        if (PANNumber === "") {
            warning("Please enter PAN Number", warningNotification);
            return;
        }
        if (!regex.test(PANNumber)) {
            warning("Invalid PAN Number", warningNotification);
            return;
        }
        if (values.croppedIma === "") {
            warning("Please choose image file", warningNotification);
            return;
        }

        Axios({
            method: "get",
            url: values.croppedIma, // blob url eg. blob:http://127.0.0.1:8000/e89c5d87-a634-4540-974c-30dc476825cc
            responseType: "blob",
        }).then(function (response) {

            var reader = new FileReader();
            reader.readAsDataURL(response.data);
            reader.onloadend = function () {
                var base64data = reader.result;

                const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
                var imageDetail = values.imageDetail;
                var passValue = {
                    documentId: 0,
                    userid: UserData.id,
                    document: localStorage.getItem("PANCARD"),
                    documentName: "PAN",
                    documentType: imageDetail.type,
                    documentBase: base64data,
                };
                localStorage.removeItem("PANCARD", values.pancard);
                UserSettingService.SaveProfileGovIdService(passValue,
                    (res) => {
                        if (res.data.success) {
                            success("PAN card updated successfully", successNotification);
                            window.location.reload();
                        }
                        else {
                            error("Something wents worng.", errorNotification);
                        }
                    },
                    (error) => {
                        console.log(error);
                    }
                );
            }
        });
    }


    render() {
        const { crop } = this.state;
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        src: this.state.src,
                        crop: this.state.crop,
                        croppedIma: this.state.croppedIma,
                        crop: this.state.crop,
                        croppedImageUrl: this.state.croppedImageUrl,
                        imageDetail: this.state.imageDetail,
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid,
                        pancard: this.state.pancard,
                        panSRC: this.state.panSRC,
                        panNumber: this.state.panNumber,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>

                        <div className="row">
                            <div className="col-lg-6 mb-2">
                                <label className="medium" htmlFor="First">PAN Card Number</label> (size. 150x300)
                                <Field
                                    type="text"
                                    id="pancard"
                                    name="pancard"
                                    maxLength="10"
                                    placeholder="PAN Card Number"
                                    onChange={(event) => {

                                        this.setState({ pancard: event.target.value });
                                        localStorage.setItem("PANCARD", event.target.value);
                                    }}
                                    className={`form-control ${touched.pancard && errors.pancard
                                        ? "is-invalid"
                                        : ""
                                        }`}
                                />
                                <ErrorMessage
                                    component="div"
                                    name="pancard"
                                    className="text-danger"
                                />
                                <input type="file" accept="image/*" onChange={this.onSelectFile} />
                                {" "}  <SubmitButton
                                    type="submit"
                                    variant="contained"
                                >
                                    Save PAN-Card
                                                            </SubmitButton>

                            </div>

                            <div className="col-lg-6 mb-2 text_align_center">
                                <label className="medium" htmlFor="First">{"PAN NUMBER - "}</label> {values.panNumber}<br />
                                <img src={values.panSRC === "" ? pancard : values.panSRC} alt="Pan Card" className="img-govermentId"></img>
                            </div>
                            <div className="col-lg-6 mb-2">
                                {values.src && (
                                    <ReactCrop
                                        src={values.src}
                                        crop={crop}
                                        ruleOfThirds
                                        onImageLoaded={this.onImageLoaded}
                                        onComplete={this.onCropComplete}
                                        onChange={this.onCropChange}
                                    />
                                )}
                            </div>
                            <div className="col-lg-6 mb-2 text_align_center">
                                {values.croppedImageUrl && (
                                    <img alt="Crop" width="300" height="150" className="cropImg" src={values.croppedImageUrl} />
                                )}
                            </div>
                        </div>

                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}
export default PanCard;
