import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import UserLeftSideNotification from "../UserLeftSideNotification/userleftsidenotificatio";
import MUIDataTable from "mui-datatables";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { optionsMail, themeMail } from "../../../common/columnFeature";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CurrencyValue } from "../../User/CommonComponent";
import { Formik, Form, Field, ErrorMessage } from "formik";

import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import MessageIcon from '@material-ui/icons/Message';
import { IconButton } from "@material-ui/core";
import EmailIcon from '@material-ui/icons/Email';
import VisibilityIcon from '@material-ui/icons/Visibility';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';

import StarIcon from '@material-ui/icons/Star';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';

import Modal from 'react-bootstrap/Modal';
import { SendMessageDetail } from "../../../action/UserAction/SendMessageAction";
import { Global_var } from "../../../global/global_var";

import { FetchGalleryPhoto } from "../../../action/UserAction/UserSettingAction";
import { Loader } from "../CommonComponent";
import * as moment from "moment-timezone";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            SendMessageDetail: SendMessageDetail,
            FetchGalleryPhoto: FetchGalleryPhoto
        },
        dispatch
    );

class UserHomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            getInboxMailsDataList: [{
                Reply: "data"
            }],
            UserProfileData: [],
            SendMessagePopUp: false,
            useridto: 0,
            requested_userid: 0,
            message: "",
            profilePhoto: "",
            profilename: "",
            viewUserProfilePopUp: false,
            viewUserProfile: [],
            galleryPhoto: [],
            submitting: false,
            loaderMgs: "",
            
        };
    }

    componentDidMount() {
        localStorage.removeItem("getUserProfile");
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        this.setState({
            name: username,
            profileUniqueId: UserData.profileUniqueId,
            gender: UserData.gender,
        });
        
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        
        var passValue = {
            userid: UserData.id,
            casteId: userloginDetail.caste,
            gender: UserData.gender,
            profileUniqueId: UserData.profileUniqueId
        };

        this.props.FetchUserProfile(passValue,
            (res) => {

                if (res.data.success) {
                    this.setState({ UserProfileData: res.data.responseList })
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    //#region

    // send message pop up----------------------------------
    _handleClickSendMessagePopUp = () => {
        this.setState({ SendMessagePopUp: true });
    };

    _handleCloseSendMessagePopUp = () => {
        this.setState({ SendMessagePopUp: false });
    };


    // Large pop up-----------------------------------------

    _handleClickViewProfilePopUp = () => {
        this.setState({ viewUserProfilePopUp: true });
    };

    _handleCloseViewProfilePopUp = () => {
        this.setState({ viewUserProfilePopUp: false });
    };

    //#endregion

    _sendMessage(sendMessageData, Req) {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        if (Req === 2) {
            this._handleClickSendMessagePopUp();
            this.setState({
                useridto: sendMessageData.profileUserid,
                requested_userid: UserData.id,
                message: "",
                profilename: sendMessageData.firstName + " " + sendMessageData.lastName
            });
        }
        else {
            var passValue = {
                userid: sendMessageData.profileUserid,
                requestedUserid: UserData.id,
                message: "Hello " + sendMessageData.firstName + " " + sendMessageData.lastName
            };
            this.props.SendMessageDetail(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Request message sent.", successNotification);
                        this._handleCloseSendMessagePopUp();
                    }
                    else {
                        error("Invalid user.", errorNotification);
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    };

    _sendingMessage(values) {
        if (values.message === "") {
            warning("Please enter the message.", warningNotification);
            return;
        }

        var passValue = {
            userid: values.useridto,
            requestedUserid: values.requested_userid,
            message: values.message
        };
        this.props.SendMessageDetail(passValue,
            (res) => {

                if (res.data.success) {
                    success("Message sent.", successNotification);
                    this._handleCloseSendMessagePopUp();
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _UserProfileDetails(UserProfileData) {
        localStorage.removeItem("getUserProfile");
        localStorage.setItem("getUserProfile", CryptoCode.encryption(JSON.stringify(UserProfileData)));
        this.props.history.push("/userProfile", UserProfileData);
    }

    _viewUserProfile(profiledata) {
        this._handleClickViewProfilePopUp();
        this._fatchGalleryPhoto(profiledata);
        this.setState({ viewUserProfile: profiledata });
    }

    _fatchGalleryPhoto(profiledata) {


        this.setState({ submitting: true });
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: profiledata.profileUserid,
            profileUniqueId: profiledata.profileUniqueId,
        }
        this.props.FetchGalleryPhoto(passValue,
            (res) => {

                if (res.success) {
                    this.setState({ galleryPhoto: res.responseList, submitting: false });
                }
                else {
                    error("Something wents worng.", errorNotification);
                    this.setState({ submitting: false });
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    // Short list profile

    _profileShortlist(profiledata) {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: profiledata.profileUserid,
            shortlistedUserid: UserData.id,
        }
        this.props.SaveProfileShortlist(passValue,
            (res) => {

                if (res.data.success) {
                    success("Profile Shortlisted.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    // Interest sent profile

    _profileInterest(profiledata) {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: profiledata.profileUserid,
            interestUserid: UserData.id,
        }
        this.props.SaveProfileInterest(passValue,
            (res) => {

                if (res.data.success) {
                    success("Interest Sent.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    render() {
        const columns = [
            {
                name: "Reply",
                label: "Reply",
                field: "Reply",
                options: {
                    filter: false,
                    customBodyRender: (value, tableMeta, updateValue) => {

                        var profiledata = tableMeta.tableData[tableMeta.rowIndex];
                        return (

                            <div className="layout-margin">
                                <div className="row no-gutters">
                                    <div className="col-lg-3 text_align_center  mt-4">
                                        <img onClick={(event) => {
                                            this._viewUserProfile(profiledata);
                                        }}
                                            src={profiledata.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + profiledata.profilePhotoname : (this.state.gender == 23 ? male : female)} className="view-profile-image-data" alt="logo">
                                        </img>
                                        {/* <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span className="profile-textfont">Profile created by : </span>
                                        </label> {profiledata.profileCreated} */}
                                    </div>
                                    <div className="col-lg-9">
                                        <div className="">
                                            <div className="row">
                                                <div className="col-lg-8  mt-4">
                                                    <span className="text_align_Left">
                                                        <h6 className="card-title"><b>{profiledata.firstName + " " + profiledata.lastName} {" "}</b> ({profiledata.profileUniqueId})</h6>
                                                    </span>
                                                    <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span className="profile-textfont">Profile created by : </span>
                                                    </label> {profiledata.profileCreated + " "}
                                                    <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span className="profile-textfont"> | Last Active : </span>
                                                    </label> {moment(profiledata.lastLogin).format('DD MMM YYYY')}
                                                </div>

                                            </div>
                                            <div className="row">
                                                <div className="col-lg-12 mb-2 details-mb">
                                                    <span className="profile-textfont"> Caste:</span> <span className="profile-textfontName"> {profiledata.caste || "--"} </span> |  <span className="profile-textfont"> Age:</span><span className="profile-textfontName">  {profiledata.age || "--"} yr</span> |<span className="profile-textfont"> Height-Feet:</span> <span className="profile-textfontName"> {profiledata.hightFeet || "--"}' {profiledata.hightInch || "--"}''</span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-8 details-mb">
                                                    <span className="profile-textfont"> Education:</span>  <span className="profile-textfontName">   {profiledata.education || "--"}</span>|
                                                    <span className="profile-textfont"> Branch:</span> <span className="profile-textfontName">  {profiledata.branch || "--"}</span>| <span className="profile-textfont">Occupation:</span>  <span className="profile-textfontName"> {profiledata.occupation || "--"}</span>
                                                </div>
                                                <div className="col-lg-4 mb-2">
                                                    <span className="dropdown text_align_right like-member-dropdownlist">
                                                        <label className="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span className="like-this-member">
                                                                <IconButton color="inherit" size="small">
                                                                    <ThumbUpIcon></ThumbUpIcon>
                                                                </IconButton> Like this member?{" "}
                                                            </span>
                                                        </label>
                                                        <div id="profileaction" className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._profileInterest(profiledata);
                                                            }}>
                                                                <span className="padding-right" >
                                                                    <IconButton color="inherit" size="small">
                                                                        <LabelImportantIcon></LabelImportantIcon>
                                                                    </IconButton>
                                                                </span>
                                                                Send Interest
                                                                </a>
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._sendMessage(profiledata, 2);
                                                            }}>
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <MessageIcon></MessageIcon>
                                                                    </IconButton>
                                                                </span>
                                                                Send Message
                                                                </a>
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._UserProfileDetails(profiledata);
                                                            }}>
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <VisibilityIcon></VisibilityIcon>
                                                                    </IconButton>
                                                                </span>
                                                                View Profile
                                                                </a>
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._profileShortlist(profiledata);
                                                            }}>
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <StarIcon></StarIcon>
                                                                    </IconButton>
                                                                </span>
                                                                 Short list this Profile
                                                              </a>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div >
                        );
                    },
                },
            }
        ];

        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        name: this.state.name,
                        profileUniqueId: this.state.profileUniqueId,
                        gender: this.state.gender,
                        getInboxMailsDataList: this.state.getInboxMailsDataList,
                        UserProfileData: this.state.UserProfileData,
                        SendMessagePopUp: this.state.SendMessagePopUp,
                        useridto: this.state.useridto,
                        requested_userid: this.state.requested_userid,
                        message: this.state.message,
                        profilename: this.state.profilename,
                        viewUserProfilePopUp: this.state.viewUserProfilePopUp,
                        viewUserProfile: this.state.viewUserProfile,
                        galleryPhoto: this.state.galleryPhoto,
                        userGalleryPhoto: "",
                        submitting: this.state.submitting,
                        loaderMgs: this.state.loaderMgs,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="py-3">
                                <div className="main-layout">
                                    {values.submitting ? <Loader state={values.loaderMgs}></Loader> : null}
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <UserLeftSideNotification {...this.props}></UserLeftSideNotification>
                                        </div>
                                        <div className="col-lg-9">
                                            <div className="page-title">{(values.UserProfileData || []).length + " - "} Your Daily Recommendations</div>
                                            <div className="layout-margin">
                                                <MuiThemeProvider
                                                    theme={themeMail}
                                                >
                                                    <MUIDataTable
                                                        data={values.UserProfileData || []}
                                                        columns={columns}
                                                        options={optionsMail}
                                                    />
                                                </MuiThemeProvider>

                                                <Modal
                                                    show={values.SendMessagePopUp}
                                                    onHide={this._handleCloseSendMessagePopUp}
                                                    backdrop="static"
                                                    keyboard={false}
                                                    centered
                                                >
                                                    <Modal.Header closeButton>
                                                        <Modal.Title><h6>Send Message to - {values.profilename}</h6></Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>
                                                        <Field
                                                            type="text"
                                                            id="message"
                                                            name="message"
                                                            maxLength="200"
                                                            placeholder="Message"
                                                            component="textarea"
                                                            rows="4"
                                                            onChange={(event) => {
                                                                setFieldValue(
                                                                    (values.message = event.target.value)
                                                                );
                                                            }}
                                                            className="form-control"
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="message"
                                                            className="text-danger"
                                                        />
                                                    </Modal.Body>
                                                    <Modal.Footer>
                                                        <SubmitButton
                                                            type="submit"
                                                            variant="contained"
                                                            onClick={(event) => {

                                                                this._sendingMessage(values);
                                                            }}
                                                        >
                                                            Send Message
                                                    </SubmitButton>{" "}
                                                        <CloseButton
                                                            variant="contained"
                                                            onClick={(event) => {

                                                                this._handleCloseSendMessagePopUp(values);
                                                            }}
                                                        >
                                                            Cancel
                                                    </CloseButton>


                                                    </Modal.Footer>
                                                </Modal>

                                                {/*--------------- View User Profile on pop up ------------------*/}

                                                <Modal
                                                    show={values.viewUserProfilePopUp}
                                                    onHide={this._handleCloseViewProfilePopUp}
                                                    backdrop="static"
                                                    keyboard={false}
                                                    centered
                                                    size="xl"
                                                >
                                                    {/* <Modal.Header closeButton>
                                                        <Modal.Title><h6>Send Message to - {values.viewUserProfile.firstName + " " + values.viewUserProfile.lastName}</h6></Modal.Title>
                                                    </Modal.Header> */}
                                                    <Modal.Header closeButton>

                                                    </Modal.Header>
                                                    <Modal.Body>
                                                        <div className="mb-3 layout-margin">
                                                            <div className="row no-gutters">
                                                                <div className="col-lg-5 mb-2 text_align_center">
                                                                    <div>
                                                                        <img src={values.userGalleryPhoto === "" ? (values.viewUserProfile.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.viewUserProfile.profilePhotoname : (this.state.gender == 23 ? male : female)) : values.userGalleryPhoto} className="view-profile-image" alt="logo">
                                                                        </img>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-7">
                                                                    <div className="">

                                                                        <div className="row">
                                                                            <div className="col-lg-8 mb-2">
                                                                                <div>
                                                                                    <span className="text_align_Left view-profile-Name">
                                                                                        {values.viewUserProfile.firstName + " " + values.viewUserProfile.lastName} {" "} <span className="view-profile-uniquecode">({values.viewUserProfile.profileUniqueId})</span>
                                                                                    </span>
                                                                                </div>
                                                                                <div>
                                                                                    <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span className="profile-textfont">Profile created by : </span>
                                                                                    </label> {values.viewUserProfile.profileCreated + " "}
                                                                                    <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span className="profile-textfont">| Last Active : </span>
                                                                                    </label> {moment(values.viewUserProfile.lastLogin).format('DD MMM YYYY')}
                                                                                </div>
                                                                            </div>
                                                                            <div className="col-lg-4 mb-2">
                                                                                <span className="dropdown text_align_right like-member-dropdownlist">
                                                                                    <label className="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span className="like-this-member">
                                                                                            <IconButton color="inherit" size="small">
                                                                                                <ThumbUpIcon></ThumbUpIcon>
                                                                                            </IconButton> Like this member?{" "}
                                                                                        </span>
                                                                                    </label>
                                                                                    <div id="profileaction" className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                                                        <a className="dropdown-item" href="#" onClick={(event) => {
                                                                                            this._profileInterest(values.viewUserProfile);
                                                                                        }}>
                                                                                            <span className="padding-right" >
                                                                                                <IconButton color="inherit" size="small">
                                                                                                    <LabelImportantIcon></LabelImportantIcon>
                                                                                                </IconButton>
                                                                                            </span>
                                                                                            Send Interest
                                                                                            </a>
                                                                                        <a className="dropdown-item" href="#" onClick={(event) => {
                                                                                            this._sendMessage(values.viewUserProfile, 2);
                                                                                        }}>
                                                                                            <span className="padding-right">
                                                                                                <IconButton color="inherit" size="small">
                                                                                                    <MessageIcon></MessageIcon>
                                                                                                </IconButton>
                                                                                            </span>
                                                                                        Send Message
                                                                                        </a>
                                                                                        <a className="dropdown-item" href="#" onClick={(event) => {
                                                                                            this._UserProfileDetails(values.viewUserProfile);
                                                                                        }}>
                                                                                            <span className="padding-right">
                                                                                                <IconButton color="inherit" size="small">
                                                                                                    <VisibilityIcon></VisibilityIcon>
                                                                                                </IconButton>
                                                                                            </span>
                                                                                            View Profile
                                                                                            </a>
                                                                                        <a className="dropdown-item" href="#" onClick={(event) => {
                                                                                            this._profileShortlist(values.viewUserProfile);
                                                                                        }}>
                                                                                            <span className="padding-right">
                                                                                                <IconButton color="inherit" size="small">
                                                                                                    <StarIcon></StarIcon>
                                                                                                </IconButton>
                                                                                            </span>
                                                                                            Short list this Profile
                                                                                        </a>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-lg-6 details-mb">
                                                                                <span className="profile-textfont"> Caste:</span> <span className="profile-textfontName"> {values.viewUserProfile.caste || "--"}</span>
                                                                            </div>
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont"> Sub-Caste:</span><span className="profile-textfontName"> {values.viewUserProfile.subCaste1 || "--"}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont"> Education:</span>  <span className="profile-textfontName">   {values.viewUserProfile.education || "--"}</span>
                                                                            </div>
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont"> Branch:</span> <span className="profile-textfontName">  {values.viewUserProfile.branch || "--"}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont">Occupation:</span>  <span className="profile-textfontName"> {values.viewUserProfile.occupation || "--"}</span>
                                                                            </div>
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont"> Monthly Income:</span> <span className="profile-textfontName"> ₹  <CurrencyValue state={values.viewUserProfile.monthlyIncome || 0}></CurrencyValue></span>
                                                                            </div>
                                                                        </div>
                                                                        <div className="row">
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont"> Age:</span><span className="profile-textfontName">  {values.viewUserProfile.age || "--"} yr</span>
                                                                            </div>
                                                                            <div className="col-lg-6 mb-2 details-mb">
                                                                                <span className="profile-textfont"> Height-Feet:</span> <span className="profile-textfontName"> {values.viewUserProfile.hightFeet || "--"}' {values.viewUserProfile.hightInch || "--"}''</span>
                                                                            </div>
                                                                        </div>
                                                                        <div className="dropdown-divider"></div>
                                                                        <div className="view-profile-image-border">
                                                                            <div className="row">
                                                                                <div className="col-lg-12 mb-2 details-mb text_align_center">
                                                                                    <span className="like-this-member">Photo Gallery</span>
                                                                                </div>
                                                                            </div>
                                                                            <div className="row">
                                                                                <div className="col-lg-12 text_align_center">
                                                                                    <img src={values.viewUserProfile.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.viewUserProfile.profilePhotoname : (this.state.gender == 23 ? male : female)}
                                                                                        className="view-profile-image-fav" alt="logo"
                                                                                        onClick={(event) => {
                                                                                            setFieldValue((values.userGalleryPhoto = Global_var.URL_USER_PROFILE_PHOTO + values.viewUserProfile.profilePhotoname));
                                                                                        }}
                                                                                    ></img>
                                                                                    {(values.galleryPhoto || []).map(
                                                                                        (item) => {

                                                                                            return (
                                                                                                <img src={values.documentName !== null ? Global_var.URL_USER_GALLERY + item.documentName : (this.state.gender == 22 ? male : female)}
                                                                                                    className="view-profile-image-fav" alt="logo"
                                                                                                    onClick={(event) => {
                                                                                                        setFieldValue((values.userGalleryPhoto = Global_var.URL_USER_GALLERY + item.documentName));
                                                                                                    }}
                                                                                                ></img>
                                                                                            );
                                                                                        }
                                                                                    )}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div >
                                                    </Modal.Body>
                                                    {/* <Modal.Footer>
                                                        <CloseButton
                                                            variant="contained"
                                                            onClick={(event) => {

                                                                this._handleCloseViewProfilePopUp(values);
                                                            }}
                                                        >
                                                            Cancel
                                                        </CloseButton>
                                                    </Modal.Footer> */}
                                                </Modal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserHomePage);
