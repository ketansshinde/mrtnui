import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import {
    CryptoCode,
} from "../../../common/cryptoCode";
import queryString from "query-string";
import TextField from '@material-ui/core/TextField';
import "./Message.css";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import * as moment from "moment-timezone";
import SendIcon from '@material-ui/icons/Send';
import {
    IconButton
} from "@material-ui/core";
import { Global_var } from "../../../global/global_var";


export class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            userid: 0,
            messageTo: [],
            messageFrom: [],
            allMessage: [],
            messageStore: []
        };
    }

    componentDidMount() {
        this._loadChatMessage();
    }

    _loadChatMessage() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        debugger;
        this.setState({
            userid: UserData.id,
            name: username,
            profileUniqueId: UserData.profileUniqueId,
            gender: UserData.gender
        });

        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId
        };

        this.props.FetchMessage(passValue,
            (res) => {
                debugger;
                if (res.success) {
                    var messageData = res.responseList;
                    var messageTo = messageData.filter(x => x.messageUserid != UserData.id);
                    var messageFrom = messageData.filter(x => x.messageUserid == UserData.id);

                    this.setState({
                        allMessage: messageData,
                        messageTo: messageTo,
                        messageFrom: messageFrom
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }



    _handleSubmit = (values, { resetForm }, actions) => {
        var passValue = {

        }
        this.props.AddRegistration(passValue,
            (res) => {

                if (res.data.success) {
                    success("User register successfully.", successNotification);
                    this.props.history.push("/login");
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _sendingMessage(values) {
        debugger;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        if (values.message === "") {
            warning("Please enter the message.", warningNotification);
            return;
        }

        var passValue = {
            userid: values.MessageUserid,
            requestedUserid: UserData.id,
            message: values.message
        };


        this.props.SendMessageDetail(passValue,
            (res) => {

                if (res.data.success) {
                    success("Message Send.", successNotification);
                    this._handleClosepopup();
                    this._loadChatMessage();
                    this.setState({ message: "", MessageUserid: passValue.userid });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (

            <React.Fragment>
                <div>
                    <section className="py-3">

                        <Formik
                            enableReinitialize
                            initialValues={{
                                message: this.state.message,
                                messageTo: this.state.messageTo,
                                messageFrom: this.state.messageFrom,
                                allMessage: this.state.allMessage,
                                messageStore: this.state.messageStore,
                                userid: this.state.userid,
                                ReceiverMessageUser: []
                            }}
                            validationSchema={this._userAccountSchema}
                            onSubmit={this._handleSubmit}
                        >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                            <Form>
                                <div className="main-layout">
                                    {/* <div className="page-title">
                                        <div className="row gutters">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h5 className="title">Sent Messages</h5>
                                            </div>
                                        </div>
                                    </div> */}
                                    <div className="container">
                                        <div className="content-wrapper">
                                            <div className="row gutters">
                                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className="page-title">Your Messages</div>
                                                    <div className="card m-0">
                                                        <div className="row no-gutters">
                                                            <div className="col-xl-12 col-lg-4 col-md-4 col-sm-3 col-3">
                                                                <div className="users-container">
                                                                    <table class="table table-striped table-light">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col">Profile Photo</th>
                                                                                <th scope="col">Name</th>
                                                                                <th scope="col">Message</th>
                                                                                <th scope="col">Date</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {(values.allMessage.filter(x => x.messageUserid !== values.userid) || []).map(
                                                                                (_profileUsers) => (
                                                                                    <tr>
                                                                                        <th scope="row"><img
                                                                                            style={{
                                                                                                height: "50px",
                                                                                                padding: "0px",
                                                                                                borderRadius: "50%"
                                                                                            }}

                                                                                            src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + _profileUsers.receiverDocumentName : (this.state.gender == 22 ? male : female)} ></img></th>
                                                                                        <td>{_profileUsers.receiverFirstName + " " + _profileUsers.receiverLastName}</td>
                                                                                        <td>{_profileUsers.message}</td>
                                                                                        <td>{_profileUsers.messageCreatedDate}</td>
                                                                                    </tr>
                                                                                )
                                                                            )}
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        )}
                        </Formik>
                    </section>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    //...getLogin(state),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Message);
