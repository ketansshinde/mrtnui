import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import queryString from "query-string";
import { CryptoCode } from "../../../common/cryptoCode";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );



class PaymentStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paymentStatus: false
        };
    }
    componentDidMount() {
        // if (this.props.location.search === "") {
        //     this.props.history.push("/Pricing");
        //     return;
        // }
        // const values = queryString.parse(this.props.location.search);
        // //console.log(values.validateCode); // "top"
        // this._getPaymentStatusDetail(values.validateCode)
    }

    _getPaymentStatusDetail(validateCode) {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
            generatedCode: validateCode
        };

        this.props.GetvalidatePaymentStatus(passValue,
            (res) => {
                if (res.success) {

                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _gotoOrderdetail = () => {
        this.props.history.push("/orderdetail");
    }

    _gotoPricingPage = () => {
        this.props.history.push("/Pricing");
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        paymentStatus: this.state.paymentStatus
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <section className="py-3">
                            <div className="main-layout">
                                <div className="row">
                                    <div className="col-lg-6 offset-md-3">
                                        <div className="card layout-margin">
                                            <div className="card-body">
                                                {/* <h5 className="card-title card-title-set">Forget Password</h5> */}
                                                {values.paymentStatus ?
                                                    <div>
                                                        <div className="row" >
                                                            <div className="col-lg-12 mb-2 text_align_center">
                                                                <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                                                    <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                                                    <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                                                </svg>
                                                            </div>
                                                            <div className="col-lg-12 mb-2 text_align_center">

                                                                <b>Your Subcription has been activated</b>

                                                            </div>
                                                            <div className="col-lg-12 mb-2 text_align_center">

                                                                <SubmitButton
                                                                    type="submit"
                                                                    variant="contained"
                                                                    onClick={this._gotoOrderdetail}
                                                                >
                                                                    Click Here
                                                                </SubmitButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="row" >
                                                            <div className="col-lg-12 mb-2 text_align_center">
                                                                <i class="fa fa-exclamation-triangle red-color " style={{ color: "#e73535d9", fontSize: "50px" }}></i>
                                                            </div>
                                                            <div className="col-lg-12 mb-2 text_align_center">

                                                                <b>Something wents wrong !</b>

                                                            </div>
                                                            <div className="col-lg-12 mb-2 text_align_center">

                                                                <SubmitButton
                                                                    type="submit"
                                                                    variant="contained"
                                                                    onClick={this._gotoPricingPage}
                                                                >
                                                                    Try Again
                                                                </SubmitButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default PaymentStatus;