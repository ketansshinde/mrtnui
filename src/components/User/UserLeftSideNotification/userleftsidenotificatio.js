import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import MessageIcon from '@material-ui/icons/Message';
import { IconButton } from "@material-ui/core";
import EmailIcon from '@material-ui/icons/Email';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import SettingsIcon from '@material-ui/icons/Settings';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PersonIcon from '@material-ui/icons/Person';
import PaymentIcon from '@material-ui/icons/Payment';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Global_var } from "../../../global/global_var";
import "./userleftsidenotification.css";
import FilterListIcon from '@material-ui/icons/FilterList';
import BlockIcon from '@material-ui/icons/Block';
import SettingsBackupRestoreIcon from '@material-ui/icons/SettingsBackupRestore';
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

class UserLeftSideNotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            profilePhoto: "",
            Userid: "",
            profilePercentage: 30,
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({
            Userid: UserData.id,
            name: username,
            profileUniqueId: UserData.profileUniqueId,
            gender: UserData.gender
        });
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        this.setState({ profilePhoto: userloginDetail.documentName });

        this._fatchProfilePercentage();
        // var passvalue = {
        //     userid: UserData.id,
        //     profileUniqueId: UserData.profileUniqueId
        // }
        // this.props.FetchUserProfilePhoto(passvalue,
        //     (res) => {
        //         
        //         if (res.success) {
        //             const profilePhotoData = JSON.parse(CryptoCode.decryption(localStorage.getItem("ProfilePhoto")));
        //             if (profilePhotoData.length !== 0)
        //                 this.setState({ profilePhoto: profilePhotoData[0].documentType + "," + profilePhotoData[0].documentBase });
        //         }
        //         else {
        //             // error("Invalid user.", errorNotification);
        //         }
        //     },
        //     (error) => {
        //         console.log(error);
        //     }
        // );

    }
    _sendMessage = () => {
        this.props.history.push("/sendmessage");
    }
    _pricing = () => {
        this.props.history.push("/pricing");
    }
    _changeprofilephoto = () => {
        this.props.history.push("/changeprofilephoto");
    }
    _profileViewer = () => {
        this.props.history.push("/profilevisitor");
    }
    _profileShortlist = () => {
        this.props.history.push("/profileshortlist");
    }
    _profilerequest = () => {
        this.props.history.push("/profilerequest");
    }
    _editprofile = () => {
        this.props.history.push("/editprofile");
    }

    _profileShortlistbyYou = () => {
        this.props.history.push("/profileshortlistbyyou");
    }

    _profileDecline = () => {
        this.props.history.push("/profiledecline");
    }

    _profileDeclineByYou = () => {
        this.props.history.push("/profiledeclinebyyou");
    }



    // get profile percentage
    _fatchProfilePercentage() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        }
        this.props.ProfilePercentage(passValue,
            (res) => {
                
                if (res.data.success) {
                    this.setState({ profilePercentage: res.data.responseList });
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid,
                        profilePercentage: this.state.profilePercentage
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>

                        <div className="user-leftsidemenu-page">
                            <div className="user-leftsidemenu-page-body">
                                <div className="text_align_center" onClick={this._editprofile}>
                                    <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.profilePhoto : (this.state.gender == 22 ? male : female)} className="user-leftsidemenu-profile-image" alt="logo"></img>
                                </div>
                                <h5 className="card-title card-title-set text_align_center card-title-set-2">{this.state.name}</h5>
                                <h6 className="card-subtitle mb-2 text_align_center padding-bottum" >{this.state.profileUniqueId}</h6>
                                <div className="margin-bottom text_align_center">
                                    <div className="progress">
                                        <div className="progress-bar progress-bar-striped progress-bar-animated bg-success" style={{ width: values.profilePercentage + "%" }}></div>
                                    </div>
                                    {values.profilePercentage}% Completeness Profile
                                    </div>
                                <div className="dropdown-divider"></div>
                                <div className="user-leftsidemenu-header">Message</div>

                                <div className="layout-margin hyperlink" onClick={this._sendMessage}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <MessageIcon></MessageIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left">Message </span>
                                    <span className="text_align_right"><span className="badge badge-danger">0</span></span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._sendMessage}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <EmailIcon></EmailIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left">Sent Message </span>
                                    <span className="text_align_right"><span className="badge badge-danger">0</span></span>
                                </div>


                                <div className="user-leftsidemenu-header">Activities</div>


                                <div className="layout-margin hyperlink" onClick={this._profileViewer}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <VisibilityIcon></VisibilityIcon>
                                        </IconButton>

                                    </span>
                                    <span className="text_align_left">Who Visited Profile</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileShortlist}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <PersonAddIcon></PersonAddIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Who Shortlisted You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileShortlistbyYou}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <FilterListIcon></FilterListIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Shortlisted By You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profilerequest}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <LabelImportantIcon></LabelImportantIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Who Interested You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileDecline}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <BlockIcon></BlockIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Who Declined You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileDeclineByYou}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <SettingsBackupRestoreIcon></SettingsBackupRestoreIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Declined By You</span>
                                </div>

                                <div className="user-leftsidemenu-header">Personal Settings</div>

                                <div className="layout-margin hyperlink" onClick={this._changeprofilephoto}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <SettingsIcon></SettingsIcon>
                                        </IconButton></span>
                                    <span className="text_align_left">Setting</span>
                                </div>
                                <div className="layout-margin hyperlink">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <FeedbackIcon></FeedbackIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Feedback</span>
                                </div>
                                <div className="layout-margin hyperlink">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <PersonIcon></PersonIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left " >Profile Verification</span>
                                </div>

                                <div className="user-leftsidemenu-header">Purchase Plans</div>

                                <div className="layout-margin hyperlink" onClick={this._pricing}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <PaymentIcon></PaymentIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Upgrade Profile</span>
                                </div>
                            </div>
                        </div>

                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserLeftSideNotification);
