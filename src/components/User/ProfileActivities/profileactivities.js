import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import MessageIcon from '@material-ui/icons/Message';
import { IconButton } from "@material-ui/core";
import EmailIcon from '@material-ui/icons/Email';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import SettingsIcon from '@material-ui/icons/Settings';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PersonIcon from '@material-ui/icons/Person';
import PaymentIcon from '@material-ui/icons/Payment';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Global_var } from "../../../global/global_var";
import "./profileactivities.css";
import FilterListIcon from '@material-ui/icons/FilterList';
import BlockIcon from '@material-ui/icons/Block';
import SettingsBackupRestoreIcon from '@material-ui/icons/SettingsBackupRestore';

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

class ProfileActivities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            profilePhoto: "",
            Userid: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;

        this.setState({
            Userid: UserData.id,
            name: username,
            profileUniqueId: UserData.profileUniqueId,
            gender: UserData.gender
        });
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        this.setState({ profilePhoto: userloginDetail.documentName });

    }
  
    _profileViewer = () => {
        this.props.history.push("/profilevisitor");
    }
    _profileShortlist = () => {
        this.props.history.push("/profileshortlist");
    }
    _profilerequest = () => {
        this.props.history.push("/profilerequest");
    }
    _editprofile = () => {
        this.props.history.push("/editprofile");
    }

    _profileShortlistbyYou = () => {
        this.props.history.push("/profileshortlistbyyou");
    }

    _profileDecline = () => {
        this.props.history.push("/profiledecline");
    }

    _profileDeclineByYou = () => {
        this.props.history.push("/profiledeclinebyyou");
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>

                        <div className="user-leftsidemenu-page">
                            <div className="user-leftsidemenu-page-body">
                                <div className="user-leftsidemenu-header">Activities</div>
                                <div className="layout-margin hyperlink" onClick={this._profileViewer}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <VisibilityIcon></VisibilityIcon>
                                        </IconButton>

                                    </span>
                                    <span className="text_align_left">Who Visited Profile</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileShortlist}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <PersonAddIcon></PersonAddIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Who Shortlisted You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileShortlistbyYou}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <FilterListIcon></FilterListIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Shortlisted By You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profilerequest}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <LabelImportantIcon></LabelImportantIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Who Interested You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileDecline}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <BlockIcon></BlockIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Who Declined You</span>
                                </div>
                                <div className="layout-margin hyperlink" onClick={this._profileDeclineByYou}>
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <SettingsBackupRestoreIcon></SettingsBackupRestoreIcon>
                                        </IconButton>
                                    </span>
                                    <span className="text_align_left ">Declined By You</span>
                                </div>
                            </div>
                        </div>

                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileActivities);
