import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );


class UserLifeStyle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DietData: [],
            SmokeData: [],
            DrinkData: [],
            lifeStyleId: 0,
            userid: 0,
            diet: 0,
            smoke: 0,
            drink: 0,
            createdDate: "",
            modifiedDate: "",
            user: ""
        };
    }
    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var DietData = SubMasterValue.SubMaster(MasterValue.Diet);
        var SmokeData = SubMasterValue.SubMaster(MasterValue.Smoke);
        var DrinkData = SubMasterValue.SubMaster(MasterValue.Drink);

        this.setState({
            DietData: DietData,
            SmokeData: SmokeData,
            DrinkData: DrinkData,
        });

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));
        
        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId
        };

        UserLayoutService.GetUserLifeStyle(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        userid: UserData.userid,
                        diet: UserData.diet,
                        smoke: UserData.smoke,
                        drink: UserData.drink,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        DietData: this.state.DietData,
                        SmokeData: this.state.SmokeData,
                        DrinkData: this.state.DrinkData,
                        userid: this.state.userid,
                        diet: this.state.diet,
                        smoke: this.state.smoke,
                        drink: this.state.drink,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-life-ring" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Life Style</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="diet">Diet</label>
                                        <br />
                                        {values.diet === null || values.diet === undefined ? "--" :
                                        (values.DietData.filter(x => x.subMasterId === values.diet) || []).map(
                                            (_subMasterData) => (
                                                <span> {_subMasterData.name || "--"}</span>
                                            )
                                        )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="smoke">Smoke</label>
                                        <br />
                                        {values.smoke === null || values.smoke === undefined ? "--" :
                                        (values.SmokeData.filter(x => x.subMasterId === values.smoke) || []).map(
                                            (_subMasterData) => (
                                                <span> {_subMasterData.name || "--"}</span>
                                            )
                                        )}
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="drink">Drink</label>
                                        <br />
                                        {values.drink === null || values.drink === undefined ? "--" :
                                        (values.DrinkData.filter(x => x.subMasterId === values.drink) || []).map(
                                            (_subMasterData) => (
                                                <span> {_subMasterData.name || "--"}</span>
                                            )
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserLifeStyle);