import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { IconButton } from "@material-ui/core";

class UserExpectation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profileExpId: 0,
            userid: 0,
            yourSelf: "",
            expectation: "",
            createdDate: "",
            modifiedDate: "",
            user: ""
        };
    }
    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));
        
        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId
        };

        UserLayoutService.GetUserExpectationDetail(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        profileExpId: UserData.profileExpId,
                        userid: UserData.userid,
                        yourSelf: UserData.yourSelf,
                        expectation: UserData.expectation,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        profileExpId: this.state.profileExpId,
                        userid: this.state.userid,
                        yourSelf: this.state.yourSelf,
                        expectation: this.state.expectation,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-outdent" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Personal Expectation  (What I am looking for)</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Your Self</label>
                                        <br />

                                        {values.yourSelf || "--"}

                                    </div>
                                    <div className="col-lg-8 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Expectation</label>
                                        <br />

                                        {values.expectation || "--"}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}
export default UserExpectation;
