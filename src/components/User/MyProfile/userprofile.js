import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { IconButton } from "@material-ui/core";
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import * as moment from "moment-timezone";

const mapStateToProps = (state) => ({
    // UserProfileData: GetUserDetailByID(state.RegistrationReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            // fetchUserByID: fetchUserByID
        },
        dispatch
    );


class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            GenderData: [],
            ProfileCreatedData: [],
            profileUniqueId: "",
            userid: 0,
            firstName: "",
            middleName: "",
            lastName: "",
            motherName: "",
            gender: 22,
            mobileNo: "",
            emailId: "",
            password: "",
            profileCreatedBy: "",
            isActive: true,
            lastLogin: "",
            ipaddress: "",
            isSubscriber: false,
            showDetail: false
        };
    }
    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var GenderData = SubMasterValue.SubMaster(MasterValue.Gender);
        var ProfileCreatedData = SubMasterValue.SubMaster(MasterValue.Profile_created_by);
        this.setState({
            GenderData: GenderData,
            ProfileCreatedData: ProfileCreatedData,
            profileUniqueId: UserData.profileUniqueId,
        });

        // Checking Subcription Details
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        debugger;
        if (userloginDetail.subcriptionId === null) {
            this.setState({ isSubscriber: false });
        } else {
            var CurrentDT = new Date();
            var CurrentDate = moment(CurrentDT).format('YYYY-MM-DD');
            var subEndDate = moment(userloginDetail.subEndDate).format('YYYY-MM-DD');
            if (moment(CurrentDate).isSameOrBefore(subEndDate))  // true
            {
                this.setState({ showDetail: true });
                console.log("Daa");
            }
        }
        //--------------------------------------------------------

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));

        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId,
            viewerUserid: UserData.id,
            isSubscription: 0
        };

        UserLayoutService.GetUserDetailByID(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        profileUniqueId: UserData.profileUniqueId,
                        userid: UserData.userid,
                        firstName: UserData.firstName,
                        middleName: UserData.middleName,
                        lastName: UserData.lastName,
                        motherName: UserData.motherName,
                        gender: UserData.gender,
                        mobileNo: UserData.mobileNo,
                        emailId: UserData.emailId,
                        password: UserData.password,
                        profileCreatedBy: UserData.profileCreatedBy,
                        isActive: UserData.isActive,
                        lastLogin: UserData.lastLogin,
                        ipaddress: UserData.ipaddress,
                        isSubscriber: UserData.isSubscriber
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );



    }

    _handleSubmit = (values, { resetForm }, actions) => {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));

        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId,
            viewerUserid: UserData.id,
            isSubscription: 1
        };

        UserLayoutService.GetUserDetailByID(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        profileUniqueId: UserData.profileUniqueId,
                        userid: UserData.userid,
                        firstName: UserData.firstName,
                        middleName: UserData.middleName,
                        lastName: UserData.lastName,
                        motherName: UserData.motherName,
                        gender: UserData.gender,
                        mobileNo: UserData.mobileNo,
                        emailId: UserData.emailId,
                        password: UserData.password,
                        profileCreatedBy: UserData.profileCreatedBy,
                        isActive: UserData.isActive,
                        lastLogin: UserData.lastLogin,
                        ipaddress: UserData.ipaddress,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    // Go to the Subcription page
    _gotoSubecription = () => {
        this.props.history.push("/Pricing");
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        GenderData: this.state.GenderData,
                        ProfileCreatedData: this.state.ProfileCreatedData,
                        profileUniqueId: this.state.profileUniqueId,
                        userid: this.state.userid,
                        firstName: this.state.firstName,
                        middleName: this.state.middleName,
                        lastName: this.state.lastName,
                        motherName: this.state.motherName,
                        gender: this.state.gender,
                        mobileNo: this.state.mobileNo,
                        emailId: this.state.emailId,
                        password: this.state.password,
                        profileCreatedBy: this.state.profileCreatedBy,
                        isActive: this.state.isActive,
                        lastLogin: this.state.lastLogin,
                        ipaddress: this.state.ipaddress,
                        isSubscriber: this.state.isSubscriber,
                        showDetail: this.state.showDetail
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <PersonIcon></PersonIcon>
                                        </IconButton></span>
                                    About Me ({values.profileUniqueId})</h5>
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="First">First Name</label>
                                        <br />
                                        {values.firstName || "--"}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Middle">Middle Name</label>
                                        <br />
                                        {values.middleName || "--"}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Last">Last Name</label>
                                        <br />
                                        {values.lastName || "--"}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Mother">Mother Name</label>
                                        <br />
                                        {values.motherName || "--"}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Profile">Profile Created By</label>
                                        <br />
                                        {(values.ProfileCreatedData.filter(x => x.subMasterId === values.profileCreatedBy) || []).map(
                                            (_subMasterData) => (
                                                <span> {_subMasterData.name || "--"}</span>
                                            )
                                        )}

                                    </div>

                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Gender">Gender</label>
                                        <br />
                                        {(values.GenderData.filter(x => x.subMasterId === values.gender) || []).map(
                                            (_subMasterData) => (
                                                <span> {_subMasterData.name || "--"}</span>
                                            )
                                        )}
                                    </div>

                                </div>
                                {values.isSubscriber === true ? (
                                    <div>
                                        <div className="row">
                                            <div className="col-lg-4 mb-2">
                                                <label className="medium" htmlFor="Mobile">Mobile No.</label>
                                                <br />
                                                {values.mobileNo || "--"}

                                            </div>
                                            <div className="col-lg-4 mb-2">
                                                <label className="medium" htmlFor="inputEmailAddress">Email Id</label>
                                                <br />
                                                {values.emailId || "--"}
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div>
                                        {values.showDetail ?
                                            <div className="row">
                                                <div className="col-lg-5 mb-2">
                                                    <div className="my-profile-sucription-detail">
                                                        <span className="my-profile-sucription-text">
                                                            <IconButton color="inherit" size="small">
                                                                <LockIcon></LockIcon>
                                                            </IconButton>
                                                            <span className="my-profile-sucription-btn"> Contact details</span>
                                                            <SubmitButton
                                                                type="submit"
                                                                variant="contained"
                                                            >
                                                                SHOW DETAILS
                                                            </SubmitButton>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            :
                                            <div className="row">
                                                <div className="col-lg-5 mb-2">
                                                    <div className="my-profile-sucription-detail">
                                                        <span className="my-profile-sucription-text">
                                                            <IconButton color="inherit" size="small">
                                                                <LockIcon></LockIcon>
                                                            </IconButton>
                                                            <span className="my-profile-sucription-btn"> Unlock contact details</span>
                                                            <SubmitButton
                                                                variant="contained"
                                                                onClick={this._gotoSubecription}
                                                            >
                                                                UPGRADE NOW
                                                            </SubmitButton>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>


                                        }
                                    </div>
                                )}
                            </div>

                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);