import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import TextField from '@material-ui/core/TextField';
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as moment from "moment-timezone";
import { IconButton } from "@material-ui/core";
import LockIcon from '@material-ui/icons/Lock';

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );


class UserPersonal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            GanData: [],
            NadiData: [],
            NakashatraData: [],
            RasData: [],
            CharanData: [],
            MaritalStatusData: [],
            BloodGroupData: [],
            BodyTypeData: [],
            ComplexionData: [],
            SpectableData: [],
            AnyDisablityData: [],

            profileId: 0,
            userid: 0,
            dob: "",
            birthTime: "",
            age: "",
            birthPlace: "",
            placeOrigin: "",
            gan: 0,
            nadi: 0,
            nakshtra: 0,
            ras: 0,
            charan: 0,
            bloodGroup: 0,
            maritalStatus: 0,
            hightFeet: 0,
            hightInch: 0,
            bodyType: 0,
            complexion: 0,
            spectacle: 0,
            anyDisability: 0,
            isSubscriber: false,
            showDetail: false
        };
    }
    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        // Checking Subcription Details
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        debugger;
        if (userloginDetail.subcriptionId === null) {
            this.setState({ isSubscriber: false });
        } else {
            var CurrentDT = new Date();
            var CurrentDate = moment(CurrentDT).format('YYYY-MM-DD');
            var subEndDate = moment(userloginDetail.subEndDate).format('YYYY-MM-DD');
            if (moment(CurrentDate).isSameOrBefore(subEndDate))  // true
            {
                this.setState({ showDetail: true });
                console.log("Daa");
            }
        }
        //--------------------------------------------------------

        var GanData = SubMasterValue.SubMaster(MasterValue.Gan);
        var NadiData = SubMasterValue.SubMaster(MasterValue.Nadi);
        var NakashatraData = SubMasterValue.SubMaster(MasterValue.Nakshatra);
        var RasData = SubMasterValue.SubMaster(MasterValue.Ras);
        var CharanData = SubMasterValue.SubMaster(MasterValue.Charan);
        var MaritalStatusData = SubMasterValue.SubMaster(MasterValue.Marital_Status);
        var BloodGroupData = SubMasterValue.SubMaster(MasterValue.Blood_Group);
        var BodyTypeData = SubMasterValue.SubMaster(MasterValue.Body_Type);
        var ComplexionData = SubMasterValue.SubMaster(MasterValue.Complexion);
        var SpectableData = SubMasterValue.SubMaster(MasterValue.Spectacle);
        var AnyDisablityData = SubMasterValue.SubMaster(MasterValue.Any_Disability);

        this.setState({
            GanData: GanData,
            NadiData: NadiData,
            NakashatraData: NakashatraData,
            RasData: RasData,
            CharanData: CharanData,
            MaritalStatusData: MaritalStatusData,
            BloodGroupData: BloodGroupData,
            BodyTypeData: BodyTypeData,
            ComplexionData: ComplexionData,
            SpectableData: SpectableData,
            AnyDisablityData: AnyDisablityData,
        });

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));

        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId,
            viewerUserid: UserData.id,
            isSubscription: 0
        };

        UserLayoutService.GetUserPersonalById(passValue,
            (res) => {

                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    var date = moment(UserData.dob).format('YYYY-MM-DD');
                    //var DateDetail = new Date(date);

                    this.setState({
                        profileId: UserData.profileId,
                        userid: UserData.userid,
                        dob: date,
                        birthTime: UserData.birthTime,
                        age: UserData.age,
                        birthPlace: UserData.birthPlace,
                        placeOrigin: UserData.placeOrigin,
                        gan: UserData.gan,
                        nadi: UserData.nadi,
                        nakshtra: UserData.nakshtra,
                        ras: UserData.ras,
                        charan: UserData.charan,
                        bloodGroup: UserData.bloodGroup,
                        maritalStatus: UserData.maritalStatus,
                        hightFeet: UserData.hightFeet,
                        hightInch: UserData.hightInch,
                        bodyType: UserData.bodyType,
                        complexion: UserData.complexion,
                        spectacle: UserData.spectacle,
                        anyDisability: UserData.anyDisability,
                        isSubscriber: UserData.isSubscriber
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }


     // Go to the Subcription page
     _gotoSubecription = () => {
        this.props.history.push("/Pricing");
    }


    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        GanData: this.state.GanData,
                        NadiData: this.state.NadiData,
                        NakashatraData: this.state.NakashatraData,
                        RasData: this.state.RasData,
                        CharanData: this.state.CharanData,
                        MaritalStatusData: this.state.MaritalStatusData,
                        BloodGroupData: this.state.BloodGroupData,
                        BodyTypeData: this.state.BodyTypeData,
                        ComplexionData: this.state.ComplexionData,
                        SpectableData: this.state.SpectableData,
                        AnyDisablityData: this.state.AnyDisablityData,

                        profileId: this.state.profileId,
                        userid: this.state.userid,
                        dob: this.state.dob,
                        birthTime: this.state.birthTime,
                        age: this.state.age,
                        birthPlace: this.state.birthPlace,
                        placeOrigin: this.state.placeOrigin,
                        gan: this.state.gan,
                        nadi: this.state.nadi,
                        nakshtra: this.state.nakshtra,
                        ras: this.state.ras,
                        charan: this.state.charan,
                        bloodGroup: this.state.bloodGroup,
                        maritalStatus: this.state.maritalStatus,
                        hightFeet: this.state.hightFeet,
                        hightInch: this.state.hightInch,
                        bodyType: this.state.bodyType,
                        complexion: this.state.complexion,
                        spectacle: this.state.spectacle,
                        anyDisability: this.state.anyDisability,

                        isSubscriber: this.state.isSubscriber,
                        showDetail: this.state.showDetail
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-user-circle" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Horoscope</h5>
                                {values.isSubscriber === true ? (
                                    <div className="row">
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="inputEmailAddress">Date Of Birth</label>
                                            <Field
                                                type="date"
                                                id="dob"
                                                name="dob"
                                                //component={TextField}
                                                //defaultValue="2000-05-24"
                                                // timezone={Intl.DateTimeFormat().resolvedOptions().timeZone}
                                                inputlabelprops={{
                                                    shrink: true,
                                                }}
                                                placeholder="Date Of Birth"

                                                onChange={(event) => {
                                                    setFieldValue(
                                                        (values.dob = event.target.value)
                                                    );
                                                }}
                                                className={`form-control ${touched.dob && errors.dob
                                                    ? "is-invalid"
                                                    : ""
                                                    }`}
                                                disabled
                                                style={{
                                                    background: "#fff",
                                                    border: "none",
                                                    fontWeight: "bold"
                                                }}
                                            />
                                            <span className="currency-word">
                                                Date : MM/DD/YYYY
                                            </span>

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="inputEmailAddress">Birth Time</label>
                                            <input type="time"
                                                className="form-control"
                                                placeholder="Birth Time"
                                                //Component={TextField}
                                                //defaultValue="07:30"
                                                inputlabelprops={{
                                                    shrink: true,
                                                }}
                                                // InputProps={{
                                                //     step: 300, // 5 min
                                                // }}
                                                name="birthTime"
                                                value={values.birthTime}
                                                onChange={(event) => {
                                                    setFieldValue(
                                                        (values.birthTime = event.target.value)
                                                    );
                                                }}
                                                disabled
                                                style={{
                                                    background: "#fff",
                                                    border: "none",
                                                    fontWeight: "bold"
                                                }}
                                            />

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="birthPlace">Birth Place</label>
                                            <br />
                                            {values.birthPlace || "--"}

                                        </div>
                                    </div>
                                ) : (
                                    <div>
                                        {values.showDetail ?
                                            <div className="row">
                                                <div className="col-lg-5 mb-2">
                                                    <div className="my-profile-sucription-detail">
                                                        <span className="my-profile-sucription-text">
                                                            <IconButton color="inherit" size="small">
                                                                <LockIcon></LockIcon>
                                                            </IconButton>
                                                            <span className="my-profile-sucription-btn"> Contact details</span>
                                                            <SubmitButton
                                                                type="submit"
                                                                variant="contained"
                                                            >
                                                                SHOW DETAILS
                                                            </SubmitButton>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            :
                                            <div className="row">
                                                <div className="col-lg-5 mb-2">
                                                    <div className="my-profile-sucription-detail">
                                                        <span className="my-profile-sucription-text">
                                                            <IconButton color="inherit" size="small">
                                                                <LockIcon></LockIcon>
                                                            </IconButton>
                                                            <span className="my-profile-sucription-btn"> Unlock contact details</span>
                                                            <SubmitButton
                                                                variant="contained"
                                                                onClick={this._gotoSubecription}
                                                            >
                                                                UPGRADE NOW
                                                            </SubmitButton>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>


                                        }
                                    </div>
                                )}
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Gan</label>
                                        <br />
                                        {values.gan === null ? "--" :
                                            (values.GanData.filter(x => x.subMasterId === values.gan) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name}</span>
                                                )
                                            )
                                        }

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Nadi</label>
                                        <br />
                                        {values.nadi === null ? "--" :
                                            (values.NadiData.filter(x => x.subMasterId === values.nadi) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )
                                        }

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Nakashatra</label>
                                        <br />
                                        {values.nakshtra === null ? "--" :
                                            (values.NakashatraData.filter(x => x.subMasterId === values.nakshtra) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Ras</label>
                                        <br />
                                        {values.ras === null ? "--" :
                                            (values.RasData.filter(x => x.subMasterId === values.ras) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Charan</label>
                                        <br />
                                        {values.charan === null ? "--" :
                                            (values.CharanData.filter(x => x.subMasterId === values.charan) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Marital Status</label>

                                        <br />
                                        {values.maritalStatus === null ? "--" :
                                            (values.MaritalStatusData.filter(x => x.subMasterId === values.maritalStatus) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Hight-Feet</label>
                                        <br />
                                        {values.hightFeet || "--"}
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Hight-Inch</label>
                                        <br />
                                        {values.hightInch || "--"}

                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Blood Group</label>
                                        <br />
                                        {values.bloodGroup === null ? "--" :
                                            (values.BloodGroupData.filter(x => x.subMasterId === values.bloodGroup) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>

                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="bodyType">Body Type</label>
                                        <br />
                                        {values.bodyType === null ? "--" :
                                            (values.BodyTypeData.filter(x => x.subMasterId === values.bodyType) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )
                                        }

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Complexion</label>
                                        <br />
                                        {values.complexion === null ? "--" :
                                            (values.ComplexionData.filter(x => x.subMasterId === values.complexion) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="spectacle">Spectable</label>
                                        <br />
                                        {values.spectacle === null ? "--" :
                                            (values.SpectableData.filter(x => x.subMasterId === values.spectacle) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="anyDisability">Any Disablity</label>
                                        <br />
                                        {values.anyDisability === null ? "--" :
                                            (values.AnyDisablityData.filter(x => x.subMasterId === values.anyDisability) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name || "--"}</span>
                                                )
                                            )}

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserPersonal);
