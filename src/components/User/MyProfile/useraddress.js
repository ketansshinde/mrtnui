import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";

import { fetchCountry } from "../../../action/AdminAction/CountryAction";
import { GetCountry } from "../../../reducer/AdminReducer/CountryReducer";

import { fetchState } from "../../../action/AdminAction/StateAction";
import { GetState } from "../../../reducer/AdminReducer/StateReducer";

import { fetchCity } from "../../../action/AdminAction/CityAction";
import { GetCity } from "../../../reducer/AdminReducer/CityReducer";

import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { IconButton } from "@material-ui/core";
import RoomIcon from '@material-ui/icons/Room';

const mapStateToProps = (state) => ({
    CountryData: GetCountry(state.CountryReducer),
    stateData: GetState(state.StateReducer),
    CityData: GetCity(state.CityReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchCountry: fetchCountry,
            fetchState: fetchState,
            fetchCity: fetchCity,
        },
        dispatch
    );



class UserAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userRegistrationId: 0,
            userid: 0,
            addressDetail: "",
            country: 0,
            state: 0,
            area: "",
            city: 0,
            pincode: 0,
            registrationDate: "",
            expirationDate: "",
            activationDate: "",
            createdDate: "",
            modifiedDate: "",
            user: ""
        };
    }
    componentDidMount() {
        const { fetchCountry } = this.props;
        fetchCountry();

        const { fetchState } = this.props;
        fetchState();

        const { fetchCity } = this.props;
        fetchCity();

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));


        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));
        
        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId
        };

        UserLayoutService.GetUserAddressDetail(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        userRegistrationId: UserData.userRegistrationId,
                        userid: UserData.userid,
                        addressDetail: UserData.addressDetail,
                        country: UserData.country,
                        state: UserData.state,
                        area: UserData.area,
                        city: UserData.city,
                        pincode: UserData.pincode,
                        // registrationDate: UserData.userRegistrationId,
                        // expirationDate: UserData.userRegistrationId,
                        // activationDate: UserData.userRegistrationId,
                        // createdDate: UserData.userRegistrationId,
                        // modifiedDate: UserData.userRegistrationId,
                        // user: UserData.userRegistrationId,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        userRegistrationId: this.state.userRegistrationId,
                        userid: this.state.userid,
                        addressDetail: this.state.addressDetail,
                        country: this.state.country,
                        state: this.state.state,
                        area: this.state.area,
                        city: this.state.city,
                        pincode: this.state.pincode,
                        registrationDate: this.state.registrationDate,
                        expirationDate: this.state.expirationDate,
                        activationDate: this.state.activationDate,
                        createdDate: this.state.createdDate,
                        modifiedDate: this.state.modifiedDate,
                        user: this.state.user,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                           <RoomIcon></RoomIcon>
                                        </IconButton>
                                    </span>
                                    Address Details</h5>
                                <div className="row">

                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="country">Country </label>
                                        <br />

                                        {values.country === null || values.country === undefined ? "--" :
                                            (this.props.CountryData.filter(x => x.countryId == values.country) || []).map(
                                                (_CountryData) => (
                                                    <span>  {_CountryData.countryName || "--"} </span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="state">State</label>
                                        <br />

                                        {values.state === null || values.state === undefined ? "--" :
                                            (this.props.stateData.filter(x => x.stateId == values.state) || []).map(
                                                (_stateData) => (
                                                    <span>   { _stateData.stateName}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="city">City</label>
                                        <br />

                                        {values.city === null || values.city === undefined ? "--" :
                                            (this.props.CityData.filter(x => x.cityId == values.city) || []).map(
                                                (_CityData) => (
                                                    <span> { _CityData.cityName || "--"}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="AddressDetail">Address</label>
                                        <br />

                                        {values.addressDetail || "--"}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="pincode">Pincode</label>
                                        <br />

                                        {values.pincode || "--"}

                                    </div>
                                    {/* <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Registration Date</label>
                                        <input type="text" className="form-control" placeholder="Email Id" />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Expiration Date</label>
                                        <input type="text" className="form-control" placeholder="Email Id" />
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Activation Date</label>
                                        <input type="text" className="form-control" placeholder="Email Id" />
                                    </div> */}
                                </div>
                            </div>
                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserAddress);
