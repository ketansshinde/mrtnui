import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";

import { Formik, Form, Field, ErrorMessage } from "formik";

import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import { CurrencyWordFormat } from "../CommonComponent";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );


class UserOccupation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            EducationData: [],
            BranchData: [],
            occupationId: 0,
            userid: 0,
            educationId: 0,
            branchId: 0,
            otherEdu: "",
            occupation: "",
            companyName: "",
            designation: "",
            monthlyIncome: "",
            companyAddress: "",
            contactNo: "",
        };
    }
    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var EducationData = SubMasterValue.SubMaster(MasterValue.Education);
        var BranchData = SubMasterValue.SubMaster(MasterValue.Branch);


        this.setState({
            EducationData: EducationData,
            BranchData: BranchData,
        });

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));
        
        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId
        };

        UserLayoutService.GetUserOccupation(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        occupationId: UserData.occupationId,
                        userid: UserData.userid,
                        educationId: UserData.educationId,
                        branchId: UserData.branchId,
                        otherEdu: UserData.otherEdu,
                        occupation: UserData.occupation,
                        companyName: UserData.companyName,
                        designation: UserData.designation,
                        monthlyIncome: UserData.monthlyIncome,
                        companyAddress: UserData.companyAddress,
                        contactNo: UserData.contactNo,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        EducationData: this.state.EducationData,
                        BranchData: this.state.BranchData,
                        occupationId: this.state.occupationId,
                        userid: this.state.userid,
                        educationId: this.state.educationId,
                        branchId: this.state.branchId,
                        otherEdu: this.state.otherEdu,
                        occupation: this.state.occupation,
                        companyName: this.state.companyName,
                        designation: this.state.designation,
                        monthlyIncome: this.state.monthlyIncome,
                        companyAddress: this.state.companyAddress,
                        contactNo: this.state.contactNo,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Education &amp; Occupation</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="educationId">Education</label>
                                        <br />

                                        {values.educationId === null || values.educationId === 0 ? "--" :
                                            (values.EducationData.filter(x => x.subMasterId === values.educationId) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="branchId">Branch</label>
                                        <br />
                                        {values.branchId === null || values.branchId === 0 ? "--" :
                                            (values.BranchData.filter(x => x.subMasterId === values.branchId) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name}</span>
                                                )
                                            )
                                        }
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="OtherEducation">Other Education</label>
                                        <br />
                                        {values.otherEdu || "--"}
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="occupation">Occupation</label>
                                        <br />
                                        {values.occupation || "--"}

                                    </div>
                                </div>
                                
                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Company Name</label>
                                        <br />
                                        {values.companyName || "--"}


                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="designation">Designation</label>
                                        <br />
                                        {values.designation || "--"}


                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="monthlyIncome">Monthly</label>
                                        <br />
                                        {values.monthlyIncome || "--"}

                                        <span className="currency-word">
                                            (  <CurrencyWordFormat
                                                state={values.monthlyIncome || "--"}
                                            ></CurrencyWordFormat>)
                                        </span>

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="inputEmailAddress">Company Address</label>
                                        <br />
                                        {values.companyAddress || "--"}
                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="contactNo">Contact No.</label>
                                        <br />
                                        {values.contactNo || "--"}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserOccupation);