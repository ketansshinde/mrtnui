import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { CasteValue } from "../../../common/CasteValue";
import { SubCasteValue } from "../../../common/SubCasteValue";
import { IconButton } from "@material-ui/core";


const mapStateToProps = (state) => ({
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );


class UserReligion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            RelationData: [],
            CasteData: [],
            SubCasteData: [],
            MotherTongueData: [],
            ManglikData: [],
            PatrickaData: [],
            InterCasteData: [],
            FamilyValuesData: [],
            FamilyValueDetailData: [],

            patrikaId: 0,
            userid: 0,
            religion: 0,
            caste: 0,
            subCaste: 0,
            motherTongue: 0,
            manglik: 0,
            patrikRequired: 0,
            interCaste: 0,
            familyValues: 0,
            familyValuesDetail: 0,

        };
    }
    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var RelationData = SubMasterValue.SubMaster(MasterValue.Religion);
        // var CasteData = SubMasterValue.SubMaster(MasterValue.caste);
        //var SubCasteData = SubMasterValue.SubMaster(MasterValue.sub_caste);
        var MotherTongueData = SubMasterValue.SubMaster(MasterValue.Mother_Tongue);
        var ManglikData = SubMasterValue.SubMaster(MasterValue.Manglik);
        var PatrickaData = SubMasterValue.SubMaster(MasterValue.Patricka);
        var InterCasteData = SubMasterValue.SubMaster(MasterValue.Inter_caste);
        var FamilyValuesData = SubMasterValue.SubMaster(MasterValue.Family_values);
        var FamilyValueDetailData = SubMasterValue.SubMaster(MasterValue.Family_Value_Detail);
        var CasteData = CasteValue.Caste();

        var SubCasteData = SubCasteValue.SubCaste();


        this.setState({
            RelationData: RelationData,
            CasteData: CasteData,
            SubCasteData: SubCasteData,
            MotherTongueData: MotherTongueData,
            ManglikData: ManglikData,
            PatrickaData: PatrickaData,
            InterCasteData: InterCasteData,
            FamilyValuesData: FamilyValuesData,
            FamilyValueDetailData: FamilyValueDetailData
        });

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));
        
        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId
        };

        UserLayoutService.GetUserReligionById(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        patrikaId: UserData.patrikaId,
                        userid: UserData.userid,
                        religion: UserData.religion,
                        caste: UserData.caste,
                        subCaste: UserData.subCaste,
                        motherTongue: UserData.motherTongue,
                        manglik: UserData.manglik,
                        patrikRequired: UserData.patrikRequired,
                        interCaste: UserData.interCaste,
                        familyValues: UserData.familyValues,
                        familyValuesDetail: UserData.familyValuesDetail,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        RelationData: this.state.RelationData,
                        CasteData: this.state.CasteData,
                        SubCasteData: this.state.SubCasteData,
                        MotherTongueData: this.state.MotherTongueData,
                        ManglikData: this.state.ManglikData,
                        PatrickaData: this.state.PatrickaData,
                        InterCasteData: this.state.InterCasteData,
                        FamilyValuesData: this.state.FamilyValuesData,
                        FamilyValueDetailData: this.state.FamilyValueDetailData,

                        patrikaId: this.state.patrikaId,
                        userid: this.state.userid,
                        religion: this.state.religion,
                        caste: this.state.caste,
                        subCaste: this.state.subCaste,
                        motherTongue: this.state.motherTongue,
                        manglik: this.state.manglik,
                        patrikRequired: this.state.patrikRequired,
                        interCaste: this.state.interCaste,
                        familyValues: this.state.familyValues,
                        familyValuesDetail: this.state.familyValuesDetail,

                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <div className="layout-margin">
                                <div className="card-body">
                                    <h5 className="card-title card-title-set">
                                        <span className="padding-right">
                                            <IconButton color="inherit" size="small">
                                                <i className="fa fa-globe" aria-hidden="true"></i>
                                            </IconButton>
                                        </span>
                                    My Religion</h5>

                                    <div className="row">
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="religion">Religion</label>
                                            <br />
                                            {values.religion === null || values.religion === undefined ? "--" :
                                                (values.RelationData.filter(x => x.subMasterId === values.religion) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="caste">Caste</label>
                                            <br />
                                            {values.caste === null || values.caste === undefined ? "--" :
                                                (values.CasteData.filter(x => x.casteId === values.caste) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.casteName}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="subCaste">Sub-Caste</label>
                                            <br />
                                            {values.subCaste === null || values.subCaste === undefined ? "--" :
                                                (values.SubCasteData.filter(x => x.subCasteId === values.subCaste) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.subCasteName}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="motherTongue">Mother Tongue</label>
                                            <br />
                                            {values.motherTongue === null || values.motherTongue === undefined ? "--" :
                                                (values.MotherTongueData.filter(x => x.subMasterId === values.motherTongue) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="manglik">Manglik</label>
                                            <br />
                                            {values.manglik === null || values.manglik === undefined ? "--" :
                                                (values.ManglikData.filter(x => x.subMasterId === values.manglik) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="inputEmailAddress">Patrika Required</label>
                                            <br />
                                            {values.patrikRequired === null || values.patrikRequired === undefined ? "--" :
                                                (values.PatrickaData.filter(x => x.subMasterId === values.patrikRequired) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="interCaste">Inter Caste</label>
                                            <br />
                                            {values.interCaste === null || values.interCaste === undefined ? "--" :
                                                (values.InterCasteData.filter(x => x.subMasterId === values.interCaste) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="familyValues">Family Value</label>
                                            <br />
                                            {values.familyValues === null || values.familyValues === undefined ? "--" :
                                                (values.FamilyValuesData.filter(x => x.subMasterId === values.familyValues) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                        <div className="col-lg-4 mb-2">
                                            <label className="medium" htmlFor="familyValuesDetail">Family Value details</label>
                                            <br />
                                            {values.familyValuesDetail === null || values.familyValuesDetail === undefined ? "--" :
                                                (values.FamilyValueDetailData.filter(x => x.subMasterId === values.familyValuesDetail) || []).map(
                                                    (_subMasterData) => (
                                                        <span> {_subMasterData.name}</span>
                                                    )
                                                )}

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserReligion);