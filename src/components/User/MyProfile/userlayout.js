import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import UserProfile from "./userprofile";
import UserAddress from "./useraddress";
import UserPersonal from "./userpersonal";
import UserReligion from "./userreligion";
import UserOccupation from "./useroccupation";
import UserLifeStyle from "./userlifestyle";
import UserFamilyBG from "./userfamilybg";
import UserExpectation from "./userexpectation";
import { IconButton } from "@material-ui/core";
import { Global_var } from "../../../global/global_var";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as moment from "moment-timezone";
import StarIcon from '@material-ui/icons/Star';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import MessageIcon from '@material-ui/icons/Message';
import VisibilityIcon from '@material-ui/icons/Visibility';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import { CurrencyValue } from "../../User/CommonComponent";
import { FetchGalleryPhoto } from "../../../action/UserAction/UserSettingAction";
import { SendMessageDetail } from "../../../action/UserAction/SendMessageAction";

import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import "./myprofile.css";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            SendMessageDetail: SendMessageDetail,
            FetchGalleryPhoto: FetchGalleryPhoto
        },
        dispatch
    );

class UserLayout extends Component {
    constructor(props) {

        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            userid: "",
            profilePhotoname: "",
            userProfileData: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        //const username = UserData.firstName + " " + UserData.lastName;
        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));

        this.setState({
            name: userProfileData.firstName + " " + userProfileData.lastName,
            profileUniqueId: userProfileData.profileUniqueId,
            gender: userProfileData.gender,
            userid: userProfileData.userid,
            profilePhotoname: userProfileData.profilePhotoname,
            userProfileData: userProfileData
        });

        this._fatchGalleryPhoto(userProfileData);

    }

    _fatchGalleryPhoto(profiledata) {
        this.setState({ submitting: true });
        var passValue = {
            userid: profiledata.profileUserid,
            profileUniqueId: profiledata.profileUniqueId,
        }

        this.props.FetchGalleryPhoto(passValue,
            (res) => {

                if (res.success) {
                    this.setState({ galleryPhoto: res.responseList, submitting: false });
                }
                else {
                    error("Something wents worng.", errorNotification);
                    this.setState({ submitting: false });
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    // Get profile shortlist
    _profileShortlist(profiledata) {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: profiledata.userid,
            shortlistedUserid: UserData.id,
        }
        this.props.SaveProfileShortlist(passValue,
            (res) => {

                if (res.data.success) {
                    success("Profile Shortlisted.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    // Interest sent profile

    _profileInterest(profiledata) {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: profiledata.userid,
            interestUserid: UserData.id,
        }
        this.props.SaveProfileInterest(passValue,
            (res) => {

                if (res.data.success) {
                    success("Interest Sent.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        profilePhoto: this.state.profilePhoto,
                        name: this.state.name,
                        profileUniqueId: this.state.profileUniqueId,
                        gender: this.state.gender,
                        userid: this.state.userid,
                        profilePhotoname: this.state.profilePhotoname,
                        userProfileData: this.state.userProfileData,
                        galleryPhoto: this.state.galleryPhoto,
                        userGalleryPhoto: "",
                    }}
                    validationSchema={this._countrySchema}
                    onSubmit={this._handleSubmit}
                >
                    {({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                        <Form>
                            <div>
                                <section className="py-3">
                                    <div className="main-layout">
                                        <div className="row my-profile-layout">
                                            <div className="col-lg-5 mb-2 text_align_center">
                                                <div>
                                                    <img src={values.userGalleryPhoto === "" ? (values.userProfileData.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.userProfileData.profilePhotoname : (this.state.gender == 23 ? male : female)) : values.userGalleryPhoto} className="view-profile-image-one" alt="logo">
                                                    </img>
                                                </div>
                                            </div>
                                            <div className="col-lg-7">
                                                <div className="padding-top">

                                                    <div className="row">
                                                        <div className="col-lg-8 mb-2">
                                                            <div>
                                                                <span className="text_align_Left view-profile-Name">
                                                                    {values.userProfileData.firstName + " " + values.userProfileData.lastName} {" "} <span className="view-profile-uniquecode">({values.userProfileData.profileUniqueId})</span>
                                                                </span>
                                                            </div>
                                                            <div>
                                                                <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span className="profile-textfont">Profile created by : </span>
                                                                </label> {values.userProfileData.profileCreated + " "}
                                                                <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span className="profile-textfont">| Last Active : </span>
                                                                </label> {moment(values.userProfileData.lastLogin).format('DD MMM YYYY')}
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-4 mb-2">
                                                            <span className="dropdown text_align_right like-member-dropdownlist">
                                                                <label className="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <span className="like-this-member">
                                                                        <IconButton color="inherit" size="small">
                                                                            <ThumbUpIcon></ThumbUpIcon>
                                                                        </IconButton> Like this member?{" "}
                                                                    </span>
                                                                </label>
                                                                <div id="profileaction" className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                                    <a className="dropdown-item" href="#" onClick={(event) => {
                                                                        this._profileInterest(values.userProfileData);
                                                                    }}>
                                                                        <span className="padding-right" >
                                                                            <IconButton color="inherit" size="small">
                                                                                <LabelImportantIcon></LabelImportantIcon>
                                                                            </IconButton>
                                                                        </span>
                                                                                            Send Interest
                                                                                            </a>
                                                                    <a className="dropdown-item" href="#" onClick={(event) => {
                                                                        this._sendMessage(values.userProfileData, 2);
                                                                    }}>
                                                                        <span className="padding-right">
                                                                            <IconButton color="inherit" size="small">
                                                                                <MessageIcon></MessageIcon>
                                                                            </IconButton>
                                                                        </span>
                                                                                        Send Message
                                                                                        </a>
                                                                    <a className="dropdown-item" href="#" onClick={(event) => {
                                                                        this._UserProfileDetails(values.userProfileData);
                                                                    }}>
                                                                        <span className="padding-right">
                                                                            <IconButton color="inherit" size="small">
                                                                                <VisibilityIcon></VisibilityIcon>
                                                                            </IconButton>
                                                                        </span>
                                                                                            View Profile
                                                                                            </a>
                                                                    <a className="dropdown-item" href="#" onClick={(event) => {
                                                                        this._profileShortlist(values.userProfileData);
                                                                    }}>
                                                                        <span className="padding-right">
                                                                            <IconButton color="inherit" size="small">
                                                                                <StarIcon></StarIcon>
                                                                            </IconButton>
                                                                        </span>
                                                                                            Short list this Profile
                                                                                        </a>
                                                                </div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6 details-mb">
                                                            <span className="profile-textfont"> Caste:</span> <span className="profile-textfontName"> {values.userProfileData.caste || "--"}</span>
                                                        </div>
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont"> Sub-Caste:</span><span className="profile-textfontName"> {values.userProfileData.subCaste1 || "--"}</span>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont"> Education:</span>  <span className="profile-textfontName">   {values.userProfileData.education || "--"}</span>
                                                        </div>
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont"> Branch:</span> <span className="profile-textfontName">  {values.userProfileData.branch || "--"}</span>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont">Occupation:</span>  <span className="profile-textfontName"> {values.userProfileData.occupation || "--"}</span>
                                                        </div>
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont"> Monthly Income:</span> <span className="profile-textfontName"> ₹  <CurrencyValue state={values.userProfileData.monthlyIncome || 0}></CurrencyValue></span>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont"> Age:</span><span className="profile-textfontName">  {values.userProfileData.age || "--"} yr</span>
                                                        </div>
                                                        <div className="col-lg-6 mb-2 details-mb">
                                                            <span className="profile-textfont"> Height-Feet:</span> <span className="profile-textfontName"> {values.userProfileData.hightFeet || "--"}' {values.userProfileData.hightInch || "--"}''</span>
                                                        </div>
                                                    </div>
                                                    <div className="dropdown-divider"></div>
                                                    <div className="view-profile-image-border">
                                                        <div className="row">
                                                            <div className="col-lg-12 mb-2 details-mb text_align_center">
                                                                <span className="like-this-member">Photo Gallery</span>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-lg-12 text_align_center">
                                                                {/* <img src={values.userProfileData.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.userProfileData.profilePhotoname : (this.state.gender == 23 ? male : female)}
                                                                    className="view-profile-image-fav" alt="logo"
                                                                    onClick={(event) => {
                                                                        setFieldValue((values.userProfileData.profilePhotoname = Global_var.URL_USER_PROFILE_PHOTO + values.userProfileData.profilePhotoname));
                                                                    }}
                                                                ></img> */}
                                                                {(values.galleryPhoto || []).map(
                                                                    (item) => {

                                                                        return (
                                                                            <img src={values.documentName !== null ? Global_var.URL_USER_GALLERY + item.documentName : (this.state.gender == 22 ? male : female)}
                                                                                className="view-profile-image-fav" alt="logo"
                                                                                onClick={(event) => {
                                                                                    setFieldValue((values.userGalleryPhoto = Global_var.URL_USER_GALLERY + item.documentName));
                                                                                }}
                                                                            ></img>
                                                                        );
                                                                    }
                                                                )}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            {/* <div className="col-lg-3">
                                                <div className="card">
                                                    <div className="card-body">
                                                        <h5 className="card-title card-title-set text_align_center">{values.name}</h5>
                                                        <h6 className="card-subtitle mb-2 text_align_center padding-bottum" >{values.profileUniqueId}</h6>
                                                        <div className="text_align_center">
                                                            <img src={values.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.profilePhotoname : (this.state.gender == 22 ? male : female)} className="profile-image" alt="logo" width="250" height="250"></img>
                                                        </div>
                                                        <div className="text_align_center layout-margin">
                                                            Profile Photo
                                                        </div>
                                                        
                                                        <div className="layout-margin hyperlink" onClick={this._changeprofilephoto}>
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-info-circle" aria-hidden="true"></i>
                                                                </IconButton></span>
                                                            <span className="text_align_left">About Me</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-map-marker" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left ">Address Details</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-user-circle" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left " >Horoscope</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-globe" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left " >Religious Background</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-graduation-cap" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left " >Education &amp; Occupation</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-life-ring" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left " >Life Style</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-heartbeat" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left ">Family Details</span>
                                                        </div>
                                                        <div className="layout-margin hyperlink">
                                                            <span className="padding-right">
                                                                <IconButton color="inherit" size="small">
                                                                    <i className="fa fa-outdent" aria-hidden="true"></i>
                                                                </IconButton>
                                                            </span>
                                                            <span className="text_align_left " >Expectation</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> */}
                                            <div className="col-lg-9 my-profile-sub-detail">
                                                <UserProfile {...this.props}></UserProfile>
                                                <UserAddress {...this.props}></UserAddress>
                                                <UserPersonal {...this.props}></UserPersonal>
                                                <UserReligion {...this.props}></UserReligion>
                                                <UserOccupation {...this.props}></UserOccupation>
                                                <UserLifeStyle {...this.props}></UserLifeStyle>
                                                <UserFamilyBG {...this.props}></UserFamilyBG>
                                                <UserExpectation {...this.props}></UserExpectation>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </Form>
                    )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserLayout);

