import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { CryptoCode } from "../../../common/cryptoCode";
import { MasterValue } from "../../../global/global_var";
import { SubMasterValue } from "../../../common/SubMasterValue";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { UserLayoutService } from "../../../services/UserService/UserLayoutService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import { IconButton } from "@material-ui/core";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );


class UserFamilyBG extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Familyoccupation: [],
            userFamilyBackgroundId: 0,
            userid: 0,
            fatherOccupation: 0,
            motherOccupation: 0,
            noOfBrother: 0,
            noOfBrotherMarried: 0,
            noOfSister: 0,
            noOfSisterMarried: 0,
        };
    }
    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));

        var Familyoccupation = SubMasterValue.SubMaster(MasterValue.Family_occupation);

        this.setState({
            Familyoccupation: Familyoccupation,
        });

        var userProfileData = JSON.parse(CryptoCode.decryption(localStorage.getItem("getUserProfile")));
        
        var passValue = {
            userid: userProfileData.profileUserid,
            profileuniqueid: userProfileData.profileUniqueId
        };

        UserLayoutService.GetUserFamilyBackGround(passValue,
            (res) => {
                
                if (res.data.success) {
                    var UserData = res.data.responseList[0] || [];
                    this.setState({
                        patrikaId: UserData.patrikaId,
                        userFamilyBackgroundId: UserData.userFamilyBackgroundId,
                        userid: UserData.userid,
                        fatherOccupation: UserData.fatherOccupation,
                        motherOccupation: UserData.motherOccupation,
                        noOfBrother: UserData.noOfBrother,
                        noOfBrotherMarried: UserData.noOfBrotherMarried,
                        noOfSister: UserData.noOfSister,
                        noOfSisterMarried: UserData.noOfSisterMarried,
                    });
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }
    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        Familyoccupation: this.state.Familyoccupation,
                        patrikaId: this.state.patrikaId,
                        userFamilyBackgroundId: this.state.userFamilyBackgroundId,
                        userid: this.state.userid,
                        fatherOccupation: this.state.fatherOccupation,
                        motherOccupation: this.state.motherOccupation,
                        noOfBrother: this.state.noOfBrother,
                        noOfBrotherMarried: this.state.noOfBrotherMarried,
                        noOfSister: this.state.noOfSister,
                        noOfSisterMarried: this.state.noOfSisterMarried,
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div className="layout-margin">
                            <div className="card-body">
                                <h5 className="card-title card-title-set">
                                    <span className="padding-right">
                                        <IconButton color="inherit" size="small">
                                            <i className="fa fa-heartbeat" aria-hidden="true"></i>
                                        </IconButton>
                                    </span>
                                    Family Background</h5>

                                <div className="row">
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Father">Father</label>
                                        <br />
                                        {values.fatherOccupation === null  || values.fatherOccupation === undefined? "--" :
                                            (values.Familyoccupation.filter(x => x.subMasterId === values.fatherOccupation) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name}</span>
                                                )
                                            )}

                                    </div>
                                    <div className="col-lg-4 mb-2">
                                        <label className="medium" htmlFor="Mother">Mother</label>
                                        <br />
                                        {values.motherOccupation === null || values.motherOccupation === undefined ? "--" :
                                            (values.Familyoccupation.filter(x => x.subMasterId === values.motherOccupation) || []).map(
                                                (_subMasterData) => (
                                                    <span> {_subMasterData.name}</span>
                                                )
                                            )}
                                    </div>
                                </div>
                                
                                <div className="row">
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfBrother">No. of Bother</label>
                                        <br />
                                        {values.noOfBrother || "--"}

                                    </div>
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfBrotherMarried">Marital Status of Brothers</label>
                                        <br />
                                        {values.noOfBrotherMarried || "--"}

                                    </div>
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfSister">No. of Sister</label>
                                        <br />
                                        {values.noOfSister || "--"}

                                    </div>
                                    <div className="col-lg-3 mb-2">
                                        <label className="medium" htmlFor="noOfSisterMarried">Marital Status of Sisters</label>
                                        <br />
                                        {values.noOfSisterMarried || "--"}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="dropdown-divider"></div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserFamilyBG);