import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { loginService } from "../../../services/loginService";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import {
    CryptoCode,
} from "../../../common/cryptoCode";
import background from "./../../../assets/images/DSC_5535.jpg";

class ContactPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            ipaddress: "168.192.86.12",
            roleid: "1",
            DateTimeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        };
    }

    componentDidMount() {

    }

    _userAccountSchema = Yup.object().shape({
        username: Yup.string()
            .required('Username is required!')
            .email('Invalid email address format!'),
        password: Yup.string().required("Password is required!")
    });


    _handleSubmit = (values, { resetForm }, actions) => {
        var passValue = {
            username: values.username,
            password: values.password,
            ipaddress: values.ipaddress,
            roleid: 1
        };
        loginService.Authenticate(passValue,
            (res) => {
                
                if (res.status === "success") {
                    localStorage.setItem("userData", CryptoCode.encryption(JSON.stringify(res)));
                    localStorage.setItem("token", CryptoCode.encryption(res.token));
                    this.props.history.push("/admindashboard");
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <div>
                    <section className="py-3">

                        <Formik
                            enableReinitialize
                            initialValues={{
                                username: this.state.username,
                                password: this.state.password,
                                ipaddress: this.state.ipaddress,
                                roleid: this.state.roleid,
                            }}
                            validationSchema={this._userAccountSchema}
                            onSubmit={this._handleSubmit}
                        >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                            <Form>
                                <div className="main-layout">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="card">
                                                    <div className="card-body">
                                                        <h5 className="card-title card-title-set text_align_left">Contact</h5>
                                                        <div className="row">
                                                            <div className="col-lg-4 mb-2">
                                                                <b>Our Address</b>
                                                                <p>A108 Adam Street, New York, NY 535022</p>
                                                            </div>
                                                            <div className="col-lg-4 mb-2">
                                                                <b>Email Us</b>
                                                                <p>info@example.com<br />contact@example.com</p>
                                                            </div>
                                                            <div className="col-lg-4 mb-2">
                                                                <b>Call Us</b>
                                                                <p>+1 5589 55488 55<br />+1 6678 254445 41</p>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                        <div className="row">
                                                            <div className="col-lg-3 mb-2">
                                                                <label className="medium" htmlFor="inputEmailAddress">Name</label>
                                                                <input type="text" className="form-control" placeholder="name" />
                                                            </div>
                                                            <div className="col-lg-3 mb-2">
                                                                <label className="medium" htmlFor="inputEmailAddress">Email -ID</label><br />
                                                                <input type="text" className="form-control" placeholder="Email -ID" />
                                                            </div>
                                                            <div className="col-lg-12 mb-2">
                                                                <label className="medium" htmlFor="inputEmailAddress">Subject</label>
                                                                <textarea className="form-control" rows="5" id="comment"></textarea>
                                                            </div>
                                                        </div>
                                                        <hr />
                                                        <div className="row">
                                                            <div className="col-lg-12 mb-2 text_align_center">
                                                                <SubmitButton
                                                                    type="submit"
                                                                    variant="contained"
                                                                >
                                                                    Register
                                                                </SubmitButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Form>
                        )}
                        </Formik>
                    </section>
                </div>
            </React.Fragment>
        );
    }
}
export default ContactPage;
