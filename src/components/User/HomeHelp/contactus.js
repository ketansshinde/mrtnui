import React, { Component } from 'react';
import {
    SubmitButton, CloseButton, LoginButton, InviteButton
} from "../../../assets/MaterialControl";
import FeedbackIcon from '@material-ui/icons/Feedback';
import { IconButton } from "@material-ui/core";
import LocationOnIcon from '@material-ui/icons/LocationOn';
import CallIcon from '@material-ui/icons/Call';
import MailIcon from '@material-ui/icons/Mail';
import "./homehelp.css";


class FeedBack extends Component {
    render() {
        return (
            <React.Fragment>
                <section className="py-3">
                    <div className="container">
                        <div className="main-layout home-main-layout">
                            <div class="row feedback-form">
                                <div class="col-lg-12">
                                    <div className="text_align_center">
                                        <h3>
                                            Contact us</h3>
                                    </div>
                                </div>
                                <div class="col-lg-4 ">
                                    <div className="dropdown-divider"></div>
                                    <div className="contact-box">
                                        <div className="text_align_center ">
                                            <IconButton color="inherit">
                                                <LocationOnIcon></LocationOnIcon>
                                            </IconButton>
                                        </div>
                                        <div className="text_align_center">
                                            Sinhgad Road , Pune - 411041
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div className="dropdown-divider"></div>
                                    <div className="contact-box">
                                        <div className="text_align_center">
                                            <IconButton color="inherit">
                                                <CallIcon></CallIcon>
                                            </IconButton>
                                        </div>
                                        <div className="text_align_center">
                                            +91 - 7620179104
                                    </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div className="dropdown-divider"></div>
                                    <div className="contact-box">
                                        <div className="text_align_center">
                                            <IconButton color="inherit">
                                                <MailIcon></MailIcon>
                                            </IconButton>
                                        </div>
                                        <div className="text_align_center">
                                            support@perfectmatch.com
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default FeedBack;