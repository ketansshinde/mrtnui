import React, { Component } from 'react';
import {
    SubmitButton, CloseButton, LoginButton, InviteButton
} from "../../../assets/MaterialControl";
import FeedbackIcon from '@material-ui/icons/Feedback';
import { IconButton } from "@material-ui/core";
import "./homehelp.css";

class FeedBack extends Component {
    render() {
        return (
            <React.Fragment>
                <section className="py-3">
                    <div className="container">
                        <div className="main-layout home-main-layout">
                            <div class="row feedback-form">
                                <div class="col-lg-12">
                                    <div className="text_align_center">
                                        <h3>
                                            <IconButton color="inherit">
                                                <FeedbackIcon></FeedbackIcon>
                                            </IconButton> {" "}Feedback</h3>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="txtName" class="form-control" placeholder="Your Name *" value="" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="txtEmail" class="form-control" placeholder="Your Email *" value="" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *" value="" />
                                    </div>
                                    <div class="form-group">
                                        <SubmitButton
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                        >
                                            Send Feedback
                                        </SubmitButton>


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea name="txtMsg" class="form-control" placeholder="Your Message *" style={{ width: "100%", height: "140px" }}></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default FeedBack;