import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { IconButton } from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';
import male from "../../../../assets/images/man.png";
import manblur from "../../../../assets/images/man-blur.png";
import manside from "../../../../assets/images/man-side.jpg";
import mangroup from "../../../../assets/images/man-group.jpg";
import manwatarmark from "../../../../assets/images/man-watarmark.png";


class ValidPhoto extends Component {
    constructor(props) {

        super(props);
        this.state = {
            gender: ""
        };
    }

    render() {
        return (
            <React.Fragment>

                <div className="row">
                    <div className="col-lg-4 text_align_center" style={{ padding: "40px" }}>
                        <div><b>What to avoid</b></div>
                        <div>Do not upload photos that look like any of these</div>
                    </div>
                    <div className="col-lg-2 text_align_center">
                        <img src={manblur} className="profile-image-avoid" alt="logo" width="230" height="230"></img>
                        <div>

                            Blur photos
                                                                </div>
                    </div>
                    <div className="col-lg-2 text_align_center">
                        <img src={manside} className="profile-image-avoid" alt="logo" width="230" height="230"></img>
                        <div>

                            Side face</div>
                    </div>
                    <div className="col-lg-2 text_align_center">
                        <img src={mangroup} className="profile-image-avoid" alt="logo" width="230" height="230"></img>
                        <div>

                            Group photo</div>
                    </div>
                    <div className="col-lg-2 text_align_center">
                        <img src={manwatarmark} className="profile-image-avoid watermark" alt="logo" width="230" height="230"></img>
                        <div>

                            Water-mark</div>
                    </div>

                </div>

            </React.Fragment>
        );
    }
}
export default ValidPhoto;
