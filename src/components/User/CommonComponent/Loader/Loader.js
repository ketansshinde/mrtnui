import React, { Component } from "react";
import "./Loader.css";

class Loader extends Component {
  // Default constructor in component

  constructor(props) {
    super(props);
  }
  render() {
    if (this.props.state === undefined || this.props.state === "") {
      return <div>
        <div className="overlay">
          <div className="overlay__inner">
            <div className="overlay__content">
              <span className="spinner"></span>
              <div>
                <h6>  Please wait . . .</h6>
              </div>
            </div>
          </div>
        </div>
      </div>;
    }
    else {
      return (
        <div>
          <div className="overlay">
            <div className="overlay__inner">
              <div className="overlay__content">
                <span className="spinner"></span>
                <div>
                  <h6> {this.props.state} . . .</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

export default Loader;
