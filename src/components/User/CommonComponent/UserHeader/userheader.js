import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../../common/cryptoCode";
import male from "../../../../assets/images/man.png";
import female from "../../../../assets/images/woman.png";
import { IconButton } from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from "redux";
import PersonIcon from '@material-ui/icons/Person';
import MessageIcon from '@material-ui/icons/Message';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import EmailIcon from '@material-ui/icons/Email';
import NotificationsIcon from '@material-ui/icons/Notifications';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { Global_var } from "../../../../global/global_var";
import { Formik, Form, Field, ErrorMessage } from "formik";
import HelpIcon from '@material-ui/icons/Help';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FilterListIcon from '@material-ui/icons/FilterList';
import BlockIcon from '@material-ui/icons/Block';
import SettingsBackupRestoreIcon from '@material-ui/icons/SettingsBackupRestore';
import "./userheader.css";

class UserHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profilePhoto: "",
            Userid: ""
        };
    }

    componentDidMount() {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        this.setState({ Userid: UserData.id, name: username, profileUniqueId: UserData.profileUniqueId, gender: UserData.gender });
        const userloginDetail = JSON.parse(CryptoCode.decryption(localStorage.getItem("userloginDetail")));
        this.setState({ profilePhoto: userloginDetail.documentName });

        // if (localStorage.getItem("ProfilePhoto") !== null) {
        //     const profilePhotoData = JSON.parse(CryptoCode.decryption(localStorage.getItem("ProfilePhoto")));
        //     if (profilePhotoData.length !== 0)
        //         this.setState({ profilePhoto: profilePhotoData[0].documentType + "," + profilePhotoData[0].documentBase });
        // }
    }

    _userhomepage = () => {
        this.props.history.push("/userhomepage");
    }

    _usersearch = () => {
        this.props.history.push("/usersearch");
    }

    _pricing = () => {
        this.props.history.push("/Pricing");
    }

    _editprofile = () => {
        this.props.history.push("/editprofile");
    }

    _changeprofilephoto = () => {
        this.props.history.push("/changeprofilephoto");
    }

    _sendmessage = () => {
        this.props.history.push("/sendmessage");
    }

    _orderDetail = () => {
        this.props.history.push("/orderdetail");
    }

    _profileViewer = () => {
        this.props.history.push("/profilevisitor");
    }

    _profileShortlist = () => {
        this.props.history.push("/profileshortlist");
    }

    _userhomepage = () => {
        this.props.history.push("/userhomepage");
    }

    _profileShortlistbyYou = () => {
        this.props.history.push("/profileshortlistbyyou");
    }
    _profilerequest = () => {
        this.props.history.push("/profilerequest");
    }
    _profileDecline = () => {
        this.props.history.push("/profiledecline");
    }
    _profileDeclineByYou = () => {
        this.props.history.push("/profiledeclinebyyou");
    }
    _message = () => {
        this.props.history.push("/message");
    }


    render() {
        return (
            <React.Fragment>

                <Formik
                    enableReinitialize
                    initialValues={{
                        name: this.state.name,
                        profilePhoto: this.state.profilePhoto,
                        Userid: this.state.Userid,
                        profileUniqueId: this.state.profileUniqueId
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <nav className="navbar navbar-expand-md navbar-dark bg-dark-header">
                            <a className="navbar-brand userheader-logo" onClick={this._userhomepage}>Sath<span>Sobat</span></a>
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <div className="collapse navbar-collapse" id="navbarsExample04">
                                <ul className="navbar-nav mr-auto li-padding">
                                    <li className="nav-item active">
                                        <a className="nav-link margin-right" onClick={this._userhomepage}>My Home<span className="sr-only">(current)</span></a>
                                    </li>
                                    <li className="nav-item active">
                                        <a className="nav-link margin-right" onClick={this._usersearch}>Search<span className="sr-only">(current)</span></a>
                                    </li>
                                    {/* <li className="nav-item active">
                                        <a className="nav-link margin-right" href="/userprofile">Matches<span className="sr-only">(current)</span></a>
                                    </li> */}
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle margin-right" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Message
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                            <a className="dropdown-item"  onClick={this._message}>
                                                <span className="padding-right">
                                                    <IconButton color="inherit" size="small">
                                                        <EmailIcon></EmailIcon>
                                                    </IconButton>
                                                </span>
                                            Message</a>
                                            <a className="dropdown-item" onClick={this._sendmessage}>
                                                <span className="padding-right">
                                                    <IconButton color="inherit" size="small">
                                                        <MessageIcon></MessageIcon>
                                                    </IconButton>
                                                </span>
                                            Sent Message</a>

                                        </div>
                                    </li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle margin-right" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Activities
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                            <a className="dropdown-item" onClick={this._profileViewer}>
                                                <span className="padding-right">   <IconButton color="inherit" size="small">
                                                    <VisibilityIcon></VisibilityIcon>
                                                </IconButton>
                                                </span>
                                            Who Visited Profile</a>
                                            <a className="dropdown-item" onClick={this._profileShortlist}>
                                                <span className="padding-right">    <IconButton color="inherit" size="small">
                                                    <PersonAddIcon></PersonAddIcon>
                                                </IconButton>
                                                </span>
                                            Who Shortlisted You</a>
                                            <a className="dropdown-item" href="#" onClick={this._profileShortlistbyYou}>
                                                <span className="padding-right">   <IconButton color="inherit" size="small">
                                                    <FilterListIcon></FilterListIcon>
                                                </IconButton>
                                                </span>
                                            Shortlisted By You</a>
                                            <a className="dropdown-item" href="#" onClick={this._profilerequest}>
                                                <span className="padding-right">   <IconButton color="inherit" size="small">
                                                    <LabelImportantIcon></LabelImportantIcon>
                                                </IconButton>
                                                </span>
                                            Who's Interested</a>
                                            <a className="dropdown-item" href="#" onClick={this._profileDecline}>
                                                <span className="padding-right">   <IconButton color="inherit" size="small">
                                                    <BlockIcon></BlockIcon>
                                                </IconButton>
                                                </span>
                                            Who's Declined</a>
                                            <a className="dropdown-item" href="#" onClick={this._profileDeclineByYou}>
                                                <span className="padding-right">   <IconButton color="inherit" size="small">
                                                    <SettingsBackupRestoreIcon></SettingsBackupRestoreIcon>
                                                </IconButton>
                                                </span>
                                            Declined By You</a>
                                        </div>
                                    </li>

                                    <li className="nav-item active">
                                        <a className="nav-link" onClick={this._pricing}>Purchase Plan <span className="sr-only">(current)</span></a>
                                    </li>
                                </ul>
                                <div className="form-inline my-2 my-md-0">
                                    <span onClick={this._pricing}>
                                        <span className="padding-right">
                                            <IconButton>
                                                <NotificationsIcon></NotificationsIcon>
                                            </IconButton>
                                        </span>
                                    </span>
                                    <ul className="navbar-nav ml-auto ml-md-0">
                                        <li className="nav-item dropdown">
                                            <a className="logout-header" style={{ color: "#fff", textDecoration: "none" }} id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {/* <i className="far fa-user-circle" aria-hidden="true"></i> */}
                                                <img src={values.profilePhoto !== null ? Global_var.URL_USER_PROFILE_PHOTO + values.profilePhoto : (this.state.gender == 22 ? male : female)} className="header-avtar" alt="logo"></img> <span className="header-profile-name"> {this.state.name}</span>
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown" style={{ boxShadow: "2px 4px 2px #5a545445" }}>
                                                <a className="dropdown-item" onClick={this._editprofile}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <PersonIcon></PersonIcon>
                                                        </IconButton>
                                                    </span>My Profile ({values.profileUniqueId})</a>
                                                {/* Profile : {this.state.name}</a> */}
                                                <div className="dropdown-divider"></div>
                                                <a className="dropdown-item" onClick={this._changeprofilephoto}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <SettingsIcon></SettingsIcon>
                                                        </IconButton>
                                                    </span>
                                                    Settings &amp; Privacy</a>
                                                <a className="dropdown-item" onClick={this._orderDetail}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <FavoriteBorderIcon></FavoriteBorderIcon>
                                                        </IconButton>
                                                    </span>
                                                    Active Plan</a>
                                                <a className="dropdown-item" onClick={this._changeprofilephoto}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <HelpIcon></HelpIcon>
                                                        </IconButton>
                                                    </span>
                                                    Help &amp; Support</a>
                                                <a className="dropdown-item" onClick={this._changeprofilephoto}>
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <LibraryBooksIcon></LibraryBooksIcon>
                                                        </IconButton>
                                                    </span>
                                                    Terms &amp; Policies</a>
                                                {/* <a className="dropdown-item" href="#">
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <CheckCircleOutlineIcon></CheckCircleOutlineIcon>
                                                        </IconButton>
                                                    </span>Activity Log</a> */}
                                                <div className="dropdown-divider"></div>
                                                <a className="dropdown-item" href="/">
                                                    <span className="padding-right">
                                                        <IconButton color="inherit" size="small">
                                                            <ExitToAppIcon></ExitToAppIcon>
                                                        </IconButton>
                                                    </span>
                                                    Logout</a>
                                            </div>
                                        </li>
                                    </ul>
                                    {/* <input className="form-control" type="text" placeholder="Search" /> */}
                                </div>
                            </div>
                        </nav>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
        },
        dispatch
    );
export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(UserHeader);