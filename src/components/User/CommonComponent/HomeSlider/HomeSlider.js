import React, { Component } from 'react';

import img1 from "../../../../assets/images/slide11.jpg";
import img2 from "../../../../assets/images/slide22.jpg";
import img3 from "../../../../assets/images/slide33.jpg";
import img4 from "../../../../assets/images/slide44.jpg";
import img5 from "../../../../assets/images/slide55.jpg";

import "./HomeSlider.css";

class HomeSlider extends Component {
    render() {
        return (
            <div id="demo" className="carousel slide" data-ride="carousel">

                <ul className="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" className="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                    <li data-target="#demo" data-slide-to="3"></li>
                    <li data-target="#demo" data-slide-to="4"></li>
                </ul>

                <div className="carousel-inner" style={{ borderRadius: "10px" }}>
                    <div className="carousel-item active">
                        <img src={img1} alt="Los Angeles" width="1100" height="500" />
                        {/* <div className="slide-centered home-heading headerSection">
                            Someone is looking for you.
                            Be found and choose your perfect one partner.
                            
                        </div> */}
                    </div>
                    <div className="carousel-item">
                        <img src={img2} alt="Chicago" width="1100" height="500" />
                        {/* <div className="slide-centered home-heading headerSection">
                            Commemorate your wedding by giving your groom a thoughtful gift. We’ve put together a list of options that will mark the beginning of a new chapter in your lives.
                            
                        </div> */}
                    </div>
                    <div className="carousel-item">
                        <img src={img3} alt="New York" width="1100" height="500" />
                        {/* <div className="slide-centered home-heading headerSection">
                            Here is a list of what are 2021s popular bridal makeup trends
                           
                        </div> */}
                    </div>
                    <div className="carousel-item">
                        <img src={img4} alt="New York" width="1100" height="500" />
                        {/* <div className="slide-centered home-heading headerSection">
                            More and more couples are innovating when it comes to their wedding invitation design. Get in on the party and explore these options for yours!
                          
                        </div> */}
                    </div>
                    <div className="carousel-item">
                        <img src={img5} alt="New York" width="1100" height="500" />
                        {/* <div className="slide-centered home-heading headerSection">
                            If you’re looking for ideas and inspiration related to wedding and wedding planning, you’re at the right place. SathSobat blog section is home to our best stories, ideas, and how-to guides relevant to couples and families who are about to embark upon their wedding planning journey
                        </div> */}
                    </div>
                </div>

                <a className="carousel-control-prev" href="#demo" data-slide="prev">
                    <span className="carousel-control-prev-icon"></span>
                </a>
                <a className="carousel-control-next" href="#demo" data-slide="next">
                    <span className="carousel-control-next-icon"></span>
                </a>
            </div>
        );
    }
}

export default HomeSlider;