import React, { Component } from "react";
import { CuurencyToWord } from "./CurrencyToWord";

class CurrencyWordFormat extends Component {
  // Default constructor in component

  constructor(props) {
    super(props);
  }

  render() {
    
    if (this.props.state === undefined) {
      return <span></span>;
    }
    if (this.props.state === "") {
      return <span></span>;
    } else {
      var val = CuurencyToWord.toWord(Number(this.props.state) || 0);
      return <span>{val}</span>;
    }
  }
}

export default CurrencyWordFormat;
