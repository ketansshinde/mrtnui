import React, { Component } from "react";

class CurrencyValue extends Component {
  // Default constructor in component

  constructor(props) {
    super(props);
  }
  render() {

    if (this.props === undefined) {
      return <span></span>;
    }
    if (this.props.state === undefined) {
      return <span></span>;
    }
    if (this.props.state === "") {
      return <span></span>;
    } else {

      var val = "";
      if (this.props.state >= 10000000)
        val = (this.props.state / 10000000).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,") + " Cr";
      else if (this.props.state >= 100000)
        val = (this.props.state / 100000).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,") + " lakh";
      else if (this.props.state >= 1000)
        // val = (this.props.state).toFixed(2).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
        val = (this.props.state / 1).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
      else {
        // val = (this.props.state).toFixed(2).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
        val = (this.props.state / 1).toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
      }
      return <span>{val}</span>;
    }
  }
}

export default CurrencyValue;
