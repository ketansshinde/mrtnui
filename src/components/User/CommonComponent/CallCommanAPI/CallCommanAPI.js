import React, { Component } from "react";
import {
    error,
    success,
    successNotification,
    errorNotification,
    warning,
    warningNotification
} from "../../../../components/notification/notifications";
import { Global_var } from "../../../../global/global_var";

import { UserSettingService } from "../../../../services/UserService/UserSettingService";

export const CallCommanAPI = {
    GetUserProfilePhoto,
    GetGovIDService,
    GetGalleryPhoto
};


function GetUserProfilePhoto(userData, fn) {
    UserSettingService.GetUserProfilePhoto(userData, (res) => {
        fn(res.data);
    });
}
function GetGovIDService(userData, fn) {
    UserSettingService.GetGovIDService(userData, (res) => {
        fn(res.data);
    });
}
function GetGalleryPhoto(userData, fn) {
    UserSettingService.GetGalleryPhotoService(userData, (res) => {
        fn(res.data);
    });
}




