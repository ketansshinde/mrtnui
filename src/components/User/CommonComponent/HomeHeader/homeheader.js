import React, { Component } from "react";
import { withRouter, useHistory } from "react-router-dom";
import {
    SubmitButton,
    CloseButton, ProSubButton
} from "./../../../../assets/MaterialControl";
import { connect } from 'react-redux';
import { bindActionCreators, compose } from "redux";
import logo from "./../../../../assets/images/logo.png";
import i18n from "../../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from "react-i18next";

class HomeHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedLanguage: "English",
        };

        this.initialState = this.state;
    }


    componentDidMount() {
        // i18n.changeLanguage("mr");
    }

    _login = () => {
        this.props.history.push("/login");
    };

    _homePage = () => {
        this.props.history.push("/");
    };

    _changelang(lang) {

        if (lang === "en") {
            i18n.changeLanguage("en");
            localStorage.setItem("lang", "en");
            //window.location.reload();
        }
        else if (lang === "mr") {
            i18n.changeLanguage("mr");
            localStorage.setItem("lang", "mr");
            //window.location.reload();
        }
        else {
            i18n.changeLanguage("en");
            localStorage.setItem("lang", "en");
            // window.location.reload();
        }
    }

    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <nav className="navbar navbar-expand-md navbar-dark bg-dark-header home-navbar">
                        <a className="navbar-brand userheader-logo" href="#" onClick={this._homePage}> <img src={logo} width="35"></img> Sath<span>Sobat</span></a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarsExample04">
                            <ul className="navbar-nav mr-auto li-padding">
                                {/* <li className="nav-item active">
                                <a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item active">
                                <a className="nav-link" href="/Contact">Contact <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item active">
                                <a className="nav-link" href="/Service">Service <span className="sr-only">(current)</span></a>
                            </li> */}
                                {/* <li className="nav-item dropdown">
                                    <select class="form-control"
                                        value={this.state.selectedLanguage}
                                        onChange={(event) => {
                                            this.setState({ selectedLanguage: event.target.value });
                                            this._changelang(event.target.value);
                                        }}>
                                        <option>Select Language</option>
                                        <option value="en">
                                            English
                                        </option>
                                        <option value="mr">
                                            मराठी
                                        </option>
                                    </select>
                                </li> */}
                            </ul>
                            <div className="form-inline my-2 my-md-0 head-login-box">
                                <ul className="navbar-nav ml-auto ml-md-0">
                                    <li className="nav-item dropdown">
                                        <span className="already-member" onClick={this._login}>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span style={{ marginRight: "10px" }}>{t("Already_Member")}</span>
                                                )}
                                            </Translation>
                                            {/* Already Member? {"  "} */}
                                            <SubmitButton
                                                variant="contained"
                                                color="primary"
                                            >
                                                <Translation>
                                                    {(t, { i18n }) => (
                                                        <div>{t("Login")}</div>
                                                    )}
                                                </Translation>
                                            </SubmitButton>
                                        </span>
                                    </li>
                                </ul>
                                {/* <input className="form-control" type="text" placeholder="Search" /> */}
                            </div>
                        </div>
                    </nav>
                </I18nextProvider>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({

});
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
        },
        dispatch
    );

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(HomeHeader);