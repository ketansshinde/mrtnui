// import Loader from "./loader/Loader";
import UserFooter from "./UserFooter/userfooter";
import UserHeader from "./UserHeader/userheader";
import HomeHeader from "./HomeHeader/homeheader";
import HomeFooter from "./HomeFooter/homefooter";
import CurrencyWordFormat from "./CurrencyValue/CurrencyWordFormat";
import CurrencyValue from "./CurrencyValue/CurrencyValue";
import ImageCropper from "./ImageCropper/ImageCropper";
import ProfilePhoto from "./ProfilePhoto/ProfilePhoto";
import ValidPhoto from "./ValidPhoto/validphoto";
import Loader from "./Loader/Loader";

export {
    //   Loader,
    UserFooter,
    UserHeader,
    HomeHeader,
    HomeFooter,
    CurrencyWordFormat,
    CurrencyValue,
    ImageCropper,
    ProfilePhoto,
    ValidPhoto,
    Loader
};
