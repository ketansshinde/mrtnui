import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as Yup from "yup";
import queryString from "query-string";

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );



class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            confirmPassword: "",
            guidDetail: ""
        };
    }
    componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        var param = values.id;
        this.setState({ guidDetail: param });
    }

    _userAccountSchema = Yup.object().shape({
        password: Yup.string()
            .required('New password is required')
            .min(8, 'Password must be 8 characters at minimum')
            .max(16, 'Password must be 16 characters at maximum')
            .matches(/[a-z]/, 'At least one lowercase char')
            .matches(/[A-Z]/, 'At least one uppercase char')
            .matches(/(?=.*\d)/, 'Must contain a number')
            .matches(/[!@#$%^&*(),.?":{}|<>]/, 'Must contain special character')
            .matches(/^\S*$/, 'Password should not allow space'),
        confirmPassword: Yup.string()
            .required('Confirm password is required')
            .min(8, 'Password must be 8 characters at minimum')
            .oneOf([Yup.ref('password'), null], 'Passwords must match'),
    });

    _handleSubmit = (values, { resetForm }, actions) => {
        debugger;
        if (values.guidDetail == undefined || values.guidDetail == "") {
            warning("Invalid link.", warningNotification);
            return;
        }
        var passValue = {
            password: values.password,
            link_guid: values.guidDetail,
        }
        this.props.ResetPasswordUser(passValue,
            (res) => {
                if (res.data.success) {
                    success("Password has been changed.", successNotification);
                    this.props.history.push("/login");
                }
                else {
                    error(res.data.errorlog, errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        password: this.state.password,
                        confirmPassword: this.state.confirmPassword,
                        guidDetail: this.state.guidDetail
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <section className="py-3">
                            <div className="main-layout">
                                <div className="row">
                                    <div className="col-lg-6 offset-md-3">
                                        <div className="card layout-margin">
                                            <div className="card-body">
                                                <h5 className="card-title card-title-set">Reset Password</h5>

                                                <div className="row">
                                                    <div className="col-lg-12 mb-2">

                                                        <label className="medium" htmlFor="Password">New Password</label>
                                                        <Field
                                                            type="password"
                                                            id="password"
                                                            name="password"
                                                            maxLength="50"
                                                            placeholder="New Password e.g - Abc@12345"
                                                            onChange={(event) => {
                                                                setFieldValue(
                                                                    (values.password = event.target.value)
                                                                );
                                                            }}
                                                            className={`form-control ${touched.password && errors.password
                                                                ? "is-invalid"
                                                                : ""
                                                                }`}
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="password"
                                                            className="text-danger"
                                                        />
                                                    </div>
                                                    <div className="col-lg-12 mb-2">

                                                        <label className="medium" htmlFor="Email" htmlFor="confirmpassword">Confirm password</label>
                                                        <Field
                                                            type="password"
                                                            id="confirmPassword"
                                                            name="confirmPassword"
                                                            maxLength="50"
                                                            placeholder="Confirm Password"
                                                            onChange={(event) => {
                                                                setFieldValue(
                                                                    (values.confirmPassword = event.target.value)
                                                                );
                                                            }}
                                                            className={`form-control ${touched.confirmPassword && errors.confirmPassword
                                                                ? "is-invalid"
                                                                : ""
                                                                }`}
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="confirmPassword"
                                                            className="text-danger"
                                                        />
                                                    </div>
                                                    <div className="col-lg-12 mb-2 text_align_right">

                                                        <SubmitButton
                                                            type="submit"
                                                            variant="contained"
                                                        >
                                                            Reset Password
                                                    </SubmitButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);