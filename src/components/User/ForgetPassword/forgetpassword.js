import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
import * as Yup from "yup";


const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
        },
        dispatch
    );



class ForgetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ""
        };
    }
    componentDidMount() {


    }

    _userAccountSchema = Yup.object().shape({
        email: Yup.string()
            .required("Email-Id is requird").email("Please enter the valid Email-ID"),
    });

    _handleSubmit = (values, { resetForm }, actions) => {
        debugger;
        var passValue = {
            email: values.email,
        }
        this.props.ForgotPasswordUser(passValue,
            (res) => {
                if (res.data.success) {
                    success("Kindly check your email to reset password.", successNotification);
                }
                else {
                    error("Something wents worng.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        email: this.state.email
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <section className="py-3">
                            <div className="main-layout">
                                <div className="row">
                                    <div className="col-lg-6 offset-md-3">
                                        <div className="card layout-margin">
                                            <div className="card-body">
                                                <h5 className="card-title card-title-set">Forget Password</h5>

                                                <div className="row">
                                                    <div className="col-lg-12 mb-2">

                                                        <label className="medium" htmlFor="Email">Email</label>
                                                        <Field
                                                            type="email"
                                                            id="email"
                                                            name="email"
                                                            maxLength="200"
                                                            placeholder="Enter the Email-ID"
                                                            onChange={(event) => {
                                                                setFieldValue(
                                                                    (values.email = event.target.value)
                                                                );
                                                            }}
                                                            className={`form-control ${touched.email && errors.email
                                                                ? "is-invalid"
                                                                : ""
                                                                }`}
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="email"
                                                            className="text-danger"
                                                        />
                                                    </div>
                                                    <div className="col-lg-12 mb-2 text_align_right">

                                                        <SubmitButton
                                                            type="submit"
                                                            variant="contained"
                                                        >
                                                            Reset
                                                    </SubmitButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </Form>
                )}
                </Formik>
            </React.Fragment >
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);