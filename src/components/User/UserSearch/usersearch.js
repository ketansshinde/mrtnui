import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { CryptoCode } from "../../../common/cryptoCode";
import male from "../../../assets/images/man.png";
import female from "../../../assets/images/woman.png";
import FileBase64 from "react-file-base64";
import {
    SubmitButton, CloseButton
} from "../../../assets/MaterialControl";
//import UserLeftSideNotification from "../UserLeftSideNotification/userleftsidenotificatio";
import MUIDataTable from "mui-datatables";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { optionsMail, themeMail } from "../../../common/columnFeature";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { CurrencyValue } from "../../User/CommonComponent";
import { Formik, Form, Field, ErrorMessage } from "formik";

import {
    error,
    success,
    warning,
    warningNotification,
    errorNotification,
    successNotification,
} from "../../notification/notifications";
// import { CasteValue } from "../../../common/CasteValue";
// import { SubCasteValue } from "../../../common/SubCasteValue";
import MessageIcon from '@material-ui/icons/Message';
import { IconButton } from "@material-ui/core";
import EmailIcon from '@material-ui/icons/Email';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import SettingsIcon from '@material-ui/icons/Settings';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PersonIcon from '@material-ui/icons/Person';
import PaymentIcon from '@material-ui/icons/Payment';
import StarIcon from '@material-ui/icons/Star';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { SendMessageDetail } from "../../../action/UserAction/SendMessageAction";
import { Global_var } from "../../../global/global_var";
import Modal from 'react-bootstrap/Modal';

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            SendMessageDetail: SendMessageDetail
        },
        dispatch
    );

class UserSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            profileUniqueId: "",
            gender: "",
            getInboxMailsDataList: [{
                Reply: "data"
            }],
            UserProfileData: [],
            // CasteData: [],
            // SubCasteData: [],
            // caste: 0,
            // subCaste: 0,
            Openpopup: false,
        };
    }

    componentDidMount() {

        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        const username = UserData.firstName + " " + UserData.lastName;
        // var CasteData = CasteValue.Caste();
        // 
        // var SubCasteData = SubCasteValue.SubCaste();
        // 
        this.setState({
            name: username,
            profileUniqueId: UserData.profileUniqueId,
            gender: UserData.gender,
            // CasteData: CasteData,
            // SubCasteData: SubCasteData
        });

        var passValue = {
            userid: UserData.id,
            casteId: 1,
            gender: UserData.gender,
            profileUniqueId: UserData.profileUniqueId
        };

        this.props.FetchUserProfile(passValue,
            (res) => {

                if (res.data.success) {
                    this.setState({ UserProfileData: res.data.responseList })
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _handleClickOpenpopup = () => {
        this.setState({ Openpopup: true });
    };

    _handleClosepopup = () => {
        this.setState({ Openpopup: false });
    };



    _sendMessage(sendMessageData, Req) {
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        if (Req === 2) {
            this._handleClickOpenpopup();
            this.setState({
                useridto: sendMessageData.userid,
                requested_userid: UserData.id,
                message: ""
            });
        }
        else {
            var passValue = {
                userid: sendMessageData.userid,
                requestedUserid: UserData.id,
                message: "Hello " + sendMessageData.firstName + " " + sendMessageData.lastName
            };
            this.props.SendMessageDetail(passValue,
                (res) => {

                    if (res.data.success) {
                        success("Request message sent.", successNotification);
                        this._handleClosepopup();
                    }
                    else {
                        error("Invalid user.", errorNotification);
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    };

    _sendingMessage(values) {
        if (values.message === "") {
            warning("Please enter the message.", warningNotification);
            return;
        }

        var passValue = {
            userid: values.useridto,
            requestedUserid: values.requested_userid,
            message: values.message
        };
        this.props.SendMessageDetail(passValue,
            (res) => {

                if (res.data.success) {
                    success("Message sent.", successNotification);
                    this._handleClosepopup();
                }
                else {
                    error("Invalid user.", errorNotification);
                }
            },
            (error) => {
                console.log(error);
            }
        );
    }

    _UserProfileDetails(UserProfileData) {
        localStorage.removeItem("getUserProfile");
        localStorage.setItem("getUserProfile", CryptoCode.encryption(JSON.stringify(UserProfileData)));
        this.props.history.push("/userProfile", UserProfileData);
    }

    render() {

        const columns = [
            {
                name: "Reply",
                label: "Reply",
                field: "Reply",
                options: {
                    filter: false,
                    setCellProps: () => ({ style: { width: "195px" } }),
                    customBodyRender: (value, tableMeta, updateValue) => {

                        var profiledata = tableMeta.tableData[tableMeta.rowIndex];
                        
                        return (
                            <div className="mb-3 layout-margin">
                                <div className="row no-gutters">
                                    <div className="col-lg-3 mb-2 text_align_center">
                                        <img src={profiledata.profilePhotoname !== null ? Global_var.URL_USER_PROFILE_PHOTO + profiledata.profilePhotoname : (this.state.gender == 23 ? male : female)} className="profile-search-image" alt="logo"></img>
                                        <label data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span className="profile-textfont">Profile created by : </span>   {profiledata.profileCreated}
                                        </label>
                                    </div>
                                    <div className="col-lg-9">
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-lg-6 mb-2">
                                                    <span className="text_align_Left"><h6 className="card-title"><b>{profiledata.firstName + " " + profiledata.lastName} {" "}</b>({profiledata.profileUniqueId})</h6></span>
                                                </div>
                                                <div className="col-lg-6 mb-2">
                                                    <span className="dropdown text_align_right" style={{ float: "right" }}>


                                                        <label className="dropdown-toggle" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span className="like-this-member">
                                                                <IconButton color="inherit" size="small">
                                                                    <ThumbUpIcon></ThumbUpIcon>
                                                                </IconButton> Like this member?{" "}
                                                            </span>
                                                        </label>
                                                        <div id="profileaction" className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._sendMessage(profiledata, 1);
                                                            }}>
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <LabelImportantIcon></LabelImportantIcon>
                                                                    </IconButton>
                                                                </span>
                                                                Send Request
                                                                </a>
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._sendMessage(profiledata, 2);
                                                            }}>
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <MessageIcon></MessageIcon>
                                                                    </IconButton>
                                                                </span>
                                                                Send Message
                                                                </a>
                                                            <a className="dropdown-item" href="#" onClick={(event) => {
                                                                this._UserProfileDetails(profiledata);
                                                            }}>
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <VisibilityIcon></VisibilityIcon>
                                                                    </IconButton>
                                                                </span>
                                                                View Profile
                                                                </a>
                                                            <a className="dropdown-item" href="#">
                                                                <span className="padding-right">
                                                                    <IconButton color="inherit" size="small">
                                                                        <StarIcon></StarIcon>
                                                                    </IconButton>
                                                                </span>
                                                                 Short list this Profile
                                                              </a>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Caste:</span> <span className="profile-textfontName"> {profiledata.caste || "--"}</span>
                                                </div>
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Sub-Caste:</span><span className="profile-textfontName"> {profiledata.subCaste1 || "--"}</span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Education:</span>  <span className="profile-textfontName">   {profiledata.education || "--"}</span>
                                                </div>
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Branch:</span> <span className="profile-textfontName">  {profiledata.branch || "--"}</span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont">Occupation:</span>  <span className="profile-textfontName"> {profiledata.occupation || "--"}</span>
                                                </div>
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Monthly Income:</span> <span className="profile-textfontName"> ₹  <CurrencyValue state={profiledata.monthlyIncome || 0}></CurrencyValue></span>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Age:</span><span className="profile-textfontName">  {profiledata.age || "--"}</span>
                                                </div>
                                                <div className="col-lg-6 mb-2">
                                                    <span className="profile-textfont"> Height-Feet:</span> <span className="profile-textfontName"> {profiledata.hightFeet || "--"}' {profiledata.hightInch || "--"}'''</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div >
                        );
                    },
                },
            }
        ];

        return (
            <React.Fragment>
                <Formik
                    enableReinitialize
                    initialValues={{
                        name: this.state.name,
                        profileUniqueId: this.state.profileUniqueId,
                        gender: this.state.gender,
                        getInboxMailsDataList: this.state.getInboxMailsDataList,
                        UserProfileData: this.state.UserProfileData,
                        // CasteData: this.state.CasteData,
                        // SubCasteData: this.state.SubCasteData,
                        // caste: this.state.caste,
                        // subCaste: this.state.subCaste,
                        Openpopup: this.state.Openpopup
                    }}
                    validationSchema={this._userAccountSchema}
                    onSubmit={this._handleSubmit}
                >{({ handleSubmit, handleChange, handleBlur, values, touched, isInvalid, errors, setFieldValue, isSubmitting }) => (
                    <Form>
                        <div>
                            <section className="py-3">
                                <div className="main-layout">
                                    <div className="row">
                                        <div className="col-lg-3">
                                            <div className="card">
                                                <div className="card-body">
                                                    <h5 className="card-title card-title-set">Search Filter</h5>

                                                    <div className="row">
                                                        <div className="col-lg-12 mb-2 text_align_left">
                                                            <label className="medium" htmlFor="caste">Caste</label>
                                                            <Field
                                                                as="select"
                                                                name="caste"

                                                                className="form-control"
                                                                onChange={(event) => {

                                                                    const relationship = event.target.options[
                                                                        event.target.options.selectedIndex
                                                                    ].getAttribute("data-key");
                                                                    setFieldValue(
                                                                        (values.caste = event.target.value),
                                                                    );
                                                                }}
                                                            >
                                                                <option value="">Select Caste</option>
                                                                {(values.CasteData || []).map(
                                                                    (_subMasterData) => (
                                                                        <option
                                                                            data-key={_subMasterData.casteId}
                                                                            key={_subMasterData.casteId}
                                                                            value={JSON.stringify(
                                                                                _subMasterData.casteId
                                                                            )}
                                                                        >
                                                                            {_subMasterData.casteName}
                                                                        </option>
                                                                    )
                                                                )}
                                                            </Field>
                                                            <label className="medium" htmlFor="subCaste">Sub-Caste</label>
                                                            <Field
                                                                as="select"
                                                                name="subCaste"

                                                                className="form-control"
                                                                onChange={(event) => {

                                                                    const relationship = event.target.options[
                                                                        event.target.options.selectedIndex
                                                                    ].getAttribute("data-key");
                                                                    setFieldValue(
                                                                        (values.subCaste = event.target.value),
                                                                    );
                                                                    setFieldValue(
                                                                        (values.SubCasteData = values.SubCasteData.filter(x => x.casteId === event.target.value)),
                                                                    );
                                                                }}
                                                            >
                                                                <option value="">Select Sub-Caste</option>
                                                                {(values.SubCasteData || []).map(
                                                                    (_subMasterData) => (
                                                                        <option
                                                                            data-key={_subMasterData.subCasteId}
                                                                            key={_subMasterData.subCasteId}
                                                                            value={JSON.stringify(
                                                                                _subMasterData.subCasteId
                                                                            )}
                                                                        >
                                                                            {_subMasterData.subCasteName}
                                                                        </option>
                                                                    )
                                                                )}
                                                            </Field>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-9">
                                            <div className="page-title">Search your profile matches</div>
                                            <div className="layout-margin">
                                                <MuiThemeProvider
                                                    theme={themeMail}
                                                >
                                                    <MUIDataTable
                                                        data={values.UserProfileData || []}
                                                        columns={columns}
                                                        options={optionsMail}
                                                    />
                                                </MuiThemeProvider>

                                                <Modal
                                                    show={values.Openpopup}
                                                    onHide={this._handleClosepopup}
                                                    backdrop="static"
                                                    keyboard={false}
                                                    centered
                                                >
                                                    <Modal.Header closeButton>
                                                        <Modal.Title>Send Message</Modal.Title>
                                                    </Modal.Header>
                                                    <Modal.Body>
                                                        <Field
                                                            type="text"
                                                            id="message"
                                                            name="message"
                                                            maxLength="200"
                                                            placeholder="Message"
                                                            component="textarea"
                                                            rows="4"
                                                            onChange={(event) => {
                                                                setFieldValue(
                                                                    (values.message = event.target.value)
                                                                );
                                                            }}
                                                            className="form-control"
                                                        />
                                                        <ErrorMessage
                                                            component="div"
                                                            name="message"
                                                            className="text-danger"
                                                        />
                                                    </Modal.Body>
                                                    <Modal.Footer>
                                                        <CloseButton
                                                            variant="contained"
                                                            onClick={(event) => {

                                                                this._handleClosepopup(values);
                                                            }}
                                                        >
                                                            Cancel
                                                    </CloseButton>
                                                        <SubmitButton
                                                            type="submit"
                                                            variant="contained"
                                                            onClick={(event) => {

                                                                this._sendingMessage(values);
                                                            }}
                                                        >
                                                            Send Message
                                                    </SubmitButton>
                                                    </Modal.Footer>
                                                </Modal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </Form>
                )}
                </Formik>
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserSearch);
