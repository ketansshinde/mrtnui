import React, { Component } from 'react';
import logo from "../../../assets/images/logo.png";
import "./legalpolicy.css";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from 'react-i18next';

class TermOfUse extends Component {
    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <section className="py-3">
                        <div className="container">
                            <div className="main-layout home-main-layout">
                                <div className="row legal-boby">

                                    <div className="col-lg-12 text_align_center">
                                        <span className="legal-title">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Terms_of_use")}</span>
                                                )}
                                            </Translation>
                                        </span>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("term_condtion_Header")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 text_align_left term-and-use-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("term_condtion_Eligibility")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <b>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_PM_Membership_and_rights")}</span>
                                                )}
                                            </Translation>
                                        </b>
                                        <ul className="term-and-use-heading-ui">
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Indian_Nationals")}</span>
                                                )}
                                            </Translation> </li>
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Persons_of_Indian")}</span>
                                                )}
                                            </Translation></li>
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Non_Resident_Indians")}</span>
                                                )}
                                            </Translation></li>
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Persons_of_Indian_Descent")}</span>
                                                )}
                                            </Translation></li>
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Persons_who_intend")}</span>
                                                )}
                                            </Translation></li>
                                        </ul>
                                        <b>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Further_in_capacity")}</span>
                                                )}
                                            </Translation>
                                        </b>
                                        <ul className="term-and-use-heading-ui">
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_18_years_21")}</span>
                                                )}
                                            </Translation></li>
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Legally_competent")}</span>
                                                )}
                                            </Translation></li>
                                            <li> <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Not_prohibited")}</span>
                                                )}
                                            </Translation></li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-12  text_align_left term-and-use-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("term_condtion_Mode_of_Payment")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <p> <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("term_condtion_Payment_made")}</span>
                                            )}
                                        </Translation></p>
                                        <p> <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("term_condtion_PM_may_use")}</span>
                                            )}
                                        </Translation></p>
                                        <p>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Member_who_has")}</span>
                                                )}
                                            </Translation>
                                        </p>
                                    </div>
                                    <div className="col-lg-12  text_align_left term-and-use-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("term_condtion_Refund_Policy")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <p>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("term_condtion_Only_for_INR_payments")}</span>
                                                )}
                                            </Translation>
                                        </p>
                                    </div>

                                </div >
                            </div >
                        </div >
                    </section >
                </I18nextProvider>
            </React.Fragment >
        );
    }
}

export default TermOfUse;