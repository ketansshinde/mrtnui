import React, { Component } from 'react';
import logo from "../../../assets/images/logo.png";
import "./legalpolicy.css";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from "react-i18next";


class AboutUs extends Component {
    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <section className="py-3">
                        <div className="container">
                            <div className="main-layout home-main-layout">
                                <div className="row legal-boby ">

                                    <div className="col-lg-12 text_align_center">
                                        <span className="legal-title">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("About_Us")}</span>
                                                )}
                                            </Translation>
                                        </span>
                                    </div>
                                    <div className="col-lg-12">
                                        <p>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("About_heading")}</span>
                                                )}
                                            </Translation>
                                        </p>
                                    </div>
                                    <div className="col-lg-12 text_align_left">
                                        <ul className="about-us-heading-ui">
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Registration")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Featured_Profile_Listing")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Members_Account")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Able_to_view_the_profile_of_the_concerned_person")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Upload_and_view_Photos_and_Kundli")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Contacts_Made_Received")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Private_Message_Board")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Instant_Messenger")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>—  <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Members_Search")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Membership")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Match_Making")}</span>
                                                )}
                                            </Translation>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-lg-12 text_align_left">
                                        <p>
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Our_goal")}</span>
                                                )}
                                            </Translation>
                                        </p>
                                    </div>
                                    <div className="col-lg-12 text_align_left about-us-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Our_Website_Services")}</span>
                                            )}
                                        </Translation>
                                    </div>

                                    <div className="col-lg-12 text_align_left">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Perfect_Match_provides_comprehensive_services_like")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 text_align_left">
                                        <ul className="about-us-heading-ui">
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Registration_is_absolutely_free")}</span>
                                                )}
                                            </Translation></li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Every_single_member")}</span>
                                                )}
                                            </Translation></li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Every_member_can_upload")}</span>
                                                )}
                                            </Translation></li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Different_search_features")}</span>
                                                )}
                                            </Translation></li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Express_interest_feature")}</span>
                                                )}
                                            </Translation></li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Every_member_can_fill")}</span>
                                                )}
                                            </Translation></li>
                                            <li>— <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Bride_and_Groom_photo_gallery")}</span>
                                                )}
                                            </Translation></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </I18nextProvider>
            </React.Fragment>
        );
    }
}

export default AboutUs;