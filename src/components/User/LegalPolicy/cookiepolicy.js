import React, { Component } from 'react';
import logo from "../../../assets/images/logo.png";
import "./legalpolicy.css";

class CookiePolicy extends Component {
    render() {
        return (
            <React.Fragment>
                <section className="py-3">
                    <div className="container">
                        <div className="main-layout home-main-layout">
                            <div className="row legal-boby">

                                <div className="col-lg-12 text_align_center">
                                    <span className="legal-title">Cookies Policy</span>
                                </div>
                                <div className="col-lg-12">
                                    perfect match is one of the leading and most trusted matrimony websites in India. Making happy marriages happen since 1998, Jeevansathi understands the importance of choosing the right partner for marriage, especially in the Indian cultural setup. It believes in providing the most secure and convenient matchmaking experience to all its members by ensuring 100% screening,exclusive privacy options, photo protection features and verification of phone numbers and more information. While the online matrimonial site connects millions of people directly, Jeevansathi also maintains a dedicated Customer Care team and offers offline Match Point Centers across the country, for deeper and personal interaction with prospective brides, grooms and /or families.
                                    Please note: Jeevansathi is only meant for users with a bonafide intent to enter into a matrimonial alliance and is not meant for users interested in dating only. Jeevansathi platform should not be used to post any obscene material, such actions may lead to permanent deletion of the profile used to upload such content.
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default CookiePolicy;