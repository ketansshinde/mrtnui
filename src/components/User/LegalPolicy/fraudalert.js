import React, { Component } from 'react';
import logo from "../../../assets/images/logo.png";
import "./legalpolicy.css";
import i18n from "../../../i18n";
import { I18nextProvider } from "react-i18next";
import { Translation } from "react-i18next";

class FraudAlert extends Component {
    render() {
        return (
            <React.Fragment>
                <I18nextProvider i18n={i18n}>
                    <section className="py-3">
                        <div className="container">
                            <div className="main-layout home-main-layout">
                                <div className="row legal-boby">

                                    <div className="col-lg-12 text_align_center">
                                        <span className="legal-title">
                                            <Translation>
                                                {(t, { i18n }) => (
                                                    <span>{t("Fraud_Alert")}</span>
                                                )}
                                            </Translation>
                                        </span>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_Heading")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 fraud-alert-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_q1")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_aws1")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 fraud-alert-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_q2")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_aws2")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 fraud-alert-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_q3")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_aws3")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 fraud-alert-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_q4")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_aws4")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 fraud-alert-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_q5")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_aws5")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12 fraud-alert-heading">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_q6")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                    <div className="col-lg-12">
                                        <Translation>
                                            {(t, { i18n }) => (
                                                <span>{t("Fraud_Alert_aws6")}</span>
                                            )}
                                        </Translation>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </I18nextProvider>
            </React.Fragment>
        );
    }
}

export default FraudAlert;