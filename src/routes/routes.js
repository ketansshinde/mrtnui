import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  Switch,
  withRouter,
  browserHistory,
} from "react-router-dom";
import { createBrowserHistory } from "history";
import LoginContainer from "../containers/UserContainer/login/loginContainer";
import RegistrationContainer from "../containers/UserContainer/Registration/RegistrationContainer";
import HomeContainer from "../containers/UserContainer/Home/HomeContainer";
import PricingContainer from "../containers/UserContainer/Pricing/PricingContainer";
import ServicesContainer from "../containers/UserContainer/Services/ServicesContainer";
import ContactContainer from "../containers/UserContainer/Contact/ContactContainer";
import UserLayoutContainer from "../containers/UserContainer/UserLayout/UserLayoutContainer";
import MyProfileContainer from "../containers/UserContainer/MyProfile/MyProfileContainer";
import UserSearchContainer from "../containers/UserContainer/UserSearch/UserSearchContainer";
import SendMessageConatiner from "../containers/UserContainer/SendMessage/SendMessageConatiner";
import UserHomePageContainer from "../containers/UserContainer/UserHomePage/UserHomePageContainer";
import UserProfileSettingContainer from "../containers/UserContainer/UserProfileSetting/UserProfileSettingContainer";
import ChangePasswordContainer from "../containers/UserContainer/UserProfileSetting/ChangePasswordContainer";
import ForgetPasswordContainer from "../containers/UserContainer/ForgetPassword/ForgetPasswordContainer";
import PrivateSecurityContainer from "../containers/UserContainer/UserProfileSetting/PrivateSecurityContainer";
import GovermentIdContainer from "../containers/UserContainer/UserProfileSetting/GovermentIdContainer";
import UserGalleryContainer from "../containers/UserContainer/UserProfileSetting/UserGallaryContainer";
import UpgradeSubscriptionContainer from "../containers/UserContainer/SubcriptionPage/UpgradeSubscriptionContainer";
import OrderDetailContainer from "../containers/UserContainer/SubcriptionPage/OrderDetailContainer";
import ProfileViewerContainer from "../containers/UserContainer/ProfileViewer/ProfileViewerContainer";
import ProfileShortlistContainer from "../containers/UserContainer/ProfileShortlist/ProfileShortlistContainer";
import ProfileInterestContainer from "../containers/UserContainer/ProfileInterest/ProfileInterestContainer";
import ProfileShortlistByYouContainer from "../containers/UserContainer/ProfileShortlistByYou/ProfileShortlistByYouContainer";
import ProfileDeclineContainer from "../containers/UserContainer/ProfileInterest/ProfileDeclineContainer";
import ProfileDeclineByYouContainer from "../containers/UserContainer/ProfileInterest/ProfileDeclineByYouContainer";
import FeedbackContainer from "../containers/UserContainer/HomeHelp/FeedbackContainer";
import ContactUsContainer from "../containers/UserContainer/HomeHelp/ContactUsContainer";
import MessageContainer from "../containers/UserContainer/Message/MessageContainer";
import RegistrationConfirmContainer from "../containers/UserContainer/Registration/RegistrationConfirmContainer";
import ResetPasswordContainer from "../containers/UserContainer/ForgetPassword/ResetPasswordContainer";
import PaymentStatusContainer from "../containers/UserContainer/SubcriptionPage/PaymentStatusContainer";

//------------Legal Policy--------------------------------------------------------

import AboutUsContainer from "../containers/UserContainer/LegalPolicy/AboutUsContainer";
import CookiesPolicyContainer from "../containers/UserContainer/LegalPolicy/CookiesPolicyContainer";
import FraudAlertContainer from "../containers/UserContainer/LegalPolicy/FraudAlertContainer";
import PrivacyPolicyContainer from "../containers/UserContainer/LegalPolicy/PrivacyPolicyContainer";
import TermsOfUseContainer from "../containers/UserContainer/LegalPolicy/TermsOfUseContainer";

//------------Admin Pages -------------------------------------------------

import AdminLoginContainer from "../containers/AdminContainer/AdminLogin/AdminLoginContainer";
import AdminDashboardContainer from "../containers/AdminContainer/Dashboard/AdminDashboardContainer";
import CityContainer from "../containers/AdminContainer/City/CityContainer";
import CountryContainer from "../containers/AdminContainer/Country/CountryContainer";
import StateContainer from "../containers/AdminContainer/State/StateContainer";
import MasterContainer from "../containers/AdminContainer/Master/MasterContainer";
import SubMasterContainer from "../containers/AdminContainer/SubMaster/SubMasterContainer";
import UserDetailContainer from "../containers/AdminContainer/UserDetail/UserDetailContainer";
import ErrorLogContainer from "../containers/AdminContainer/ErrorLog/ErrorLogContainer";
import CasteContainer from "../containers/AdminContainer/Caste/CasteContainer";
import DocumentContainer from "../containers/AdminContainer/Document/DocumentContainer";
import UserOrderContainer from "../containers/AdminContainer/UserOrder/UserOrderContainer";

//------------Business card pages-------------------------------------------------

import BusinessHomeContainer from "../containers/BusinessContainer/BusinessHomePage/BusinessHomeContainer";
import BusinessLoginContainer from "../containers/BusinessContainer/BusinessLogin/BusinessLoginContainer";
import BusinessRegistrationContainer from "../containers/BusinessContainer/BusinessRegistration/BusinessRegistrationContainer";
import BusinessForgotPasswordContainer from "../containers/BusinessContainer/BusinessForgotPassword/BusinessForgotPasswordContainer";
import BusinessPageContainer from "../containers/BusinessContainer/BusinessPage/BusinessPageContainer";
import BusinessAmenitiesContainer from "../containers/BusinessContainer/BusinessAmenities/BusinessAmenitiesContainer";
import BusinessEnquiryContainer from "../containers/BusinessContainer/BusinessEnquiry/BusinessEnquiryContainer";
import BusinessFeedbackContainer from "../containers/BusinessContainer/BusinessFeedback/BusinessFeedbackContainer";
import BusinessGalleryContainer from "../containers/BusinessContainer/BusinessGallery/BusinessGalleryContainer";
import BusinessHourContainer from "../containers/BusinessContainer/BusinessHour/BusinessHourContainer";
import BusinessPaymentContainer from "../containers/BusinessContainer/BusinessPayment/BusinessPaymentContainer";
import BusinessOrderDetailContainer from "../containers/BusinessContainer/BusinessSubcriptionPage/BusinessOrderDetailContainer";
import BusinessUpgradeSubscriptionContainer from "../containers/BusinessContainer/BusinessSubcriptionPage/BusinessUpgradeSubscriptionContainer";



export const history = createBrowserHistory();

export default class Routes extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router history={history}>
        <div>
          <Link to="/"></Link>
          <Switch>
            <Route
              exact
              path="/"
              render={(props) => {
                return <HomeContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/pricing"
              render={(props) => {
                return <PricingContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/login"
              render={(props) => {
                return <LoginContainer {...props} pageName={"Login"} />;
              }}
            ></Route>
            <Route
              exact
              path="/registration"
              render={(props) => {
                return <RegistrationContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/registrationconfirm"
              render={(props) => {
                return <RegistrationConfirmContainer {...props} />;
              }}
            ></Route>

            <Route
              exact
              path="/service"
              render={(props) => {
                return <ServicesContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/contact"
              render={(props) => {
                return <ContactContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/userhomepage"
              render={(props) => {
                return <UserHomePageContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/editprofile"
              render={(props) => {
                return <MyProfileContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/userprofile"
              render={(props) => {
                return <UserLayoutContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/usersearch"
              render={(props) => {
                return <UserSearchContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/sendmessage"
              render={(props) => {
                return <SendMessageConatiner  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/message"
              render={(props) => {
                return <MessageContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/changeprofilephoto"
              render={(props) => {
                return <UserProfileSettingContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/changepassword"
              render={(props) => {
                return <ChangePasswordContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/forgetpassword"
              render={(props) => {
                return <ForgetPasswordContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/resetpassword"
              render={(props) => {
                return <ResetPasswordContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/privatesecurity"
              render={(props) => {
                return <PrivateSecurityContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/govermentidproof"
              render={(props) => {
                return <GovermentIdContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/usergallery"
              render={(props) => {
                return <UserGalleryContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/upgradesubscription"
              render={(props) => {
                return <UpgradeSubscriptionContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/orderdetail"
              render={(props) => {
                return <OrderDetailContainer  {...props} />;
              }}
            ></Route>

            <Route
              exact
              path="/profilevisitor"
              render={(props) => {
                return <ProfileViewerContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/profileshortlist"
              render={(props) => {
                return <ProfileShortlistContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/profileshortlistbyyou"
              render={(props) => {
                return <ProfileShortlistByYouContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/profilerequest"
              render={(props) => {
                return <ProfileInterestContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/profiledecline"
              render={(props) => {
                return <ProfileDeclineContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/profiledeclinebyyou"
              render={(props) => {
                return <ProfileDeclineByYouContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/paymentstatus"
              render={(props) => {
                return <PaymentStatusContainer  {...props} />;
              }}
            ></Route>



            {/* -----------------------Legal Policy-------------------------------------------- */}
            <Route
              exact
              path="/aboutus"
              render={(props) => {
                return <AboutUsContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/cookiespolicy"
              render={(props) => {
                return <CookiesPolicyContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/fraudalert"
              render={(props) => {
                return <FraudAlertContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/privacypolicy"
              render={(props) => {
                return <PrivacyPolicyContainer  {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/termsoduse"
              render={(props) => {
                return <TermsOfUseContainer  {...props} />;
              }}
            ></Route>
            {/* -----------------------Home Help-------------------------------------------- */}


            <Route
              exact
              path="/feedback"
              render={(props) => {
                return <FeedbackContainer  {...props} />;
              }}
            ></Route>

            <Route
              exact
              path="/contactus"
              render={(props) => {
                return <ContactUsContainer  {...props} />;
              }}
            ></Route>


            {/* -----------------------Admin Role-------------------------------------------- */}

            <Route
              exact
              path="/adminLogin"
              render={(props) => {
                return <AdminLoginContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/admindashboard"
              render={(props) => {
                return <AdminDashboardContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/admincity"
              render={(props) => {
                return <CityContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/admincountry"
              render={(props) => {
                return <CountryContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/adminstate"
              render={(props) => {
                return <StateContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/adminmaster"
              render={(props) => {
                return <MasterContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/adminsubmaster"
              render={(props) => {
                return <SubMasterContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/adminuserdetail"
              render={(props) => {
                return <UserDetailContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/adminerrorlog"
              render={(props) => {
                return <ErrorLogContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/admincaste"
              render={(props) => {
                return <CasteContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/admindocument"
              render={(props) => {
                return <DocumentContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/userorder"
              render={(props) => {
                return <UserOrderContainer {...props} />;
              }}
            ></Route>



            {/* -----------------------Business Card Route-------------------------------------------- */}
            <Route
              exact
              path="/businesshome"
              render={(props) => {
                return <BusinessHomeContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businesslogin"
              render={(props) => {
                return <BusinessLoginContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businessregistration"
              render={(props) => {
                return <BusinessRegistrationContainer {...props} />;
              }}
            ></Route>

            <Route
              exact
              path="/businessforgotpassword"
              render={(props) => {
                return <BusinessForgotPasswordContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businesspage"
              render={(props) => {
                return <BusinessPageContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businessamenities"
              render={(props) => {
                return <BusinessAmenitiesContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businessenquiry"
              render={(props) => {
                return <BusinessEnquiryContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businessfeedback"
              render={(props) => {
                return <BusinessFeedbackContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businessgallery"
              render={(props) => {
                return <BusinessGalleryContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businesshour"
              render={(props) => {
                return <BusinessHourContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businesspayment"
              render={(props) => {
                return <BusinessPaymentContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/businessorder"
              render={(props) => {
                return <BusinessOrderDetailContainer {...props} />;
              }}
            ></Route>
            <Route
              exact
              path="/busiessupgrade"
              render={(props) => {
                return <BusinessUpgradeSubscriptionContainer {...props} />;
              }}
            ></Route>

          </Switch>
        </div>
      </Router >
    );
  }
}
