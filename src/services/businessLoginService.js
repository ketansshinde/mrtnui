import { Business_Global_var } from "../global/global_var";
import RestDataSource from "./restdatasource";
import { checkUserSession } from "../common/userSessionExpiry";


export const businessLoginService = {
    Authenticate,
    getIpAddress,
    GetBusinessUserLoginDetail
};

// Generate Token
function Authenticate(UserData, fn, error) {
    let url = Business_Global_var.BUSINESS_BASEURL + Business_Global_var.URL_JWT_TOKEN;

    return new RestDataSource(url, null, "CORE", fn).Store(UserData, (res) => {

        if (res.message == null) {
            return fn(res.data)
        }
    });
}

function getIpAddress() {
    let url = Business_Global_var.URL_IP_ADDRESS;
    return new RestDataSource(url).GetOne((res) => {

        if (res.ip != null) {
            localStorage.setItem("IpAddress", JSON.stringify(res.ip));
        }
    });
}

function GetBusinessUserLoginDetail(userData, fn) {
    let url = Business_Global_var.BUSINESS_BASEURL + Business_Global_var.URL_GET_USER_LOGIN_DETAIL;
    return new RestDataSource(url, fn).Store(userData, (res) => {
        if (res !== null) {
            return fn(res);
        }
    });
}



