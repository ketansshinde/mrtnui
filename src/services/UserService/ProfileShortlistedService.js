import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";

export const ProfileShortlistedService = {
    GetProfileShortlistedService,
    SaveProfileShortlistedService,
    GetProfileShortlistByYouService,
    DeleteProfileShortlistedService
}

// Profile Viewer
function GetProfileShortlistedService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_PROFILE_SHORTLIST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// save Profile shortlisted
function SaveProfileShortlistedService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SAVE_PROFILE_SHORTLIST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


// Get Profile Shortlist By You Service
function GetProfileShortlistByYouService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_PROFILE_SHORTLIST_BY_YOU;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Delete Profile shortlisted
function DeleteProfileShortlistedService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_DELETE_SHORTFROM_LIST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}
