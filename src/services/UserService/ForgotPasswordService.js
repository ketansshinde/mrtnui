import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const ForgotPasswordService = {
    ForgotPassword,
    ResetPassword
};


//  Forgot Password
function ForgotPassword(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_FORGOT_PASSWORD;
    
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

//  Reset Password
function ResetPassword(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_RESET_PASSWORD;
    
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}