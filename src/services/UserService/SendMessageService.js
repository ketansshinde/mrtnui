import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const SendMessageService = {
    GetSendMessage,
    SendMessageData,
    GetUserMessage
};


// Get user profile list
function GetSendMessage(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_INCOMEING_SEND_REQUEST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}

// Get user profile list
function SendMessageData(data, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_SEND_REQUEST;
    return new RestDataSource(url, fn).Store(data, (res) => {

        if (res != null) {
            fn(res)
        }
    });
}

// Get user profile list
function GetUserMessage(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_ALL_MESSAGE;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}

