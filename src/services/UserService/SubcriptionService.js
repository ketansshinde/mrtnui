import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const SubcriptionService = {
    GetSubcription,
    GetOrderDetailService,
    GetTrasactionDetailByUserIDService,
    GetTrasactionDetailService,
    GetvalidatePaymentStatusService
};


// Get user profile list
function GetSubcription(fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBCRIPTION;
    return new RestDataSource(url, fn).GetData((res) => {
        if (res != null) {
            fn(res)
        }
    });
}

// Get Order details
function GetOrderDetailService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_ORDER_DETAIL;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res)
        }
    });
}

//Get Trasaction Detail By UserID Service
function GetTrasactionDetailByUserIDService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_USER_ORDER_TRASACTION;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

//Get Trasaction Detail Service
function GetTrasactionDetailService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_ALL_USER_ORDER_TRASACTION;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


//Get validate payment status
function GetvalidatePaymentStatusService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_VALIDATE_PAYMENT_STATUS;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}
