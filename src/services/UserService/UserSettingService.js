import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";
import {
    CryptoCode,
} from "../../common/cryptoCode";

export const UserSettingService = {
    SaveProfilePhotoService,
    SaveProfilePhotoGalleryService,
    SaveProfileGovIdService,
    GetUserProfilePhoto,
    changePasswordService,
    GetGovIDService,
    GetGalleryPhotoService,
    DeleteGalleryPhotoService
};


//Save User profile photo
function SaveProfilePhotoService(UserData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_CHANGE_PROFILE_PHOTO;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

//Save User profile photo gallery
function SaveProfilePhotoGalleryService(UserData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_SAVE_GALLERY_PHOTO;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Get User Gallery photo

function GetGalleryPhotoService(userData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_GET_GALLERY_PHOTO;
    return new RestDataSource(url, fn).Store(userData, (res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}



// User profile Gov id proof
function SaveProfileGovIdService(UserData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_SAVE_GOV_ID;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

function GetGovIDService(userData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_GET_GOV_ID;
    return new RestDataSource(url, fn).Store(userData, (res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}


function GetUserProfilePhoto(userData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_DOCUMENT;
    return new RestDataSource(url, fn).Store(userData, (res) => {
        if (res != null) {
            localStorage.setItem("ProfilePhoto", CryptoCode.encryption(JSON.stringify(res.data.responseList)));
            fn(res)
        }
    });
}

// Change Password
function changePasswordService(UserData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_CHANGE_PASSWORD;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Delete gallery Photo
function DeleteGalleryPhotoService(UserData, fn) {
    
    let url = Global_var.BASEURL + Global_var.URL_DELETE_GALLERY_PHOTO;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}