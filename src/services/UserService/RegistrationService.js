import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const RegistrationService = {
    SaveRegistration,
    GetUserDetailByID,
    RegistrationConfirm
};


// 
function SaveRegistration(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_REGISTRATION;
    
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

function GetUserDetailByID(UserId, ProfileUniqueId, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USERPROFILE_BYID + "Userid=" + UserId + "&ProfileUniqueId=" + ProfileUniqueId;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}

function RegistrationConfirm(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_CONFRIM_ACCOUNT;
    
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// function GetUserAddressDetail(UserId, ProfileUniqueId, fn) {
//     let url = Global_var.BASEURL + Global_var.URL_USERPROFILE_BYID + "Userid=" + UserId + "&ProfileUniqueId=" + ProfileUniqueId;
//     return new RestDataSource(url, fn).GetData((res) => {
        
//         if (res != null) {
//             fn(res)
//         }
//     });
// }





