import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const UserProfileService = {
    GetUserProfileService,
    GetProfileViewerService,
    GetIncomingSendRequestService
};


// Get user profile list
function GetUserProfileService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_USER_PROFILE;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Get user profile viewer list
function GetProfileViewerService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_REGISTRATION;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Get user Incoming Send Request 
function GetIncomingSendRequestService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_REGISTRATION;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}






