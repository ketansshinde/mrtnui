import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";

export const ProfileInterestService = {
    GetProfileInterestService,
    GetProfileInterestDeclineService,
    GetProfileInterestDeclineByYouService,
    SaveProfileInterestService,
    SaveProfileInterestDeclineService,
    SaveChangeMindInterestService
}

// Profile Interest
function GetProfileInterestService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_PROFILE_INTEREST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Get Profile Interest Decline Service
function GetProfileInterestDeclineService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_PROFILE_INTEREST_DECLINE;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

//GetProfile Interest Decline By You Service
function GetProfileInterestDeclineByYouService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_PROFILE_INTEREST_DECLINE_BY_YOU;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// save Profile Interestlisted
function SaveProfileInterestService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SAVE_PROFILE_INTEREST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

// Save Profile Interest Decline Service
function SaveProfileInterestDeclineService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SAVE_PROFILE_INTEREST_DECLINE;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


// Save Change Mind Interest Service
function SaveChangeMindInterestService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SAVE_PROFILE_INTEREST;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}
