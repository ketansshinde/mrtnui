import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";

export const ProfileViewerService = {
    GetProfileViewerService,
}

// Profile Viewer
function GetProfileViewerService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_GET_USER_PROFILEVIEWER;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}
