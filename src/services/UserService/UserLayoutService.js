import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";

export const UserLayoutService = {
    GetUserDetailByID,
    GetUserAddressDetail,
    GetUserExpectationDetail,
    GetUserLifeStyle,
    GetUserOccupation,
    GetUserReligionById,
    GetUserPersonalById,
    GetUserFamilyBackGround,

    //Update user details

    UpdateUserProfileService,
    UpdateUserAddressService,
    UpdateUserExpectationService,
    UpdateUserFamilyBackgeoundService,
    UpdateUserLifeStyleService,
    UpdateUserOccupationService,
    UpdateUserPersonalService,
    UpdateUserReligionService

};

function GetUserDetailByID(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USERPROFILE_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

function GetUserAddressDetail(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_ADDRESS_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

function GetUserExpectationDetail(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_EXPECTATION_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


function GetUserLifeStyle(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_LIFESTYLE_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

function GetUserOccupation(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_OCCUPATION_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


function GetUserReligionById(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_RELIGION_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res)
        }
    });
}

function GetUserPersonalById(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_PERSONAL_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

function GetUserFamilyBackGround(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_FAMILYBACKGROUND_BYID;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

//-------------- User Update Details------------------------------------
//Update user details
///-----------------Update USER Profie----------------------------------
function UpdateUserProfileService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_PROFILE;

    return new RestDataSource(url,fn).Update(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER Address----------------------------------
function UpdateUserAddressService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_ADDRESS;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER Expectation----------------------------------
function UpdateUserExpectationService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_EXPECTATION;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER family Background----------------------------------
function UpdateUserFamilyBackgeoundService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_FAMILY_BACKGROUND;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER Life style----------------------------------
function UpdateUserLifeStyleService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_LIFESTYLE;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER Occupation----------------------------------
function UpdateUserOccupationService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_OCCUPATION;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER personal----------------------------------

function UpdateUserPersonalService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_PERSONAL;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}

///-----------------Update USER Religion----------------------------------
function UpdateUserReligionService(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_UPDATE_USER_RELIGION;

    return new RestDataSource(url,fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}




