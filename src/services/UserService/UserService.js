import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";
import {
    CryptoCode,
} from "../../common/cryptoCode";

export const UserService = {
    GetProfilePercentage
};

function GetProfilePercentage(userData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_PROFIE_COMPLETENESS;
    return new RestDataSource(url, fn).Store(userData, (res) => {
        if (res.response !== undefined) {
            if (res.response.status === 401 || res.response.statusText === "Unauthorized")
                checkUserSession.refreshToken();
        }
        else if (res != null) {
            fn(res);
        }
    });
}
