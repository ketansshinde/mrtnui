import Axios from "axios";
import { CryptoCode } from "./../common/cryptoCode";
import { Global_var } from "../global/global_var";
import { checkUserSession } from "../common/userSessionExpiry";

export default class RestDataSource {
  constructor(base_url, errorCallback) {
    Axios.defaults.headers.common["Authorization"] =
      "Bearer " + CryptoCode.decryption(localStorage.getItem("token")) === null
        ? ""
        : "Bearer " + CryptoCode.decryption(localStorage.getItem("token"));

    // Userid and userLogin
    // Axios.defaults.headers.common["userId"] = "";
    // Axios.defaults.headers.common["userlogin"] = "";

    // Portal URL of API
    this.BASE_URL = base_url;
    this.handleError = errorCallback;
  }

  async GetData(callback) {
    this.SendRequest("get", this.BASE_URL, callback);
  }
  async GetOneByParam(id, callback) {
    this.SendRequest("get", `${this.BASE_URL}?${id}`, callback);
  }
  async GetOne(data, callback) {
    this.SendRequest("get", this.BASE_URL, callback, data);
  }
  async Store(data, callback) {
    this.SendRequest("post", this.BASE_URL, callback, data);
  }
  async StoreTemp(data, callback) {
    this.TempSendRequest("post", this.BASE_URL, callback, data);
  }
  async Update(data, callback) {
    this.SendRequest("put", this.BASE_URL, callback, data);
  }
  async Delete(data, callback) {
    this.SendRequest("delete", this.BASE_URL, callback, data);
  }
  async SendRequest(method, url, callback, data) {
    await Axios.request({
      method: method,
      url: url,
      data: data,
    }).then(function (response) {
      callback(response);
    }).catch(function (error) {
      if (error.response !== undefined) {
        if (error.response.status === 401 || error.response.statusText === "Unauthorized")
          checkUserSession.refreshToken();
      }
    });
  }
}
