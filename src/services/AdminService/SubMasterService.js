import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";
import {
    CryptoCode,
} from "../../common/cryptoCode";

export const SubMasterService = {
    GetSubMaster,
    SaveSubMaster,
    UpdateSubMaster,
    DeleteSubMaster
};

function GetSubMaster(fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBMASTER;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            localStorage.setItem("submaster", CryptoCode.encryption(JSON.stringify(res.data.responseList)));
            fn(res)
        }
    });
}


// SaveSubMaster 
function SaveSubMaster(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBMASTER;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}


// UpdateSubMaster 
function UpdateSubMaster(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBMASTER;

    return new RestDataSource(url, fn).Update(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}



// DeleteSubMaster 
function DeleteSubMaster(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBMASTER;

    return new RestDataSource(url, fn).Delete(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}







