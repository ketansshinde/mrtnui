import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";
import {
    CryptoCode,
  } from "../../common/cryptoCode";

export const SubCasteService = {
    GetSubCasteService,
    SaveUpdateSubCaste,
    DeleteSubCaste
};

function GetSubCasteService(fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBCASTE;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            localStorage.setItem("subcaste", CryptoCode.encryption(JSON.stringify(res.data.responseList)));
            fn(res)
        }
    });
}


// SaveMaster 
function SaveUpdateSubCaste(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBCASTE;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}

// UpdateMaster 
function DeleteSubCaste(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_SUBCASTE;

    return new RestDataSource(url, fn).Delete(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}







