import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const ErrorLogService = {
    GetErrorLog,
};

function GetErrorLog(fn) {
    let url = Global_var.BASEURL + Global_var.URL_ERRORLOG;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}






