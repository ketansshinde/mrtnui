import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const CityService = {
    GetCity,
    SaveCity,
    UpdateCity,
    DeleteCity
};
//GetCity
function GetCity(fn) {
    let url = Global_var.BASEURL + Global_var.URL_CITY;
    return new RestDataSource(url, fn).GetData((res) => {
        if (res != null) {
            fn(res);
        }
    });
}


// SaveCity 
function SaveCity(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_CITY;

    return new RestDataSource(url, null, "CORE", fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


// UpdateCity 
function UpdateCity(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_CITY;

    return new RestDataSource(url, null, "CORE", fn).Update(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}



// DeleteCity 
function DeleteCity(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_CITY;

    return new RestDataSource(url, null, "CORE", fn).Delete(UserData, (res) => {
        if (res != null) {
            fn(res)
        }
    });
}





