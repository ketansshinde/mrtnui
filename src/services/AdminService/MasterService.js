import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";
import {
    CryptoCode,
} from "../../common/cryptoCode";

export const MasterService = {
    GetMaster,
    SaveMaster,
    UpdateMaster,
    DeleteMaster
};

function GetMaster(fn) {
    let url = Global_var.BASEURL + Global_var.URL_MASTER;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            localStorage.setItem("master", CryptoCode.encryption(JSON.stringify(res.data.responseList)));
            fn(res)
        }
    });
}


// SaveMaster 
function SaveMaster(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_MASTER;

    return new RestDataSource(url, fn).Store(UserData, (res) => {

        if (res != null) {
            return fn(res)
        }
    });
}


// UpdateMaster 
function UpdateMaster(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_MASTER;

    return new RestDataSource(url, fn).Update(UserData, (res) => {

        if (res != null) {
            return fn(res)
        }
    });
}



// UpdateMaster 
function DeleteMaster(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_MASTER;

    return new RestDataSource(url, fn).Delete(UserData, (res) => {

        if (res != null) {
            return fn(res)
        }
    });
}







