import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const DocumentService = {
    GetDocumentService,
};

function GetDocumentService(userdata, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USER_DOCUMENT;
    return new RestDataSource(url, fn).Store(userdata, (res) => {
        if (res != null) {
            fn(res)
        }
    });
}






