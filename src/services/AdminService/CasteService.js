import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";
import {
    CryptoCode,
  } from "../../common/cryptoCode";

export const CasteService = {
    GetCasteService,
    SaveUpdateCaste,
    DeleteCaste
};

function GetCasteService(fn) {
    let url = Global_var.BASEURL + Global_var.URL_CASTE;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            localStorage.setItem("caste", CryptoCode.encryption(JSON.stringify(res.data.responseList)));
            fn(res)
        }
    });
}


// SaveMaster 
function SaveUpdateCaste(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_CASTE;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}

// UpdateMaster 
function DeleteCaste(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.Caste;

    return new RestDataSource(url, fn).Delete(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}







