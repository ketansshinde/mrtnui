import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const UserDetailService = {
    GetUserDetail,
};

function GetUserDetail(UserId, fn) {
    let url = Global_var.BASEURL + Global_var.URL_USERPROFILE + UserId;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}







