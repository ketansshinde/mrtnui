import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const StateService = {
    GetState,
    SaveState,
    UpdateState,
    DeleteState
};

function GetState(fn) {
    let url = Global_var.BASEURL + Global_var.URL_STATE;
    return new RestDataSource(url, fn).GetData((res) => {
        if (res != null) {
            fn(res);
        }
    });
}


// SaveState 
function SaveState(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_STATE;

    return new RestDataSource(url, null, "CORE", fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}


// UpdateState 
function UpdateState(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_STATE;

    return new RestDataSource(url, null, "CORE", fn).Update(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}



// UpdateState 
function DeleteState(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_STATE;

    return new RestDataSource(url, null, "CORE", fn).Delete(UserData, (res) => {
        if (res != null) {
            fn(res)
        }
    });
}







