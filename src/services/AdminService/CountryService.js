import { Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const CountryService = {
    GetCountry,
    SaveCountry,
    UpdateCountry,
    DeleteCountry
};

function GetCountry(fn) {
    let url = Global_var.BASEURL + Global_var.URL_COUNTRY;
    return new RestDataSource(url, fn).GetData((res) => {
        
        if (res != null) {
            fn(res)
        }
    });
}


// SaveCountry 
function SaveCountry(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_COUNTRY;

    return new RestDataSource(url, fn).Store(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}


// UpdateCountry 
function UpdateCountry(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_COUNTRY;

    return new RestDataSource(url, fn).Update(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}



// UpdateCountry 
function DeleteCountry(UserData, fn) {
    let url = Global_var.BASEURL + Global_var.URL_COUNTRY;

    return new RestDataSource(url, fn).Delete(UserData, (res) => {
        
        if (res != null) {
            return fn(res)
        }
    });
}







