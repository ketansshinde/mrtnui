import { Business_Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const BusinessSubcriptionService = {
    GetSubcription,
    // GetOrderDetailService,
    // GetTrasactionDetailByUserIDService,
    // GetTrasactionDetailService
};


// Get user profile list
function GetSubcription(fn) {
    let url = Business_Global_var.BUSINESS_BASEURL + Business_Global_var.URL_SUBCRIPTION;
    return new RestDataSource(url, fn).GetData((res) => {
        if (res != null) {
            fn(res)
        }
    });
}

// Get Order details
// function GetOrderDetailService(UserData, fn) {
//     let url = Business_Global_var.BASEURL + Business_Global_var.URL_GET_ORDER_DETAIL;
//     return new RestDataSource(url, fn).Store(UserData, (res) => {
//         if (res != null) {
//             fn(res)
//         }
//     });
// }

// //Get Trasaction Detail By UserID Service
// function GetTrasactionDetailByUserIDService(UserData, fn) {
//     let url = Business_Global_var.BASEURL + Business_Global_var.URL_GET_USER_ORDER_TRASACTION;
//     return new RestDataSource(url, fn).Store(UserData, (res) => {
//         if (res != null) {
//             fn(res);
//         }
//     });
// }

// //Get Trasaction Detail Service
// function GetTrasactionDetailService(UserData, fn) {
//     let url = Business_Global_var.BASEURL + Business_Global_var.URL_GET_ALL_USER_ORDER_TRASACTION;
//     return new RestDataSource(url, fn).Store(UserData, (res) => {
//         if (res != null) {
//             fn(res);
//         }
//     });
// }
