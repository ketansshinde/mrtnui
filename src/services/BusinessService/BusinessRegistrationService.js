import { Business_Global_var } from "../../global/global_var";
import RestDataSource from "../restdatasource";
import { checkUserSession } from "../../common/userSessionExpiry";


export const BusinessRegistrationService = {
    SaveBusinessRegistrationService
};


// BUSINESS REGISTRATION
function SaveBusinessRegistrationService(UserData, fn) {
    let url = Business_Global_var.BUSINESS_BASEURL + Business_Global_var.URL_BUSINESS_REGISTRATION;
    return new RestDataSource(url, fn).Store(UserData, (res) => {
        if (res != null) {
            fn(res);
        }
    });
}






