export const Global_var = {
  BASEURL: "http://localhost:4000/api",// Local URL
  URL_USER_PROFILE_PHOTO: "http://localhost:81/MRTN/Images/users/profile-photo/", //Local URL
  URL_USER_GALLERY: "http://localhost:81/MRTN/Images/users/user-gallery/", //Local URL
  URL_USER_GOV_ID: "http://localhost:81/MRTN/Images/users/document/", //Local URL

  URL_IP_ADDRESS: "http://api.ipify.org?format=json",

  URL_JWT_TOKEN: "/Users/authenticate",
  URL_REFRESH_JWT_TOKEN: "/users/refresh",
  URL_GET_USER_LOGIN_DETAIL: "/UserPortal/GetUserLoginDetail",
  URL_USERPROFILE: "/UserPortal?Userid=",
  URL_COUNTRY: "/Country",
  URL_STATE: "/State",
  URL_CITY: "/City",
  URL_MASTER: "/Master",
  URL_SUBMASTER: "/SubMaster",
  URL_ERRORLOG: "/ErrorLog",
  URL_CASTE: "/Caste",
  URL_SUBCASTE: "/SubCaste",
  URL_SUBCRIPTION: "/Subcription",
  URL_USER_DOCUMENT: "/UserDocument/GetUserPorfilePhoto",



  // User 
  URL_REGISTRATION: "/UserReg/CreateRegistrationStaging",
  URL_FORGOT_PASSWORD: "/UserReg/ForgotPassword",
  URL_RESET_PASSWORD: "/UserReg/ResetPassword",
  URL_CONFRIM_ACCOUNT: "/UserReg/ConfirmUserAccount",
  URL_USERPROFILE_BYID: "/UserPortal/GetByUser",
  URL_USER_ADDRESS_BYID: "/UserRegistration/GetUserRegistrationByUserId",
  URL_USER_EXPECTATION_BYID: "/PersonalExpection/GetUserExpectationByID",
  URL_USER_LIFESTYLE_BYID: "/UserLifeStyle/GetUserLifestyleByID",
  URL_USER_OCCUPATION_BYID: "/Occupation/GetOccupationsByID",
  URL_USER_RELIGION_BYID: "/UserReligious/GetReligionBYID",
  URL_USER_PERSONAL_BYID: "/UserProfile/GetUserProfileById",
  URL_USER_FAMILYBACKGROUND_BYID: "/UserFamilyBackground/GetUserFamilyBackgroundByID",

  //Update user details

  URL_UPDATE_USER_PROFILE: "/UserPortal",
  URL_UPDATE_USER_ADDRESS: "/UserRegistration",
  URL_UPDATE_USER_EXPECTATION: "/PersonalExpection/SaveUpdatePersonalExpection",
  URL_UPDATE_USER_FAMILY_BACKGROUND: "/UserFamilyBackground",
  URL_UPDATE_USER_LIFESTYLE: "/UserLifeStyle",
  URL_UPDATE_USER_OCCUPATION: "/Occupation",
  URL_UPDATE_USER_PERSONAL: "/UserProfile",
  URL_UPDATE_USER_RELIGION: "/UserReligious",
  URL_PROFIE_COMPLETENESS: "/UserPortal/GetProfilePercentage",


  // User Report
  URL_GET_USER_PROFILE: "/MrtnProfile/GetUserProfile",
  URL_GET_USER_PROFILEVIEWER: "/MrtnProfile/GetProfileViewer",
  URL_GET_INCOMEING_SEND_REQUEST: "/MrtnProfile/GetIncomingSendRequest",
  URL_SEND_REQUEST: "/SendRequest",

  // Messages
  URL_GET_ALL_MESSAGE: "/SendRequest/GetUserMessages",

  // User Activity 
  URL_PROFILE_SHORTLIST: "/ProfileShortlisted/GetShortlisted",
  URL_PROFILE_SHORTLIST_BY_YOU: "/ProfileShortlisted/GetShortlistedByYou",
  URL_SAVE_PROFILE_SHORTLIST: "/ProfileShortlisted/SaveProfileShortlist",
  URL_DELETE_SHORTFROM_LIST: "/ProfileShortlisted/DeleteProfileShortlist",

  // Interest Profile
  URL_PROFILE_INTEREST: "/ProfileInterest/GetProfileInterest",
  URL_PROFILE_INTEREST_DECLINE: "/ProfileInterest/GetProfileInterestDecline",
  URL_PROFILE_INTEREST_DECLINE_BY_YOU: "/ProfileInterest/GetProfileInterestDeclineByYou",
  URL_SAVE_PROFILE_INTEREST: "/ProfileInterest/SaveProfileInterest",
  URL_SAVE_PROFILE_INTEREST_DECLINE: "/ProfileInterest/SaveProfileInterestDecline",
  URL_CHANGE_MIND_INTEREST: "/ProfileInterest/SaveProfileInterestChangeMyMind",


  // User Setting 
  URL_CHANGE_PROFILE_PHOTO: "/UserDocument",
  URL_SAVE_GALLERY_PHOTO: "/UserDocument/SaveGallery",
  URL_GET_GALLERY_PHOTO: "/UserDocument/GetUserGallery",
  URL_DELETE_GALLERY_PHOTO: "/UserDocument/DeleteUserGallery",
  URL_SAVE_GOV_ID: "/UserDocument/SaveGovID",
  URL_GET_GOV_ID: "/UserDocument/GetGovID",
  URL_CHANGE_PASSWORD: "/UserPortal/UserChangePassord",


  // User Subcription
  URL_GET_ORDER_DETAIL: "/OrderDetail/CreateNewOrder",
  URL_GET_USER_ORDER_TRASACTION: "/OrderDetail/GetAllOrderByUser",
  URL_GET_ALL_USER_ORDER_TRASACTION: "/OrderDetail/GetAllOrder",
  URL_VALIDATE_PAYMENT_STATUS: "/OrderDetail/ValidatePaymentStatus"


};

export const Business_Global_var = {
  BUSINESS_BASEURL: "http://localhost:5000/api",// Local URL
  URL_IP_ADDRESS: "http://api.ipify.org?format=json",

  URL_JWT_TOKEN: "/Users/authenticate",
  URL_BUSINESS_REGISTRATION: "/UserBusiness/SaveBusiness",
  URL_SUBCRIPTION: "/Subcription",
}

// -------- Local storage encyption key-----------------------------
export const encryptionAndDecryption = {
  key: "10101010101010101010101"
}

// --------- Master Values------------------------------------------
export const MasterValue = {
  Marital_Status: 1,
  Body_Type: 2,
  Complexion: 3,
  Spectacle: 4,
  Profile_created_by: 5,
  Gender: 6,
  Diet: 7,
  Smoke: 8,
  Drink: 9,
  Manglik: 10,
  Patricka: 11,
  Family_values: 12,
  Family_occupation: 13,
  Inter_caste: 14,
  Gan: 15,
  Nadi: 16,
  Nakshatra: 17,
  Ras: 18,
  Charan: 19,
  Blood_Group: 20,
  Any_Disability: 21,
  Education: 22,
  Branch: 23,
  test_master112: 25,
  Relation: 26,
  caste: 27,
  sub_caste: 28,
  Religion: 29,
  Mother_Tongue: 30,
  Family_Value_Detail: 33,
  Business: 34,
  Week_Days: 35,
  Business_Photo: 36,
  Payment_Type: 37
}


export const RegexCode = {
  PAN: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
  ADHAR: /^[2-9]{1}[0-9]{3}\s{1}[0-9]{4}\s{1}[0-9]{4}$/,
}