import React from "react";
import { withStyles, createMuiTheme } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import { TextField, Checkbox } from "formik-material-ui";

// The `withStyles()` higher-order component is injecting a `classes`
// prop that is used by the `Button` component.

const color = "#f1592a";

export const LoginButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #d5486a 30%, #d5486a 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 30px",
    width: "100%",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const RegisterButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #0492cd 30%, #0492cd 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 30px",
    width: "100%",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const NewMailButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #a53939 30%, #ff1717 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 10px",
    width: "206px",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const SubmitButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #d5486a 30%, #d5486a 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 10px",
    width: "auto",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const CloseButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #768eab 30%, #768eab 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 10px",
    width: "auto",
    marginRight: "5px",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const ProSubButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #0f75bc 30%, #0f75bc 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 10px",
    width: "auto",
    marginRight: "5px",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

// white button
export const SubscribeSubButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #ffffff 30%, #ffffff 90%)",
    borderRadius: 3,
    border: "1px solid #ccc",
    color: "black",
    height: 35,
    padding: "0 10px",
    width: "auto",
    marginRight: "5px",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

// Invite  button
export const InviteButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #d5486a 30%, #d5486a 90%)",
    borderRadius: 10,
    border: 0,
    color: "white",
    height: 45,
    padding: "0 10px",
    width: "auto",
    fontSize: "16px",
    fontFamily:"Rubik"
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);



// Blue button
export const BackButton = withStyles({
  root: {
    background: "linear-gradient(45deg, #768eab 30%, #768eab 90%)",
    borderRadius: 3,
    border: 0,
    color: "white",
    height: 35,
    padding: "0 10px",
    width: "auto",
    marginRight: "5px",
  },
  label: {
    textTransform: "capitalize",
  },
})(Button);

export const TextBoxFieldLogin = withStyles({
  root: {
    borderRadius: 3,
    border: "#111",
    color: "#111",
    width: "100%",
    marginRight: "5px",
  },
  label: {
    textTransform: "capitalize",
  },
})(TextField);

export const TextBoxField = withStyles({
  root: {
    borderRadius: 3,
    border: "#111",
    color: "#111",
    width: "100%",
    marginRight: "5px",
    fontSize: "14px !important"
  },
  label: {
    textTransform: "capitalize",
  },
})(TextField);


export const CheckBoxField = withStyles({
  root: {
    borderRadius: 3,
    border: "#111",
  },
  label: {
    textTransform: "capitalize",
  },
})(Checkbox);

export const getMuiTheme = () =>
  createMuiTheme({
    overrides: {
      MUIDataTable: {
        root: {
          backgroundColor: "#cccccc",
          fontSize: "14px",
          borderTopWidth: 1,
          borderColor: "red",
          borderStyle: "solid",
        },
        paper: {
          boxShadow: "none",
        },
      },
      MUIDataTableBodyCell: {
        root: {
          // backgroundColor: "#eeeeee",
          fontSize: "14px",
        },
      },
      MUIDataTableHeadCell: {
        root: {
          // backgroundColor: "#eeeeee",
          fontSize: "14px",
          fontWeight: "bold",
        },
      },
    },
  });
