import { MasterService } from "../../services/AdminService/MasterService";

export const FETCH_MASTER_SUCCESS = "FETCH_MASTER_SUCCESS";
export const FETCH_MASTER_PENDING = "FETCH_MASTER_PENDING";

export const ADD_MASTER_SUCCESS = "ADD_MASTER_SUCCESS";
export const ADD_MASTER_PENDING = "ADD_MASTER_PENDING";

export const UPDATE_MASTER_SUCCESS = "UPDATE_MASTER_SUCCESS";
export const UPDATE_MASTER_PENDING = "UPDATE_MASTER_PENDING";


export function fetchMasterSuccess(MasterData) {
  return {
    type: FETCH_MASTER_SUCCESS,
    payload: MasterData,
  };
}

export function fetchMasterPending() {
  return {
    type: FETCH_MASTER_PENDING,

  };
}

export function fetchMaster() {
  
  return (dispatch) => {
    dispatch(fetchMasterPending());
    MasterService.GetMaster((res) => {
      dispatch(fetchMasterSuccess(res.data.responseList));
    });
  };
}


export function AddMaster(data, fn) {
  return (dispatch) => {
    dispatch({
      type: ADD_MASTER_SUCCESS,
      payload: data,
    });
    MasterService.SaveMaster(data, (res) => fn(res));
  };
}

export function EditMaster(data, fn) {
  return (dispatch) => {
    dispatch({
      type: UPDATE_MASTER_SUCCESS,
      payload: data,
    });
    MasterService.UpdateMaster(data, (res) => fn(res));
  };
}
