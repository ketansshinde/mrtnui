import { ErrorLogService } from "../../services/AdminService/ErrorLogService";

export const FETCH_ERRORLOG_SUCCESS = "FETCH_ERRORLOG_SUCCESS";
export const FETCH_ERRORLOG_PENDING = "FETCH_ERRORLOG_PENDING";



export function fetchErrorLogSuccess(MasterData) {
  return {
    type: FETCH_ERRORLOG_SUCCESS,
    payload: MasterData,
  };
}

export function fetchErrorLogPending() {
  return {
    type: FETCH_ERRORLOG_PENDING,

  };
}

export function fetchErrorLog() {
  
  return (dispatch) => {
    dispatch(fetchErrorLogPending());
    ErrorLogService.GetErrorLog((res) => {
      dispatch(fetchErrorLogSuccess(res.data.responseList));
    });
  };
}

