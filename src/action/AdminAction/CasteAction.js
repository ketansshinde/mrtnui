import { CasteService } from "../../services/AdminService/CasteService";

export const FETCH_CASTE_SUCCESS = "FETCH_CASTE_SUCCESS";
export const FETCH_CASTE_PENDING = "FETCH_CASTE_PENDING";

export const ADD_CASTE_SUCCESS = "ADD_CASTE_SUCCESS";
export const ADD_CASTE_PENDING = "ADD_CASTE_PENDING";

export function fetchCasteSuccess(CasteData) {
  return {
    type: FETCH_CASTE_SUCCESS,
    payload: CasteData,
  };
}

export function fetchCastePending() {
  return {
    type: FETCH_CASTE_PENDING,

  };
}

export function fetchCaste() {
  
  return (dispatch) => {
    dispatch(fetchCastePending());
    CasteService.GetCasteService((res) => {
      dispatch(fetchCasteSuccess(res.data.responseList));
    });
  };
}


export function AddCaste(data, fn) {
  return (dispatch) => {
    dispatch({
      type: ADD_CASTE_SUCCESS,
      payload: data,
    });
    CasteService.SaveUpdateCaste(data, (res) => fn(res));
  };
}
