import { StateService } from "../../services/AdminService/StateService";

export const FETCH_STATE_SUCCESS = "FETCH_STATE_SUCCESS";
export const FETCH_STATE_PENDING = "FETCH_STATE_PENDING";

export const ADD_STATE_SUCCESS = "ADD_STATE_SUCCESS";
export const ADD_STATE_PENDING = "ADD_STATE_PENDING";

export const UPDATE_STATE_SUCCESS = "UPDATE_STATE_SUCCESS";
export const UPDATE_STATE_PENDING = "UPDATE_STATE_PENDING";


export function fetchStateSuccess(StateData) {
    return {
        type: FETCH_STATE_SUCCESS,
        payload: StateData,
    };
}

export function fetchStatePending() {
    return {
        type: FETCH_STATE_PENDING,

    };
}

export function fetchState() {
    
    return (dispatch) => {
        dispatch(fetchStatePending());
        StateService.GetState((res) => {
            dispatch(fetchStateSuccess(res.data.responseList));
        });
    };
}


export function AddState(data, fn) {
    return (dispatch) => {
        dispatch({
            type: ADD_STATE_SUCCESS,
            payload: data,
        });
        StateService.SaveState(data, (res) => fn(res));
    };
}

export function EditState(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_STATE_SUCCESS,
            payload: data,
        });
        StateService.UpdateState(data, (res) => fn(res));
    };
}
