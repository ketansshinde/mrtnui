import { SubCasteService } from "../../services/AdminService/SubCasteService";

export const FETCH_SUBCASTE_SUCCESS = "FETCH_SUBCASTE_SUCCESS";
export const FETCH_SUBCASTE_PENDING = "FETCH_SUBCASTE_PENDING";

export const ADD_SUBCASTE_SUCCESS = "ADD_SUBCASTE_SUCCESS";
export const ADD_SUBCASTE_PENDING = "ADD_SUBCASTE_PENDING";

export function fetchSubCasteSuccess(SubCasteData) {
  return {
    type: FETCH_SUBCASTE_SUCCESS,
    payload: SubCasteData,
  };
}

export function fetchSubCastePending() {
  return {
    type: FETCH_SUBCASTE_PENDING,

  };
}

export function fetchSubCaste() {
  
  return (dispatch) => {
    dispatch(fetchSubCastePending());
    SubCasteService.GetSubCasteService((res) => {
      dispatch(fetchSubCasteSuccess(res.data.responseList));
    });
  };
}


export function AddSubCaste(data, fn) {
  return (dispatch) => {
    dispatch({
      type: ADD_SUBCASTE_SUCCESS,
      payload: data,
    });
    SubCasteService.SaveUpdateSubCaste(data, (res) => fn(res));
  };
}
