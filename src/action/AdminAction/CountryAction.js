import { CountryService } from "../../services/AdminService/CountryService";

export const FETCH_COUNTRY_SUCCESS = "FETCH_COUNTRY_SUCCESS";
export const FETCH_COUNTRY_PENDING = "FETCH_COUNTRY_PENDING";

export const ADD_COUNTRY_SUCCESS = "ADD_COUNTRY_SUCCESS";
export const ADD_COUNTRY_PENDING = "ADD_COUNTRY_PENDING";

export const UPDATE_COUNTRY_SUCCESS = "UPDATE_COUNTRY_SUCCESS";
export const UPDATE_COUNTRY_PENDING = "UPDATE_COUNTRY_PENDING";


export function fetchCountrySuccess(CountryData) {
  return {
    type: FETCH_COUNTRY_SUCCESS,
    payload: CountryData,
  };
}

export function fetchCountryPending() {
  return {
    type: FETCH_COUNTRY_PENDING,

  };
}

export function fetchCountry() {
  
  return (dispatch) => {
    dispatch(fetchCountryPending());
    CountryService.GetCountry((res) => {
      dispatch(fetchCountrySuccess(res.data.responseList));
    });
  };
}


export function AddCoutry(data, fn) {
  return (dispatch) => {
    dispatch({
      type: ADD_COUNTRY_SUCCESS,
      payload: data,
    });
    CountryService.SaveCountry(data, (res) => fn(res));
  };
}

export function EditCountry(data, fn) {
  return (dispatch) => {
    dispatch({
      type: UPDATE_COUNTRY_SUCCESS,
      payload: data,
    });
    CountryService.UpdateCountry(data, (res) => fn(res));
  };
}
