import { DocumentService } from "../../services/AdminService/DocumentService";

export const FETCH_DOCUMENT_SUCCESS = "FETCH_DOCUMENT_SUCCESS";
export const FETCH_DOCUMENT_PENDING = "FETCH_DOCUMENT_PENDING";



export function fetchDocumentSuccess(DocumentData) {
  return {
    type: FETCH_DOCUMENT_SUCCESS,
    payload: DocumentData,
  };
}

export function fetchDocumentPending() {
  return {
    type: FETCH_DOCUMENT_PENDING,

  };
}

export function fetchDocument(userdata,fn) {
  
  return (dispatch) => {
    dispatch(fetchDocumentPending());
    DocumentService.GetDocumentService(userdata, (res) => {
        
      dispatch(fetchDocumentSuccess(res.data.responseList));
    });
  };
}

