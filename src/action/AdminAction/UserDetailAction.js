import { UserDetailService } from "../../services/AdminService/UserDetailService";

export const FETCH_USERDETAIL_SUCCESS = "FETCH_USERDETAIL_SUCCESS";
export const FETCH_USERDETAIL_PENDING = "FETCH_USERDETAIL_PENDING";

export function fetchUserDetailSuccess(UserData) {
    return {
        type: FETCH_USERDETAIL_SUCCESS,
        payload: UserData,
    };
}

export function fetchUserDetailPending() {
    return {
        type: FETCH_USERDETAIL_PENDING,

    };
}

export function fetchUserDetail(User) {
    
    return (dispatch) => {
        dispatch(fetchUserDetailPending());
        UserDetailService.GetUserDetail(User, (res) => {
            dispatch(fetchUserDetailSuccess(res.data.responseList));
        });
    };
}