import { SubMasterService } from "../../services/AdminService/SubMasterService";

export const FETCH_SUBMASTER_SUCCESS = "FETCH_SUBMASTER_SUCCESS";
export const FETCH_SUBMASTER_PENDING = "FETCH_SUBMASTER_PENDING";

export const ADD_SUBMASTER_SUCCESS = "ADD_SUBMASTER_SUCCESS";
export const ADD_SUBMASTER_PENDING = "ADD_SUBMASTER_PENDING";

export const UPDATE_SUBMASTER_SUCCESS = "UPDATE_SUBMASTER_SUCCESS";
export const UPDATE_SUBMASTER_PENDING = "UPDATE_SUBMASTER_PENDING";


export function fetchSubMasterSuccess(MasterData) {
  return {
    type: FETCH_SUBMASTER_SUCCESS,
    payload: MasterData,
  };
}

export function fetchSubMasterPending() {
  return {
    type: FETCH_SUBMASTER_PENDING,

  };
}

export function fetchSubMaster() {
  
  return (dispatch) => {
    dispatch(fetchSubMasterPending());
    SubMasterService.GetSubMaster((res) => {
      dispatch(fetchSubMasterSuccess(res.data.responseList));
    });
  };
}


export function AddSubMaster(data, fn) {
  return (dispatch) => {
    dispatch({
      type: ADD_SUBMASTER_SUCCESS,
      payload: data,
    });
    SubMasterService.SaveSubMaster(data, (res) => fn(res));
  };
}

export function EditSubMaster(data, fn) {
  return (dispatch) => {
    dispatch({
      type: UPDATE_SUBMASTER_SUCCESS,
      payload: data,
    });
    SubMasterService.UpdateSubMaster(data, (res) => fn(res));
  };
}
