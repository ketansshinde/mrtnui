import { CityService } from "../../services/AdminService/CityService";

export const FETCH_CITY_SUCCESS = "FETCH_CITY_SUCCESS";
export const FETCH_CITY_PENDING = "FETCH_CITY_PENDING";
export const FETCH_CITY_ERROR = "FETCH_CITY_ERROR";

export const ADD_CITY_SUCCESS = "ADD_CITY_SUCCESS";
export const ADD_CITY_PENDING = "ADD_CITY_PENDING";

export const UPDATE_CITY_SUCCESS = "UPDATE_CITY_SUCCESS";
export const UPDATE_CITY_PENDING = "UPDATE_CITY_PENDING";

export function fetchCitySuccess(expensesData) {
    return {
      type: FETCH_CITY_SUCCESS,
      payload: expensesData,
    };
  }
  
  export function fetchCityPending() {
    return {
      type: FETCH_CITY_PENDING,
  
    };
  }
  
  export function fetchCity() {
    return (dispatch) => {
      dispatch(fetchCityPending());
      CityService.GetCity((res) => {
        
        dispatch(fetchCitySuccess(res.data.responseList));
      });
    };
  }


  
export function AddCity(data, fn) {
  return (dispatch) => {
      dispatch({
          type: ADD_CITY_SUCCESS,
          payload: data,
      });
      CityService.SaveCity(data, (res) => fn(res));
  };
}

export function EditCity(data, fn) {
  return (dispatch) => {
      dispatch({
          type: UPDATE_CITY_SUCCESS,
          payload: data,
      });
      CityService.UpdateCity(data, (res) => fn(res));
  };
}
