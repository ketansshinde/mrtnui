import { BusinessRegistrationService } from "../../services/BusinessService/BusinessRegistrationService";

export const REGISTRATION_SUCCESS = "REGISTRATION_SUCCESS";
export const REGISTRATION_PENDING = "REGISTRATION_PENDING";

export function SaveBusinessRegistration(data, fn) {
    return (dispatch) => {
        dispatch({
            type: REGISTRATION_SUCCESS,
            payload: data,
        });
        BusinessRegistrationService.SaveBusinessRegistrationService(data, (res) =>
            fn(res)
        );
    };
}