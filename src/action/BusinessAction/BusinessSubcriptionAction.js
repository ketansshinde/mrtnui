import { BusinessSubcriptionService } from "../../services/BusinessService/BusinessSubcriptionService";

export const FETCH_SUBCRIPTION_SUCCESS = "FETCH_SUBCRIPTION_SUCCESS";
export const FETCH_SUBCRIPTION_PENDING = "FETCH_SUBCRIPTION_PENDING";
export const FETCH_SUBCRIPTION_ERROR = "FETCH_SUBCRIPTION_ERROR";



// Get user profile list
export function fetchSubscriptionSuccess(SubcriptionData) {
    return {
        type: FETCH_SUBCRIPTION_SUCCESS,
        payload: SubcriptionData,
    };
}

export function fetchSubscriptionPending() {
    return {
        type: FETCH_SUBCRIPTION_PENDING,

    };
}

export function FetchSubscription(fn) {
    
    return (dispatch) => {
        dispatch(fetchSubscriptionPending());
        BusinessSubcriptionService.GetSubcription((res) => {
            dispatch(fetchSubscriptionSuccess(res.data));
            fn(res.data);
        });
    };
}

//----------------------------------------------------------------------------------

export const GET_ORDER = "GET_ORDER";



export function GetOrderDetail(UserData, fn) {
    return (dispatch) => {
        dispatch({
            type: GET_ORDER,
            payload: UserData,
        });
        BusinessSubcriptionService.GetOrderDetailService(UserData, (res) => fn(res.data));
    };
}
//----------------------------------------------------------------------------------

export const GET_TRANCATION_DETAIL = "GET_TRANCATION_DETAIL";

export function GetTrasactionDetailByUserID(UserData, fn) {
    return (dispatch) => {
        dispatch({
            type: GET_TRANCATION_DETAIL,
            payload: UserData,
        });
        BusinessSubcriptionService.GetTrasactionDetailByUserIDService(UserData, (res) => fn(res.data));
    };
}

//----------------------------------------------------------------------------------

export const GET_ALL_TRANCATION_DETAIL = "GET_ALL_TRANCATION_DETAIL";

export function GetTrasactionDetail(UserData, fn) {
    return (dispatch) => {
        dispatch({
            type: GET_ALL_TRANCATION_DETAIL,
            payload: UserData,
        });
        BusinessSubcriptionService.GetTrasactionDetailService(UserData, (res) => fn(res.data));
    };
}