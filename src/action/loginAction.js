import { loginService } from "../services/loginService";
import { persistor } from "../store";

export const USERS_LOGIN_REQUEST = "USERS_LOGIN_REQUEST";
export const USERS_LOGIN_SUCCESS = "USERS_LOGIN_SUCCESS";
export const USERS_LOGIN_FAILURE = "USERS_LOGIN_FAILURE";
export const USER_LOGOUT = "USER_LOGOUT";
export const VALIDATE_SUBSCRIPTION = "VALIDATE_SUBSCRIPTION";

export function loginpending() {
  return {
    type: USERS_LOGIN_REQUEST,
  };
}

export function login(userData, fn) {
  return (dispatch) => {
    dispatch(loginpending());
    loginService.login(userData, (res) => {
      dispatch(loginSucess(res.data.responseObject));
      fn(res.data);
    });
  };
}

export function loginSucess(userdata) {
  return {
    type: USERS_LOGIN_SUCCESS,
    payload: userdata,
  };
}

export function loginError(error) {
  return {
    type: USERS_LOGIN_FAILURE,
    error: error,
  };
}
export async function logout() {
  await persistor.purge();
  await persistor.flush();
  return {
    type: USER_LOGOUT,
  };
}
export function logoutUser({ userName }, fn) {
  return (dispatch) => {
    loginService.logout({ userName }, async (res) => {
      const logoutAction = await logout();
      dispatch(logoutAction);
      fn(res.data);
    });
  };
}

export function validateSubscription(userData, fn) {
  return (dispatch) => {
    dispatch({
      type: VALIDATE_SUBSCRIPTION,
      payload: userData,
    });
    loginService.validateSubscription(userData, (res) => fn(res));
  };
}


