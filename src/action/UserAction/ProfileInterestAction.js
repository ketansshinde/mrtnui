import { ProfileInterestService } from "../../services/UserService/ProfileInterestService";

export const FETCH_PROFILE_INTEREST_SUCCESS = "FETCH_PROFILE_INTEREST_SUCCESS";
export const FETCH_PROFILE_INTEREST_PENDING = "FETCH_PROFILE_INTEREST_PENDING";

export function fetchProfileInterestSuccess(UserData) {
    return {
        type: FETCH_PROFILE_INTEREST_SUCCESS,
        payload: UserData,
    };
}

export function fetchProfileInterestPending() {
    return {
        type: FETCH_PROFILE_INTEREST_PENDING,

    };
}

export function FetchProfileInterest(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchProfileInterestPending());
        ProfileInterestService.GetProfileInterestService(UserData, (res) => {
            dispatch(fetchProfileInterestSuccess(res));
            fn(res);
        });
    };
}

///------------------- Fetch Profile Interest Decline------------------------

export const FETCH_PROFILE_INTEREST_DECLINE = "FETCH_PROFILE_INTEREST_DECLINE";

export function FetchProfileInterestDecline(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchProfileInterestPending());
        ProfileInterestService.GetProfileInterestDeclineService(UserData, (res) => {
            dispatch(fetchProfileInterestSuccess(res));
            fn(res);
        });
    };
}


///------------------- Fetch Profile Interest Decline------------------------

export const FETCH_PROFILE_INTEREST_DECLINE_BY_YOU = "FETCH_PROFILE_INTEREST_DECLINE_BY_YOU";

export function FetchProfileInterestDeclineByYou(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchProfileInterestPending());
        ProfileInterestService.GetProfileInterestDeclineByYouService(UserData, (res) => {
            dispatch(fetchProfileInterestSuccess(res));
            fn(res);
        });
    };
}

///------------------- Save Profile Interest------------------------

export const SAVE_PROFILE_INTEREST = "SAVE_PROFILE_INTEREST";

export function SaveProfileInterest(data, fn) {

    return (dispatch) => {
        dispatch({
            type: SAVE_PROFILE_INTEREST,
            payload: data,
        });
        ProfileInterestService.SaveProfileInterestService(data, (res) => fn(res));
    };
}

///------------------- Save Profile Interest Decline------------------------

export const SAVE_PROFILE_INTEREST_DECLINE = "SAVE_PROFILE_INTEREST_DECLINE";

export function SaveProfileInterestDecline(data, fn) {

    return (dispatch) => {
        dispatch({
            type: SAVE_PROFILE_INTEREST_DECLINE,
            payload: data,
        });
        ProfileInterestService.SaveProfileInterestDeclineService(data, (res) => fn(res));
    };
}

///------------------- Change mind Profile Interest------------------------

export const CHANGE_MIND_PROFILE_INTEREST = "CHANGE_MIND_PROFILE_INTEREST";

export function SaveChangeMindInterest(data, fn) {

    return (dispatch) => {
        dispatch({
            type: CHANGE_MIND_PROFILE_INTEREST,
            payload: data,
        });
        ProfileInterestService.SaveChangeMindInterestService(data, (res) => fn(res));
    };
}