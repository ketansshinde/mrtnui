import { SendMessageService } from "../../services/UserService/SendMessageService";

export const FETCH_SEND_MESSAGE_SUCCESS = "FETCH_SEND_MESSAGE_SUCCESS";
export const FETCH_SEND_MESSAGE_PENDING = "FETCH_SEND_MESSAGE_PENDING";
export const FETCH_SEND_MESSAGE_ERROR = "FETCH_SEND_MESSAGE_ERROR";


export const SEND_MESSGAE_SUCCESS= "SEND_MESSGAE_SUCCESS";
export const SEND_MESSAGE_PENDING= "SEND_MESSAGE_PENDING";


// Get user profile list
export function fetchSendMessageSuccess(MessageData) {
    return {
        type: FETCH_SEND_MESSAGE_SUCCESS,
        payload: MessageData,
    };
}

export function fetchSendMessagePending() {
    return {
        type: FETCH_SEND_MESSAGE_PENDING,

    };
}

export function FetchSendMessage(UserData, fn) {
    
    return (dispatch) => {
        dispatch(fetchSendMessagePending());
        SendMessageService.GetSendMessage(UserData, (res) => {
            dispatch(fetchSendMessageSuccess(res.data));
            fn(res.data);
        });
    };
}



export function SendMessageDetail(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: SEND_MESSGAE_SUCCESS,
            payload: data,
        });
        SendMessageService.SendMessageData(data, (res) => fn(res));
    };
}

//------------------------------------Get Message of users-------------------------------------------

export const FETCH_GET_MESSAGE_SUCCESS = "FETCH_GET_MESSAGE_SUCCESS";
export const FETCH_GET_MESSAGE_PENDING = "FETCH_GET_MESSAGE_PENDING";
export const FETCH_GET_MESSAGE_ERROR = "FETCH_GET_MESSAGE_ERROR";

export function fetchGetMessageSuccess(MessageData) {
    return {
        type: FETCH_GET_MESSAGE_SUCCESS,
        payload: MessageData,
    };
}

export function fetchGetMessagePending() {
    return {
        type: FETCH_GET_MESSAGE_PENDING,

    };
}

export function FetchMessage(UserData, fn) {
    
    return (dispatch) => {
        dispatch(fetchGetMessagePending());
        SendMessageService.GetUserMessage(UserData, (res) => {
            dispatch(fetchGetMessageSuccess(res.data));
            fn(res.data);
        });
    };
}
