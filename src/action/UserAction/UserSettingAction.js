import { UserSettingService } from "../../services/UserService/UserSettingService";

export const CHANGE_PROFILE_PHOTO_SUCCESS = "CHANGE_PROFILE_PHOTO_SUCCESS";
export const CHANGE_PROFILE_PHOTO_PENDING = "CHANGE_PROFILE_PHOTO_PENDING";

export const FETCH_USERPROFILE_PHOTO_SUCCESS = "FETCH_USERPROFILE_PHOTO_SUCCESS";
export const FETCH_USERPROFILE_PHOTO_PENDING = "FETCH_SUBCRIPTION_PENDING";

export const CHANGE_PASSWORD_SUCCESS = "CHANGE_PASSWORD_SUCCESS";


export const FETCH_GOV_ID_SUCCESS = "FETCH_GOV_ID_SUCCESS";
export const FETCH_GOV_ID_PENDING = "FETCH_GOV_ID_PENDING";



export function SaveProfilePhoto(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: CHANGE_PROFILE_PHOTO_SUCCESS,
            payload: data,
        });
        UserSettingService.SaveProfilePhotoService(data, (res) => fn(res));
    };
}


// Get user profile list
export function fetchProfilePhotoSuccess(SubcriptionData) {
    return {
        type: FETCH_USERPROFILE_PHOTO_SUCCESS,
        payload: SubcriptionData,
    };
}

export function fetchProfilePhotoPending() {
    return {
        type: FETCH_USERPROFILE_PHOTO_PENDING,

    };
}

export function FetchUserProfilePhoto(UserData, fn) {
    
    
    return (dispatch) => {
        dispatch(fetchProfilePhotoPending());
        UserSettingService.GetUserProfilePhoto(UserData, (res) => {
            dispatch(fetchProfilePhotoSuccess(res.data));
            fn(res.data);
        });
    };
}

export function ChangePasswordUser(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: CHANGE_PASSWORD_SUCCESS,
            payload: data,
        });
        UserSettingService.changePasswordService(data, (res) => fn(res));
    };
}


// ------------Get Gov ID--------------------------------------------

export function fetchGovIDSuccess(GovData) {
    return {
        type: FETCH_GOV_ID_SUCCESS,
        payload: GovData,
    };
}

export function fetchGovIDPending() {
    return {
        type: FETCH_GOV_ID_PENDING,
    };
}

export function FetchGovID(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchGovIDPending());
        UserSettingService.GetGovIDService(UserData, (res) => {
            dispatch(fetchGovIDSuccess(res.data));
            fn(res.data);
        });
    };
}

///------------------- Save Gallery Photo------------------------


export const GALLERY_PHOTO_SUCCESS = "GALLERY_PHOTO_SUCCESS";

export function SaveProfilePhotoGallery(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: GALLERY_PHOTO_SUCCESS,
            payload: data,
        });
        UserSettingService.SaveProfilePhotoGalleryService(data, (res) => fn(res));
    };
}

///------------------- Fetch Gallery Photo------------------------

export const FETCH_GALLERY_SUCCESS = "FETCH_GALLERY_SUCCESS";
export const FETCH_GALLERY_PEDDING = "FETCH_GALLERY_PEDDING";

export function fetchGallerySuccess(GalleryPhotoData) {
    return {
        type: FETCH_GALLERY_SUCCESS,
        payload: GalleryPhotoData,
    };
}

export function fetchGalleryPending() {
    return {
        type: FETCH_GALLERY_PEDDING,
    };
}

export function FetchGalleryPhoto(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchGalleryPending());
        UserSettingService.GetGalleryPhotoService(UserData, (res) => {
            dispatch(fetchGallerySuccess(res.data));
            fn(res.data);
        });
    };
}

///------------------- Remove Gallery Photo------------------------

export const DELETE_GALLERY_PHOTO = "DELETE_GALLERY_PHOTO";

export function DeleteGalleryPhoto(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: DELETE_GALLERY_PHOTO,
            payload: data,
        });
        UserSettingService.DeleteGalleryPhotoService(data, (res) => fn(res));
    };
}
