import { ForgotPasswordService } from "../../services/UserService/ForgotPasswordService";

export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";
export const FORGOT_PASSWORD_PENDING = "FORGOT_PASSWORD_PENDING";

export function ForgotPasswordUser(data, fn) {
    return (dispatch) => {
        dispatch({
            type: FORGOT_PASSWORD_SUCCESS,
            payload: data,
        });
        ForgotPasswordService.ForgotPassword(data, (res) => fn(res));
    };
}

export function ResetPasswordUser(data, fn) {
    return (dispatch) => {
        dispatch({
            type: FORGOT_PASSWORD_SUCCESS,
            payload: data,
        });
        ForgotPasswordService.ResetPassword(data, (res) => fn(res));
    };
}