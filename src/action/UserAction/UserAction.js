
import { UserService } from "../../services/UserService/UserService";

// ----------------------- User profile Percentage-------------------------------

export const FETCH_PROFILE_PERCENTAGE = "FETCH_PROFILE_PERCENTAGE";


export function ProfilePercentage(data, fn) {
    return (dispatch) => {
        dispatch({
            type: FETCH_PROFILE_PERCENTAGE,
            payload: data,
        });
        UserService.GetProfilePercentage(data, (res) => fn(res));
    };
}
