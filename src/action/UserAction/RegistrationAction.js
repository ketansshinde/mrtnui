import { RegistrationService } from "../../services/UserService/RegistrationService";

export const REGISTRATION_SUCCESS = "REGISTRATION_SUCCESS";
export const REGISTRATION_PENDING = "REGISTRATION_PENDING";

export const FETCH_USER_SUCCESS = "FETCH_USER_SUCCESS";
export const FETCH_USER_PENDING = "FETCH_USER_PENDING";


export function AddRegistration(data, fn) {
    return (dispatch) => {
        dispatch({
            type: REGISTRATION_SUCCESS,
            payload: data,
        });
        RegistrationService.SaveRegistration(data, (res) => fn(res));
    };
}

export function fetchUserSuccess(UserData) {
    return {
        type: FETCH_USER_SUCCESS,
        payload: UserData,
    };
}

export function fetchUserPending() {
    return {
        type: FETCH_USER_PENDING,

    };
}

export function fetchUserByID(Userid, profileUniqueId) {
    
    return (dispatch) => {
        dispatch(fetchUserPending());
        RegistrationService.GetUserDetailByID(Userid, profileUniqueId, (res) => {
            dispatch(fetchUserSuccess(res.data.responseList));
        });
    };
}

//------------------------------------------------------------------------
export const REGISTRATION_CONFRIM_SUCCESS = "REGISTRATION_CONFRIM_SUCCESS";
export const REGISTRATION_CONFRIM_PENDING = "REGISTRATION_CONFRIM_PENDING";



export function UserRegistrationConfirm(data, fn) {
    return (dispatch) => {
        dispatch({
            type: REGISTRATION_CONFRIM_SUCCESS,
            payload: data,
        });
        RegistrationService.RegistrationConfirm(data, (res) => fn(res));
    };
}