import { UserProfileService } from "../../services/UserService/UserProfileService";

export const FETCH_USER_PROFILE_SUCCESS = "FETCH_USER_PROFILE_SUCCESS";
export const FETCH_USER_PROFILE_PENDING = "FETCH_USER_PROFILE_PENDING";
export const FETCH_USER_PROFILE_ERROR = "FETCH_USER_PROFILE_ERROR";

export const FETCH_USER_PROFILE_VIEWER_SUCCESS = "FETCH_USER_PROFILE_VIEWER_SUCCESS";
export const FETCH_USER_PROFILE_VIEWER_PENDING = "FETCH_USER_PROFILE_VIEWER_PENDING";
export const FETCH_USER_PROFILE_VIEWER_ERROR = "FETCH_USER_PROFILE_VIEWER_ERROR";

export const FETCH_USER_INCOMEING_REQUEST_SUCCESS = "FETCH_USER_INCOMEING_REQUEST_SUCCESS";
export const FETCH_USER_INCOMEING_REQUEST_PENDING = "FETCH_USER_INCOMEING_REQUEST_PENDING";
export const FETCH_USER_INCOMEING_REQUEST_ERROR = "FETCH_USER_INCOMEING_REQUEST_ERROR";

// Get user profile list
export function fetchUserProfileSuccess(UserData) {
    return {
        type: FETCH_USER_PROFILE_SUCCESS,
        payload: UserData,
    };
}

export function fetchUserPending() {
    return {
        type: FETCH_USER_PROFILE_PENDING,

    };
}

export function FetchUserProfile(UserData, fn) {
    
    return (dispatch) => {
        dispatch(fetchUserPending());
        UserProfileService.GetUserProfileService(UserData, (res) => {
            dispatch(fetchUserProfileSuccess(res));
            fn(res);
        });
    };
}


// Get user profile viewer list
export function fetchUserProfileViewerSuccess(UserData) {
    return {
        type: FETCH_USER_PROFILE_VIEWER_SUCCESS,
        payload: UserData,
    };
}

export function fetchUserProfileViewerPending() {
    return {
        type: FETCH_USER_PROFILE_VIEWER_PENDING,

    };
}

export function FetchUserProfileViewer(UserData) {
    return (dispatch) => {
        dispatch(fetchUserProfileViewerPending());
        UserProfileService.GetProfileViewerService(UserData, (res) => {
            dispatch(fetchUserProfileViewerSuccess(res));
        });
    };
}


// Get user Incoming Send Request 
export function fetchIncomingSendRequestSuccess(UserData) {
    return {
        type: FETCH_USER_INCOMEING_REQUEST_SUCCESS,
        payload: UserData,
    };
}

export function fetchIncomingSendRequestPending() {
    return {
        type: FETCH_USER_INCOMEING_REQUEST_PENDING,

    };
}

export function FetchUserIncomingSendRequest(UserData) {
    return (dispatch) => {
        dispatch(fetchIncomingSendRequestPending());
        UserProfileService.GetIncomingSendRequestService(UserData, (res) => {
            dispatch(fetchIncomingSendRequestSuccess(res));
        });
    };
}

