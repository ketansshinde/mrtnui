import { ProfileViewerService } from "../../services/UserService/ProfileViewerService";

export const FETCH_PROFILE_VIEWER_SUCCESS = "FETCH_PROFILE_VIEWER_SUCCESS";
export const FETCH_PROFILE_VIEWER_PENDING = "FETCH_PROFILE_VIEWER_PENDING";

export function fetchProfileViwerSuccess(UserData) {
    return {
        type: FETCH_PROFILE_VIEWER_SUCCESS,
        payload: UserData,
    };
}

export function fetchProfileViwerPending() {
    return {
        type: FETCH_PROFILE_VIEWER_PENDING,

    };
}

export function FetchProfileViewer(UserData, fn) {
    
    return (dispatch) => {
        dispatch(fetchProfileViwerPending());
        ProfileViewerService.GetProfileViewerService(UserData, (res) => {
            dispatch(fetchProfileViwerSuccess(res));
            fn(res);
        });
    };
}