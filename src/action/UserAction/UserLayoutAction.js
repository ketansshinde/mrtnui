import { UserLayoutService } from "../../services/UserService/UserLayoutService";

export const UPDATE_USERPROFILE_SUCCESS = "UPDATE_USERPROFILE_SUCCESS";
export const UPDATE_USERPROFILE_PENDING = "UPDATE_USERPROFILE_PENDING";

export const UPDATE_USERADDRESS_SUCCESS = "UPDATE_USERADDRESS_SUCCESS";
export const UPDATE_USERADDRESS_PENDING = "UPDATE_USERADDRESS_PENDING";

export const UPDATE_USEREXPECTATION_SUCCESS = "UPDATE_USEREXPECTATION_SUCCESS";
export const UPDATE_USEREXPECTATION_PENDING = "UPDATE_USEREXPECTATION_PENDING";

export const UPDATE_FAMILYBACKGROUND_SUCCESS = "UPDATE_FAMILYBACKGROUND_SUCCESS";
export const UPDATE_FAMILYBACKGROUND_PENDING = "UPDATE_FAMILYBACKGROUND_PENDING";

export const UPDATE_USERLIFESTYLE_SUCCESS = "UPDATE_USERLIFESTYLE_SUCCESS";
export const UPDATE_USERLIFESTYLE_PENDING = "UPDATE_FAMILYBACKGROUND_PENDING";

export const UPDATE_USEROCCUPATION_SUCCESS= "UPDATE_USEROCCUPATION_SUCCESS";
export const UPDATE_USEROCCUPATION_PENDING= "UPDATE_USEROCCUPATION_PENDING";

export const UPDATE_USERPERSONAL_SUCCESS= "UPDATE_USERPERSONAL_SUCCESS";
export const UPDATE_USERPERSONAL_PENDING= "UPDATE_USERPERSONAL_PENDING";

export const UPDATE_USERRELIGION_SUCCESS= "UPDATE_USERRELIGION_SUCCESS";
export const UPDATE_USERRELIGION_PENDING= "UPDATE_USERRELIGION_PENDING";


///-----------------Update USER Profie----------------------------------

export function UpdateUserProfile(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USERPROFILE_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserProfileService(data, (res) => fn(res));
    };
}

///-----------------Update USER Address----------------------------------

export function UpdateUserAddress(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USERADDRESS_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserAddressService(data, (res) => fn(res));
    };
}

///-----------------Update USER Expectation----------------------------------

export function UpdateUserExpectation(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USEREXPECTATION_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserExpectationService(data, (res) => fn(res));
    };
}

///-----------------Update USER family Background----------------------------------

export function UpdateUserFamilyBackgeound(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_FAMILYBACKGROUND_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserFamilyBackgeoundService(data, (res) => fn(res));
    };
}


///-----------------Update USER Life style----------------------------------

export function UpdateUserLifeStyle(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USERLIFESTYLE_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserLifeStyleService(data, (res) => fn(res));
    };
}


///-----------------Update USER Occupation----------------------------------

export function UpdateUserOccupation(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USEROCCUPATION_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserOccupationService(data, (res) => fn(res));
    };
}

///-----------------Update USER personal----------------------------------

export function UpdateUserPersonal(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USERPERSONAL_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserPersonalService(data, (res) => fn(res));
    };
}

///-----------------Update USER Religion----------------------------------

export function UpdateUserReligion(data, fn) {
    return (dispatch) => {
        dispatch({
            type: UPDATE_USERRELIGION_SUCCESS,
            payload: data,
        });
        UserLayoutService.UpdateUserReligionService(data, (res) => fn(res));
    };
}
