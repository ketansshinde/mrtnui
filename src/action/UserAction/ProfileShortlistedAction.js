import { ProfileShortlistedService } from "../../services/UserService/ProfileShortlistedService";

export const FETCH_PROFILE_SHORTLISTED_SUCCESS = "FETCH_PROFILE_SHORTLISTED_SUCCESS";
export const FETCH_PROFILE_SHORTLISTED_PENDING = "FETCH_PROFILE_SHORTLISTED_PENDING";

export function fetchProfileShortlistedSuccess(UserData) {
    return {
        type: FETCH_PROFILE_SHORTLISTED_SUCCESS,
        payload: UserData,
    };
}

export function fetchProfileShortlistedPending() {
    return {
        type: FETCH_PROFILE_SHORTLISTED_PENDING,

    };
}

export function FetchProfileShortListed(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchProfileShortlistedPending());
        ProfileShortlistedService.GetProfileShortlistedService(UserData, (res) => {
            dispatch(fetchProfileShortlistedSuccess(res));
            fn(res);
        });
    };
}

///------------------- Save Profile Shortlist------------------------


export const SAVE_PROFILE_SHORTLISTED = "SAVE_PROFILE_SHORTLISTED";

export function SaveProfileShortlist(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: SAVE_PROFILE_SHORTLISTED,
            payload: data,
        });
        ProfileShortlistedService.SaveProfileShortlistedService(data, (res) => fn(res));
    };
}

//----------------- Profile Shortlist by you------------------------------


export const FETCH_PROFILE_SHORTLISTED_BY_YOU_SUCCESS = "FETCH_PROFILE_SHORTLISTED_BY_YOU_SUCCESS";
export const FETCH_PROFILE_SHORTLISTED_BY_YOU_PENDING = "FETCH_PROFILE_SHORTLISTED_BY_YOU_PENDING";

export function fetchProfileShortlistedByYouSuccess(UserData) {
    return {
        type: FETCH_PROFILE_SHORTLISTED_BY_YOU_SUCCESS,
        payload: UserData,
    };
}

export function fetchProfileShortlistedByYouPending() {
    return {
        type: FETCH_PROFILE_SHORTLISTED_BY_YOU_PENDING,

    };
}

export function FetchProfileShortListedByYou(UserData, fn) {
    return (dispatch) => {
        dispatch(fetchProfileShortlistedByYouPending());
        ProfileShortlistedService.GetProfileShortlistByYouService(UserData, (res) => {
            dispatch(fetchProfileShortlistedByYouSuccess(res));
            fn(res);
        });
    };
}

///------------------- Save Profile Shortlist------------------------


export const DELETE_PROFILE_SHORTLISTED = "DELETE_PROFILE_SHORTLISTED";

export function DeleteProfileShortlist(data, fn) {
    
    return (dispatch) => {
        dispatch({
            type: DELETE_PROFILE_SHORTLISTED,
            payload: data,
        });
        ProfileShortlistedService.DeleteProfileShortlistedService(data, (res) => fn(res));
    };
}
