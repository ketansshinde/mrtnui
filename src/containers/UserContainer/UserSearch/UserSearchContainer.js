import React, { Component } from "react";
import UserSearch from "../../../components/User/UserSearch/usersearch";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchUserProfile } from "../../../action/UserAction/UserProfileAction";
import { GetUserProfile } from "../../../reducer/UserReducer/UserProfileReducer";

const mapStateToProps = (state) => ({
    UserProfileData: GetUserProfile(state.UserProfileReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            FetchUserProfile: FetchUserProfile
        },
        dispatch
    );

export class UserSearchContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const { FetchUserProfile } = this.props;
        // FetchUserProfile(passValue);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <UserSearch {...this.props}></UserSearch>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserSearchContainer);
