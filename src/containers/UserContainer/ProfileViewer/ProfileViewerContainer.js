import React, { Component } from "react";
import ProfileViewer from "../../../components/User/ProfileViewer/profileviewer";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchProfileViewer } from "../../../action/UserAction/ProfileViewerAction";
import { GetProfileViewerData } from "../../../reducer/UserReducer/ProfileViewerReducer";
import { SaveProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";

import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";
import { CryptoCode } from "../../../common/cryptoCode";

import { SaveProfileInterest } from "../../../action/UserAction/ProfileInterestAction";

const mapStateToProps = (state) => ({
    ProfileViewerData: GetProfileViewerData(state.ProfileViewerReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchProfileViewer: FetchProfileViewer,
    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist,
    SaveProfileInterest: SaveProfileInterest
}, dispatch);

export class ProfileViewerContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        // var passValue = {
        //     userid: UserData.id,
        //     profileUniqueId: UserData.profileUniqueId,
        // }
        // const { FetchProfileViewer } = this.props;
        // this.props.FetchProfileViewer(passValue);

    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <ProfileViewer {...this.props}></ProfileViewer>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileViewerContainer);
