import React, { Component } from "react";
import ResetPassword from "../../../components/User/ForgetPassword/resetpassword";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { HomeFooter, HomeHeader } from "./../../../components/User/CommonComponent";
import { ResetPasswordUser } from "../../../action/UserAction/ForgetPasswordAction";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    ResetPasswordUser: ResetPasswordUser
}, dispatch);

export class ResetPasswordContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {

    }

    render() {
        return (
            <React.Fragment>
                <HomeHeader></HomeHeader>
                <ResetPassword {...this.props}></ResetPassword>
                <HomeFooter></HomeFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordContainer);
