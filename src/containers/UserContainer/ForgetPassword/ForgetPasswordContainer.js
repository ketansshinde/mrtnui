import React, { Component } from "react";
import ForgetPassword from "../../../components/User/ForgetPassword/forgetpassword";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { HomeFooter, HomeHeader } from "./../../../components/User/CommonComponent";
import { ForgotPasswordUser } from "../../../action/UserAction/ForgetPasswordAction";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  ForgotPasswordUser: ForgotPasswordUser
}, dispatch);

export class ForgetPasswordContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {

  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <ForgetPassword {...this.props}></ForgetPassword>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ForgetPasswordContainer);
