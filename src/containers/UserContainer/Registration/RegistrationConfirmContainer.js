import React, { Component } from "react";
import RegistrationConfirm from "../../../components/User/Registration/registrationconfirm";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeHeader, HomeFooter } from "./../../../components/User/CommonComponent";
import { UserRegistrationConfirm } from "./../../../action/UserAction/RegistrationAction";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  UserRegistrationConfirm: UserRegistrationConfirm
}, dispatch);

export class RegistrationConfirmContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <RegistrationConfirm {...this.props}></RegistrationConfirm>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RegistrationConfirmContainer);
