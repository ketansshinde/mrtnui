import React, { Component } from "react";
import RegistrationPage from "../../../components/User/Registration/registration";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeHeader, HomeFooter } from "./../../../components/User/CommonComponent";
import { AddRegistration } from "./../../../action/UserAction/RegistrationAction";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  AddRegistration: AddRegistration
}, dispatch);

export class RegistrationContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <RegistrationPage {...this.props}></RegistrationPage>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RegistrationContainer);
