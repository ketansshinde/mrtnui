import React, { Component } from "react";
import ProfileShortlist from "../../../components/User/ProfileShortlist/profileshortlist";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchProfileShortListed, SaveProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";
import { GetProfileShortlistedData } from "../../../reducer/UserReducer/ProfileShortlistedReducer";

import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";
import { CryptoCode } from "../../../common/cryptoCode";

import { SaveProfileInterest } from "../../../action/UserAction/ProfileInterestAction";

const mapStateToProps = (state) => ({
    ProfileShortListedData: GetProfileShortlistedData(state.ProfileShortlistedReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchProfileShortListed: FetchProfileShortListed,
    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist,
    SaveProfileInterest: SaveProfileInterest
}, dispatch);

export class ProfileShortlistContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <ProfileShortlist {...this.props}></ProfileShortlist>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileShortlistContainer);
