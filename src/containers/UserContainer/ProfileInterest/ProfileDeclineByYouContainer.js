import React, { Component } from "react";
import ProfileDeclineByYou from "../../../components/User/ProfileInterest/profiledeclinebyyou";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "../../../components/User/CommonComponent";
import { SaveProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";
import { GetProfileShortlistedData } from "../../../reducer/UserReducer/ProfileShortlistedReducer";

import { SaveProfileInterest, FetchProfileInterest, SaveChangeMindInterest, FetchProfileInterestDeclineByYou } from "../../../action/UserAction/ProfileInterestAction";
import { GetProfileInterestData } from "../../../reducer/UserReducer/ProfileInterestReducer";

import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";
import { CryptoCode } from "../../../common/cryptoCode";

const mapStateToProps = (state) => ({
    ProfileShortListedData: GetProfileShortlistedData(state.ProfileShortlistedReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
    ProfileInterestData: GetProfileInterestData(state.ProfileInterestReducer)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchProfileInterest: FetchProfileInterest,
    SaveProfileInterest: SaveProfileInterest,
    FetchProfileInterestDeclineByYou: FetchProfileInterestDeclineByYou,

    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist,
    SaveChangeMindInterest: SaveChangeMindInterest
}, dispatch);

export class ProfileDeclineByYouContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <ProfileDeclineByYou {...this.props}></ProfileDeclineByYou>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileDeclineByYouContainer);
