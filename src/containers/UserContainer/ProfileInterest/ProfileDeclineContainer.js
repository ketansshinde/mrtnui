import React, { Component } from "react";
import ProfileDecline from "../../../components/User/ProfileInterest/profiledecline";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "../../../components/User/CommonComponent";
import { SaveProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";
import { GetProfileShortlistedData } from "../../../reducer/UserReducer/ProfileShortlistedReducer";

import { SaveProfileInterest, FetchProfileInterest, FetchProfileInterestDecline } from "../../../action/UserAction/ProfileInterestAction";
import { GetProfileInterestData } from "../../../reducer/UserReducer/ProfileInterestReducer";

import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";
import { CryptoCode } from "../../../common/cryptoCode";

const mapStateToProps = (state) => ({
    ProfileShortListedData: GetProfileShortlistedData(state.ProfileShortlistedReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
    ProfileInterestData: GetProfileInterestData(state.ProfileInterestReducer)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchProfileInterest: FetchProfileInterest,
    SaveProfileInterest: SaveProfileInterest,
    FetchProfileInterestDecline: FetchProfileInterestDecline,

    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist
}, dispatch);

export class ProfileDeclineContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <ProfileDecline {...this.props}></ProfileDecline>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileDeclineContainer);
