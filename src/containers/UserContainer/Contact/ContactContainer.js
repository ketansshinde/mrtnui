import React, { Component } from "react";
import ContactPage from "../../../components/User/Contact/Contact";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeHeader, HomeFooter } from "./../../../components/User/CommonComponent";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class ContactContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <HomeHeader></HomeHeader>
                <ContactPage {...this.props}></ContactPage>
                <HomeFooter></HomeFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactContainer);
