import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import PrivateSecurity from "../../../components/User/UserProfileSetting/privatesecurity";


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export class PrivateSecurityContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <PrivateSecurity {...this.props}></PrivateSecurity>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(PrivateSecurityContainer);
