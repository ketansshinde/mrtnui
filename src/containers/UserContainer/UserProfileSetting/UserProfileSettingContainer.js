import React, { Component } from "react";
import UserProfileSetting from "../../../components/User/UserProfileSetting/userprofilesetting";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export class UserProfileSettingContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <UserProfileSetting {...this.props}></UserProfileSetting>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserProfileSettingContainer);
