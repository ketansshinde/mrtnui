import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import ChangePassword from "../../../components/User/UserProfileSetting/changepassword";


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export class ChangePasswordContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <ChangePassword {...this.props}></ChangePassword>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordContainer);
