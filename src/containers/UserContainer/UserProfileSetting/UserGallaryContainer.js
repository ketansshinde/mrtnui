import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "../../../components/User/CommonComponent";
import UserGallary from "../../../components/User/UserProfileSetting/usergallery";

import { FetchGalleryPhoto } from "../../../action/UserAction/UserSettingAction";
import { GetGalleryPhotoData } from "../../../reducer/UserReducer/UserSettingReducer";
import { CryptoCode } from "../../../common/cryptoCode";

const mapStateToProps = (state) => ({
    GalleryPhotoData: GetGalleryPhotoData(state.UserSettingReducer)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchGalleryPhoto: FetchGalleryPhoto
}, dispatch);

export class UserGalleryContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        const { FetchGalleryPhoto } = this.props;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        }
        FetchGalleryPhoto(passValue);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <UserGallary {...this.props}></UserGallary>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserGalleryContainer);
