import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import GovermentId from "../../../components/User/UserProfileSetting/govermentId";
import { CryptoCode } from "../../../common/cryptoCode";

import { FetchGovID } from "../../../action/UserAction/UserSettingAction";
import { GetGovID } from "../../../reducer/UserReducer/UserSettingReducer";




const mapStateToProps = (state) => ({
    GovData: GetGovID(state.UserSettingReducer),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchGovID: FetchGovID
}, dispatch);

export class GovermentIdContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        const { FetchGovID } = this.props;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValue = {
            userid: UserData.id,
            profileUniqueId: UserData.profileUniqueId,
        }
        FetchGovID(passValue);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <GovermentId {...this.props}></GovermentId>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(GovermentIdContainer);
