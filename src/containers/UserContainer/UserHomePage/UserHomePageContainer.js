import React, { Component } from "react";
import UserHomePage from "../../../components/User/UserHomePage/userhomepage";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchUserProfile } from "../../../action/UserAction/UserProfileAction";
import { GetUserProfile } from "../../../reducer/UserReducer/UserProfileReducer";
import { SaveProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";

import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";

import { SaveProfileInterest } from "../../../action/UserAction/ProfileInterestAction";

import { ProfilePercentage } from "../../../action/UserAction/UserAction";

const mapStateToProps = (state) => ({
    UserProfileData: GetUserProfile(state.UserProfileReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchUserProfile: FetchUserProfile,
    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist,
    SaveProfileInterest: SaveProfileInterest,
    ProfilePercentage: ProfilePercentage
}, dispatch);

export class UserHomePageContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <UserHomePage {...this.props}></UserHomePage>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserHomePageContainer);
