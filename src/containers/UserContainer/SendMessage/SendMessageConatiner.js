import React, { Component } from "react";
import SendMessage from "../../../components/User/SendMessage/sendmessage";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchSendMessage } from "../../../action/UserAction/SendMessageAction";
import { GetSendMessage } from "../../../reducer/UserReducer/SendMessageReducer";
import { ThemeProvider } from "@material-ui/core";
import {
    CryptoCode,
} from "../../../common/cryptoCode";

const mapStateToProps = (state) => ({
    SendMessageData: GetSendMessage(state.SendMessageReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            FetchSendMessage: FetchSendMessage
        },
        dispatch
    );

export class SendMessageContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <SendMessage {...this.props}></SendMessage>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SendMessageContainer);
