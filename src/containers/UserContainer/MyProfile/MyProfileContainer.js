import React, { Component } from "react";
import ReadUserLayout from "../../../components/User/UserLayout/readuserlayout";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchProfileViewer } from "../../../action/UserAction/ProfileViewerAction";
import { GetProfileViewerData } from "../../../reducer/UserReducer/ProfileViewerReducer";
import { SaveProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";
import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";
import { SaveProfileInterest } from "../../../action/UserAction/ProfileInterestAction";

const mapStateToProps = (state) => ({
    ProfileViewerData: GetProfileViewerData(state.ProfileViewerReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchProfileViewer: FetchProfileViewer,
    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist,
    SaveProfileInterest: SaveProfileInterest
}, dispatch);

export class MyProfileContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);

  }

  render() {
    return (
      <React.Fragment>
        <UserHeader></UserHeader>
        <ReadUserLayout {...this.props}></ReadUserLayout>
        <UserFooter></UserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(MyProfileContainer);
