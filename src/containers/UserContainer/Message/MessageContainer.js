import React, { Component } from "react";
import Message from "../../../components/User/Message/Message";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import { FetchSendMessage, FetchMessage } from "../../../action/UserAction/SendMessageAction";
import { GetSendMessage, GetAllUserMessage } from "../../../reducer/UserReducer/SendMessageReducer";
import { ThemeProvider } from "@material-ui/core";
import {
    CryptoCode,
} from "../../../common/cryptoCode";

const mapStateToProps = (state) => ({
    SendMessageData: GetSendMessage(state.SendMessageReducer),
    MessageData: GetAllUserMessage(state.SendMessageReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            FetchSendMessage: FetchSendMessage,
            FetchMessage: FetchMessage
        },
        dispatch
    );

export class MessageContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        window.scrollTo(0, 0);

    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <Message {...this.props}></Message>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MessageContainer);
