import React, { Component } from "react";
import OrderDetail from "../../../components/User/SubcriptionPage/OrderDetail";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserFooter, UserHeader } from "../../../components/User/CommonComponent";
import { GetTrasactionDetailByUserID } from "../../../action/UserAction/SubcriptionAction";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  GetTrasactionDetailByUserID: GetTrasactionDetailByUserID
}, dispatch);

export class OrderDetailContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    // window.scrollTo(0, 0);
    // const { FetchSubscription } = this.props;
    // FetchSubscription();
  }

  render() {
    return (
      <React.Fragment>
        <UserHeader></UserHeader>
        <OrderDetail {...this.props}></OrderDetail>
        <UserFooter></UserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailContainer);
