import React, { Component } from "react";
import UpgradeSubscription from "../../../components/User/SubcriptionPage/UpgradeSubscription";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserFooter, UserHeader } from "../../../components/User/CommonComponent";
import { GetOrderDetail } from "../../../action/UserAction/SubcriptionAction";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    GetOrderDetail: GetOrderDetail
}, dispatch);

export class UpgradeSubscriptionContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <React.Fragment>
        <UserHeader></UserHeader>
        <UpgradeSubscription {...this.props}></UpgradeSubscription>
        <UserFooter></UserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UpgradeSubscriptionContainer);
