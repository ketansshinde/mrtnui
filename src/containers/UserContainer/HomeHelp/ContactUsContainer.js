import React, { Component } from "react";
import ContactUs from "../../../components/User/HomeHelp/contactus";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeFooter, HomeHeader } from "./../../../components/User/CommonComponent";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class ContactUsContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);

    }

    render() {
        return (
            <React.Fragment>
                <HomeHeader></HomeHeader>
                <ContactUs {...this.props}></ContactUs>
                <HomeFooter></HomeFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactUsContainer);
