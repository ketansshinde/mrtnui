import React, { Component } from "react";
import CookiePolicy from "../../../components/User/LegalPolicy/cookiepolicy";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeFooter, HomeHeader } from "./../../../components/User/CommonComponent";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class CookiesPolicyContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);

  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <CookiePolicy {...this.props}></CookiePolicy>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(CookiesPolicyContainer);
