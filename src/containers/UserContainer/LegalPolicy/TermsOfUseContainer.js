import React, { Component } from "react";
import TermOfUse from "../../../components/User/LegalPolicy/termsofuse";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeFooter , HomeHeader} from "./../../../components/User/CommonComponent";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class TermOfUseContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);

  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <TermOfUse {...this.props}></TermOfUse>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TermOfUseContainer);
