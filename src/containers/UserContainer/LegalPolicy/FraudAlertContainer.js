import React, { Component } from "react";
import FraudAlert from "../../../components/User/LegalPolicy/fraudalert";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeFooter, HomeHeader } from "./../../../components/User/CommonComponent";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class FraudAlertContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);


  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <FraudAlert {...this.props}></FraudAlert>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FraudAlertContainer);
