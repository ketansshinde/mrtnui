import React, { Component } from "react";
import PrivacyPolicy from "../../../components/User/LegalPolicy/privacypolicy";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { HomeFooter , HomeHeader} from "./../../../components/User/CommonComponent";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class PrivatePolicyContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);

  }

  render() {
    return (
      <React.Fragment>
        <HomeHeader></HomeHeader>
        <PrivacyPolicy {...this.props}></PrivacyPolicy>
        <HomeFooter></HomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(PrivatePolicyContainer);
