import React, { Component } from "react";
import ProfileShortlistByYou from "../../../components/User/ProfileShortlistByYou/profileshortlistbyyou";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserHeader, UserFooter } from "./../../../components/User/CommonComponent";
import {  SaveProfileShortlist, FetchProfileShortListedByYou, DeleteProfileShortlist } from "../../../action/UserAction/ProfileShortlistedAction";
import { GetProfileShortlistedByYouData } from "../../../reducer/UserReducer/ProfileShortlistedReducer";

import { FetchUserProfilePhoto } from "../../../action/UserAction/UserSettingAction";
import { GetUserProfilePhoto } from "../../../reducer/UserReducer/UserSettingReducer";
import { CryptoCode } from "../../../common/cryptoCode";

import { SaveProfileInterest } from "../../../action/UserAction/ProfileInterestAction";

const mapStateToProps = (state) => ({
    ProfileShortListedByYouData: GetProfileShortlistedByYouData(state.ProfileShortlistedReducer),
    UserProfilePhotoData: GetUserProfilePhoto(state.UserSettingReducer),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    FetchProfileShortListedByYou: FetchProfileShortListedByYou,
    FetchUserProfilePhoto: FetchUserProfilePhoto,
    SaveProfileShortlist: SaveProfileShortlist,
    SaveProfileInterest: SaveProfileInterest,
    DeleteProfileShortlist:DeleteProfileShortlist
}, dispatch);

export class ProfileShortlistByYouContainer extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        window.scrollTo(0, 0);
        // const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    }

    render() {
        return (
            <React.Fragment>
                <UserHeader></UserHeader>
                <ProfileShortlistByYou {...this.props}></ProfileShortlistByYou>
                <UserFooter></UserFooter>
            </React.Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ProfileShortlistByYouContainer);
