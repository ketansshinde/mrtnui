import React, { Component } from "react";
import PricingPage from "../../../components/User/Pricing/pricing";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { UserFooter, UserHeader } from "../../../components/User/CommonComponent";
import { FetchSubscription } from "../../../action/UserAction/SubcriptionAction";
import { GetSubcriptionPlan } from "../../../reducer/UserReducer/SubcriptionReducer";

const mapStateToProps = (state) => ({
  SubscriptionData: GetSubcriptionPlan(state.SubcriptionReducer)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  FetchSubscription: FetchSubscription
}, dispatch);

export class PricingContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    const { FetchSubscription } = this.props;
    FetchSubscription();
  }

  render() {
    return (
      <React.Fragment>
        <UserHeader></UserHeader>
        <PricingPage {...this.props}></PricingPage>
        <UserFooter></UserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(PricingContainer);
