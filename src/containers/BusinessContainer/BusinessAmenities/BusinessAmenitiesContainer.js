import React, { Component } from "react";
import BusinessAmenities from "../../../components/Business/BusinessAmenities/BusinessAmenities";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessUserHeader, BusinessUserFooter } from "../../../components/Business/CommonComponent";


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export class BusinessAmenitiesContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessUserHeader></BusinessUserHeader>
        <BusinessAmenities {...this.props}></BusinessAmenities>
        <BusinessUserFooter></BusinessUserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessAmenitiesContainer);
