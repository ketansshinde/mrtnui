import React, { Component } from "react";
import BusinessHomePage from "../../../components/Business/BusinessHomePage/BusinessHomePage";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessHomeFooter, BusinessHomeHeader } from "../../../components/Business/CommonComponent";


import { fetchMaster } from "../../../action/AdminAction/MasterAction";
import { GetMaster } from "../../../reducer/AdminReducer/MasterReducer";
import { fetchSubMaster } from "../../../action/AdminAction/SubMasterAction";
import { GetSubMaster } from "../../../reducer/AdminReducer/SubMasterReducer";

import { fetchCaste } from "../../../action/AdminAction/CasteAction";
import { GetCaste } from "../../../reducer/AdminReducer/CasteReducer";
import { fetchSubCaste } from "../../../action/AdminAction/SubCasteAction";
import { GetSubCaste } from "../../../reducer/AdminReducer/SubCasteReducer";


const mapStateToProps = (state) => ({
  subMasterData: GetSubMaster(state.SubMasterReducer),
  masterData: GetMaster(state.MasterReducer),
  CasteData: GetCaste(state.CasteReducer),
  SubCasteData: GetSubCaste(state.SubCasteReducer)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchMaster: fetchMaster,
  fetchSubMaster: fetchSubMaster,
  fetchCaste: fetchCaste,
  fetchSubCaste: fetchSubCaste,
}, dispatch);


export class BusinessHomeContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    localStorage.clear();
    window.scrollTo(0, 0);
    this._loadMaster();
  }

  _loadMaster() {

    //Master Details
    const { fetchSubMaster } = this.props;
    fetchSubMaster();

    const { fetchMaster } = this.props;
    fetchMaster();

    //Caste Details
    const { fetchCaste } = this.props;
    fetchCaste();

    const { fetchSubCaste } = this.props;
    fetchSubCaste();

  }


  render() {
    return (
      <React.Fragment>
        <BusinessHomeHeader></BusinessHomeHeader>
        <BusinessHomePage {...this.props}></BusinessHomePage>
        <BusinessHomeFooter></BusinessHomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessHomeContainer);
