import React, { Component } from "react";
import BusinessLogin from "../../../components/Business/BusinessLogin/BusinessLogin";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessHomeFooter, BusinessHomeHeader } from "../../../components/Business/CommonComponent";


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export class BusinessLoginContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessHomeHeader></BusinessHomeHeader>
        <BusinessLogin {...this.props}></BusinessLogin>
        <BusinessHomeFooter></BusinessHomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessLoginContainer);
