import React, { Component } from "react";
import BusinessPayment from "../../../components/Business/BusinessPayment/BusinessPayment";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessUserHeader, BusinessUserFooter } from "../../../components/Business/CommonComponent";

import { FetchSubscription } from "../../../action/BusinessAction/BusinessSubcriptionAction";
import { GetSubcriptionPlan } from "../../../reducer/BusinessReducer/SubcriptionReducer";


const mapStateToProps = (state) => ({
  SubscriptionData: GetSubcriptionPlan(state.SubcriptionReducer)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  FetchSubscription: FetchSubscription

}, dispatch);

export class BusinessPaymentContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessUserHeader></BusinessUserHeader>
        <BusinessPayment {...this.props}></BusinessPayment>
        <BusinessUserFooter></BusinessUserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessPaymentContainer);
