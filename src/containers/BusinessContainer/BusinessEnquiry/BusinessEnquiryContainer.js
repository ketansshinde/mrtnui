import React, { Component } from "react";
import BusinessEnquiry from "../../../components/Business/BusinessEnquiry/BusinessEnquiry";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessUserHeader, BusinessUserFooter } from "../../../components/Business/CommonComponent";


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({

}, dispatch);

export class BusinessEnquiryContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessUserHeader></BusinessUserHeader>
        <BusinessEnquiry {...this.props}></BusinessEnquiry>
        <BusinessUserFooter></BusinessUserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessEnquiryContainer);
