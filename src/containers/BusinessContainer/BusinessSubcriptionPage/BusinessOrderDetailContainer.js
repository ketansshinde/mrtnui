import React, { Component } from "react";
import BusinessOrderDetail from "../../../components/Business/BusinessSubcriptionPage/BusinessOrderDetail";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessUserHeader, BusinessUserFooter } from "../../../components/Business/CommonComponent";

import { GetTrasactionDetailByUserID } from "../../../action/BusinessAction/BusinessSubcriptionAction";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  GetTrasactionDetailByUserID: GetTrasactionDetailByUserID

}, dispatch);

export class BusinessOrderDetailContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessUserHeader></BusinessUserHeader>
        <BusinessOrderDetail {...this.props}></BusinessOrderDetail>
        <BusinessUserFooter></BusinessUserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessOrderDetailContainer);
