import React, { Component } from "react";
import BusinessUpgradeSubscription from "../../../components/Business/BusinessSubcriptionPage/BusinessUpgradeSubscription";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessUserHeader, BusinessUserFooter } from "../../../components/Business/CommonComponent";

import { GetOrderDetail } from "../../../action/BusinessAction/BusinessSubcriptionAction";


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  GetOrderDetail: GetOrderDetail

}, dispatch);

export class BusinessUpgradeSubscriptionContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessUserHeader></BusinessUserHeader>
        <BusinessUpgradeSubscription {...this.props}></BusinessUpgradeSubscription>
        <BusinessUserFooter></BusinessUserFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessUpgradeSubscriptionContainer);
