import React, { Component } from "react";
import BusinessRegistration from "../../../components/Business/BusinessRegistration/BusinessRegistration";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";
import { BusinessHomeFooter, BusinessHomeHeader } from "../../../components/Business/CommonComponent";
import { SaveBusinessRegistration } from "../../../action/BusinessAction/BusinessRegistrationAction";


const mapStateToProps = (state) => ({

});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  SaveBusinessRegistration: SaveBusinessRegistration
}, dispatch);

export class BusinessRegistrationContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }


  render() {
    return (
      <React.Fragment>
        <BusinessHomeHeader></BusinessHomeHeader>
        <BusinessRegistration {...this.props}></BusinessRegistration>
        <BusinessHomeFooter></BusinessHomeFooter>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BusinessRegistrationContainer);
