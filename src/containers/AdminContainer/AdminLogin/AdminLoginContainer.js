import React, { Component } from "react";
import AdminLogin from "../../../components/Admin/AdminLogin/adminlogin";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Global_var } from "../../../global/global_var";

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export class AdminLoginContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {    
  }

  render() {
    return (
      <React.Fragment>
        <AdminLogin {...this.props}></AdminLogin>
      </React.Fragment>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminLoginContainer);
