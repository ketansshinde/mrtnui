import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Caste } from "../../../components/Admin/Caste/caste";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchCaste, AddCaste } from "../../../action/AdminAction/CasteAction";
import { GetCaste } from "../../../reducer/AdminReducer/CasteReducer";

class CasteContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        const { fetchCaste } = this.props;
        fetchCaste();
    }
    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <div id="layoutSidenav">
                    <div id="layoutSidenav_content">
                        <Caste {...this.props}></Caste>
                        <AdminFooter></AdminFooter>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    CasteData: GetCaste(state.CasteReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchCaste: fetchCaste,
            AddCaste: AddCaste,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(CasteContainer);
