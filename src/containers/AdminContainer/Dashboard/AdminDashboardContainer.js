import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AdminDashboard } from "../../../components/Admin/AdminDashboard/AdminDashboard";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";

class AdminDashboardContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {


    }

    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <div id="layoutSidenav">
                    <div id="layoutSidenav_content">
                        <AdminDashboard></AdminDashboard>
                        <AdminFooter></AdminFooter>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({

});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {

        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(AdminDashboardContainer);
