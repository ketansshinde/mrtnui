import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Country } from "../../../components/Admin/Country/country";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchCountry, AddCoutry, EditCountry } from "../../../action/AdminAction/CountryAction";
import { GetCountry } from "../../../reducer/AdminReducer/CountryReducer";

class CountryContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        const { fetchCountry } = this.props;
        fetchCountry();
    }
    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <div id="layoutSidenav">
                    <div id="layoutSidenav_content">
                        <Country {...this.props}></Country>
                        <AdminFooter></AdminFooter>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    CountryData: GetCountry(state.CountryReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchCountry: fetchCountry,
            AddCoutry: AddCoutry,
            EditCountry: EditCountry
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(CountryContainer);
