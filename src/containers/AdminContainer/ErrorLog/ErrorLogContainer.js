import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ErrorLog } from "../../../components/Admin/ErrorLog/errorlog";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchErrorLog } from "../../../action/AdminAction/ErrorLogAction";
import { GetErrorLog } from "../../../reducer/AdminReducer/ErrorLogReducer";

class ErrorLogContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

        const { fetchErrorLog } = this.props;
        fetchErrorLog();
    }

    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <ErrorLog {...this.props}></ErrorLog>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    ErrorLogData: GetErrorLog(state.ErrorLogReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchErrorLog: fetchErrorLog,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(ErrorLogContainer);
