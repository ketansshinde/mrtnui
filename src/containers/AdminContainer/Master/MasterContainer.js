import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Master } from "../../../components/Admin/Master/master";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchMaster, AddMaster, EditMaster } from "../../../action/AdminAction/MasterAction";
import { GetMaster } from "../../../reducer/AdminReducer/MasterReducer";

class MasterContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

        const { fetchMaster } = this.props;
        fetchMaster();
    }

    render() {
        return (
            <React.Fragment>
                <AdminHeader></AdminHeader>
                <Master {...this.props}></Master>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    masterData: GetMaster(state.MasterReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchMaster: fetchMaster,
            AddMaster: AddMaster,
            EditMaster: EditMaster
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(MasterContainer);
