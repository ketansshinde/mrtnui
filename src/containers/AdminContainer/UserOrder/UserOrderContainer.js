import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import  UserOrder  from "../../../components/Admin/UserOrder/UserOrder";
import { AdminFooter, AdminHeader} from "../../../components/Admin/CommonComponent";
import { GetTrasactionDetail } from "../../../action/UserAction/SubcriptionAction";

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  GetTrasactionDetail: GetTrasactionDetail
}, dispatch);


class UserOrderContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

    }

    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <UserOrder {...this.props}></UserOrder>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(UserOrderContainer);
