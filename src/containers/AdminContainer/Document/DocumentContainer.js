import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Document } from "../../../components/Admin/Document/document";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchDocument } from "../../../action/AdminAction/DocumentAction";
import { getDocument } from "../../../reducer/AdminReducer/DocumentReducer";
import {
    CryptoCode,
} from "../../../common/cryptoCode";

class DocumentContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

        const { fetchDocument } = this.props;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        var passValues = {
            userid: UserData.id
        }
        fetchDocument(passValues);
    }

    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <Document {...this.props}></Document>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    DocumentData: getDocument(state.DocumentReducer),
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchDocument: fetchDocument,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(DocumentContainer);
