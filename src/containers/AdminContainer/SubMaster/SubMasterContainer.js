import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { SubMaster } from "../../../components/Admin/SubMaster/submaster";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchSubMaster, AddSubMaster, EditSubMaster } from "../../../action/AdminAction/SubMasterAction";
import { GetSubMaster } from "../../../reducer/AdminReducer/SubMasterReducer";

import { fetchMaster } from "../../../action/AdminAction/MasterAction";
import { GetMaster } from "../../../reducer/AdminReducer/MasterReducer";

class SubMasterContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {


        const { fetchSubMaster } = this.props;
        fetchSubMaster();

        const { fetchMaster } = this.props;
        fetchMaster();
    }

    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <SubMaster {...this.props}></SubMaster>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    subMasterData: GetSubMaster(state.SubMasterReducer),
    masterData: GetMaster(state.MasterReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchMaster: fetchMaster,
            fetchSubMaster: fetchSubMaster,
            AddSubMaster: AddSubMaster,
            EditSubMaster: EditSubMaster
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(SubMasterContainer);
