import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { State } from "../../../components/Admin/State/state";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";

import { fetchCountry } from "../../../action/AdminAction/CountryAction";
import { GetCountry } from "../../../reducer/AdminReducer/CountryReducer";

import { fetchState, AddState, EditState } from "../../../action/AdminAction/StateAction";
import { GetState } from "../../../reducer/AdminReducer/StateReducer";

class StateContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        const { fetchCountry } = this.props;
        fetchCountry();

        const { fetchState } = this.props;
        fetchState();
    }
    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <State {...this.props}></State>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    CountryData: GetCountry(state.CountryReducer),
    stateData: GetState(state.StateReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchCountry: fetchCountry,
            fetchState: fetchState,
            AddState: AddState,
            EditState: EditState
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(StateContainer);
