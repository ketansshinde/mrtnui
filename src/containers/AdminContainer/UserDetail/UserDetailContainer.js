import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { UserDetail } from "../../../components/Admin/UserDetail/userdetail";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";
import { fetchUserDetail } from "../../../action/AdminAction/UserDetailAction";
import { GetUserDetail } from "../../../reducer/AdminReducer/UserDetailReducer";
import { CryptoCode } from "../../../common/cryptoCode";

class UserDetailContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        const { fetchUserDetail } = this.props;
        const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
        fetchUserDetail(UserData.id);
    }

    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <UserDetail {...this.props}></UserDetail>
                <AdminFooter></AdminFooter>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    UserDetailData: GetUserDetail(state.UserDetailReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchUserDetail: fetchUserDetail
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailContainer);
