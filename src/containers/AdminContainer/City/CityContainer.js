import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { City } from "../../../components/Admin/City/city";
import { AdminFooter, AdminHeader, AdminLeftSideMenu } from "../../../components/Admin/CommonComponent";

import { fetchCity, AddCity, EditCity } from "../../../action/AdminAction/CityAction";
import { GetCity } from "../../../reducer/AdminReducer/CityReducer";

import { fetchState } from "../../../action/AdminAction/StateAction";
import { GetState } from "../../../reducer/AdminReducer/StateReducer";


class CityContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    componentDidMount() {
        const { fetchCity } = this.props;
        fetchCity();

        const { fetchState } = this.props;
        fetchState();
    }
    render() {
        return (

            <React.Fragment>
                <AdminHeader></AdminHeader>
                <div id="layoutSidenav">
                    <div id="layoutSidenav_content">
                        <City {...this.props}></City>
                        <AdminFooter></AdminFooter>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => ({
    CityData: GetCity(state.CityReducer),
    stateData: GetState(state.StateReducer)
});
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchState: fetchState,
            fetchCity: fetchCity,
            AddCity: AddCity,
            EditCity: EditCity
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(CityContainer);
