import {
    error,
    success,
    successNotification,
    errorNotification,
    warning,
    warningNotification
} from "../components/notification/notifications";
import { loginService } from "../services/loginService";
import { Global_var } from "../global/global_var";
import { CryptoCode } from "./../common/cryptoCode";
import Axios from "axios";

export const checkUserSession = {
    UserSession,
    refreshToken
};

function UserSession(responseDetail) {
    if (responseDetail.data.status === "fail" && responseDetail.data.reasonCode === "SM-10") {

        var username = JSON.parse(localStorage.getItem("userData") || [])["userName"];
        loginService.logout({ username }, async (res) => {
            localStorage.clear();
            error(
                "Your session has been expired. Please login again.",
                errorNotification
            );
            window.open("/", "_self")
            //this.props.history.push("/");
        });
    }
    else {
        return;
    }
}

function refreshToken(fn) {
    const UserData = JSON.parse(CryptoCode.decryption(localStorage.getItem("userData")));
    var UserAccess = {
        id: UserData.id,
        emailId: UserData.username,
        token: UserData.token
    };
    var url = Global_var.BASEURL + Global_var.URL_REFRESH_JWT_TOKEN;
    Axios.request({
        method: "post",
        url: url,
        data: UserAccess,
    }).then(function (response) {
        localStorage.setItem("token", CryptoCode.encryption(response.data.refreshToken));
        window.location.reload();
    });
}
