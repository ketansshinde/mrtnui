const colors = [
    "#FFB900",
    "#D83B01",
    "#B50E0E",
    "#E81123",
    "#B4009E",
    "#5C2D91",
    "#0078D7",
    "#00B4FF",
    "#008272",
    "#107C10"
];
function calculateColor(email) {
    var sum = 0;
    var index;
    for (index in email) {
        sum += email.charCodeAt(index);
    }
    return sum % colors.length;
}
function EmailIdColorCode(email) {
    var colorId = calculateColor(email);
    return colors[colorId];
}

export const avtarColor = {
    EmailIdColorCode
};

