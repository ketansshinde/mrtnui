import { Global_var } from "../global/global_var";
import { CryptoCode } from "./cryptoCode";

export const SubMasterValue = {
    SubMaster
};

function SubMaster(MasterId) {
    
    var _subMasterValue = JSON.parse(CryptoCode.decryption(localStorage.getItem("submaster")));
    return _subMasterValue.filter(x => x.masterId === MasterId);
}


