import { withStyles, createMuiTheme } from "@material-ui/core/styles";
import { getMuiTheme } from "../assets/MaterialControl";


// --------------------- Email Data Grid  theme feature------------------------------------------------------//

export const themeMail = createMuiTheme({
    overrides:
    {
        MUIDataTableToolbar:
        {
            root: {
                display: 'none',
                backgroundColor: "#e4e9f0"
            }
        },
        MUIDataTableBodyCell: {
            root: {
                paddingTop: "2px !important",
                paddingBottom: "2px !important",
                shadows: "1px solid #dddddd",
                fontFamily: "Rubik !important",
                color:"#000000"
            },
        },
        MUIDataTableHeadCell: {
            root: {
                backgroundColor: "#e4e9f0",
                fontWeight: "bold",
                display: 'none',
                fontFamily: "Rubik !important"
            },
        },
        MuiTableCell: {
            footer: {
                backgroundColor: "#d8e7d0",
                height: "25px",
                padding: "0 !important"
            }
        },
        MUIDataTable: {
            responsiveScroll: {
                // // overflowX: "none",
                // maxHeight: "unset",
                maxHeight: " 100% !important",
            },
        },
        MUIDataTableBodyRow: {
            root: {
                height: "220px",
            },
        },
        MuiPaper: {
            elevation4: {
                boxShadow: "none",
                border: "1px solid #ddddddbd"
            }
        }
    }
});
// --------------------------------------------------------------------------------------------------------//

// --------------------- Email Data Grid Option feature----------------------------------------------------//

export const optionsMail = {
    jsonMode: true,
    filterType: "dropdown",
    searchable: false,
    print: false,
    filter: false,
    sort: false,
    download: false,
    viewColumns: false,
    // responsive,
    search: false,
    rowsPerPage: 15,
    rowsPerPageOptions: [15, 30, 50, 100],
    pagination: {
        next: "Next",
        previous: "Previous",
        rowsPerPage: "Rows per page:",
        displayRows: "of",
    },
    selectableRows: "none", // comment to show checkboxes
    // responsive: "vertical"
    responsive: "scroll",
};

// --------------------------------------------------------------------------------------------------------//

// --------------------- Other Data Grid feature-----------------------------------------------------------//

export const themeOtherGrid = createMuiTheme({
    overrides:
    {
        MUIDataTableBodyCell: {
            root: {
                paddingTop: "3px !important",
                paddingBottom: "3px !important",
                shadows: "1px solid #dddddd",
                // // fontFamily: "poppinsregular !important"
            },
        },
        MUIDataTableHeadCell: {
            root: {
                backgroundColor: "#e4e9f0",
                fontWeight: "bold",
            },
        },
        MUIDataTableBodyRow: {
            root: {
                padding: "6px !important",
            }
        },
        MUIDataTableToolbar: {
            root: {
                backgroundColor: "#e4e9f0",
                minHeight: "56px"
            },

            filterPaper: {
                maxWidth: "100%",
            },
        },
        MuiTableCell: {
            footer: {
                backgroundColor: "#e4e9f0",
                height: "25px",
                // fontFamily: "poppinsregular !important"
            }
        },
        MUIDataTableHeadCell: {
            fixedHeader: {
                backgroundColor: "#e4e9f0",
                fontWeight: "bold",
                paddingTop: "8px !important",
                paddingBottom: "8px !important",
                // fontFamily: "poppinsregular !important"
            }
        },
        MUIDataTableFilter: {
            root: {
                width: "270px"
            }
        },
        MuiTableCell: {
            root: {
                padding: "5px 5px 5px 10px !important;",
            }
        },
        MuiTableCell: {
            footer: {
                backgroundColor: "#e4e9f0",
                height: "25px",
                padding: "0 !important"
                // fontFamily: "poppinsregular !important"
            }
        },
        MUIDataTable: {
            responsiveScroll: {
                // // overflowX: "none",
                // maxHeight: "unset",
                maxHeight: " 100% !important",
            },
        },

    }
});

// --------------------------------------------------------------------------------------------------------//


// --------------------- Options other Data Grid feature----------------------------------------------------//

export const optionsOtherGrid = {
    jsonMode: true,
    filter: true,
    filterType: "dropdown",
    rowsPerPage: 10,
    rowsPerPageOptions: [10, 15, 30, 50, 100],
    pagination: {
        next: "Next",
        previous: "Previous",
        rowsPerPage: "Rows per page:",
        displayRows: "of",
    },
    selectableRows: "none", // comment to show checkboxes
    // responsive: "vertical"
    responsive: "scroll",
};

// ---------------------------------------------------------------------------------------------------------//


// ---------------------Transaction theme feature------------------------------------------------------//

export const themeTransaction = createMuiTheme({
    overrides:
    {
        MUIDataTableToolbar:
        {
            root: {
                backgroundColor: "#e4e9f0"
            }
        },
        MUIDataTableBodyCell: {
            root: {
                paddingTop: "2px !important",
                paddingBottom: "2px !important",
                shadows: "1px solid #dddddd",
                fontFamily: "Rubik !important",
                color:"#475465"
            },
        },
        MUIDataTableHeadCell: {
            root: {
                backgroundColor: "#e4e9f0",
                fontWeight: "bold",
                fontFamily: "Rubik !important"
            },
        },
        MuiTableCell: {
            footer: {
                backgroundColor: "#d8e7d0",
                height: "25px",
                padding: "0 !important"
            }
        },
        MUIDataTable: {
            responsiveScroll: {
                // // overflowX: "none",
                // maxHeight: "unset",
                maxHeight: " 100% !important",
            },
        },
        MUIDataTableBodyRow: {
            root: {
                height: "60px",
            },
        },
        MuiPaper: {
            elevation4: {
                boxShadow: "none",
                border: "1px solid #ddddddbd"
            }
        }
    }
});

// ---------------------------------------------------------------------------------------------------------//


