import React from 'react';
import { encryptionAndDecryption } from "../global/global_var";
//Including all libraries, for access to extra methods.
var CryptoJS = require("crypto-js");

export const CryptoCode = {
    encryption,
    decryption,
    getRandomString
};


function encryption(data) {
    // Encrypt
    if (data !== null) {
        var ciphertext = CryptoJS.AES.encrypt(data, encryptionAndDecryption.key).toString();
        return ciphertext;
    }
    return "";
}


function decryption(ciphertext) {
    // Decrypt
    if (ciphertext !== null) {
        var bytes = CryptoJS.AES.decrypt(ciphertext, encryptionAndDecryption.key);
        var decryptedData = bytes.toString(CryptoJS.enc.Utf8);
        return decryptedData;
    }
    return "";
}


function getRandomString() {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    for (var i = 0; i < 50; i++) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}