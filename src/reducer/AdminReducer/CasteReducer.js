import {
    FETCH_CASTE_SUCCESS,
    FETCH_CASTE_PENDING,
} from "./../../action/AdminAction/CasteAction";

const initialState = {
    CasteData: [],
    pending: false,
};
const CasteReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CASTE_SUCCESS:
            
            return {
                ...state,
                CasteData: action.payload,
                pending: false,
            };
        case FETCH_CASTE_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default CasteReducer;
export const GetCaste = (state) => state.CasteData;


