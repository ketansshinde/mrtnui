import {
    FETCH_MASTER_SUCCESS,
    FETCH_MASTER_PENDING,
} from "./../../action/AdminAction/MasterAction";

const initialState = {
    masterData: [],
    pending: false,
};
const MasterReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MASTER_SUCCESS:
            
            return {
                ...state,
                masterData: action.payload,
                pending: false,
            };
        case FETCH_MASTER_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default MasterReducer;
export const GetMaster = (state) => state.masterData;


