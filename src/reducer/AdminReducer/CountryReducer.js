import {
    FETCH_COUNTRY_SUCCESS,
    FETCH_COUNTRY_PENDING,
} from "./../../action/AdminAction/CountryAction";

const initialState = {
    CountryData: [],
    pending: false,
};
const CountryReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COUNTRY_SUCCESS:
            
            return {
                ...state,
                CountryData: action.payload,
                pending: false,
            };
        case FETCH_COUNTRY_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default CountryReducer;
export const GetCountry = (state) => state.CountryData;


