import {
 FETCH_DOCUMENT_SUCCESS,
 FETCH_DOCUMENT_PENDING
} from "./../../action/AdminAction/DocumentAction";

const initialState = {
    DocumentData: [],
    pending: false,
};
const DocumentReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DOCUMENT_SUCCESS:
            
            return {
                ...state,
                DocumentData: action.payload,
                pending: false,
            };
        case FETCH_DOCUMENT_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default DocumentReducer;
export const getDocument = (state) => state.DocumentData;


