import {
    FETCH_SUBCASTE_SUCCESS,
    FETCH_SUBCASTE_PENDING,
} from "./../../action/AdminAction/SubCasteAction";

const initialState = {
    SubCasteData: [],
    pending: false,
};
const SubCasteReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SUBCASTE_SUCCESS:

            return {
                ...state,
                SubCasteData: action.payload,
                pending: false,
            };
        case FETCH_SUBCASTE_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default SubCasteReducer;
export const GetSubCaste = (state) => state.SubCasteData;


