import {
    FETCH_STATE_SUCCESS,
    FETCH_STATE_PENDING,
} from "./../../action/AdminAction/StateAction";

const initialState = {
    stateData: [],
    pending: false,
};
const StateReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_STATE_SUCCESS:
            
            return {
                ...state,
                stateData: action.payload,
                pending: false,
            };
        case FETCH_STATE_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default StateReducer;
export const GetState = (state) => state.stateData;


