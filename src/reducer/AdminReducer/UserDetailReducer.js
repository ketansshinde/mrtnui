import {
    FETCH_USERDETAIL_PENDING,
    FETCH_USERDETAIL_SUCCESS,
} from "./../../action/AdminAction/UserDetailAction";

const initialState = {
    UserDetailData: [],
    pending: false,
};
const UserDetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERDETAIL_SUCCESS:
            
            return {
                ...state,
                UserDetailData: action.payload,
                pending: false,
            };
        case FETCH_USERDETAIL_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default UserDetailReducer;
export const GetUserDetail = (state) => state.UserDetailData;


