import {
    FETCH_SUBMASTER_SUCCESS,
    FETCH_SUBMASTER_PENDING,
} from "./../../action/AdminAction/SubMasterAction";

const initialState = {
    subMasterData: [],
    pending: false,
};
const SubMasterReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SUBMASTER_SUCCESS:
            
            return {
                ...state,
                subMasterData: action.payload,
                pending: false,
            };
        case FETCH_SUBMASTER_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default SubMasterReducer;
export const GetSubMaster = (state) => state.subMasterData;


