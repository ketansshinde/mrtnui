import {
    FETCH_ERRORLOG_SUCCESS,
    FETCH_ERRORLOG_PENDING,
} from "./../../action/AdminAction/ErrorLogAction";

const initialState = {
    ErrorLogData: [],
    pending: false,
};
const ErrorLogReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ERRORLOG_SUCCESS:
            
            return {
                ...state,
                ErrorLogData: action.payload,
                pending: false,
            };
        case FETCH_ERRORLOG_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default ErrorLogReducer;
export const GetErrorLog = (state) => state.ErrorLogData;


