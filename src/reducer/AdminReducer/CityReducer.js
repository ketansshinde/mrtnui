import {
    FETCH_CITY_SUCCESS,
    FETCH_CITY_PENDING,
} from "./../../action/AdminAction/CityAction";

const initialState = {
    CityData: [],
    pending: false,
};
const CityReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CITY_SUCCESS:
            
            return {
                ...state,
                CityData: action.payload,
                pending: false,
            };
        case FETCH_CITY_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default CityReducer;
export const GetCity = (state) => state.CityData;


