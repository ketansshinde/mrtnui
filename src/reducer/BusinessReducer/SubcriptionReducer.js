import {
    FETCH_SUBCRIPTION_SUCCESS,
    FETCH_SUBCRIPTION_PENDING,
} from "./../../action/BusinessAction/BusinessSubcriptionAction";

const initialState = {
    SubscriptionData: [],
    pending: false,
};
const BusinessSubcriptionReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SUBCRIPTION_SUCCESS:

            return {
                ...state,
                SubscriptionData: action.payload,
                pending: false,
            };
        case FETCH_SUBCRIPTION_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default BusinessSubcriptionReducer;
export const GetSubcriptionPlan = (state) => state.SubscriptionData;


