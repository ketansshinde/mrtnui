import {
    FETCH_USER_SUCCESS,
    FETCH_USER_PENDING,
} from "./../../action/UserAction/RegistrationAction";

const initialState = {
    UserProfileData: [],
    pending: false,
};
const RegistrationReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_SUCCESS:
            
            return {
                ...state,
                UserProfileData: action.payload,
                pending: false,
            };
        case FETCH_USER_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default RegistrationReducer;
export const GetUserDetailByID = (state) => state.UserProfileData;


