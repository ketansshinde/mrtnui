import {
    FETCH_PROFILE_INTEREST_PENDING,
    FETCH_PROFILE_INTEREST_SUCCESS
} from "./../../action/UserAction/ProfileInterestAction";

const initialState = {
    ProfileInterestData: [],
    pending: false,
};
const ProfileInterestReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROFILE_INTEREST_SUCCESS:

            return {
                ...state,
                ProfileInterestData: action.payload,
                pending: false,
            };
        case FETCH_PROFILE_INTEREST_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default ProfileInterestReducer;
export const GetProfileInterestData = (state) => state.ProfileInterestData;


