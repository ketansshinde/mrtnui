import {
    FETCH_PROFILE_SHORTLISTED_PENDING,
    FETCH_PROFILE_SHORTLISTED_SUCCESS,
    //-------------------------------
    FETCH_PROFILE_SHORTLISTED_BY_YOU_PENDING,
    FETCH_PROFILE_SHORTLISTED_BY_YOU_SUCCESS
} from "./../../action/UserAction/ProfileShortlistedAction";

const initialState = {
    ProfileShortListedData: [],
    ProfileShortListedByYouData: [],
    pending: false,
};
const ProfileShortlistedReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROFILE_SHORTLISTED_SUCCESS:

            return {
                ...state,
                ProfileShortListedData: action.payload,
                pending: false,
            };
        case FETCH_PROFILE_SHORTLISTED_PENDING:
            return {
                ...state,
                pending: false,
            };

        case FETCH_PROFILE_SHORTLISTED_BY_YOU_SUCCESS:

            return {
                ...state,
                ProfileShortListedByYouData: action.payload,
                pending: false,
            };
        case FETCH_PROFILE_SHORTLISTED_BY_YOU_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default ProfileShortlistedReducer;
export const GetProfileShortlistedData = (state) => state.ProfileShortListedData;
export const GetProfileShortlistedByYouData = (state) => state.ProfileShortListedByYouData

