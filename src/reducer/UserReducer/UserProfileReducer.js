import {
    FETCH_USER_PROFILE_SUCCESS,
    FETCH_USER_PROFILE_PENDING,
    FETCH_USER_PROFILE_VIEWER_SUCCESS,
    FETCH_USER_PROFILE_VIEWER_PENDING,
    FETCH_USER_INCOMEING_REQUEST_SUCCESS,
    FETCH_USER_INCOMEING_REQUEST_PENDING
} from "./../../action/UserAction/UserProfileAction";

const initialState = {
    UserProfileData: [],
    UserProfileViewerData: [],
    UserIncomingSendRequestData: [],
    pending: false,
};
const UserProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_PROFILE_SUCCESS:

            return {
                ...state,
                UserProfileData: action.payload,
                pending: false,
            };
        case FETCH_USER_PROFILE_PENDING:
            return {
                ...state,
                pending: false,
            };

        case FETCH_USER_PROFILE_VIEWER_SUCCESS:

            return {
                ...state,
                UserProfileViewerData: action.payload,
                pending: false,
            };
        case FETCH_USER_PROFILE_VIEWER_PENDING:
            return {
                ...state,
                pending: false,
            };

        case FETCH_USER_INCOMEING_REQUEST_SUCCESS:

            return {
                ...state,
                UserIncomingSendRequestData: action.payload,
                pending: false,
            };
        case FETCH_USER_INCOMEING_REQUEST_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default UserProfileReducer;
export const GetUserProfile = (state) => state.UserProfileData;
export const GetUserProfileViewer = (state) => state.UserProfileViewerData;
export const GetUserIncomingSendRequest = (state) => state.UserIncomingSendRequestData;


