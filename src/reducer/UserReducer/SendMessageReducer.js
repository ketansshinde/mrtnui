import {
    FETCH_SEND_MESSAGE_SUCCESS,
    FETCH_SEND_MESSAGE_PENDING,
    FETCH_GET_MESSAGE_PENDING,
    FETCH_GET_MESSAGE_SUCCESS
} from "./../../action/UserAction/SendMessageAction";

const initialState = {
    SendMessageData: [],
    MessageData: [],
    pending: false,
};
const SendMessageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SEND_MESSAGE_SUCCESS:

            return {
                ...state,
                SendMessageData: action.payload,
                pending: false,
            };
        case FETCH_SEND_MESSAGE_PENDING:
            return {
                ...state,
                pending: false,
            };

        case FETCH_GET_MESSAGE_SUCCESS:

            return {
                ...state,
                MessageData: action.payload,
                pending: false,
            };
        case FETCH_GET_MESSAGE_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default SendMessageReducer;
export const GetSendMessage = (state) => state.SendMessageData;
export const GetAllUserMessage = (state) => state.MessageData;


