import {
    FETCH_USERPROFILE_PHOTO_SUCCESS,
    FETCH_USERPROFILE_PHOTO_PENDING,
    FETCH_GOV_ID_SUCCESS,
    FETCH_GOV_ID_PENDING,
    FETCH_GALLERY_SUCCESS,
    FETCH_GALLERY_PEDDING
} from "./../../action/UserAction/UserSettingAction";

const initialState = {
    UserProfilePhotoData: [],
    pending: false,
    GovData: [],
    GalleryPhotoData:[]

};
const UserSettingReducer = (state = initialState, action) => {
    switch (action.type) {

        //--------------------------------------------------------------

        case FETCH_USERPROFILE_PHOTO_SUCCESS:

            return {
                ...state,
                UserProfilePhotoData: action.payload,
                pending: false,
            };
        case FETCH_USERPROFILE_PHOTO_PENDING:
            return {
                ...state,
                pending: false,
            };

        //--------------------------------------------------------------

        case FETCH_GOV_ID_SUCCESS:

            return {
                ...state,
                GovData: action.payload,
                pending: false,
            };
        case FETCH_GOV_ID_PENDING:
            return {
                ...state,
                pending: false,
            };

        //--------------------------------------------------------------

        case FETCH_GALLERY_SUCCESS:

            return {
                ...state,
                GalleryPhotoData: action.payload,
                pending: false,
            };
        case FETCH_GALLERY_PEDDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default UserSettingReducer;
export const GetUserProfilePhoto = (state) => state.UserProfilePhotoData;
export const GetGovID = (state) => state.GovData;
export const GetGalleryPhotoData = (state) => state.GalleryPhotoData

