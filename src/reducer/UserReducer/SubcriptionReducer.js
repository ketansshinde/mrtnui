import {
    FETCH_SUBCRIPTION_SUCCESS,
    FETCH_SUBCRIPTION_PENDING,
} from "./../../action/UserAction/SubcriptionAction";

const initialState = {
    SubscriptionData: [],
    pending: false,
};
const SubcriptionReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SUBCRIPTION_SUCCESS:

            return {
                ...state,
                SubscriptionData: action.payload,
                pending: false,
            };
        case FETCH_SUBCRIPTION_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default SubcriptionReducer;
export const GetSubcriptionPlan = (state) => state.SubscriptionData;


