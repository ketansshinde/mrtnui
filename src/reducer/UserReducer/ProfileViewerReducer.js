import {
FETCH_PROFILE_VIEWER_PENDING,
FETCH_PROFILE_VIEWER_SUCCESS
} from "./../../action/UserAction/ProfileViewerAction";

const initialState = {
    ProfileViewerData: [],
    pending: false,
};
const ProfileViewerReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROFILE_VIEWER_SUCCESS:
            
            return {
                ...state,
                ProfileViewerData: action.payload,
                pending: false,
            };
        case FETCH_PROFILE_VIEWER_PENDING:
            return {
                ...state,
                pending: false,
            };

        default:
            return state;
    }
};


export default ProfileViewerReducer;
export const GetProfileViewerData = (state) => state.ProfileViewerData;


