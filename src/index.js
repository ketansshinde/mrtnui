import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./containers/App";
import * as serviceWorker from "./serviceWorker";

import { Provider } from "react-redux";
import { createBrowserHistory } from "history";
import { BrowserRouter } from "react-router-dom";
import ReactNotification from "react-notifications-component";
import "react-notifications-component/dist/theme.css";

import { store, persistor } from "../src/store";

// import {store, persistor} from '/store'
import { PersistGate } from "redux-persist/integration/react";
import i18n from "./i18n";
import { I18nextProvider } from "react-i18next";

export const history = createBrowserHistory();

ReactDOM.render(
  // <React.StrictMode store={createStore}>
  //   <App />
  // </React.StrictMode>,
  <I18nextProvider i18n={i18n}>
    <BrowserRouter>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <div className="app-container">
            <ReactNotification />
          </div>
          <App />
        </PersistGate>
      </Provider>
    </BrowserRouter>
  </I18nextProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
