import { createStore, applyMiddleware, combineReducers } from "redux";
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
//import loginReducer from "../reducer/loginReducer";
import notificationReducer from "../reducer/notificationReducer";

// Admin Reducers
import CountryReducer from "../reducer/AdminReducer/CountryReducer";
import MasterReducer from "../reducer/AdminReducer/MasterReducer";
import SubMasterReducer from "../reducer/AdminReducer/SubMasterReducer";
import StateReducer from "../reducer/AdminReducer/StateReducer";
import ErrorLogReducer from "../reducer/AdminReducer/ErrorLogReducer";
import CityReducer from "../reducer/AdminReducer/CityReducer";
import UserDetailReducer from "../reducer/AdminReducer/UserDetailReducer";
import CasteReducer from "../reducer/AdminReducer/CasteReducer";
import SubCasteReducer from "../reducer/AdminReducer/SubCasteReducer";
import DocumentReducer from "../reducer/AdminReducer/DocumentReducer";

//User Reducers
import RegistrationReducer from "../reducer/UserReducer/RegistrationReducer";
import UserProfileReducer from "../reducer/UserReducer/UserProfileReducer";
import SubcriptionReducer from "../reducer/UserReducer/SubcriptionReducer";
import SendMessageReducer from "../reducer/UserReducer/SendMessageReducer";
import UserSettingReducer from "../reducer/UserReducer/UserSettingReducer";
import ProfileViewerReducer from "../reducer/UserReducer/ProfileViewerReducer";
import ProfileShortlistedReducer from "../reducer/UserReducer/ProfileShortlistedReducer";
import ProfileInterestReducer from "../reducer/UserReducer/ProfileInterestReducer";


//Business Reducer
import BusinessSubcriptionReducer from "../reducer/BusinessReducer/SubcriptionReducer";


const loggerMiddleware = createLogger();

const persistConfig = { // configuration object for redux-persist
  key: 'root',
  storage, // define which storage to use
}

const persistedReducer = persistReducer(persistConfig, combineReducers({
  //loginReducer,
  notificationReducer,
  CountryReducer,
  MasterReducer,
  SubMasterReducer,
  StateReducer,
  ErrorLogReducer,
  CityReducer,
  UserDetailReducer,
  CasteReducer,
  SubCasteReducer,
  DocumentReducer,

  //User
  RegistrationReducer,
  UserProfileReducer,
  SubcriptionReducer,
  SendMessageReducer,
  UserSettingReducer,
  ProfileViewerReducer,
  ProfileShortlistedReducer,
  ProfileInterestReducer,



  //Business Reducer
  BusinessSubcriptionReducer,

  
})) // create a persisted reducer


const store = createStore(
  persistedReducer,
  applyMiddleware(thunkMiddleware, loggerMiddleware)
);

const persistor = persistStore(store); // used to create the persisted store, persistor will be used in the next step

export { store, persistor }
